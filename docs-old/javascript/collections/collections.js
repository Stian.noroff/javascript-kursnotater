const obj = {
  foo: 'bar',
  baz: 'qux',
}

console.log(obj.foo)
// => 'bar'

const propName = 'foo'
console.log(obj[propName])
// => 'bar'

const fooFn = () => 'foo'

const obj = {
  [fooFn()]: 'bar',
}

console.log(obj[fooFn()])
// => 'bar'

const foo = 'bar'

const obj = {
  foo
}

console.log(obj.foo)
// => 'bar'

const first = {
  foo: 'a',
  bar: 'b',
  baz: 'c',
  qux: 'd',
}

const second = {
  bar: 'x',
  baz: 'y',
  quuux: 'zzz',
}

const merged = {
  ...first,
  foo: 'w',
  ...second,
}

console.log(merged)
// => { foo: 'w', bar: 'x', baz: 'y', qux: 'd', quuux: 'zzz' }

const obj = {
  foo: {
    bar: 'baz'
  }
}
const clone = {
  ...obj
}

console.log(obj === clone)
// false

console.log(obj.foo)
// { bar: 'baz' }

console.log(obj.foo === clone.foo)
// true

obj.foo.bar = 'abc'

console.log(clone.foo)
// { bar: 'abc' }

const obj = {
  foo: 'abc',
  bar: 'def'
}
const {
  foo,
  bar
} = obj

console.log(foo, bar)
// => 'abc def'

const obj = {
  foo: 'abc',
  bar: 'def'
}
const foo = obj.foo
const bar = obj.bar

console.log(foo, bar)
// => 'abc def'

const obj = {
  foo: 'abc'
}
const {
  foo: bar
} = obj

console.log(foo)
// => undefined

console.log(bar)
// => 'abc'

const obj = {}
const {
  foo = 'abc'
} = obj

console.log(obj.foo)
// => undefined

console.log(foo)
// => 'abc'

const obj = {
  foo: 'abc',
  bar: 'def',
  baz: 'ghi'
}
const {
  foo,
  ...rest
} = obj

console.log(foo)
// => 'abc'

console.log(rest)
// => { bar: 'def', baz: 'ghi' }

function foo({
  bar
}) {
  console.log(bar)
}

foo({
  bar: 'abc'
})
// => 'abc'

const arr = [{
  data: {
    foo: 'abc',
    bar: 'def'
  }
}]
console.log(arr.map(({
  data: {
    foo,
    bar
  }
}) => `${foo} ${bar}`))
// => ['abc def']

// defineProperty

const obj = {}

const descriptor = {
  configurable: false,
  enumerable: false,
  writable: false,
  value: 'bar',
}

Object.defineProperty(obj, 'foo', descriptor)

// Alternatively
Object.defineProperties(obj, {
  foo: descriptor,
})

console.log(obj)
// => {}
// The property `foo` does not show because it is declared not enumerable

console.log(obj.foo)
// => 'bar'
// Despite being not enumerable, the value is still accessible.

obj.foo = 'baz'
// This will throw an error because `foo` is neither configurable nor writable.

// assign

const foo = {
  foo: 'abc'
}
const bar = {
  bar: 'def'
}
const bar2 = {
  bar: 'xyz'
}

console.log(Object.assign({}, foo, bar, bar2))
// => { foo: 'abc', bar: 'xyz' }

Object.assign(foo, bar, bar2)
console.log(foo)
// => { foo: 'abc', bar: 'xyz' }

// keys

const foo = {
  foo: 'bar',
  baz: 'qux'
}
console.log(Object.keys(foo))
// => ['foo', 'baz']

const foo = {
  foo: 'bar',
  baz: 'qux'
}
console.log(Object.values(foo))
// => ['bar', 'qux']

const foo = {
  foo: 'bar',
  baz: 'qux'
}
console.log(Object.entries(foo))
// => [['foo', 'bar'], ['baz', 'qux']]

const arr = ['foo', 'bar']
console.log(arr.length)
// => 2

const arr = ['foo', 'bar']
arr.baz = 'qux'

console.log(arr.length)
// => 2

console.log(arr)
// => ['foo', 'bar', baz: 'qux']

const foo = ['foo', 'bar']
const baz = ['baz', 'qux']

console.log([...baz, ...foo])
// => ['baz', 'qux', 'foo', 'bar']

const arr = ['foo', 'bar', 'baz', 'qux']
const [foo, , , qux] = arr

console.log(`${foo} ${qux}`)
// => 'foo qux'

[a, b] = [b, a]

function makeRangeIterator(start = 0, end = Infinity, step = 1) {
  let nextIndex = start
  let iterationCount = 0

  return {
    next: () => {
      let result
      if (nextIndex < end) {
        result = {
          value: nextIndex,
          done: false
        }
        nextIndex += step
        iterationCount++
        return result
      }
      return {
        value: iterationCount,
        done: true
      }
    },
  }
}

function* makeRangeIterator(start = 0, end = Infinity, step = 1) {
  let iterationCount = 0
  for (let i = start; i < end; i++) {
    iterationCount++
    yield i
  }
  return iterationCount
}

{
  done: true,
  value: iterationCount
}

function* fibonacci() {
  let [first, second] = [0, 1]

  while (true) {
    if (yield first) {
      [first, second] = [0, 1]
      continue
    }

    [first, second] = [second, first + second]
  }
}

function makeRangeIterator(start = 0, end = Infinity, step = 1) {
  // ...
  return {
    [Symbol.iterator]: function () {
      return this
    },
    next: () => {
      // ...
    },
  }
}

class MyIterable {
  next() {
    // ...
  }

  [Symbol.iterator]() {
    return this
  }
}

const it = makeRangeIterator(1, 10, 2)

let {
  value,
  done
} = it.next()

while (!done) {
  console.log(value) {
    value,
    done
  } = it.next()
}
// => 1, 3, 5, 7, 9
// (newlines omitted and commas added for easier reading)

for (let count of makeRangeIterator(1, 10, 2)) {
  console.log(count)
}
// => 1, 3, 5, 7, 9
// (newlines omitted and commas added for easier reading)

const arr = Array.from(makeRangeIterator(1, 10, 2))
// OR
const arr = [...makeRangeIterator(1, 10, 2)]

console.log(arr)
// => [1, 3, 5, 7, 9]