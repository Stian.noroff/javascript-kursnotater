# Objects

Objects form the basis of just about everything in JavaScript, with the only exception being primitive values such as Numbers and Booleans. Despite being identified as different [types](../variables/README.md#variable-type) by the [`typeof` operator](../operators/README.md#typeof-operator), and being built into the language itself with native implementations, Strings, [Arrays](#arrays), Symbols and Functions are all examples of JavaScript objects.

The most common use of an object in JavaScript is as a collection of keyed-values; also referred to as a POJO _(Plain-Old-JavaScript-Object)_:

```javascript
const obj = {
  foo: 'bar',
  baz: 'qux',
}
```

For each key-value pair, the value can be anything, including other objects (nested indefinitely), but the key _must_ be unique and must be either a String or a Symbol; other types will be coerced into a string before being used.

## Accessing Properties

Reading properties of an object is done in the following way:

```javascript
console.log(obj.foo)
// => 'bar'
```

## Computed Property Names

In many cases, objects will not be standardised and will have varying property names; however, as long as the property name can be computed then the property can be accessed. This might be accomplished in the following way:

```javascript
const propName = 'foo'
console.log(obj[propName])
// => 'bar'
```

Where `propName` is used to provide the computed property name, any valid JavaScript expression may be used in its place.

Computed property names can also be used with the object literal syntax:

```javascript
const fooFn = () => 'foo'

const obj = {
  [fooFn()]: 'bar',
}

console.log(obj[fooFn()])
// => 'bar'
```

Objects will also infer their name if being assigned to from a variable with the same name:

```javascript
const foo = 'bar'

const obj = { foo }

console.log(obj.foo)
// => 'bar'
```

## Object Spread Operator

Objects can also be defined in terms of some existing objects using the spread operator:

```javascript
const first = {
  foo: 'a',
  bar: 'b',
  baz: 'c',
  qux: 'd',
}

const second = {
  bar: 'x',
  baz: 'y',
  quuux: 'zzz',
}

const merged = {
  ...first,
  foo: 'w',
  ...second,
}

console.log(merged)
// => { foo: 'w', bar: 'x', baz: 'y', qux: 'd', quuux: 'zzz' }
```

The spread operator iterates over the properties of each object, in order, and does assignment at the same level where the objects are referenced; thus the objects are "spread" into another object, and not simply nested. As the properties are iterated, property names that appear more than once will be overritten each time they appear.

> **Caveat**
>
> Note that the use of `Object.assign()` triggers [setters](../classes/README.md#setters) (where defined) but using the spread operator does not.

## Shallow Clone

The spread operator can also be used to create a "shallow clone" of another object. A shallow clone of an object is where an object is duplicated, such that an expression such as: `obj === clone` evaluates to `false` but a similar comparison of the individual properties would evaluate to `true`. This concept is used extensively in libraries that draw from the functional paradigm such as React and Redux. Consider the following example:

```javascript
const obj = { foo: { bar: 'baz' } }
const clone = { ...obj }

console.log(obj === clone)
// false

console.log(obj.foo)
// { bar: 'baz' }

console.log(obj.foo === clone.foo)
// true

obj.foo.bar = 'abc'

console.log(clone.foo)
// { bar: 'abc' }
```

While the clone is created as its own distict object, which is a completely separate reference to the original, the properties spread into the clone are references to the same object as the original. Thus, if the "deep properties" are altered from either one of the references, the changes are reflected from in other as well.

## Destructuring Assignment

Destructuring assignment is a type of JavaScript expression that allows one to traverse and extract specific, potentially deeply nested, properties into distinct variables. Consider the following example of destructuring assignment:

```javascript
const obj = { foo: 'abc', bar: 'def' }
const { foo, bar } = obj

console.log(foo, bar)
// => 'abc def'
```

The effect of this code is two new variables, `foo` and `bar` are declared and assigned the values corresponding to those variables names from within the object supplied, in this case: `obj`. This kind of destructuring assignment is equivalent to the following:

```javascript
const obj = { foo: 'abc', bar: 'def' }
const foo = obj.foo
const bar = obj.bar

console.log(foo, bar)
// => 'abc def'
```

> **Note**
>
> When using destructuring assignment, the variables that are declared and assigned in the process will either be `let` or `const` depending on which is prefixed to the destructuring, i.e. `const { foo } = obj` will declare `foo` as a `const` variable.

## Variable Aliasing

Variables that are assigned in this way can also be renamed, or aliased, as they are destructured. This is done with the `:` character:

```javascript
const obj = { foo: 'abc' }
const { foo: bar } = obj

console.log(foo)
// => undefined

console.log(bar)
// => 'abc'
```

## Default Assignment

Variables that are assigned in this way can also be assigned a default value if the property they are destructuring does not exist on the target object. This is done with the `=` character:

```javascript
const obj = {}
const { foo = 'abc' } = obj

console.log(obj.foo)
// => undefined

console.log(foo)
// => 'abc'
```

> **Caveat**
>
> Default assignment will only work if the property in question does not exist on the target object &mdash; if the property exists, even if its value is explicitly set to `undefined`, then the default assignment will not take place.

## Rest Assignment

Using the spread operator (`...`) within a destructuring assignment allows the remaining, unspecified properties to all be assigned to a new object &mdash; a shallow copy of the original &mdash; that excludes the other properties that were explicitly destructured. Consider the following example:

```javascript
const obj = { foo: 'abc', bar: 'def', baz: 'ghi' }
const { foo, ...rest } = obj

console.log(foo)
// => 'abc'

console.log(rest)
// => { bar: 'def', baz: 'ghi' }
```

## Objects as Function Parameters

Any function parameter that is an object can be destructured inline in the function definition of any function expression or function declaration, for both normal and arrow functions, as long as the destructuring is enclosed in parentheses (`()`). Consider the following example:

```javascript
function foo({ bar }) {
  console.log(bar)
}

foo({ bar: 'abc' })
// => 'abc'
```

> **Hint**
>
> This feature is especially useful when using higher order functions as it enables even more compact single statement function expressions. For example:
>
> ```javascript
> const arr = [{ data: { foo: 'abc', bar: 'def' } }]
> console.log(arr.map(({ data: { foo, bar } }) => `${foo} ${bar}`))
> // => ['abc def']
> ```

# Object API

As with other JavaScript types, such as Strings and Numbers, a global utility class for Objects is also provided to expose APIs that are useful in many circumstances. A selection of the most useful of these APIs are now discussed.

> **Further Reading**
>
> A full list of Object APIs can be found [here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object).

## Object Properties

While not technically an API, the individual properties of any object have some metadata associated with them. These properties are arranged into a descriptor object for that property when used in conjunction with the [`defineProperty` API](#defineproperty-function). The various available properties are described below:

- `configurable` &mdash; `true` if and only if the type of this property descriptor may be changed and if the property may be deleted from the corresponding object. Defaults to `false`.
- `enumerable` &mdash; `true` if and only if this property shows up during enumeration of the properties on the corresponding object. Defaults to `false`.
- `value` &mdash; The value associated with the property. Can be any valid JavaScript value (number, object, function, etc). Defaults to `undefined`.
- `writable` &mdash; `true` if and only if the value associated with the property may be changed with an assignment operator. Defaults to `false`.
- `get` &mdash; A function which serves as a [getter](../classes/README.md#getter) for the property, or `undefined` if there is no getter. When the property is accessed, this function is called without arguments and with `this` set to the object through which the property is accessed (this may not be the object on which the property is defined due to inheritance). The return value will be used as the value of the property. Defaults to `undefined`.
- `set` &mdash; A function which serves as a [setter](../classes/README.md#setter) for the property, or `undefined` if there is no setter. When the property is assigned to, this function is called with one argument (the value being assigned to the property) and with `this` set to the object through which the property is assigned. Defaults to `undefined`.

## `defineProperty` Function

Precisely configured properties can be defined for any object with a descriptor (as defined above) and the `defineProperty` or `defineProperties` functions provided by the Object API. The API is used as follows:

```javascript
const obj = {}

const descriptor = {
  configurable: false,
  enumerable: false,
  writable: false,
  value: 'bar',
}

Object.defineProperty(obj, 'foo', descriptor)

// Alternatively
Object.defineProperties(obj, {
  foo: descriptor,
})

console.log(obj)
// => {}
// The property `foo` does not show because it is declared not enumerable

console.log(obj.foo)
// => 'bar'
// Despite being not enumerable, the value is still accessible.

obj.foo = 'baz'
// This will throw an error because `foo` is neither configurable nor writable.
```

If a descriptor has neither of `value`, `writable`, `get` and `set` keys, it is treated as a data descriptor. If a descriptor has both `value` or `writable` and `get` or `set` keys, an exception is thrown.

> **Further Reading**
>
> [`Object.defineProperty()` Reference (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty)

## `assign` Function

The assign function copies all enumerable properties from one or more source objects to a target object. It should be noted that for each property, a full assignment is done which will invoke [setters](../classes/README.md#setters), if applicable to that specific property and cause mutations to the target object.

Consider the following example:

```javascript
const foo = { foo: 'abc' }
const bar = { bar: 'def' }
const bar2 = { bar: 'xyz' }

console.log(Object.assign({}, foo, bar, bar2))
// => { foo: 'abc', bar: 'xyz' }

Object.assign(foo, bar, bar2)
console.log(foo)
// => { foo: 'abc', bar: 'xyz' }
```

> **Further Reading**
>
> [`Object.assign()` Reference (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)

## `keys` Function

Returns an array containing the keys of the given object's enumerable properties. Consider the following example:

```javascript
const foo = { foo: 'bar', baz: 'qux' }
console.log(Object.keys(foo))
// => ['foo', 'baz']
```

> **Further Reading**
>
> [`Object.keys()` Reference (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys)

## `values` Function

Returns an array containing the values of the given object's enumerable properties. Consider the following example:

```javascript
const foo = { foo: 'bar', baz: 'qux' }
console.log(Object.values(foo))
// => ['bar', 'qux']
```

> **Further Reading**
>
> [`Object.values()` Reference (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/values)

## `entries` Function

Returns an array of `[key, value]` pairs (an array of arrays) of the given object's enumerable properties. Consider the following example:

```javascript
const foo = { foo: 'bar', baz: 'qux' }
console.log(Object.entries(foo))
// => [['foo', 'bar'], ['baz', 'qux']]
```

> **Further Reading**
>
> [`Object.entries()` Reference (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/entries)

## `freeze` Function

"Freezes" an object. This iterates through each property on an object and sets their `configurable` and `writable` flags (as described in [Object Properties](#object-properties)) to `false`. This disallows any deletion or changes made to properties of that object.

> **Caveat**
>
> It should be noted that `freeze` is a shallow operation; objects nested within the object being frozen will _not_ also be frozen &mdash; this must be done explicitly.

&nbsp;

> **Further Reading**
>
> [`Object.freeze()` Reference (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/freeze)

## `seal` Function

"Seals" an object. This iterates through each property on an object and sets their `configurable` flag to `false`. This disallows deletion of properties but they can still be overridden by assignment.

> **Caveat**
>
> It should be noted that `seal` is a shallow operation; objects nested within the object being sealed will _not_ also be sealed &mdash; this must be done explicitly.

&nbsp;

> **Further Reading**
>
> [`Object.seal()` Reference (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/seal)

# Arrays

Ordered collections of data in JavaScript are represented as arrays. Like JavaScript objects, and unlike in other languages, arrays grow indefinitely and the elements inside an array can take on any type of value. Arrays in JavaScript is somewhat a misnomer; as in other languages like Python, arrays are in fact implementations of a Linked List data structure.

```javascript
const arr = ['foo', 'bar']
console.log(arr.length)
// => 2
```

Arrays in JavaScript also extend Object and are able to have properties that are separate from their contents. Adding properties in this fashion does not affect the function of the array and does not update the element counter. The array traversal and mutation operations also do not apply to these named properties. Consider the following example:

```javascript
const arr = ['foo', 'bar']
arr.baz = 'qux'

console.log(arr.length)
// => 2

console.log(arr)
// => ['foo', 'bar', baz: 'qux']
```

## Array Spread Operator

The array spread operator operates in a similar way to that of the [Object Spread Operator](#object-spread-operator); the contents of existing arrays can be easily "spread" into another, and can also be used to create a [shallow copy](#shallow-copy) of the array. Unlike the object spread operator, the array spread operator does not overwrite values that appear more than once as the order of the values is preserved as per normal array rules. Consider the following example:

```javascript
const foo = ['foo', 'bar']
const baz = ['baz', 'qux']

console.log([...baz, ...foo])
// => ['baz', 'qux', 'foo', 'bar']
```

## Destructuring Assignment

Arrays in JavaScript can also be destructured but only from the front of the array. Consider the following example:

```javascript
const arr = ['foo', 'bar', 'baz', 'qux']
const [foo, , , qux] = arr

console.log(`${foo} ${qux}`)
// => 'foo qux'
```

> **Hint**
>
> Array destructuring can also be used to swap the value of two variables without requiring a separate temporary variable; this is done as follows:
>
> ```
> [a, b] = [b, a]
> ```

As with object destructuring assignment, [variable aliasing](#variable-aliasing), [default assignment](#default-assignment), [rest assignment](#rest-assignment) and [destructuring inside a function's parameters](#objects-as-function-parameters) are possible with the exact same syntax.

# Array API

With few exceptions, the vast majority of the APIs that are provided for arrays are instance methods that enable mutation and aggregation of the data contained within.

## `from` Function

The from function is a shorthand for forcing an array-like, or [iterable](#iterable) object into an a new instance of an array.

> **Further Reading**
>
> [`Array.from()` Reference (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/from)

## Deque Methods

Arrays in JavaScript have built-in methods that allow them to act as stacks and deques (a double-sided queue):

- [`push`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/push) &mdash; Appends an element to the end of the array.
- [`pop`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/pop) &mdash; Removes an element from the end of the array and returns it.
- [`unshift`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/unshift) &mdash; Appends an element to the front of the array.
- [`shift`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/shift) &mdash; Removes an element from the front of the array and returns it.

## Mutator Methods

Other built-in utility functions for mutating arrays in JavaScript include:

- [`fill`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/fill) &mdash; Fills the entire array with a provided static value.
- [`reverse`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reverse) &mdash; Reverses the order of the elements of the array in place.
- [`sort`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort) &mdash; Lexicographically sorts the elements of the array in place.
- [`splice`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice) &mdash; Adds and removes elements from an array at a given index.

> **Caveat**
>
> All of these functions cause mutations to the original array and are a potential sources of side-effects.

## Accessor Methods

Arrays also come with multiple build-in utility functions for searching and producing aggregate values without altering the original array; these include:

- [`concat`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/concat) &mdash; Returns a new array that is the concatenation of the provided arrays.
- [`includes`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/includes) &mdash; Determines whether the array contains a value, returning `true` or `false` as appropriate.
- [`indexOf`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf) &mdash; Returns the first index of an element within the array that is equal to the provided value. If no match is found then returns `-1`.
- [`lastIndexOf`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/lastIndexOf) &mdash; The same as `indexOf` but the search is done in reverse from the end of the array.
- [`join`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/join) &mdash; Joins all of the elements of the array into a string.
- [`slice`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/slice) &mdash; Extracts a section of the calling array and returns it as a new array. The resulting array is as a [shallow copy](#shallow-copy) of the defined section of the original.

## Iteration Methods and Higher Order Functions

- [`every`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/every) &mdash; Returns `true` if every element in the array satisfies the testing callback function.
- [`some`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/some) &mdash; Returns `true` when the first element of the array satisfies the testing callback function. The remaining array elements are not tested after the first success is found.
- [`find`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find) &mdash; Returns the element of the array for which the testing callback function is satisfied. If none of the elements satisfy the test then returns `undefined`.
- [`findIndex`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/findIndex) &mdash; Identical to `find` but returns the index of the item found. Is also similar to `indexOf` in that if no matching element is found then returns `-1`.
- [`forEach`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach) &mdash; A void iteration that produces no result but calls the callback function for every element in the array.
- [`filter`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter) &mdash; Creates a new array of elements of the calling array for which the testing callback function is satisfied.
- [`map`](../functions/README.md#map-reduce) &mdash; Creates a new array of the results that are returned from calling the callback function for every element in an array.
- [`reduce`](../functions/README.md#map-reduce) &mdash; Applies a callback function against a common state variable (an _accumulator_) and each value of the array as to reduce the whole array to a single value.
- [`reduceRight`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reduceRight) &mdash; Similar to `reduce` but the array is reduced in reverse from the end of the array.

> **Note**
>
> Accessor and iteration methods generally either produce new values or return references to values within the array and thus don't have any side effects. For this reason, and only where possible, using accessors is better than using mutators.

# JSON

JSON (JavaScript Object Notation) is a syntax for serializing objects, arrays, numbers, strings, booleans and `null`. The serialization is based on the representation of objects and arrays in JavaScript but is specifically serialized with the intention of being stored or transmitted over a serial connection. Some major differences include:

- Strings in JSON, including property names, must be enclosed in double quotation marks (`"`). Single quotation marks are invalid.
- Object properties and array elements are forbidden from having trailing commas.
- Leading zeroes in numbers are prohibited.

> **Note**
>
> While not all JavaScript is valid JSON; the reverse is true. Any valid JSON is also valid JavaScript.

Any JavaScript environment provides a utility that allows conversion to and from JSON. In the simplest case these are used as follows:

```javascript
JSON.parse(json_string) // produces the corresponding JavaScript value
JSON.stringify(obj) // produces a JSON string of the provided value
```

These function calls, particularly `JSON.parse()`, may produce errors if the values provided aren't valid. To be safe, calls these (or at least to `parse`) should be enclosed in a [try-catch](../errors/README.md#try--catch-block).

> **Further Reading**
>
> [JSON (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON)

# Maps and Sets

In addition to the Object and Array data structures, ES6 also introduced native implementations of more complex data structures, in particular: Maps and Sets.

## Map

A map is an iterable JavaScript interface to a native object that stores keyed-values in a similar way to a JavaScript object. JavaScript objects and maps deviate in the following ways:

- The keys in an object can only be a String or a Symbol, but the keys in a map can be anything, including functions, objects or primitives.
- The keys in a map are ordered where those of an object are not. When iterating over a map, the elements are returned in the order they were inserted.
- The number of elements in a map can be easily determined with the `size` property.
- Maps do not have a `prototype` and exist outside of the `Object` inheritence hierarchy.
- Maps are more performant for objects that see frequent addition and removal of properties.

Maps have the following API:

- [`has`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map/has) &mdash; Returns `true` if the provided key is present in the collection.
- [`get`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map/get) &mdash; Returns the value corresponding to the provided key or `undefined` if there is none.
- [`set`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map/set) &mdash; Sets the value for the key in the collection. This operation is not idempotent and will replace the value each time, but the key will still retain its order in the map. This operation also returns the map object so calls can be "chained".
- [`delete`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map/delete) &mdash; Returns `true` if the element exists and has been deleted or `false` if it does not. `Map.has()` will return `false` for this key after it has been deleted.
- [`clear`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map/clear) &mdash; Truncates all of the key-value pairs from the collection.

> **Further Reading**
>
> [Map Reference (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map)

## Set

A set is an iterable JavaScript interface to a native object that stores unique values. The primary property of interest in a set is the uniqueness of its elements; when adding new elements to a set, the value equivalence is checked using strict equivalence.

> **Caveat**
>
> As with maps, the values of a set can be anything including special values such as a `NaN`, `undefined`, `+0` and `-0`, but are still subject to the uniqueness condition. In the case of the former, all values for `NaN` are consider equal even though `NaN !== NaN`, and in the case of the latter, `+0 === -0`.

Sets operate very similarly to maps and have a similar interface:

- [`has`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set/has) &mdash; Returns `true` if the provided key is present in the collection.
- [`add`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set/add) &mdash; Appends the value to the end of the collection. This operation is idempotent and will have no effect when adding the same value twice. This operation also returns the set object so calls can be "chained".
- [`delete`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set/delete) &mdash; Returns `true` if the element exists and has been deleted or `false` if it does not. `Set.has()` will return `false` for this key after it has been deleted.
- [`clear`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set/clear) &mdash; Truncates all of the key-value pairs from the collection.

> **Further Reading**
>
> [Set Reference (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set)

# Iterators

When creating custom implementations for various types of collections in JavaScript, it is often necessary to iterate over these collections sequenetially. For JavaScript's built-in types, this is typically done using an iterator.

ES6+ defines an interface that one can follow to make use of JavaScript's existing iteration idioms, which are already implemented on most of the built-in collection types.

> **Further Reading**
>
> [Iterators and Generators (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Iterators_and_Generators)

In JavaScript, an iterator is an object that defines a sequence of values and has a `next` property, which is a function that returns an object of the following form:

```javascript
{
  done, value
}
```

- `done` is a boolean which indicates whether there is another value in the sequence.
- `value` if present, is the iterator's return value for that element.

While an iterator is often used to iterate over an array, not all iterators necessarily use arrays to build up an interation of a particular object. Arrays must be allocated in thier entirity to be used at all but iterators only allocated resources as they are consumed, and therefore can be used to express sequences of unlimited size.

Consider the following example:

```javascript
function makeRangeIterator(start = 0, end = Infinity, step = 1) {
  let nextIndex = start
  let iterationCount = 0

  return {
    next: () => {
      let result
      if (nextIndex < end) {
        result = { value: nextIndex, done: false }
        nextIndex += step
        iterationCount++
        return result
      }
      return { value: iterationCount, done: true }
    },
  }
}
```

The implementation of an iterator &mdash; an object with a `next` function that returns an object containing `value` and `done` &mdash; is also generally as the "iterator protocol".

## Generator Functions

Iterators themselves are somewhat difficult to write because they require careful programming to manage internal state. Generator functions provide an alternative way to create iterators that does automates management of internal state and provides a convenient syntax.

Generator functions are defined using the `function*` syntax and, instead of executing the code in defined in the function itself, will return a type of iterator called a "generator". When a generator's `next` method is called, the generator will execute until it encounters the `yield` keyword.

The previous example of `makeRangeIterator` can be rewritten, to be functionally identical, using a generator function as follows:

```javascript
function* makeRangeIterator(start = 0, end = Infinity, step = 1) {
  let iterationCount = 0
  for (let i = start; i < end; i++) {
    iterationCount++
    yield i
  }
  return iterationCount
}
```

> **Note**
>
> If `return` is used within a generator function then the value returned is the final `value` that is yielded as part of the iteration. The final output would look like this:
>
> ```javascript
> { done: true, value: iterationCount }
> ```

## `yield` Parameters

Another feature that adds further power to generators is the ability for the `next()` method to accept a value that is returned by the `yield` call within the generator function. Consider the following example:

```javascript
function* fibonacci() {
  let [first, second] = [0, 1]

  while (true) {
    if (yield first) {
      ;[first, second] = [0, 1]
      continue
    }

    ;[first, second] = [second, first + second]
  }
}
```

In this example, a generator function is used to produce an infinite stream of Fibonacci sequence numbers where `next(true)` is used to reset the state of the iterator and restart the sequence.

## Iterable

Many built-in features in JavaScript are built to implement the iterable protocol, which allows JavaScript objects to define their iteration behaviour when [used with syntax that consumes an iterator](#using-iterators). Many JavaScript built-in collections such as Arrays, Maps and Sets (_not_ Objects) already implement this protocol and are therefore _iterable_.

> **Further Reading**
>
> [Iteration Protocols (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Iteration_protocols)

To implement the iterable protocol, an object must implement the `[Symbol.iterator]` method for that object that should return an iterator.

```javascript
function makeRangeIterator(start = 0, end = Infinity, step = 1) {
  let nextIndex = start
  let iterationCount = 0

  return {
    [Symbol.iterator]: function() {
      return this
    },
    next: () => {
      let result
      if (nextIndex < end) {
        result = { value: nextIndex, done: false }
        nextIndex += step
        iterationCount++
        return result
      }
      return { value: iterationCount, done: true }
    },
  }
}
```

In this instance, the iterator itself is also iterable (by returning itself). Without the addition of the `[Symbol.iterator]` method, this custom iterator function will not work in places where an iterable is required.

> **Best Practice**
>
> It isn't possible to know whether a particular object implements the iterator protocol or not, however the addition of `[Symbol.iterator]: function () { return this }` above makes consuming an iterator with various different syntaxes much easier. Thus it is very uncommon to implement the iterator protocol without implementing iterable.

## Using Iterators

The most basic way to use iterators is to create a loop that consumes an iterator by calling `next()` each time a new value is needed. Consider the following example:

```javascript
const it = makeRangeIterator(1, 10, 2)

let { value, done } = it.next()

while (!done) {
  console.log(value)
  { value, done } = it.next()
}
// => 1, 3, 5, 7, 9
// (newlines omitted and commas added for easier reading)
```

Objects that do not conform to the ES6+ iteration protocols can only be consumed in this way, however for those that do there is an alternative. Anything that implements the [iterable](#iterable) protocol and produces an object that conforms to the form of the iterator can also be used in JavaScript's [`for ... of` loop](../flow-control/README.md#for--of-loop). Consider the following example:

```javascript
for (let count of makeRangeIterator(1, 10, 2)) {
  console.log(count)
}
// => 1, 3, 5, 7, 9
// (newlines omitted and commas added for easier reading)
```

Iterators can also be forcibly iterated into an array using the `Array.from()` API or the spread operator. Consider the following example:

```javascript
const arr = Array.from(makeRangeIterator(1, 10, 2))
// OR
const arr = [...makeRangeIterator(1, 10, 2)]

console.log(arr)
// => [1, 3, 5, 7, 9]
```

As both of these methods of iteration will continue consuming the iterator until `end` is `true`, care should be taken when using these with potentially infinite iterators, as this will cause an "infinite loop".
