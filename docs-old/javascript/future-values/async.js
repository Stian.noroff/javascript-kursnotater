doSomethingAsync(...params, function (err, result) {
  if (err) {
    return console.err('async operation failed', error)
  }

  // do something with `result`
})

welcome(function (foo) {
  to(function (bar) {
    callback(function (baz) {
      hell(function (qux) {
        // do something with foo, bar, baz and qux
      })
    })
  })
})

const promise = doSomethingAsync(...params)

promise.then(result => {
  // do something with result
})

fooStringAsync()
  .then(foo => `${foo} bar`)
  .then(foobar => `${foobar} baz`)
  .then(foobarbaz => `${foobarbaz} qux`)
  .then(console.log)
// => 'foo bar baz qux'

// this function throws an error
asyncThrowError()
  // this callback is skipped to jump to the catch
  .then(result => console.log('foo'))

  // this runs to handle the error
  .catch(err => console.log('bar'))

  // this runs after the error is handled
  .then(result => console.log('baz'))
// => 'bar'
// => 'baz'

// Callback version
setTimeout(() => console.log('foo'), 10 * 1000) // 10 seconds

// Promise version
new Promise(resolve => setTimeout(resolve, 10 * 1000))
  .then(() => console.log('foo'))


new Promise((resolve, reject) => {
    doSomethingAsync(...params, function (err, result) {
      // If unsuccessful then reject
      if (err) {
        return reject(err)
      }

      // if successful then resolve
      resolve(result)
    })
  })
  .then(console.log) // log result to console.log
  .catch(console.error) // log any error to console.error

const arr = ['foo', 'bar', 'baz']

arr.map(item => doSomethingAsync(item))
// => [Promise, Promise, Promise]

Promise.all(arr.map(item => doSomethingAsync(item))).then(console.log)
// => [result1, result2, result3]

function waitTenSeconds() {
  return new Promise(resolve =>
    setTimeout(() => resolve('foo'), 10 * 1000))
}

async function doSomethingAsync() {
  console.log('bar')
  console.log(await waitTenSeconds())
}

doSomethingAsync()
// => 'bar'
// => 'foo'

async function* generator() {
  yield 'foo'
  return 'bar'
}

const iter = generator()
console.log(iter.next())
// => Promise

(async () => {
  console.log(await iter.next())
})()
// => { value: 'bar', done: true }

const iterable = {
  [Symbol.asyncIterator]: async function* () {
    yield 'foo'
    return 'bar'
  },
}

function makeAsyncIterator(start = 0, end = Infinity, step = 1) {
  let nextIndex = start
  let iterationCount = 0

  return {
    [Symbol.asyncIterator]: function () {
      return this
    },
    next: new Promise(resolve => {
      let result
      if (nextIndex < end) {
        result = {
          value: nextIndex,
          done: false
        }
        nextIndex += step
        iterationCount++
        return resolve(result)
      }

      resolve({
        value: iterationCount,
        done: true
      })
    }),
  }
}

(async () => {
  for await (const x of iterable) {
    console.log(x)
  }
})()
// => 'foo'
// => 'bar'

(async () => {
  for await (const x of makeAsyncIterator(1, 10, 2)) {
    console.log(x)
  }
})()
// => 1, 3, 5, 7, 9
// (newlines omitted and commas added for easier reading)