# Asynchronicity

When a program interacts with its environment, requested operation sometimes take a large amount of time before they complete. Reading data from persistent storage or sending and receiving data over a remote connection are examples of operations that are unreliable or take a long time to process and we have no way to predict exactly how long it will take. In situations like these; a naive programmer may cause their program to grind to a screeching halt while waiting for data that is never coming.

Since the advent of multi-core processors, most common programming languages have some concept of parallelism and most programs that have been written since are built to take full advantage of having many different threads of execution; JavaScript is not one of these languages. Instead, JavaScript has opted for a single-threaded, asynchronous approach.

JavaScript, being an interpreted language, runs inside a "virtual machine", or a "runtime" and interacts with things outside of the runtime via APIs that call native code &mdash; code that _is_ able to be run concurrently. Some of these APIs, particularly unreliable ones, accept the requests that are made and signal to the JavaScript interpreter to continue executing other code in the meanwhile. We refer to these APIs as being "asynchronous" and the result is that while the JavaScript application will not "hang" or become unresponsive while a long asynchronous result is being fetched.

> **Video**
>
> [What the heck is the event loop anyway?](https://www.youtube.com/watch?v=8aGhZQkoFbQ)

Once the request is complete and the data is ready to be consumed, the runtime will signal the interpreter that execution can continue and the JavaScript code will resume from a [callback function](#callbacks) that was passed to the request at the start. The function is invoked from within the runtime and added to the callback queue, passing in the results of the requests, and will begin executing when called. If something went wrong with the request, the same callback would also be invoked, passing in the resulting error instead of the requested data. The initial call to the API becomes a contract between the JavaScript code and the runtime that, at some, unknown point in the future, the runtime _will_ provide a result as requested, or notice of failure, to the JavaScript code; a [promise of a future value](#future-values-promises).

# Callbacks

At its simplest, a callback is simple a function that is passed into an asynchronous request that is run when the request completes. Consider the follow example of a concurrent, asynchronous API call, `setTimeout`, which is a global function that runs a given callback after some minimum elapsed time:

```javascript
console.log('foo')

setTimeout(() => console.log('bar'), 5000)

console.log('baz')
```

Callbacks differ from typical [higher order functions](../functions/README.md#higher-order-functions) in that those higher order functions provided by the JavaScript API for arrays, maps and sets are all synchronous. Generally speaking, a function is only a callback when that function is called by native code after an asynchronous, concurrent operation.

There are many ways which libraries and native APIs implement callbacks. Some asynchronous functions accept more than one callback; one for the success case and one for the failure case:

```javascript
function successCallback (result) {
    // do something with `result`
}

function failureCallback (error) {
    console.error('async operation failed', error)
}

doSomethingAsync(...params, successCallback, failureCallback)
```

Most asynchronous functions (this pattern is very popular with both the NodeJS and Browser APIs), will accept a single callback, but the first parameter when the callback is invoked will either be an instance of an `Error` or `null`. If the value of the `err` parameter is `null` then the request should be considered successful. Consider the following example:

```javascript
doSomethingAsync(...params, function(err, result) {
  if (err) {
    return console.err('async operation failed', error)
  }

  // do something with `result`
})
```

To improve the readability of functions that accept anonymous callbacks, the convention is that the callback parameter is usually left to be the last item in the parameter list. Consider the following examples:

```javascript
// splitting the parameters makes it more difficult to read this:
doSomethingAsync(foo, function(result) {}, bar, baz)

// ... when compared to this:
doSomethingAsync(foo, bar, baz, function(result) {
  // do something
})
```

## Callback Hell

Callback functions and asynchronous APIs have existed in JavaScript before the advent of ES6; however the JavaScript of old (ES5) had a major problem which is so famous that it was given a name: callback hell.

```javascript
welcome(function(foo) {
  to(function(bar) {
    callback(function(baz) {
      hell(function(qux) {
        // do something with foo, bar, baz and qux
      })
    })
  })
})
```

> **Further Reading**
>
> [Callback Hell: A guide to writing asynchronous JavaScript](http://callbackhell.com/)

Most JavaScript code just before the release of ES6 tended toward interacting with many asynchronous browser APIs. It was not uncommon for the majority of code to be nested deeply with a function stack. This made code difficult to read, debug and maintain and poorly impacted the performance of many websites.

Around the time ES6 was being standardised, JavaScript developers created libraries, such as [bluebird](https://www.npmjs.com/package/bluebird) (16M downloads weekly), which attempted to solve this problem and "flatten" callback hell by introducing a `Promise` construct into JavaScript.

# Future Values (Promises)

JavaScript Promises are objects that conform more tightly to the universal programming concept of a _future value_. A Promise is an object that represents the eventual completion or failure of an asynchronous operation, and since their introduction and integration directly into the JavaScript language, they are also returned by most asynchronous APIs that use callbacks.

The purpose of a promise is to provide an API to attach callbacks to a future value instead of passing them into a function. An asynchronous function call can return a promise, which can accept the callback that will consume the result at a later stage. Consider the following example:

```javascript
const promise = doSomethingAsync(...params)

promise.then(result => {
  // do something with result
})
```

> **Further Reading**
>
> [Promise Reference (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) and ["Using Promises" Guide (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises)

Aside from the syntactic improvements offered by promises, with ES6 promises there are also some additional guarantees that make them better than simple callbacks:

- The callback for a promise will never be called before the completion of the current run of the JavaScript event loop. This is equivalent to every callback being invoked with `setTimeout(cb(result), 0)`.
- Callbacks registered after the success or failure of the asynchronous operation will still be called and the value consumed.
- Multiple callbacks can be executed, with the output of one being fed into the parameters of the next, by calling `then()` multiple times on the promise. Each callback is executed one after another, in the order that they were added.

## Chaining

Calls to the `then()` API of a promise will always return a new instance of promise that represents not only the original future value, but also the completion of the callback that was just registered. Consider the following example:

```javascript
const promise = doSomethingAsync()
const promise2 = promise.then(function(result) {
  // do something with result
  return 'foo'
})

promise2.then(console.log)
// => 'foo'
```

The result is that we can avoid [callback hell](#callback-hell) by forming a promise chain and repeatedly appending `.then()` to the end of our function call as necessary. The resulting code is much flatter and much easier to read:

```javascript
fooStringAsync()
  .then(foo => `${foo} bar`)
  .then(foobar => `${foobar} baz`)
  .then(foobarbaz => `${foobarbaz} qux`)
  .then(console.log)
// => 'foo bar baz qux'
```

> **Caveat**
>
> One of the major improvements that Promises offer over simple callbacks is that they do not need to be nested, and any nesting of Promises is considered to be an anti-pattern in JavaScript. Any asynchronous task that produces a Promise inside of a `then()` callback should simply `return` that Promise from the callback and it will be integrated into the Promise chain.

## Error Handling

Promises provide two methods for registering callbacks: `then()` and `catch()`. The former registers a callback that will run if the operation is successful and the latter for when the operation fails. The `catch()` method works exactly like the `then()` method but is traditionally passed an `Error` object as its first parameter, just like the [callback](#callbacks) example above.

Just like with `then()`, the `catch()` method also returns a new promise that is the future value of the caught error (i.e. after your [attempted state recovery logic](../errors/README.md#try--catch-block)). These promises can continue to be extended with `then()` or `catch()` calls. Consider the following example:

```javascript
// this function throws an error
asyncThrowError()
  // this callback is skipped to jump to the catch
  .then(result => console.log('foo'))

  // this runs to handle the error
  .catch(err => console.log('bar'))

  // this runs after the error is handled
  .then(result => console.log('baz'))
// => 'bar'
// => 'baz'
```

> **Note**
>
> If a promise does not throw an error or explicitly `reject` then `catch()` callbacks will be ignored and the promise will skip to the next `then()` callback.

It was recently standardised that all promises that do have their error states handled will cause the whole process to fail. Most browsers have started preparing for this by issuing warnings when an unhandled error is detected that these will eventually be deprecated and will eventually cause catestrophic failure. If there is a chance that a promise can throw an error then the case where it does should be handled. The only exception is when the error handling is to be done by different code later on.

## Wrapping Callbacks with Promises

ES5-style callbacks and Promises do not mix very well, so for some older APIs that may not have caught up to the use of Promises yet, we can easily wrap these so that they conform to the new interface. An obvious example of this is the `setTimeout` function, which still accepts a callback and does not return a Promise.

```javascript
setTimeout(() => console.log('foo'), 10 * 1000) // 10 seconds
```

This could be rewritten as follows:

```javascript
new Promise(resolve => setTimeout(resolve, 10 * 1000)).then(() => console.log('foo'))
```

The constructor for Promises accepts two functions, the success callback and the failure callback, traditionally called `resolve` and `reject`. Consider another example of wrapping an asynchronous API with a callback:

```javascript
new Promise((resolve, reject) => {
  doSomethingAsync(...params, function(err, result) {
    // If unsuccessful then reject
    if (err) {
      return reject(err)
    }

    // if successful then resolve
    resolve(result)
  })
})
  .then(console.log) // log result to console.log
  .catch(console.error) // log any error to console.error
```

## Contrived Promises

It is possible to create promises that have already resolved or rejected using `Promise.resolve()` and `Promise.reject()` respectively. These can be useful at times, such as when wanting to turn synchronous code into a Promise chain or for contriving program state for unit tests.

> **Further Reading**
>
> [`Promise.resolve()` Reference (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/resolve) and [`Promise.reject()` Reference (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/reject

## Promise Composition

There are many cases where many Promises may need to be run in parallel. Consider the following example:

```javascript
const arr = ['foo', 'bar', 'baz']

arr.map(item => doSomethingAsync(item))
// => [Promise, Promise, Promise]
```

The Promise API provides two tools for handling cases such as these: `Promise.all()` and `Promise.race()`.

To continue the above example, we can wait for many asynchronous operations to produce results like this:

```javascript
Promise.all(arr.map(item => doSomethingAsync(item))).then(console.log)
// => [result1, result2, result3]
```

The alternate, `Promise.race()`, works similarly but will wait for the first result to be produced and return only that one result.

> **Further Reading**
>
> [`Promise.all()` Reference (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/all) and [`Promise.race()` Reference (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/race)

# `async` and `await`

With the standardisation of ES7 (or ES 2017), the `async` and `await` keywords were added to the JavaScript language which enable the definition of asynchronous functions in a way that looks very similar to standard JavaScript functions. At their core, asynchronous functions declared in this way implicitly return Promises as its result.

Within the functions, the `await` keyword can be used to block the execution of the function until a result has returned. This functionality of `async` and `await` is summarised by the following example:

```javascript
function waitTenSeconds() {
  return new Promise(resolve => setTimeout(() => resolve('foo'), 10 * 1000))
}

async function doSomethingAsync() {
  console.log('bar')
  console.log(await waitTenSeconds())
}

doSomethingAsync()
// => 'bar'
// => 'foo'
```

> **Further Reading**
>
> [`async` Function Reference (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function) and [`await` Operator Reference (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/await)

Instead of having a `catch()` callback on a Promise; `async`/`await` also has the benefit of being able to be enclosed in a normal JavaScript [`try ... catch` block](../errors/README.md#try--catch-block).

# Asynchronous Iteration

Following the introduction of the `async`/`await` syntax, a new form of loop was also introduced in ES 2018: the `for await ... of` loop; as well as an asynchronous iterator protocol and generator syntax to compliment it.

## Async Generators

The only difference from synchronous generator functions is that the resulting iterator object (with the `next()` method) returns a Promise that resolves to the standard iteration object with properties `value` and `done`. Consider the following example:

```javascript
async function* generator() {
  yield 'foo'
  return 'bar'
}

const iter = generator()
console.log(iter.next())
// => Promise
;(async () => {
  console.log(await iter.next())
})()
// => { value: 'bar', done: true }
```

## Async Iterator Protocol

The asynchronous iterator protocol is very similar to [its synchronous counterpart](../collections/README.md#iterators) but is necessary for use with the `for await ... of` loop. The only differences are that it consumes and awaits Promises when calling `next()`, as described above for [generators](#async-generators), and it uses a different symbol for defining the function that returns the iterator. Consider the following example:

```javascript
const iterable = {
  [Symbol.asyncIterator]: async function*() {
    yield 'foo'
    return 'bar'
  },
}
```

As described before with custom synchronous iterable collections, the iterator protocol and iterator object are complimentary and are seldom implemented separately. Consider the following example:

```javascript
function makeAsyncIterator(start = 0, end = Infinity, step = 1) {
  let nextIndex = start
  let iterationCount = 0

  return {
    [Symbol.asyncIterator]: function() {
      return this
    },
    next: new Promise(resolve => {
      let result
      if (nextIndex < end) {
        result = { value: nextIndex, done: false }
        nextIndex += step
        iterationCount++
        return resolve(result)
      }

      resolve({ value: iterationCount, done: true })
    }),
  }
}
```

Once an object correctly implements the async iterable protocol, it can be used with the [`for await ... of` loop](#for-await--of-loop)

## `for await ... of` Loop

The functions returning iterators from the examples above can be used with this asynchronous loop syntax:

```javascript
;(async () => {
  for await (const x of iterable) {
    console.log(x)
  }
})()
// => 'foo'
// => 'bar'
;(async () => {
  for await (const x of makeAsyncIterator(1, 10, 2)) {
    console.log(x)
  }
})()
// => 1, 3, 5, 7, 9
// (newlines omitted and commas added for easier reading)
```
