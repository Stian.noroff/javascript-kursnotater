throw 'foo'
throw 42
throw ['foo', 'bar', 'baz']
throw {
    foo: 'bar'
}
throw new Error('error message')

// Error: foo
//     at A (/path/to/index.js:2:9)
//     at B (/path/to/index.js:6:3)
//     at C (/path/to/index.js:10:3)
//     at Object.<anonymous> (/path/to/index.js:12:1)