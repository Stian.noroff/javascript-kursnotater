# Error Handling

Programs that enter a state that is not explicitly intended by the programmer are said to have entered an "error state". If a program reaches an error state, it has the potential to cause damage to the persistent state of the whole system; to avoid this, most langauges have some concept of an error, which is used to notify the system that their instructions could not be followed. JavaScript is no exception to this.

In JavaScript, there are two statements of interest for error handling, these are:

- `throw` Statements
- `try ... catch` Blocks (with many variants).

# `throw` Statement

The `throw` statement is how the JavaScript interpreter is notified of an error state. Invoking the throw statement will halt execution of that routine and unravel the program to the nearest error boundary, which takes the form of a [`try ... catch` block](#try--catch-block). The throw statement also accepts an error property which it will be passed into the catch block at the boundary.

```javascript
throw 'foo'
throw 42
throw ['foo', 'bar', 'baz']
throw { foo: 'bar' }
throw new Error('error message')
```

# Errors

JavaScript provides a global Error type that can be used to assist in debugging a JavaScript application. When invoked with the [`new` operator](../functions/README.md#new-operator), the error will gather information about the execution context and application state _at that moment in time_. The error is then thrown back with all of this information and handled.

<figure>
    <center>
        <img src="./throw.png" alt="js stack trace" width="350"/>
        <figcaption>Figure: Throwing an Error to produce a stack trace</figcaption>
        <br/>
    </center>
</figure>

```
Error: foo
    at A (/path/to/index.js:2:9)
    at B (/path/to/index.js:6:3)
    at C (/path/to/index.js:10:3)
    at Object.<anonymous> (/path/to/index.js:12:1)
```

# `try ... catch` Block

Once a program enters an error state, there are two possible responses:

1. Fail quickly
2. Attempted Recovery (failing which revert back to response #1).

Both of these responses require a try-catch block to catch the error that was thrown using the [`throw` statement](#throw-statement). Try-catch blocks have many variants; all of which will be discussed:

1. `try ... catch`
2. `try ... catch ... finally`
3. `try ... finally`

The `try` statement creates a new block (and scope), in which any errors occuring in that scope get caught and program control is given to the corresponding `catch` block.

<figure>
    <center>
        <img src="./try_catch.png" alt="js try catch" width="350"/>
        <figcaption>Figure: Try-Catch</figcaption>
        <br/>
    </center>
</figure>

The catch block takes an error parameter which is only in scope within the catch block itself. The error parameter is whatever was passed to the `throw` statement. The contents of the catch block should adequately handle the error and reset the application to a "known good state" (i.e. to attempt recovery from an error state); or should make the error known and fail quickly.

In the former option, if you are successful in resetting the application state to a "known good state" where the attempted routine (or an equivalent) is eventually complete, then it is acceptable and considered a good user experience to have the error fail silently. If the attempted action, was not completed to the users' satisfaction as a result of the error, and not automatically reattempted, then it is a good idea to inform the user of this explicitly, in a way they can understand.

In the latter option, logging the error out to see where in code the error occured is absolutely necessary in order for the error to be fixed. The catch block may also be used to log out other parts of the application state while attempting to debug that section of code.

> **Aside**
>
> Conditional catch (as per languages like C# and Java) is currently an experimental feature of JavaScript for ES2020 and is not standardised. You shouldn't attempt using this feature until it is. More information can be seen [here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/try...catch).

In addition to `try` and `catch`, there is also the `finally` block which can be used to define behaviour that happens regardless of whether an exception is thrown or not.

<figure>
    <center>
        <img src="./try_catch_finally.png" alt="js try catch finally" width="350"/>
        <figcaption>Figure: Try-Catch-Finally</figcaption>
        <br/>
    </center>
</figure>

A finally block also executes even if there is no catch block to handle possible exceptions. This is typically used when nesting try-catch blocks so that all of the errors are caught at the outer-most error boundary.

<figure>
    <center>
        <img src="./try_finally.png" alt="js try finally" width="350"/>
        <figcaption>Figure: Try-Finally</figcaption>
        <br/>
    </center>
</figure>
