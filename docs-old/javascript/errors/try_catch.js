/* global console */
'use strict'

function A() {
  throw new Error('foo')
}

function B() {
  A()
}

function C() {
  B()
}
C()

try {
  throw 'foo'
} catch (error) {
  console.error(error)
}

try {
  throw 'foo'
} catch {
  console.error('msg')
}

try {
  throw 'bar'
} catch (error) {
  console.error(error)
} finally {
  console.log('baz')
}

try {
  throw 'qux'
} finally {
  console.log('baz')
}