# Basic Operators

JavaScript supports a generous set of operators which function similarly to how they would in other languages. Mathematical operators (`+`, `-`, `*`, `**`, `/` and `%`) all function as one expects and many other operators are provided as function calls through the [`Math` API](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math). Boolean operators (`&&`, `||`, and `!`), binary operators (`&`, `|`, `^`, `~`, `>>`, `<<`, and `>>>`), and assignment operators (`=`, `++`, `+=`, `--`, `-=`, `*=`, `/=` and `%=`) are all similarly predictable. Relational operators (`<`, `>`, `<=`, and `>=`), and equality operators (`==` and `===`) will be discussed with [Conditionals and Boolean Logic](../conditionals/README.md).

Instead of enumerating all of the operators available in JavaScript, this section will focus on those that are specific to JavaScript and those that differ significantly from behaviour in other languages. More complex operators are also introduced as more features of the language are fleshed out.

> **Further Reading**
>
> Documentation and usage guides for any unfamiliar operators can be found easily online.

# Mathematical Operators

- `+` &mdash; Addition and Concatenation. The `+` operator functions as expected when interacting with numbers but JavaScript's type inferencing system will also use the `+` operator to mean concatenation if one of the values is not a number. Object types are automatically converted to strings by calling the `.toString()` method on them.

  ```javascript
  console.log('hello ' + 4 + 2)
  // => 'hello 42'
  console.log(4 + 2 + ' hello')
  // => '6 hello'
  ```

- `-` &mdash; Subtraction. Unlike the `+` operator, the `-` operator does not interact with types other than numbers; however, attempting to do so will not produce an error, instead it will produce the special value `NaN` which means "Not a Number".

  ```javascript
  console.log('hello ' - 1)
  // => NaN
  ```

- `**` &mdash; Power. Raises the left value to the power of the right.

  ```javascript
  console.log(2 ** 8)
  // => 256
  ```

- `%` &mdash; Modulus. Returns the remaineder from a division operation.

  ```javascript
  console.log(5 % 4)
  // => 1
  ```

# Assignment Operators

- `++` &mdash; Increment. This can either be prepended or appended to a variable to denote the order of reading. For example:

  ```javascript
  let foo = 0
  console.log(++foo)
  // => 1 - Increments then reads
  console.log(foo++)
  // => 1 - Reads then increments
  console.log(foo)
  // => 2
  ```

- `--` &mdash; Decrement. The same rules as apply as with the increment operator.

- `+=` &mdash; Summation Assignment and Concatentatio Assignment. As with the `+` operator, this operator can also be applied to strings with a predictable result.

- `%=` &mdash; Modulus Assignment. This is the equivalent of:

  ```javascript
  let val = 5
  let val2 = 5
  const mod = 4
  val = val % mod
  val2 %= mod
  console.log(val == val2)
  // => true
  ```

# Binary Operators

- `^` &mdash; Binary XOR.

- `~` &mdash; Binary NOT.

- `>>` &mdash; Signed Bitshift Right. This will bitshift right but without the MSB.

  ```javascript
  console.log(8 >> 1)
  // => 4
  console.log(-8 >> 1)
  // => -4
  ```

- `>>>` &mdash; Unsigned Bitshift Right. This will shift the MSB as well.

  ```javascript
  console.log(8 >>> 1)
  // => 4
  console.log(-8 >>> 1)
  // => -2147483644
  ```

# `typeof` Operator

The `typeof` operator is special kind of operator in JavaScript which returns a string denoting the type of a value.

```javascript
const foo = {}
console.log(typeof foo)
// => 'object'
```

This operator is useful when writing functions that might take many different forms of input, as JavaScript does not support function overloading. That being said, the `typeof` operator may not produce the output that you expect for certain values.

<figure>
    <center>
        <img src="./variable_types.png" alt="js types" width="550"/>
        <figcaption>Figure: Some JavaScript Types</figcaption>
        <br/>
    </center>
</figure>

> **Aside**
>
> The type system in JavaScript is notorious among programmers and is frequently the subject of [many jokes](https://www.destroyallsoftware.com/talks/wat). At first glance these type assignments may not seem logical but at least they are consistent. When considering using the `typeof` operator, you should _probably_ test your assumptions. Extensively.
