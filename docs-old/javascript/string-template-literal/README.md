# String Template Literals

String template literals allow embedded expressions whose results replace the template in the resulting string literal. String templates are defined using the `` ` `` (grave accent) character instead of the single or double quotation marks. Interpolated expressions can be embedded within template strings using the `${var_name}` syntax. Once a string template is interpolated (the "templates" substituted with their "real values"), the resulting value is stored as a string literal. Consider the following example:

```javascript
console.log(`${1 + 1}`)
// => '2'
```

Template strings may also contain escaped characters and span multiple lines:

```javascript
console.log(`foo\n
bar`)
// => 'foo\n\nbar'
```

As a general rule, template strings are more performant and more easily read than string concatentation and should therefore be preferrable to those alternatives.

> **Further Reading**
>
> [Strings (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String) and [String Template Literals (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals)
