# Strings

As in many other languages, JavaScript Strings are Object types (they extend `Object`) but are immutable. When strings are manipulated they might seem to be mutable but they are in fact being copied every time there is a mutation, incuring costs for every mutation. Consider the following example:

```javascript
console.log('foo ' + 'bar ' + 'baz')
// => 'foo bar baz'
```

In this example, the statement `'foo ' + 'bar ' + 'baz'` is evaluated as `(('foo ' + 'bar ') + 'baz')` and produces three distinct copies of the same string in memory:

- First, the string `'foo '` is stored.
- Second, the string `'foo bar '` is stored separately from the first and the first string is deallocated.
- Third, the string `'foo bar baz'` is stored separately from the second and the second string is deallocated.

One way to avoid incurring these costs is to use string template literals, which defines and allocates a parameterised string literal only once without the need for memory to be copied and reallocated.

> **Further Reading**
>
> [Strings (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)
