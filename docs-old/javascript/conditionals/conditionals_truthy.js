'use strict'

if (conditional) {
  // if conditional is true
} else if (conditional2) {
  // if conditional is false and conditional2 is true
} else {
  // if both conditional and conditional2 are false
}

switch (myVar) {
  case 0:
    console.log('foo')
    break;

  case 41:
  case 42:
    console.log('bar')
    break;

  default:
    console.log('baz')
    break;
}

// Numbers
console.log(Boolean(0))
console.log(Boolean(42))
console.log(Boolean(-42))
console.log(Boolean(NaN))
// Strings
console.log(Boolean(''))
console.log(Boolean('foo'))
console.log(Boolean('false'))
console.log(Boolean('0'))
// Objects
console.log(Boolean({}))
console.log(Boolean([]))
console.log(Boolean(null))
// Undefined
console.log(Boolean(undefined))

// Numbers
console.log(Boolean(0)) // => false
console.log(Boolean(42)) // => true
console.log(Boolean(-42)) // => true
console.log(Boolean(NaN)) // => false
// Strings
console.log(Boolean('')) // => false
console.log(Boolean('foo')) // => true
console.log(Boolean('false')) // => true
console.log(Boolean('0')) // => true
// Objects
console.log(Boolean({})) // => true
console.log(Boolean([])) // => true
console.log(Boolean(null)) // => false
// Undefined
console.log(Boolean(undefined)) // => false