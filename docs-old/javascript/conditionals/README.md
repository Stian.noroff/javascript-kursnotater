# Conditionals and Boolean Logic

The next component all programs need is to execute branching logic, which means that some code should only be executed if certain conditions are satisfied. As previously mentioned, JavaScript includes a number of common operators for used for conditional statements:

- Boolean operators (`&&`, `||`. and `!`)
- Relational operators (`<`, `>`, `<=`, and `>=`)
- Equality operators (`==`, `!=`, `===`, and `!==`)

The bulk of these function as expected with the caveats that are JavaScript's type coersion system and "strict" comparisons.

> **Further Reading**
>
> [Control Flow (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Control_flow_and_error_handling)

# `if ... else` Statement

An `if` statement is the primary way to introduce branching logic into a JavaScript program. The premise is simple and consistent with many other langauages; if a provided conditional is "truthy" then the block inside the if-statement will be executed. Otherwise, if an `else` block is provided then that will be executed. If the conditional is "falsy" and no `else` block is provided then nothing is executed.

If-statements can also be layered using an else-if pattern, where the else block of one if-statement contains another if-statement. Each conditional is tested sequentially until one is true and then the corresponding block of code is executed. When a conditional is evaluated as true, then the remaining conditionals are not tested and their code blocks not executed.

<figure>
    <center>
        <img src="./if_stmt.png" alt="js if statement" width="650"/>
        <figcaption>Figure: If-Statement</figcaption>
        <br/>
    </center>
</figure>

# `switch` Statement

JavaScript also provides a switch statement which functions as expected in comparison to other C-like languages. The switch statement accepts a variable and branches to the first case that matches the variable in question, or the default, if one is provided. If no cases match the switch then the contents of the switch statement are ignored.

<figure>
    <center>
        <img src="./switch_stmt.png" alt="js switch statement" width="350"/>
        <figcaption>Figure: Switch Statement</figcaption>
        <br/>
    </center>
</figure>

Switch statements are usually accompanied by `break` statements. These are statements that trigger the program control to be returned to the next statement after the switch block. Without a break statement, the program will continue executing the instructions for the next case, even if the case does not match the switch. In the above example, the switch statement will print the following:

- `foo` with an input of `0`
- `bar` with an input of `41` or `42` &mdash; because there is no `break` separating the two they will have the same effect.
- `baz` with any other input

# Boolean Coersion

In the same way that most values can be coerced into strings for concatenation and all manner of other uses; all values can also be coerced into boolean values for use with conditional logic. When we speak about these values, and what boolean result they would be coerced into, we refer to then as "truthy" or "falsy".

<figure>
    <center>
        <img src="./conditionals_truthy.png" alt="js truthy" width="550"/>
        <figcaption>Figure: Truthy or Falsy?</figcaption>
        <br/>
    </center>
</figure>

# Strict Comparison

The purpose of strict comparisons is to disable JavaScript's type coersion system in this comparison, the effects are as follows:

```javascript
console.log(true == 1)
// true
console.log(true === 1)
// false
console.log(undefined == null)
// true
console.log(undefined === null)
// false
```

For the strict comparison operator, JavaScript will only produce a `true` result if both the values _and_ the types of the values match.

> **Important Note**
>
> As a rule you should consider using strict comparisons in as many places as possible as their behaviour is more defined and more easily predictable.

# Floating Point Equality Caveat

JavaScript has double-precision floating point numbers, but double precision is still fallible. Like in other languages, if you are comparing floating point numbers then you should avoid using the equality operators (`==`, `===`, etc.) and instead use an error margin.

```javascript
// BAD
if (value === target) {
  // this may not run as you expect
}

// CORRECT
const error_margin = 0.00001
if (Math.abs(value - target) < error_margin) {
  // do something
}
```

> **Hint**
>
> The best way to deal with this problem is to avoid using floating point numbers altogether. One way to do this is to simply multiply out the numbers after the decimal; i.e. the floating point number `3.14` could easily be represented as the integer `314` if you adjust the math accordingly. Financial records (anything to do with money) should _never_ use floating point numbers.

# `instanceof` Operator

The `instanceof` operator is special kind of operator in JavaScript which returns true or false if a variable matches a given type.

```javascript
const foo = ['bar']
console.log(foo instanceof Array)
// => true
console.log(foo instanceof Object)
// => true
```

While support for the `instanceof` operator has grown with recent JavaScript specifications; there are still some types that do not behave intuitively. For example, using the methods provided thus far, it is very difficult to test for the value `NaN`. Consider the following:

```javascript
console.log(typeof NaN) // => 'number'
console.log(NaN instanceof Number) // => false
console.log(NaN == NaN) // => false
console.log(NaN === NaN) // => false
```

Luckily, in this particular case, JavaScript provides a helper function:

```javascript
console.log(Number.isNaN(NaN)) // => true
```

Other built-in objects that may have similar issues include:

- Arrays (`Array.isArray()`)
- Buffers (`Buffer.isBuffer()`) &mdash; NodeJS only
- etc.

> **Hint**
>
> If you're having difficulty testing for something with `instanceof`, then be sure to look at the documentation for that type.

# Ternary Operator

The ternary operator is a type of assignment operator that makes one of two assignments based on the result of a conditional statement. The form of the ternary operator looks like this:

```javascript
console.log(conditional ? 'foo' : 'bar')
// => if conditional is truthy => 'foo'
// => if conditional is falsy => 'bar'
```

Ternary operators can be used to reduce code for simple if-statements and are frequently used in [JavaScript template strings](#) (todo add link later). For example, consider the following almost-equivalent code snippets:

```javascript
// Normal if-statement
let val
if (conditional) {
  val = 'foo'
} else {
  val = 'bar'
}

// Ternary operator
const val = conditional ? 'foo' : 'bar'
// Note the additional benefit of assigning to a const
```

# `in` Operator

The `in` operator allows us to test if a property is defined in an object. Previously, this was done in the following way:

```javascript
const key = 'key'
if (obj[key] !== undefined) {
  // obj.key is defined
  // do something
}
```

With the addition of the `in` operator this can instead be rewritten as follows:

```javascript
const key = 'key'
if (key in obj) {
  // obj.key is defined
  // do something
}
```

The difference is subtle but the resulting code is clearer and easier to read.

**Caveat**

One common mistake with using the `in` operator may arise from its uses in other languages; the `in` operator cannot be used to test for a value in an array. For this, we recommend the use of the `includes` function on an array.

```javascript
const array = ['foo', 'bar']
console.log('foo' in array)
// => false

console.log(array.includes('foo'))
// => true
```
