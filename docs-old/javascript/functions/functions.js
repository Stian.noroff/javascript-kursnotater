'use strict'

function hello() {
    console.log('Hello, World!')
}

function greet(name) {
    console.log(`Hello, ${name}!`)
}

function double(foo) {
    return foo * 2
}

hello()
// => Hello, World!
greet('Foo')
// => Hello, Foo!

const double = function (foo) {
    return foo * 2
}
const four = double(2)
console.log(four)
// => 4

function fibonacci(n, state = [0, 1]) {
    // Terminate if condition is met
    if (!n || state.length < 2 || state.length > n - 1) {
        return state
    }

    // Destructure the last two elements of the state array
    // NOTE: `Array.slice()` does not alter the array
    const [x, y] = state.slice(-2)

    // Add the new element
    state.push(x + y)

    // Recurse
    return fibonacci(n, state)
}

console.log(fibonacci(10))
// => [0, 1, 1, 2, 3, 5, 8, 13, 21, 34]

const state = []

function pushFoo(foo) {
    foo.push('foo')
}

console.log(state)
// => []

pushFoo(state)
console.log(state)
// => ['foo']

pushFoo(state)
console.log(state)
// => ['foo', 'foo']

// =========================

const state = []

function pushFoo() {
    state.push('foo')
}

console.log(state)
// => []

pushFoo()
console.log(state)
// => ['foo']

pushFoo()
console.log(state)
// => ['foo', 'foo']

const buildWord = word1 =>
    word2 => `${word1} ${word2}`

const foo = buildWord('foo')
const bar = buildWord('bar')

console.log(foo('baz'))
// => 'foo baz'

console.log(foo('qux'))
// => 'foo qux'

console.log(bar('baz'))
// => 'bar baz'

console.log(bar('qux'))
// => 'bar qux'

const greet = name => `Hello, ${name}!`

const module = {
    x: 42,
    getX: function () {
        return this.x
    },
}

console.log(module.getX())
// => 42

const module = {
    x: 42,
    getX: function () {
        return this.x
    },
}

const unboundGetX = module.getX
// The function gets invoked at the global scope
console.log(unboundGetX())
// => undefined

const boundGetX = unboundGetX.bind(module)
console.log(boundGetX())
// => 42

function Car(make, model, year) {
    this.make = make
    this.model = model
    this.year = year
}

const car1 = new Car('Eagle', 'Talon TSi', 1993)

console.log(car1.make)
// => 'Eagle'

function add(x, y, fn) {
    const sum = x + y
    fn(sum)
}

add(3, 2, result => console.log(result))
// => 5

const arr = ['foo', 'bar', 'baz', 'qux']

// get the first letter of each word
console.log(arr.map(word => word.substring(0, 1)))
// => ['f', 'b', 'b', 'q']

const arr = ['foo', 'bar', 'baz', 'qux']

// append each word to the current state
console.log(arr.reduce((state, word) => state + word))
// => 'foobarbazqux'

const arr = ['foo', 'bar', 'baz', 'qux']

const mapper = word => word.substring(0, 1)
const reducer = (state, word) => state + word

console.log(arr.map(mapper).reduce(reducer))
// => 'fbbq'