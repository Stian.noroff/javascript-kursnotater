# Functions

Functions are one of the core patterns in any programming language, not just JavaScript. A function is a set of statements that performs a task, in terms of the function's parameters and context, and may return some computed value. Functions are also frequently referred to as "sub-programs" as a complete program follows the exact same model.

> **Further Reading**
>
> [Functions (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions) and [Functions Reference (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions)

Functions are declared, using the `function` keyword, in the following manner:

<figure>
    <center>
        <img src="./functions_simple.png" alt="js functions void" width="450"/>
        <figcaption>Figure: Void Function</figcaption>
        <br/>
    </center>
</figure>

The function above isn't particularly useful; it simply logs out "Hello, World!" to the console. It takes no inputs and produces no direct output.

> **Caveat**
>
> Despite predating scopes in JavaScript, when declaring a named function using the `function` keyword in ES6, the function is only declared within the local scope (as if using `let`) and **not** globally (as if using `var`).

# Function Parameters

In addition to grouping statements together and running them with a single instruction &mdash; the "function call" &mdash; functions can also be specified in terms of a set of parameters which allow the same instructions to be executed on different data. Using parameters, we can modify our example above to be slightly more useful:

<figure>
    <center>
        <img src="./functions_params.png" alt="js functions greeting" width="450"/>
        <figcaption>Figure: A simple greeting</figcaption>
        <br/>
    </center>
</figure>

<figure>
    <center>
        <img src="./functions_calling_greet.png" alt="js functions greet calling" width="350"/>
        <figcaption>Figure: Function invocation</figcaption>
        <br/>
    </center>
</figure>

# Function Hoisting

One of the many advantages of modern JavaScript's multi-pass interpreters is the introduction of "Function Hoisting". While the JavaScript code is being turned into byte code, the interpreter builds up a table of functions and scopes. When the code begins to execute, that table is used to jump between instructions meaning that, as long as they're within scope, functions can be referred to by code that appears before the code that declares the function.

> **Note**
>
> Function hoisting only applies to named functions, declared using the `function` keyword.

# `return` Statement

Most functions are written to accept inputs and produce some kind of output; those above have thus far been examples "Void Functions", which are functions that have no return value, or in the case of JavaScript, they assume the default return value of `undefined`. Most useful functions produce some kind of result which is passed back to the caller using the `return` statement:

<figure>
    <center>
        <img src="./functions_return.png" alt="js functions return" width="350"/>
        <figcaption>Figure: Values returned from a function</figcaption>
        <br/>
    </center>
</figure>

<figure>
    <center>
        <img src="./functions_calling_double.png" alt="js functions double calling" width="350"/>
        <figcaption>Figure: Function invocation</figcaption>
        <br/>
    </center>
</figure>

> **Aside**
>
> The `return` statement also halts execution of the function in a manner similar to the `break` statement; once `return` is called, control is passed back to the caller of the function and any remaining statements inside the function are ignored.

# Side Effects

It should be noted that parameters passed into&mdash; or returned from&mdash; a function are either "passed by reference" or "passed by value" depending on the type of the value passed as a parameter. Primitive values such as numbers and booleans are passed by value and their originals will not be affected by function side effects. Any complex value &mdash; anything that extends `Object` &mdash; will be passed by reference and can be altered. These alterations are known as "side effects".

<figure>
    <center>
        <img src="./functions_side_effects.png" alt="js functions side effects" width="350"/>
        <figcaption>Figure: Side effects from function parameters passed by reference</figcaption>
        <br/>
    </center>
</figure>

As a program grows, and projects involve more programmers, it becomes increasingly difficult to keep track of all the places where side effects will cause errors if used incorrectly. The functional programming paradigm promotes the concept of "purity" which is known to improve legibility, robustness and general quality of code.

Functional purity simply means that ideally a function, given the same input, should always produce the same output, without side effects.

> **Further Reading**
>
> [JavaScript and Functional Programming &mdash; An Introduction](https://hackernoon.com/javascript-and-functional-programming-an-introduction-286aa625e26d)

# Function Expressions

Functions can be declared anywhere that any other expression may be used in JavaScript and are treated the same as any other kind of variable. These functions are generally declared without a name and are referred to as "Anonymous Functions".

<figure>
    <center>
        <img src="./functions_anon.png" alt="js functions anonymous" width="450"/>
        <figcaption>Figure: Anonymous function assigned to a variable</figcaption>
        <br/>
    </center>
</figure>

## Arrow Functions

Since ES6, arrow functions provide an alternative function expression in JavaScript. The two defining features of arrow functions are a shorter syntax and non-binding of `this`.

Compared to the other kind of function expression, arrow functions provide a simpler syntax for use with [Higher Order Functions](#higher-order-functions) and [Closures](#closures), and the value of `this` inside the arrow function assumes the value of `this` outside the arrow function. This makes object-oriented style programming much easier and avoids having to define a separate variable (usually `self`) which can be closed over.

Arrow functions also provide some convenient syntax shortcuts and "single statement functions". In the event that the function body is not a block (i.e. enclosed in `{}`), then the `return` statement can be omitted as the result of the only statement in the function body is assumed to be it.

```javascript
const foo = word => `foo ${word}`

console.log(foo('bar'))
// => 'foo bar'
```

Being a simple function which just prepends `foo` to the string that is passed in; it can be written very easily as a single statement function. Single statement functions can also be used to produce object and array literals, but those should be enclosed in parentheses (`()`) to be made valid JavaScript.

```javascript
const kv = (key, value) => ({ [key]: value })

console.log(kv(foo, bar))
// => { foo: 'bar' }
```

> **Further Reading**
>
> [ES6 in Depth: Arrow Functions](https://hacks.mozilla.org/2015/06/es6-in-depth-arrow-functions/)

# Higher Order Functions

As a direct result of functions being treated as any other variable, it is possible to pass a function into a call to a function as a parameter. We refer to functions that accept functions as a parameter as "Higher Order Functions".

Higher order functions allow functions to leave some behaviour unspecified and instead allow the caller of the function to specify the behaviour themselves. This concept is used extensively in the standard JavaScript API, [Callbacks and Future Values](../future-values/README.md).

```javascript
function add(x, y, fn) {
  const sum = x + y
  fn(sum)
}

add(3, 2, result => console.log(result))
// => 5
```

## Map-Reduce

Map-Reduce is a programming technique for producing aggregate values over large sets of data in a way that can be easily parallelised.

First, for each element in a data set, a "mapping" operation is applied to produce a single new value from the old one. In JavaScript, the mapping operation is simply a function that accepts a single element as a parameter and returns the new value that is produced.

```javascript
const arr = ['foo', 'bar', 'baz', 'qux']

// get the first letter of each word
console.log(arr.map(word => word.substring(0, 1)))
// => ['f', 'b', 'b', 'q']
```

Second, each element is sequentially "reduced" into a single, aggregate state variable. Similar to map, in JavaScript the reduce operation is simply a function that accepts the current state and a single element and returns the modified state after the element has been added to it.

> **Further Reading**
>
> [Array Map Higher Order Function (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map)

```javascript
const arr = ['foo', 'bar', 'baz', 'qux']

// append each word to the current state
console.log(arr.reduce((state, word) => state + word))
// => 'foobarbazqux'
```

> **Further Reading**
>
> [Array Reduce Higher Order Function (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce)

While extremely powerful on their own, these two higher order functions can be combined to even greater effect.

```javascript
const arr = ['foo', 'bar', 'baz', 'qux']

const mapper = word => word.substring(0, 1)
const reducer = (state, word) => state + word

console.log(arr.map(mapper).reduce(reducer))
// => 'fbbq'
```

JavaScript also includes other higher order functions such as [`find`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find) and [`filter`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter) that work similarly to the above.

# `this`

`this` is a special variable whose value is determined from the context of where and how a function is called. The behaviour of `this` changes from case to case:

- Most of the time, `this` takes the value of the function's parent object or `undefined` if the function is declared in the global scope.
- In the event that the function is a property of an object, then `this` takes the value of the object.
- When bound, `this` takes the value of whatever was passed into the call to [`bind`](#bind-method) no matter the context in which the function is called.
- When a function is invoked in combination with the [`new` Operator](#new-operator), a new, empty object is instantiated for the value of `this`.

```javascript
const module = {
  x: 42,
  getX: function() {
    return this.x
  },
}

console.log(module.getX())
// => 42
```

> **Further Reading**
>
> There are many complicated edge-cases for determining the value of `this` that mostly has to be learned through experience. For more information on `this`, see the official [MDN Reference Document](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/this) on the topic.

## `bind` Method

`bind` is a special method available to all functions that creates a new function that, when called, has the value of `this` set to what was passed into the call to `bind`.

```javascript
const module = {
  x: 42,
  getX: function() {
    return this.x
  },
}

const unboundGetX = module.getX
console.log(unboundGetX()) // The function gets invoked at the global scope
// => undefined

const boundGetX = unboundGetX.bind(module)
console.log(boundGetX())
// => 42
```

> **Further Reading**
>
> [Function Reference for bind (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_objects/Function/bind)

# `new` Operator

The `new` operator is used with a function invocation to instantiate a new object with some additional behaviour that is defined by the function. The function that is invoked with the `new` operator becomes the constructor for the object, which is passed into the function as `this`. If the function does not have its own explicit return value then the new object created is returned.

```javascript
function Car(make, model, year) {
  this.make = make
  this.model = model
  this.year = year
}

const car1 = new Car('Eagle', 'Talon TSi', 1993)

console.log(car1.make)
// => 'Eagle'
```

The transferral of behaviours and methods is discussed later in the [Classes and Inheritence](../classes/README.md#prototype) (todo fix link) chapter.

> **Further Reading**
>
> [`new` Operator (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/new)

# Recursion

A function can refer to and call itself as long as it has an in-scope variable that refers to the function. Recursion is analagous to a loop and similar care should be taken to avoid infinite looping (or recursion in this case). Consider the following code:

<figure>
    <center>
        <img src="./fibonacci.png" alt="js functions context side effects" width="750"/>
        <figcaption>Figure: Fibonacci Sequence with Recursion</figcaption>
        <br/>
    </center>
</figure>

While all loops could be rewritten as recursive functions, the reverse is not also true. Some data structures and algorithms &mdash; such as trees &mdash; require recursion to function.

# Closures

When nesting one function within another, the inner function is private to its outer (parent) function. It also forms a closure, which is a function whose environment has access to many variables. The environment is said to "close" the function.

Nested functions, or closures, can access anything that is within scope for itself, or its parent, including its parent's arguments and variables. In other words, the inner function contains the scope of the outer function.

<figure>
    <center>
        <img src="./functions_closure.png" alt="js functions closure" width="450"/>
        <figcaption>Figure: Closure</figcaption>
        <br/>
    </center>
</figure>

As variables passed into functions by reference are prone to errors due to side effects, so too are variables that are in scope within a closure. The same care needs to be taken when dealing with those variables and side effects should be avoided.

<figure>
    <center>
        <img src="./functions_context.png" alt="js functions context side effects" width="350"/>
        <figcaption>Figure: Side effects from function on closed variables</figcaption>
        <br/>
    </center>
</figure>
