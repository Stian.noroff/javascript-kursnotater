# Overview

Before we discuss frontend and JavaScript, we must first choose some appropriate tools:

* A development environment
* An editor
* The NodeJS toolchain
* A Node Package Manager

# Environment

The first choice to make is for your environment. This is a full-stack web application development programme where the purpose is to train you to create web services which are _served_ by _servers_. The vast majority of the servers that run the web (90%+) run on one of many available Linux distributions (Debian, CentOS, Red Hat, etc.); for this reason this course will be taught from a server's perspective.

For a developer, this means that whichever development environment (OS, langauge, frameworks, libraries, etc.) is chosen, it becomes the responsibility of the developer to ensure that their work product is tested in the way it is intended to be deployed:

* _**Linux**_ is the most simple. The cloud is native to linux servers and therefore this environment offers the least resistence to developing cloud solutions.
* _**MacOS**_ has few complications. MacOS is based on BSD which shares UNIX as a common ancenstor with Linux. This means that MacOS and Linux are similar enough to ignore the differences between the two (with a few exceptions).
* _**Windows**_ is considerably more complicated. Windows shares no comonality with UNIX which causes the largest resistence to developing services. Microsoft have responded to this by making web technologies that are Windows-native but these have never been as popular as the open-source web technologies that dominate the web. The JavaScript technology stack used for all frontend development was written for Linux and later ported to Windows, making support for Windows forever the "second thought".

For Windows users in particular, one must find a strategy to overcome this resistence to development. There are a few options for this:

1. _Rely on Software that is ported to Windows_. This is the most obvious option and it requires the least amount of effort up front. The NodeJS toolchain, VS Code, Docker and any library on NPM that does not involve native dependencies (and even some that do) will work natively for Windows. **Any problems arising from this choice can be dealt with individually and will be the sole responsibility of the candidate.**
2. _Virtual Machine_. Various options exist for virtualization which would enable one to have access to a Linux environment while on a Windows machine. The difficulty is that these environments are not integrated. **This option is not recommended for use in this course.**
3. _Docker_. This is somewhat a hybrid of options one and two where Docker for Windows is used to run Linux containers with your applications as you're developing them. Visual Studio Code has first-class support for this possibility and has guides for how to proceed. **This option will require some effort, experimentation and research**.
4. _Windows Subsystem for Linux_. Arguably the best of the three options, particularly for those Windows' users who don't wish to surrender their GUI lifestyle and can't afford a Mac. Microsoft are currently beta testing WSLv2, which runs a full Linux kernel alongside your Windows kernel and allows you to run Windows programs from the Linux side and vise versa. The technology is not stable and requires local administrator privilges to sign up for the Windows Insider Programme. **This option may not be possible and will require significant effort on your part.**

&nbsp;

> **Further Reading**
>
> For those who wish to attempt the Docker for Development route, you can continue reading about it [here](https://code.visualstudio.com/docs/remote/containers).

&nbsp;

> **Further Reading**
>
> For those who wish to attempt the WSLv2 route, you can continue reading about it [here](https://docs.microsoft.com/en-us/windows/wsl/wsl2-install). There will (eventually) be a separate guide on tools for Linux inside WSL.

# Editors

> If programming is a craft, then your text editor is your primary tool. Choosing a text editor is a highly personal thing and you should choose a good balance between automation (usually with plugins) and comfort. Once you have chosen a text editor &mdash; most people don't change them arbitrarily.

There are many different types of editors that do not simply scale from "basic" to "complex"; they're all complex in different ways. Different editors were created to serve different purposes and therefore hold different assumptions.

The first editors were very basic programs that enabled users to read and write ASCII bytes to a file &mdash; nothing more and nothing less. In UNIX the first editor of note was `ed` and was the equivalent of DOS' `edit.com` (not a website; `com` was a file extension long before it was a TLD) and Windows' `notepad.exe`.

As programmers made more complicated programs, they also made more complicated editors. In 1976 Richard Stallman and Bill Joy respectively created the two most famous "hackable" command-line text editors which are still in active use today: Emacs (`emacs`) and Vi (`vi`). These editors were created with two major assumptions that previous editors hadn't made: that they would be used primarily by programmers and that they should be extensible.

There are 40+ years of plugins for Emacs and Vi _that still work_ for every programming language, build system, VCS, and automation tool &mdash; and more still are being published today. These editors were the original _integrated development environments_ (IDEs). The main issue with these tools is that they are difficult to learn how to use.

> **Bonus**
>
> Github curates a [collection of open source text editors](https://github.com/collections/text-editors). It isn't recommended to choose something outside of this list but if you're interested in seeing what's available then take a look.

Every worthwhile text editor and IDE since has had similar properties to these originals; particularly the latest wave of GUI editors. These options are now discussed:

## [Sublime Text 3](https://www.sublimetext.com/)

A simple, hackable cross-platform text editors that pioneered a number of features present in many text editors today, including the package manager for editor plugins, multiple cursors, and the "code minimap".

```
Vintage: 2008
Plugin Language: Python
Maintenance: Privately Owned and Maintained (Current)
License: $80
```

## [Atom](https://atom.io/)

The original pioneer of text editors built on the [Electron](https://electronjs.org/) framework which enables native applications to be built using HTML5, CSS3, JavaScript and NodeJS. While not the first editor to be built using web technologies (see [Brackets](http://brackets.io/)), it is the one that started a trend.

```
Vintage: 2014
Plugin Language: HTML + CSS + JavaScript
Maintenance: Owned and Maintained by Github (Current)
Source: https://github.com/atom/atom
License: MIT
```

## [Visual Studio Code](https://code.visualstudio.com/)

Microsoft's official response to Atom and the best of the currently available GUI text editors. This editor brings many of the good points of Microsoft's experience in creating IDEs, such as "Intellisense" and Live Share, with none of the baggage of .Net and other Microsoft-specific technologies (unless explicitly installed as plugins). VS Code is also built using Electron and is therefore also fully cross-platform.

> **Important Note**
>
> For the reasons above, this editor is the chosen editor for this course. If you do not have a strong opinion on which editor to choose, it is recommended that you choose this one.

```
Vintage: 2015
Plugin Language: HTML + CSS + JavaScript
Maintenance: Owned and Maintained by Microsoft (Current)
Source: https://github.com/Microsoft/vscode/
License: MIT
```

## [Webstorm](https://www.jetbrains.com/webstorm/)

Webstorm is a true IDE built specifically for JavaScript and the web. Built using JetBrains' popular IntelliJ editor and framework, this IDE is a powerful tool for any kind of JavaScript/NodeJS development with explicit support for various frameworks. The main deterrent for using this IDE over a text editor is it is less intuitive and requires learning some of the concepts before becoming useful.

> **Important Note**
>
> JetBrains do offer licenses for education. Ask your lecturer if you are interested.

```
Vintage: 2010
Plugin Language: Java
Maintenance: Owned and Maintained by JetBrains (Current)
License: Proprietary (see website for details)
```

# Installing NodeJS

NodeJS is the runtime for JavaScript programs on the local machine, _outside_ of a browser user agent. JavaScript programs written for NodeJS have the same level of access to your computer as any program written in C, Java or C#.

> **Important Note**
>
> Unless you have a very good reason to do something else, you should always install the _latest_ version of NodeJS **that is marked LTS**. There's a reason it has a byline that reads: "Recommended For Most Users".

Each operating system has slightly different ways to install this toolchain:

## Package Managers (OS)

Linux, MacOS and Windows all have package managers that assist in maintaining software on these respective operating system. These package managers shouldn't be confused with `npm`, which maintains JavaScript dependencies specifically.

Linux has almost as many package managers as it has distros (`apt`, `yum`, etc.), MacOS has `brew` and Windows very recently joined its siblings with `choco`. For Linux users, the package manager is usually built into the OS itself and therefore can just be used. For MacOS and Windows users, [Brew](https://brew.sh/) and [Chocolatey](https://chocolatey.org/) need to be installed manually. Either way, package managers are the preferred way to maintain your installations.

> **Further Reading**
>
> Follow the instructions for your development environment over [here](https://nodejs.org/en/download/package-manager/) to install NodeJS.

## Precompiled Binaries

The next simplest way to use NodeJS is to install it using provided, precompiled binaries and installers. For Windows users, this is probably the most "natural" way to install NodeJS but provides no automation in the maintenance thereof.

This option is only available to MacOS and Windows users; Linux users are assumed to either compile from source, or use the appropriate package manager.

## Compile From Source

This is the ultimate form of control and customisation for the Linux power user and is therefore not covered in these notes. RTFM.

> **Testing the Installation**
>
> Once you've installed NodeJS, you can check if it works correctly by running `node -v` in a terminal. If the command responds with the current version of NodeJS then you have installed it correctly.

# Node Projects

NodeJS libraries are arranged into **packages** which are published onto a **package registry**. Developers using those libraries can add them to their projects using a **package manager**, which records the dependency in the project's **package manifest** as well as downloads and installs it into the `node_modules` folder at the root of the project.

## Package Managers (NodeJS)

Once you have successfully installed NodeJS, you have also already installed the default NodeJS package manager: `npm`. There are other equivalent package managers available, such as [Yarn](https://yarnpkg.com/lang/en/) (`yarn`), that are featured in many tutorials and promise many improvements over the default. For the purpose of this course we will be using NPM.

> **Testing the Installation**
>
> Once you've installed NodeJS, you can check if NPM works correctly by running `npm -v` in a terminal. If the command responds with the current version of NPM then you are ready to proceed.

## Package Registries

The default package registry that houses the most packages is [NPM](https://www.npmjs.com/) (which is a service, separate from the CLI tool). NPM is free to publish open source JavaScript packages that are publicly available. Most of the tools, frameworks and libraries we will use in this course are available on this registry.

Recently, many services are releasing their own package registries, and for more than just NodeJS packages. Gitlab and Github each have their own offerings, and there are many other open-source alternatives that can be deployed independently. It is unlikely we will be using or publishing proprietary code, so it is unlikely that we will be using a different package manager.

## Project Initialisation and Package Manifest

When starting a new project with NodeJS (frontend or backend), the first thing to do is to initialize the package to track metadata:

```shell
cd my-project-folder
npm init
```

This will prompt the user to answer some questions about the package, some of which will have default answers that will be filled in if the question is left unanswered. The questions are as follows:

```
package name: (folder-name) package-name
version: (1.0.0)
description: This is where the package description goes
entry point: src/index.js
test command:
git repository:
keywords: some space separated key words
author: Noroff Accelerate AS
license: (ISC) MIT
```

Some of the questions will try predict what your response will be and include those values in parentheses as default values; if you leave those questions unanswered, then they will assume the default answer. Questions without default values will be omitted if left unanswered.

> **Important**
>
> As discussed in many other places throughout the notes for the various courses in this programme, the contents of the package manifest must be semantic. This document is not for you, dear developer, it is for those that come after you. Semantic names, descriptions, keywords (nothing too fancy), version (see [Semver](https://semver.org/)), license information and the inclusion of relevant scripts are all _**MANDATORY**_.

The above initialization will produce the following `package.json` file &mdash; the **package manifest** &mdash; for the project:

```json
{
  "name": "package-name",
  "version": "1.0.0",
  "description": "This is where the package description goes",
  "main": "src/index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [
    "some",
    "space",
    "separated",
    "key",
    "words"
  ],
  "author": "Noroff Accelerate AS",
  "license": "MIT"
}
```

This file, along with its companion `package-lock.json` (don't worry if it hasn't appeared yet), are both important files that should be committed to your git repository as a pair.

> **Commit Messages**
>
> When creating the repository, the initial package manifest, scripts and dependencies would _normally_ be committed along with the first commit to the repository: `feat: initial commit`. If for some reason it was not and you are committing it separately, then your commit message should be something along the lines of `feat(npm): initial package manifest`. Changes made to the package manifest later will have different commit messages describing the changes made.

## Installing Dependencies

Once there exists a package manifest, we can begin to include code libraries in our application and storing that information under the `dependencies` key in our package manifest file.

When someone clones your repository from git for the first time; they will not automatically have all of the dependencies that are required to run your code. Our repository should only contain the code that _we_ have written and not code that we're using from others. For this reason, we must track our dependencies in the package manifest so the _exact_ same dependencies (same or compatible versions, etc.) are downloaded when setting up the project from scratch.

```shell
npm install
npm i                            # shorthand
```

New dependencies can be installed and added to the package manifest using the following commands:

```shell
npm install <package>            # install the latest available version
npm install <package> --save     # save the installed package to the manifest
npm install <package>@<version>  # optionally install a specific version
npm i <package>@<version>        # shorthand
```

> **Further Reading**
>
> Dependencies can be specified in many ways, including a range of version, git repositories, git tags and local paths. See the [NPM Documentation](https://docs.npmjs.com/files/package.json#dependencies) for more info.

This will produce a `package.json` simiar to the following:

```json
{
  ...

  "dependencies": {
    "express" : "^4.16.3"
  },

  ...
}
```

Once the dependency is installed and registered in the manifest, it can be safely used in your code and committed to version control.

> **Commit Messages**
>
> Changes to the dependencies of a project usually has a message like `chore(npm): update dependencies` or `chore(npm): add express`. Some developers also use `deps` as a special scope for this purpose: `chore(deps): add express`.

## Node Modules and Package Lock

When installing dependencies, the package manager generates two artifacts: the `node_modules` folder and the `package-lock.json` file.

The package lock file contains the exact version information for all packages that are installed at that time. Once one is generated, it _**should**_ be committed to version control, as there are automated services that track these files and alert package maintainers about bugs, updates and possible threats to security.

When dependencies are downloaded, they are installed into the `node_modules` folder at the root of the project. While the folder is necessary to run the code, it should be ignored by version control as accidentally including it in a repository will effectively render the repository unusuable. At any time, one should be able to delete the folder and rebuild it using `npm i`.

> **Debugging**
>
> If one of your dependencies is giving you trouble, its worth trying to delete the `node_modules` folder and rebuilding it using `npm i`. Don't forget to rebuild your project afterward if you need to.

## Developer Dependencies

Some dependencies that we install may not be of code that we use inside our applications, but are rather code that we use to help maintain our applications. One example of this is unit testing frameworks like `mocha`.

These can be installed in a similar way to normal dependencies:

```shell
npm install <package> --save-dev
```

This will produce a package manifest that looks like this:

```json
{
  ...

  "devDependencies": {
    "mocha" : "^4.16.3"
  },

  ...
}
```

> **Further Reading**
>
> [NPM Documentation](https://docs.npmjs.com/files/package.json#devdependencies)

## CLI Utilities and Global Installation

Some other kinds of development dependencies declare external scripts that can be used. For example, `mocha` declares a script that is used to run unit tests of a specific format.

One way to run these kinds of scripts is to install the packages globally, which adds the script in question to the OS path, which enables them to be executed on the command line. Packages are installed globally like this:

```shell
npm i -g mocha
mocha -w ./test/
```

Generally speaking, installing scripts globally is considered to be an anti-pattern and is not recommended. Instead, it is recommended these packages be installed as a development dependenency so that they are also available on zero-configuration environments such as Docker. To run a script that is installed as a development dependency, we can use the `npx` utility:

```shell
npx mocha -w ./test/
```

This will run the script that is installed in the `node_modules` folder of the project, which is under the direct control of the developer.

> **Aside**
>
> All `devDependencies` are also installed when running `npm i`. In production environments, `devDependencies` can be explicitly ignored with `npm i --production`. More details are available in the [NPM Documentation](https://docs.npmjs.com/cli/install).

## Scripts

The `scripts` key of the package manifest allows us to define scripts for running our application. These scripts serve two purposes:

1. When we publish our application to our git repository, we provide predictable shortcuts to running our application for others.
2. When we deploy our application, most deployment configurations for NodeJS automatically run the scripts that we define here.

Consider the following `package.json`:

```json
{
  ...

  "scripts": {
    "start" "node src/index.js",
    "hello": "echo \"Hello, World!\"",
    "test": "echo \"Error: no test specified\" && exit 1"
  },

  ...
}
```

The `start` and `test` scripts defined here are special in that they have dedicated commands that run them with the npm CLI tool. Other scripts that are defined that do not have a specific interpretation by the NPM CLI tool must be run using the `npm run` command.

```shell
npm start         # run the start script
npm test          # run the test script
npm run hello     # run the hello script
```

> **Note**
>
> Scripts defined in `package.json` do not need to use the `npx` prefix for scripts defined as `devDependencies` for that package.

## Scripted Events

Aside from `start` and `test`, there are some other semantic script names that are run automatically. Consider the following `package.json`:

```json
{
  ...

  "scripts": {
    "start" "serve ./public",
    "build": "make ./public",
    "postinstall": "npm run build",
    "test": "echo \"Error: no test specified\" && exit 1"
  },

  ...
}
```

In this instance, we've defined the `postinstall` script, which will run automatically whenever we successfully complete a `npm install`. This is useful for managing how the application is run in an automated environment such as docker and there are many similar scripts; the full list can be found [here](https://docs.npmjs.com/misc/scripts).

> **Commit Messages**
>
> When editing the scripts in a package manifest, this should generally be committed separately to managing dependencies, package versions and other package metadata. When updating scripts, this could be done with a message like `chore(npm): build script`.

## Package Health

The contents of the package that are published to your git repository should be able to run without excessive configuration or effort. Generally the following should be sufficient:

```shell
git clone <package>
cd <package>
npm install
npm start
```

If any more than the above is required before the application will run (i.e. configuration, setting up a new database, etc.) then the exact steps and process must be described in the repository `README.md`.
