# Hello JavaScript

For the frontend, your JavaScript code is downloaded inside a browser (which is a type of _User Agent_) and runs on your own computer. As this is a _frontend_ JavaScript course, the bulk of this course takes place on the client-side and should not be confused with server-side JavaScript that is run with NodeJS.

The remainder of this chapter will focus on the actual JavaScript language -- which is mostly independent of client-side or server-side JavaScript and can be used isomorphically in both environments.

JavaScript is an interpreted language, which means that code can be directly interpreted and run. NodeJS provides a convenient REPL (Read&mdash;Evaluate&mdash;Print Loop) which may be used to test basic JavaScript statements. Alternatively, one can run the contents of a JavaScript file (`index.js`) without a REPL. These can be achieved by invoking the following commands respectively:

```shell
# REPL
node

# Run a file
node ./path/to/index.js
```

Basic JavaScript can also be tested in the console window of the developer tools in any of the popular browsers.

# Hello, World!

Traditionally the first program that one should write when learning a new language is one that simply prints the string: `hello, world!\n`. In JavaScript this can be done with a single line of code:

<figure>
    <center>
        <img src="./hello_world.png" alt="js hello world" width="450"/>
        <figcaption>Figure: Hello, JavaScript!</figcaption>
        <br/>
    </center>
</figure>

> **Activity**
>
> Try modifying this program to print out a greeting for yourself; for example: `hello, alice!\n`.
