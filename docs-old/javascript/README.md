# JavaScript

> In this module the candidate will learn the basics of JavaScript, the module provides the necessary context for working with web technologies and web development.
>
> This module will focus on getting the candidates familiar with the JavaScript language, it will have simple tasks that require the candidates to make use of different variable types and a variety of control structures.
>
> By the end of the week the candidates will be able to create simple solutions from the ground up and deploy them to hosted web services.

## Lessons

1. [**Setup**](./setup/README.md)

&nbsp;

2. [**What is JavaScript?**](./what-is-javascript/README.md)

&nbsp;

3. [**Hello, JavaScript!**](./hello-world/README.md)

&nbsp;

4. [**Variables**](./variables/README.md)

&nbsp;

5. [**Operators**](./operators/README.md)

&nbsp;

6. [**Conditionals**](./conditionals/README.md)

&nbsp;

7. [**Flow Control (Loops)**](./flow-control/README.md)

&nbsp;

8. [**Error Handling**](./errors/README.md)

&nbsp;

9. [**Functions**](./functions/README.md)

&nbsp;

10. [**String Templates**](./string-template-literal/README.md)

&nbsp;

11. [**Collections**](./collections/README.md)

&nbsp;

12. [**Classes**](./classes/README.md)

&nbsp;

13. [**Modules**](./modules/README.md)

&nbsp;

14. [**Future Values (Async)**](./future-values/README.md)

&nbsp;
