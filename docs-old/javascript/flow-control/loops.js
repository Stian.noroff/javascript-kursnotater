const threshold = 20

for (let counter = 0; counter < threshold; counter++) {
    console.log(`Counter: ${counter}`)
}

while (condition) {
    // do something
}

do {
    // do something
} while (condition)

const collection = ['foo', 'bar', 'baz']

for (let item of collection) {
    console.log(item)
}
// => 'foo', 'bar', 'baz'
// (newlines omitted for legibility)

const collection = {
    foo: 'bar',
    baz: 'qux'
}

for (let key in collection) {
    console.log(key)
}
// => 'foo', 'baz'
// (newlines omitted for legibility)