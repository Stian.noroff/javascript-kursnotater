# Flow Control

With variables and conditionals, you have everything you need to create programs in JavaScript... although doing so without the ability to "loop" over code would be very tedious. Loops allow us to abstract our code &mdash; which means to define code in terms of variables &mdash; and run the same operations over a range of values to produce a range of results.

> **Further Reading**
>
> [Loops and Iteration (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration)

JavaScript has a number of options for looping over collections. In addtion to these mentioned, there are also various higher order functions and asynchronous loops which are discussed separately in a [later chapter](#) (todo fix link). The following loops are available in JavaScript:

1. `for` loops
2. `while` loops
3. `do ... while` loops
4. `for ... in` loops
5. `for ... of` loops

# `for` Loop

A for-loop defines a block of code that repeats until the specified condition is falsy. The definition of a for-loop takes the following form:

```javascript
const threshold = 20

//   initialisation   conditional          increment
for (let counter = 0; counter < threshold; counter++) {
  // body
  console.log(`Counter: ${counter}`)
}
```

The `initialisation` statement runs once at the start of the loop; usually to initialize some kind of loop state. The `conditional` runs at the start of every iteration and if it evaluates as "truthy" then the body of the loop will be executed. The `increment` statement runs after the contents of the loop body have been run but before the `conditional` for every run of the loop.

<figure>
    <center>
        <img src="./LoopFlowCharts_for.png" alt="js for loop" width="450"/>
        <figcaption>Figure: For-Loop</figcaption>
        <br/>
    </center>
</figure>

The above for-loop produces the following output:

```
0
1
2
.
.
19
```

# `while` Loop

A while-loop defines a block of code that repeats until the specified condition is falsy. It differs from the for-loop in that it doesn't maintain a counter variable and instead leaves that management up to the programmer. The definition of a while-loop takes the following form:

```javascript
// initialisation
let counter = 0
const threshold = 20

// conditional
while (counter < threshold) {
  // body
  console.log(`Counter: ${counter}`)
  // increment is part of the body
  counter++
}
```

The conditional is evaluated at the start of each iteration. If the conditional becomes "falsy", the loop will stop executing when it next reaches the conditional statement and will pass control to the statement immediately following the loop.

<figure>
    <center>
        <img src="./LoopFlowCharts_while.png" alt="js while loop" width="450"/>
        <figcaption>Figure: While-Loop</figcaption>
        <br/>
    </center>
</figure>

This while-loop produces the same output as the [for-loop](#for-loop) before.

# `do ... while` Loop

A do-while-loop functions similarly to the [while loop](#while-loop) with the exception that the evaluation of the conditional happens after the evaluation of the body. This guarantees that the body will always run at least once before the condition is checked. The definition of a do-while-loop takes the following form:

```javascript
let counter
const threshold = 20

do {
  // body
  // initialisation could be part of the body
  if (!counter) {
    counter = 0
  }

  console.log(`Counter: ${counter}`)

  // increment
  counter++

  // conditional
} while (counter < threshold)
```

<figure>
    <center>
        <img src="./LoopFlowCharts_do_while.png" alt="js do while loop" width="450"/>
        <figcaption>Figure: Do-While-Loop</figcaption>
        <br/>
    </center>
</figure>

The above do-while-loop produces the same output as the [for-loop](#for-loop) before.

# `for ... of` Loop

A for-of-loop is a special kind of for-loop that consumes an [iterator](#iterators) (todo fix link), or an object that implement the iterable hook; i.e. an array. The definition of a for-in-loop takes the following form:

```javascript
const arr = ['foo', 'bar', 'baz', 'qux']

for (let value of arr) {
  console.log(value)
}
```

<figure>
    <center>
        <img src="./LoopFlowCharts_for_of.png" alt="js for of loop" width="450"/>
        <figcaption>Figure: For-Of-Loop</figcaption>
        <br/>
    </center>
</figure>

The above for-of-loop produces the following output:

```
foo
bar
baz
qux
```

# `for ... in` Loop

A for-in-loop is a special kind of iterator-based loop that iterates over the enumerable keys of an object. The definition of a for-in-loop takes the following form:

```javascript
const obj = {
  foo: 'bar',
  baz: 'qux',
}

for (let key in obj) {
  // body
  console.log(`${key}: ${obj[key]}`)
}
```

This kind of loop is equivalent to producing an array of the enumerable keys of an object using `Object.keys(obj)` and then running a [for-of-loop](#for--of-loop) over those values. The above for-in-loop produces the following output:

```
foo: bar
baz: qux
```

> **Caveat**
>
> It may be tempting to use a for-in-loop to iterate over an array's contents but, as discussed with the [`in` Operator](../conditionals/README.md#in-operator), this may lead to undefined behaviour. Using a for-in-loop for this purpose may iterate over the contents of the array, but it may also iterate over properties that have been assigned to the array. You should use the [for-of-loop](#for--of-loop) for this purpose instead.
