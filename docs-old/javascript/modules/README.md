# Modules

Early use of JavaScript involved defining distinct behaviours for different parts of an HTML web page in a flat structure with little to no interaction between the behaviours. Since then, JavaScript applications have grown large, and in the case of NodeJS, have left the browser environment entirely. With JavaScript applications becoming more complicated, methods to split the code into more consumable parts became necessary, which has eventually led to JavaScript modules.

> **Caveat**
>
> Although most browsers support `import` and `export` natively, it is still recommended that code be transpiled and minified using bundling tools such as Webpack and Babel. The effect of using these is that `import` and `export` statements work as intended but are removed at build-time and project dependencies are managed and bundled automatically.
>
> Additionally, while NodeJS supports the latest in the EcmaScript standards, they have opted to keep their `require` and `module.exports` syntax for their module management. Without aids such as Webpack and Babel, `import` and `export` syntax will not work as intended in NodeJS.
>
> The vast majority of the time, these concerns can be ignored as React, Vue and Angular all provide tools that handle this complexity on your behalf.

&nbsp;

> **Further Reading**
>
> [Modules (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules)

&nbsp;

> **Resource**
>
> [Webpack Starter Kits](https://webpack.js.org/starter-kits/)

# Exports

Before a module can be imported, it must first be exported.

Modules can export all kinds of variables, functions and classes at the top level of the file. This is done using an `export` statement. Consider the following example:

```javascript
export const foo = 'bar'
export function draw(ctx, length, x, y, colour) {
  // ...
}
```

You can also export many things at once by wrapping the export in a block.

```javascript
export { name, draw, reportArea, reportPerimeter }
```

## `default` Export

Every module can have a single export that becomes the default import. This is done using the `default` keyword with your export:

```javascript
export default class Foo {
  // ...
}
```

# Imports

Importing modules in JavaScript is similar to exporting them. The simplest import statement is to import the default export:

```javascript
import Foo from './foo'
```

This will import whatever is the default export from `foo.js` and alias it as the variable `Foo`. The same can also be achieved using the block import syntax:

```javascript
import { default as Foo } from './foo'
```

This can also be combined with multiple individual exports:

```javascript
import { name, draw, reportArea, reportPerimeter } from './square'
```

Lastly, the first option for importing the default export and aliasing it to a variable, can also be combined with the block import:

```javascript
import Foo, { Bar, Baz, Qux } from './foo'
```

Imports can also be done without assigning the result to a variable. This causes the code in the module to be run without any further effects.

```javascript
import './foo'
```

## Dynamic Module Loading

Sometimes loading modules immediately is undesirable for many reasons, including performance, convenience and resource management. For cases such as these we can use the `import()` function that will only run when the line is executed and will return a [Promise](../future-values/README.md#promises) for the module.

```javascript
import('./foo/').then(module => {
  // Do something with the module.
})
```

## Circular Dependency Caveat

When importing dependencies, importing two modules that import each other will cause an error to be thrown due a circular depencency requirement.

```javascript
// foo.js
import Bar from './bar'
export default class Foo {}

// bar.js
import Foo from './foo'
export default class Bar {}

// index.js
import Foo from './foo'
// throws an error due to a circular dependency.
```

In cases like this one can use [dynamic module loading](#dynamic-module-loading) to load the circular dependency only when the import statement itself is executed, thus resolving the circular dependency.

```javascript
// foo.js
import Bar from './bar'

export default class Foo {
  static doSomething() {
    Bar.doSomething()
  }

  static done() {
    console.log('done')
  }
}

// bar.js
export default class Bar {
  static doSomething() {
    import('./foo').then(Foo => Foo.done())
  }
}

// index.js
import Foo from './foo'
Foo.doSomething()
// => 'done'
```

# Module Aliasing

Any import or export can be aliased at the time they are imported or exported using the `as` keyword. Consider the following example:

```javascript
class Foo {}
class Bar {}

export Foo as AnotherFoo
export {
    Bar as AnotherBar,
}

import { default as SomeClass } from 'my_module'
```

Module aliasing can also use wildcards where many unrelated items are exported from the same module. Consider the following example:

```javascript
// foobar.js
export function foo() {
  return 'foo'
}
export function bar() {
  return 'bar'
}

// index.js
import * as FooBar from './foobar'

console.log(FooBar.foo())
// => 'foo'

console.log(FooBar.bar())
// => 'bar'
```

# Re-exporting Modules

It is also possible to re-export imported modules in a single statement. This is useful for providing a common interface for libraries and modular sets of code. Consider the following examples:

```javascript
export { Lists as List, ListItem } from './components/Lists/StaticLists'
export { default as Foo } from './foo'
export { default as Bar } from './bar'
```
