'use strict'

// Ways to declare variables
const foo = 42.00000001
const bar = true
const obj = { foo: 'bar' }
let baz = null
let qux = ['foo', { bar: false, baz: 'qux' }, undefined, NaN]
let not_defined

// Thou shalt not ever use `var` (but this is not incorrect).
var no = ['nee', 'ei', 'non', 'nein', 'nei', 'nei', 'nei']