# Variables

Most programs need to store and manipulate program state in memory before they are able to do anything useful. This is not difficult in JavaScript.

# Variable Scope

In a browser environment, when a variable is declared outside any function or block, it is assigned to the root scope, which is also known as the _global_ scope, and is therefore "within scope" for any other scope within the program. In Node, parts of a JavaScript program are separated by "module" (files) and variables are exposed and imported into other modules explicitly &mdash; but within a single module the same logic applies.

Prior to ES6, there was only a single scope, where all variables declared were global and available to all other parts of the application. Afterward, with the introduction of the `let` and `const` variable declarations, block scopes enable us to separate our application state and reuse variable names without fear of interfering with other parts of the application. A block in JavaScript is defined using the `{` and `}` symbols and new blocks may be used almost anywhere.

> **Note**
>
> Many places where `{}` are used do not automatically result in the creation of a new scope. [Closures](../functions/README.md#closures) (todo fix link) in particular are noted to not produce a new scope but a [Function](../functions/README.md), declared with the `function` keyword, does.

&nbsp;

> **Further Reading**
>
> [Grammar and Types (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Grammar_and_Types)

# Variable Declaration

As of ES6, JavaScript defines three keywords for defining variables:

1. `const` &mdash; Defines a **block scoped** _constant_ variable which can only be assigned to once. Most variables should be declared in this way.

2. `let` &mdash; Defines a **block scoped** variable which may be updated normally. Only varibles that require updates or changes should be declared in this way.

3. `var` &mdash; Defines a **globally scoped** variable whose resources are never reclaimed. Variables should _**never**_ be declared in this way.

<figure>
    <center>
        <img src="./variable_declarations.png" alt="js variable declarations" width="650"/>
        <figcaption>Figure: Variable Declarations</figcaption>
        <br/>
    </center>
</figure>

There are two ways in which these types of variables differ from eachother: their permanence and scoping.

A `const` or `let` variable is block scoped, meaning that they can only be accessed within its current block, or blocks within the current block. A `var` is assigned to the root scope, no matter where it is declared, and so is globally accessible. The concept of scoping is further discussed in [a later section](../functions/README.md).

A `const` is a constant variable, meaning that it cannot be changed or updated. Constant variables are preferable to those declared with `let` because they only invoke JavaScript's type inferencing system once when the variable is declared, thereafter they store the value and reuse it as necessary. Variables that update, such as `var` and `let`, cannot have thier types predicted and must therefore infer them every time they are accessed. The concept of type inference is discused in the next section.

# Variable Type

JavaScript variables have a very simple type system; a variable can be one of the following types:

- `String` &mdash; Strings. In JavaScript strings can be defined using single quotataion marks (`'`) or double quotation marks (`"`), as long as the same type are used to start a string or finish a string. It is recommended to be consistent in your code and most editors support standardising these automatically.

  > **Further Reading**
  >
  > Strings have many interesting functions and operations in their [API](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String) that can be used to manipulate them.

- `Number` &mdash; Numbers. This type accounts for both integers and floating point values. The largest integer value JavaScript supports is `2^53 - 1 => 9007199254740991`; this value is also accessible using `Number.MAX_SAFE_INTEGER`. Support for larger, arbitrary precision integers was recently added with the `BigInt` data type.

  > **Further Reading**
  >
  > Numbers have many interesting functions and operations in their [API](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number) that can be used to manipulate them.

- `BigInt` &mdash; Very big numbers. This type was recently added to support larger, arbitrary precision integers. As the support was recently added (ES 2020), the stability of this type cannot be guaranteed and so should be ignored (for now).

- `Boolean` &mdash; True or False.

- `Object` &mdash; A Key-Value mapping where each key is a string and each value has its own type. Just about everything in JavaScript (aside from the primitives mentioned above) is extended from an Object but have their own native implementations; one such example of this is Arrays.

  > **Further Reading**
  >
  > Objects have many interesting functions and operations in their [API](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object) that can be used to manipulate them. Arrays, having their own native implementation, have a separate [API](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array), with thier own set of operations.

- `Function` &mdash; A unit of execution and the only low-level form of abstraction that JavaScript actually provides (even in ES6, classes etc. are "just functions" with clearner syntax). Functions are a special extension of the Object type, but unlike Arrays, they have their own type designation. Functions are discussed extensively in a [later section](../functions/README.md).

- `Symbol` &mdash; While JavaScript doesn't have any member access scope controls like C# or Java, it does have the ability to create symbols which can act as keys for an Object. The symbols are effectively random values that can't be guessed and instead have to be referenced. This is how JavaScript protects many of their features internally. For all but the most advanced use cases, this type can be safely ignored.

- `undefined` &mdash; Undefined is both a special value, like `null`, and its own type. It is the default value for all variables before they are assigned and should not be confused with `null`. This will be discussed further in a [later section](../conditionals/README.md).
