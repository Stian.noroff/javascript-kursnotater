class Rectangle {
  constructor(height, width) {
    this.height = height
    this.width = width
  }
}

function Rectangle(height, width) {
  this.height = height
  this.width = width
}

console.log(new Rectangle(4, 4))
// => Rectangle { height: 4, width: 4 }

class Rectangle {
  // constructor...

  get area() {
    return this.calcArea()
  }

  calcArea() {
    return this.height * this.width
  }
}

const square = new Rectangle(10, 10)

// `area` is a getter and appears as a property
console.log(square.area)
// => 100

// Assuming Rectangle was declared with class syntax
const rect = new Rectangle(4, 4)

console.log(rect.constructor.name)
// => 'Rectangle'

console.log(rect.constructor === Rectangle)
// => true

const foo = {
  get bar() {
    return 'bar'
  }

  baz() {
    return 'baz'
  }
}

console.log(foo.bar)
// => 'bar'

console.log(foo.baz())
// => 'baz'

class Point {
  constructor(x, y) {
    this.x = x
    this.y = y
  }

  static distance(a, b) {
    const dx = a.x - b.x
    const dy = a.y - b.y
    return Math.hypot(dx, dy)
  }
}

const p1 = new Point(5, 5)
const p2 = new Point(10, 10)

console.log(p1.distance)
// => undefined
// `distance` does not exist on the class instance

console.log(Point.distance(p1, p2))
// => 7.07...

const foo = new Foo()
foo.constructor.myStaticMethod()

class Point {
  // ... as above

  distanceTo(other) {
    return this.constructor.distance(this, other)
  }
}

// ... as above
console.log(p1.distanceTo(p2))
// => 7.07...

function Employee(name, dept) {
  this.name = name
  this.dept = dept
}

Employee.prototype.printName = function () {
  console.log(this.name)
}

function Manager(name, dept, reports) {
  Employee.call(this, name, dept)
  this.reports = reports
}

Manager.prototype = Object.create(Employee.prototype)
Manager.prototype.constructor = Manager

const manager = new Manager('alice', 'it', ['foo', 'bar'])

console.log(manager.printName())
// => 'alice'

class Employee {
  constructor(name, dept) {
    this.name = name
    this.dept = dept
  }

  printName() {
    console.log(this.name)
  }
}

class Manager extends Employee {
  constructor(name, dept, reports) {
    super(name, dept)
    this.reports = reports
  }
}

class Foo {
  static foo() {
    return 'foo'
  }
}

class Bar extends Foo {
  static foobar() {
    return `${super.foo()} bar`
  }
}

console.log(Bar.foobar())
// => 'foo bar'