# Classes

JavaScript introduced the class syntax as of ES6, however the addition is primarily a wrapper around JavaScripts existing inheritence structure as of ES5. All classes are in fact functions, and can be declared with class declarations, or anonymously with class expressions. The latter syntax is similar to [function expressions](../functions/README.md#function-expressions) and will not be explicitly discussed.

> **Further Reading**
>
> [Classes Reference (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes) and [JavaScript Object-Oriented Programming Guide (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Details_of_the_Object_Model#Class-based_vs._prototype-based_languages)

Class declarations are done using the `class` keyword. Consider the following simple example:

```javascript
class Rectangle {
  constructor(height, width) {
    this.height = height
    this.width = width
  }
}
```

This class declaration is the same as the following function declaration:

```javascript
function Rectangle(height, width) {
  this.height = height
  this.width = width
}
```

Both of these may be invoked in the same way:

```javascript
const rect = new Rectangle(4, 4)
```

> **Caveat**
>
> Unlike function declarations, class declarations like the one above are _not_ [hoisted](../functions/README.md#function-hoisting). Trying to invoke a class before it is declared will result in a `ReferenceError`.

# `constructor` Method

As shown in the example above, the `constructor` method is a special method that defines the initialization behaviour of the JavaScript object that is created from the class using the [`new` operator](../functions/README.md#new-operator). This behaviour corresponds directly to the body of a function declaration and not to any auxilliary behaviours attached to a function as other properties on the function object.

Unlike the case with a function declaration, the class declaration and constructor syntax also results in the creation of a `constructor` property on the resulting object. This property is a useful way to be able to refer back to the class itself from within an instance of the class. Consider the following example:

```javascript
// Assuming Rectangle was declared with class syntax
const rect = new Rectangle(4, 4)

console.log(rect.constructor.name)
// => 'Rectangle'

console.log(rect.constructor === Rectangle)
// => true
```

# Prototype Methods

The primary benefit of the class syntax is that it provides a much cleaner, more legible way to declare additional behaviours for the class and instances created from the class when compared to ES5. This is done as follows:

```javascript
class Rectangle {
  constructor(height, width) {
    this.height = height
    this.width = width
  }

  get area() {
    return this.calcArea()
  }

  calcArea() {
    return this.height * this.width
  }
}

const square = new Rectangle(10, 10)

// `area` is a getter and appears as a property
console.log(square.area)
// => 100
```

> **Further Reading**
>
> The method definition syntax is varied and corresponds closely to the [function definition](../functions/README.md) and [collections](../collections/README.md) syntax from a previous chapter. This includes typical properties, generators, getters and setters, computed keys and asynchronous functions. For the full specification of method definition, see the [Method Definitions (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Method_definitions) reference.

Methods that do not make use of the prototype chain can also also be defined using the same syntax for individual objects. Consider the following example:

```javascript
const foo = {
    get bar () {
        return 'bar'
    }

    baz () {
        return 'baz'
    }
}

console.log(foo.bar)
// => 'bar'

console.log(foo.baz())
// => 'baz'
```

# Static Methods

The `static` keyword may be used to define methods for the class itself that are not directly accessible on instances of the class. Consider the following example:

```javascript
class Point {
  constructor(x, y) {
    this.x = x
    this.y = y
  }

  static distance(a, b) {
    const dx = a.x - b.x
    const dy = a.y - b.y
    return Math.hypot(dx, dy)
  }
}

const p1 = new Point(5, 5)
const p2 = new Point(10, 10)

console.log(p1.distance)
// => undefined
// `distance` does not exist on the class instance

console.log(Point.distance(p1, p2))
// => 7.07...
```

If one did want to access a static method of a class; this could be done through the `constructor` property of the instance:

```javascript
const foo = new Foo()
foo.constructor.myStaticMethod()
```

This can be used to provide static behaviour to instances and avoid duplication of code:

```javascript
class Point {
  // ... as above

  distanceTo(other) {
    return this.constructor.distance(this, other)
  }
}

// ... as above
console.log(p1.distanceTo(p2))
// => 7.07...
```

# Inheritence

Inheritence is the process of transferring and extending the behaviour of one class with another. Unlike in class-based langauges such as C#, C++ and Java, JavaScript achieves this using prototypes. While prototypes are less explicit in their inheritence mechanism, they have the benefit of having classes and their instances being derived from the same type. Any JavaScript object has the ability to be the prototype for another, and thereby share its properties and methods.

> **Further Reading**
>
> [Inheritence and Prototype Chain (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain)

## Manipulating the Prototype

Inheritence in ES5 is achieved as follows:

1. A constructor function `Employee` is declared and the properties `name` and `dept` are defined. It also has some behaviour to print it's own name.

   ```javascript
   function Employee(name, dept) {
     this.name = name
     this.dept = dept
   }

   Employee.prototype.printName = function() {
     console.log(this.name)
   }
   ```

2. Next, the `Manager` constructor function is declared, which calls the `Employee` constructor and specifies the `reports` property.

   ```javascript
   function Manager(name, dept, reports) {
     Employee.call(this, name, dept)
     this.reports = reports
   }
   ```

3. Lastly, a new object is created &mdash; a clone of `Employee`'s prototype &mdash; and assigned to `Manager.prototype`.

   ```javascript
   Manager.prototype = Object.create(Employee.prototype)
   Manager.prototype.constructor = Manager
   ```

4. `Manager` now behaves as an `Employee` does, with some extended behaviour.

   ```javascript
   const manager = new Manager('alice', 'it', ['foo', 'bar'])

   console.log(manager.printName())
   // => 'alice'
   ```

## `extends`

Manually manipulating the prototype chain, as one would have to do in ES5, is a tedious process that is prone to error. Luckily, ES6 has provided a much easier alternative that was introduced along with the class syntax: the `extends` keyword. All of the detail mentioned above can is handled within this simple example:

```javascript
class Employee {
  constructor(name, dept) {
    this.name = name
    this.dept = dept
  }

  printName() {
    console.log(this.name)
  }
}

class Manager extends Employee {
  constructor(name, dept, reports) {
    super(name, dept)
    this.reports = reports
  }
}
```

This produces the exact same example as before with the added benefit of being much easier to read.

## `super` Operator

The `super` keyword is used as a reference to an instance's class, or a class' parent. This can be used inside methods of a class to refer to the class itself and therefore to information that is static on the class, such as static class methods. Consider the following example:

```javascript
class Foo {
  static foo() {
    return 'foo'
  }
}

class Bar extends Foo {
  static foobar() {
    return `${super.foo()} bar`
  }
}

console.log(Bar.foobar())
// => 'foo bar'
```

In the example above, `super` is used to refer to the parent class, `Foo`, and then to the static method `foo` defined for that class. In the previous example, `super` is used by the constructor of `Manager` to call the constructor of its parent class, `Employee`.

> **Further Reading**
>
> [`super` Reference (MDN)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/super)
