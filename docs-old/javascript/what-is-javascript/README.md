# What is JavaScript?

On a technical level, JavaScript is a scripting language, or an [_interpreted programming language_](#interpreted-vs-compiled-code), that was designed to implement complex logic on the web. It was originally created by Brendan Eich in 1995 for the Netscape web browser, which eventually became the foundation for the Mozilla Foundation and thier browser, Mozilla Firefox.

> **Further Reading**
> 
> [MDN: What is JavaScript?](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/What_is_JavaScript)

Since its creation, JavaScript has evolved to permeate all aspects of the web and become its [_lingua franca_](https://en.wikipedia.org/wiki/Lingua_franca). For at least the last twenty years, even the simplest public websites have made use of JavaScript to create dynamically updating content, control media and animate images, long before JavaScript was adapted for other uses. Outside of browsers, JavaScript increasingly being used to build web services, command-line interface (CLI) tools, and other server-side applications via NodeJS. JavaScript has also been used as a scripting language for applications unrelated to the web such as in the Unity game development platform where the behaviour of game objects could be scripted with _UnityScript_, a fork of JavaScript which maintained a subset of its behaviour, although this behaviour has since been deprecated from the Unity engine.

The JavaScript language itself has also evolved considerably since its creation; the most common form of JavaScript deployed on the web today is of the standard known as ECMAScript 5 (abbreviated to ES5). ECMAScript is a "living" scripting language specification, standardised by **Ecma International** and ratified by the **ISO/IEC**, to standardise the JavaScript language across multiple independent implementations. ES5 was standardised in December 2009 and introduced the first major changes to the language since the previous standard ES3. ES5 would remain the de-facto standard for client-side scripting &mdash; with a few minor modifications &mdash; until the release of ES6 in 2015, when the specification became a "living standard" with annual releases thereafter. Even after the release of ES6, the majority of web developers still publish their web applications as ES5 to ensure backwards compatibility with older browsers. Even if the application was written in ES6, [transpilers](https://en.wikipedia.org/wiki/Source-to-source_compiler) are used to translate the source to the equivalent ES5.

> JavaScript has had a number of names since its creation, including: _Mocha_, _LiveScript_, _JavaScript_ and finally _ECMAScript_. Although the specification for the language is named differently, the language is still commonly referred to as JavaScript.

# Interpreted vs. Compiled Code

<figure>
    <center>
        <img src="./InterpreterContextProgram.png" alt="program" width="250"/>
        <figcaption>Figure: A JavaScript program</figcaption>
        <br/>
    </center>
</figure>

<figure>
    <center>
        <img src="./InterpreterContextCompiler.png" alt="compiler" width="250"/>
        <figcaption>Figure: A JavaScript to "byte code" compiler</figcaption>
        <br/>
    </center>
</figure>

In an interpreted language, code is run from top-to-bottom and the result of running each statement is returned immediately. The main characteristic of an interpreted language is that they require another program, called a [runtime](https://en.wikipedia.org/wiki/Runtime_system), to interpret and run the source code. The main advantage of using an interpreted language is that the source code can be executed without having to make assumptions about the environment it is being executed in, as long as there is a runtime available. In the case of JavaScript, all of the major browsers (Safari, Chrome, Firefox and Edge) and NodeJS each provide the JavaScript run time that interprets the source code and exposes APIs that the source code can use. 

> While browser manufacturers make an effort to provide a standard set of APIs to the running JavaScript across the different browsers; it should be noted that NodeJS does not. This will be discussed further [below](#server-side-vs-client-side-code).

Every time the runtime starts to load some JavaScript code, the source is analysed using an interpreter and reduced to an intermediate representation known as "byte code". Once the source has been interpreted, the runtime begins executing the byte code instructions and translating them to native instructions.

<figure>
    <center>
        <img src="./InterpreterDiagrams-Interpreter.png" alt="interpreter" width="450"/>
        <figcaption>Figure: A JavaScript program is run on an interpreter in real time</figcaption>
        <br/>
    </center>
</figure>

Some languages &mdash; such as C# and Java, also interpreted languages &mdash; have a compilation step where the source is compiled into a byte code "binary". This removes the cost of running the compiler by running it ahead of time, which takes a fraction of the time to load than to reproduce. Although these languages are still "compiled", they are compiled to byte code which is then interpreted on a runtime.

Other languages compile their code directly into **native instructions**. These programs don't require a runtime program to execute, they can instead be interpreted directly by hardware (with the assistance of the operating system).

> Languages like C# and Java have become much more efficient over the years by using just-in-time (JIT) compilers. These are programs that interpret byte code instructions and compile them directly to native instructions, in real time, before executing them. Programs like these are still not as efficient as native programs but are a vast improvement on interpreted languages. Most modern JavaScript interpreters make use of this technique in some form or another.

<figure>
    <center>
        <img src="./InterpreterDiagrams-JIT.png" alt="jit compiler" width="450"/>
        <figcaption>Figure: A JIT compiler runs a program that has been previously compiled to an intermediary format known as "byte code"</figcaption>
        <br/>
    </center>
</figure>

<figure>
    <center>
        <img src="./InterpreterDiagrams-Compiled.png" alt="compiled" width="450"/>
        <figcaption>Figure: A C program is compiled directly to hardware instructions and interfaces directly with the operating system</figcaption>
        <br/>
    </center>
</figure>

# Server-side vs. Client-side Code

```
Diagram: Runtime exposes API which allows native functions to be called.
```

Client-side code is code that is run on the user agent (usually a browser). When a web page is viewed, the page's JavaScript client-side code is downloaded, then run and displayed by the browser.

When JavaScript code is executed in a browser, it is run in a "sandboxed environment" with limited access to other parts of the browser or the host operating system. This environment is referred to as the JavaScript virtual machine and code running within this environment can only interact with APIs provided by the browser.

> **Further Reading**
> 
> [JavaScript Reference (browser)](https://nodejs.org/dist/latest-v12.x/docs/api/)

With NodeJS, the same process is followed, but the main difference is that NodeJS purposely exposes more access to the host operating system, including the file system, direct access to networking hardware, and the ability to run native instructions and bind them to JavaScript. These capabilities and many more are further described in the NodeJS API.

> **Further Reading**
> 
> [NodeJS API documentation](https://nodejs.org/dist/latest-v12.x/docs/api/)

While the JavaScript language between the two environments is the same, the interfaces that are provided to the running code by each are significantly different. Running JavaScript code using NodeJS is more comparable to running Java or C# code than it is to running JavaScript code in a browser. Care should be taken not to confuse the two.

# Dynamic Content and Static Code

In this instance, **dynamic content** specifically refers to the ability to update the display of a web page/app/view. A website that displays content that does not change is often referred to as a **static website**, however this concept is misleading.

The term "static website" implies that the _content_ of the website is static but it originally implied the HTML that is served to the user agent is unchanging (before JavaScript). In the post-JavaScript web, most websites that are served to clients are _technically_ static websites, in that the HTML, CSS and JavaScript served are unchanging between requests, but become dynamic once the JavaScript begins to run and change the HTML.

For comparison, there are some server-side systems, such as template engines, are capable of serving dynamic content without the help of JavaScript. These services achieve this by simply serving HTML templates that have been populated with data from a database.

> **Further Reading**
> 
> TODO: an aside rant about how bad and inefficient template engines are.

For the remainder of this programme, we will be focussing on techniques that serve **static code** that, when executed, produces **dynamic web applications** that populate themselves with data from an API.