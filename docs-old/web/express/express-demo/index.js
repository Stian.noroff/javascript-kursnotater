const express = require('express')
const morgan = require('morgan')
const app = express()
const port = process.env.PORT || 3000

app.use(morgan('tiny'))

app.use(express.static('public'))

app.listen(port, () => console.log(`Listening on port ${port}`))
