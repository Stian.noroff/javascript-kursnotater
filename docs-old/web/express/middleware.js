
app.get('/path/to/resource',
  // First Middleware
  (req, res, next) => {
    req.applicationState = {
        foo: 'bar',
    }
    next()
  },

  // Second Middleware
  (req, res, next) => {
    getQuxAsync().then(qux => {
        req.applicationState.baz = qux
        next()
    })
  },

  // Final Middleware
  (req, res) => {
    res.status(200).json(req.applicationState)
  },

  // Unused Middleware
  (req, res) => { /* THIS IS NEVER EXECUTED */ }
)

app.get('/path/to/resource', (req, res, next) => {
  asyncAction()
    .then(result => {
      res.status(200).json(result)
    })
    .catch(next)
})