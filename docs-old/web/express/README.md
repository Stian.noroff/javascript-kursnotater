# Express

Express is a fully-featured, popular JavaScript framework for building backend HTTP services with NodeJS. Like other backend frameworks, such as Java Spring and .NET Core, Express provides an API to start a service that listens for HTTP requests and allows middleware and distinct request handlers to be subscribed based on a path, router, or regular expressions. When a request is recieved and dispatched to a request handler, the corresponding request handler is expected to read the request, interact with external systems (i.e. a database) and produce an appropriate response for that request.

Express itself is a framework for building HTTP services, however past that Express is "unopinionated" and instead of directly integrating with other frameworks, provides a common interface that other frameworks may use to integrate with Express. This interface is called an [_Express middleware_](#middleware) and is discussed in detail later.

> **Further Reading**
>
> These notes will largely be following Express' [official "getting started" guide](http://expressjs.com/en/starter/installing.html).

# Hello, World!

Getting started with Express is deliberately very simple. Consider the following simple example:

```javascript
const express = require('express')
const app = express()
const port = process.env.PORT || 3000

app.get('/', (req, res) => res.status(200).send('<h1>Hello, World!</h1>'))

app.listen(port, () => {
    console.log(`Listening on port ${port}`)
})
```

This example has the following effects:

- After importing express, the first thing to be done is to create a new express application, which is seen on line 2.

- The application is then configured with [middleware](#middleware) like the request handler on line 5.

- Finally, once the application is configured, the application is instructed to start listening for requests on line 7. When the application has successfully started listening, the provided callback is called and the notification on line 8 is emitted.

Once the notice from line 8 is observed then requests can be made to the corresponding endpoint and the server will respond appropriately. The following command can be used to send a request to the server:

```sh
$ curl -vvvv localhost:3000
```

Running this command in a terminal should produce the following output:

```
* Rebuilt URL to: localhost:3000/
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 3000 (#0)

> GET / HTTP/1.1
> Host: localhost:3000
> User-Agent: curl/7.58.0
> Accept: */*
>

< HTTP/1.1 200 OK
< X-Powered-By: Express
< Content-Type: text/html; charset=utf-8
< Content-Length: 22
< ETag: ...
< Date: Sat, 22 Feb 2020 10:21:44 GMT
< Connection: keep-alive
<

* Connection #0 to host localhost left intact
<h1>Hello, World!</h1>
```

# Express and Frontend Projects

When building a web application, there are many components involved, which are roughly divided into frontend concerns and backend concerns. While these two sets of concerns are mostly separate, they eventually come into contact, and this happens when it comes time to serve the frontend application. Consider the following diagram which depicts a map of a complete web application:

<figure>
    <center>
        <img src="./WebService.png" alt="web service" width="700"/>
        <figcaption>Figure: Web Service</figcaption>
        <br/>
    </center>
</figure>

In a typical web application, the web service ("backend") is mostly responsible for providing a REST API which supplies information by applying _business logic_ to raw data pulled from a database. However, all assets for a web application, including those for the frontend, must still be served by a web server, and thus is also the responsibility of the backend.

For the purpose of this course, we are only interested in Express for its ability to serve the build artifact to the User Agent, so the application may be run. We will not be covering REST endpoints, business logic on the backend or database interaction.

# A Simple Express File Service

Although this course is primarily a frontend course, it is still recommended that separate manifests be used for the Express application and the frontend application. This is to provide a clear separation between the process for creating the frontend build artifact and serving the resulting output.

## Setup

The first step for working with Express is to create a new NodeJS project using `npm init` and install Express with `npm i express --save`.

Our project folder should now look like this:

```
../express-demo
├── package-lock.json
└── package.json

0 directories, 2 files
```

## Scripts and Nodemon

Before we continue, we must also open our package manifest (`package.json`) and add a start script so that we can run our application.

We can also optionally install a package called Nodemon (`npm i nodemon --save-dev`) which will make development slightly easier. Nodemon will automatically restart our web server when changes are made to the web service's script &mdash; otherwise the script must be manually restarted each time a change is made.

After adding both scripts, the `package.json` file should contain the following:

```javascript
{
  "name": "express-demo",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "start": "node index.js",
    "dev": "nodemon index.js",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "Noroff Accelerate AS",
  "license": "MIT",
  "dependencies": {
    "express": "^4.17.1"
  },
  "devDependencies": {
    "nodemon": "^2.0.2"
  }
}
```

If Nodemon is not used then the `devDependencies` object and `dev` script may be omitted.

## Express Server Script

Next we create a NodeJS script, called `index.js`, which will be our web service:

```javascript
const express = require('express')
const app = express()
const port = process.env.PORT || 3000

app.use(express.static('public'))

app.listen(port, () => console.log(`Listening on port ${port}`))
```

The middleware on line 5 is a utility that is supplied by Express for the purpose of serving static content. This will mean all of the contents of the `public` folder will be available on the root of the web service. Consider the following possible HTTP requests:

```
http://localhost:3000/images/kitten.jpg
http://localhost:3000/css/style.css
http://localhost:3000/js/app.js
http://localhost:3000/images/bg.png
http://localhost:3000/hello.html
```

As long as we place our build artifact inside the `public` folder, then Express will serve the contents for us.

## Static Website Files (Build Artifact)

Typically when creating frontend web applications, a script is usually provided that will produce a production-ready build artifact. This is usually accessed with something like: `npm run build` and the output is placed in a folder called `build`, `dist` or `public`, depending on the framework being used. Regardless of the form, from the perspective of the web service, these build artifacts are all equal and simply need to be placed in the `public` folder for our application to serve them.

For now, as we do not have a build artifact on hand, lets create a simple HTML file at `public/index.html`:

```html
<h1>Hello, World!</h1>
```

Our project folder should now look like this:

```
../express-demo
├── index.js
├── package-lock.json
├── package.json
└── public
    └── index.html

1 directory, 4 files
```

> **Caveat**
>
> The Webpack skeleton mentioned in the jQuery section includes a build script. If you've already got a project that uses that skeleton repository then you could probably create a build artifact for that project by running the build script of that project and serve that here.

## Testing The Service

If we run our simple Express service now with `npm run dev` then we can request our simple HTML file using the same command we used for the [_Hello, World!_](#hello-world) example.

```sh
$ curl -vvvv localhost:3000
```

This should produce the following output:

```
* Rebuilt URL to: localhost:3000/
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 3000 (#0)

> GET / HTTP/1.1
> Host: localhost:3000
> User-Agent: curl/7.58.0
> Accept: */*
>

< HTTP/1.1 200 OK
< X-Powered-By: Express
< Content-Type: text/html; charset=utf-8
< Content-Length: 22
< ETag: ...
< Date: Sat, 22 Feb 2020 10:21:44 GMT
< Connection: keep-alive
<

* Connection #0 to host localhost left intact
<h1>Hello, World!</h1>
```

# Middleware

Express provides a common interface for handling incoming requests and modularising application logic that is used by plugin authors and application developers: _middleware functions_. These are functions that are configured for one or more paths in the application that execute sequentially for every request that is handled by the service. Individual middleware are simple functions of the following form:

```javascript
app.get('/path/to/resource', (req, res, next) => {
  // 1. Read request information from `req`

  // Either:
  // 2a. Prepare and send response data using `res` interface.
  // 2b. Alter application state and invoke next middleware using the `next` callback.
})
```

## Middleware Chaining

For any incoming request, there may be multiple middleware that Express will apply to that request. These middleware are applied sequentially by executing each middleware and having the individual middleware invoke a callback when they are done. This design enables middleware authors to short-circuit their application logic if necessary; for example, if a request is determined to be malformed, then a `400 Bad Request` response can be sent without ever invoking the later middlewares for that endpoint.

> **Async Middleware and Passing State**
>
> Before ES6 and the advent of [Promises](../../javascript/future-values/#future-values-promises), this method of defining middleware for Express applications was also used to handle asynchronous operations. Asynchronous requests could be made, with a callback subscribed and, once executed, the result can be set directly onto the `req` object and the `next` function is then called. The next middleware in the chain can then reliably assume that, if it is called, the result of the previous asynchronous API call will be available where it was placed on the `req` object.

&nbsp;

> **Further Reading**
>
> [Using Express Middleware (official guide)](https://expressjs.com/en/guide/using-middleware.html)

The simplest way to chain middleware is to pass additional middleware functions as parameters to endpoint declarations. Consider the following example:

<figure>
    <center>
        <img src="./middleware-chaining.png" alt="chaining express middleware" width="500"/>
        <figcaption>Figure: Express Middleware Chaining</figcaption>
        <br/>
    </center>
</figure>

In this example:

- **Line 1** &mdash; A GET endpoint is defined on an Express application for the path `/path/to/resource`.
- **Line 4** &mdash; A variable is set on the request object. The same request object is passed to subsequent middleware.
- **Line 7** &mdash; The first middleware calls the `next` callback to pass program control back to the Express application. Express then continues to invoke the next middleware in the chain, which is defined on line 11.
- **Line 12** &mdash; An asynchronous call that returns a Promise object is made. A callback is subscribed to set the result onto the existing `applicationState` object (as previously defined on line 4-6).
- **Line 14** &mdash; The `next` callback is invoked. As this callback is invoked _inside_ the callback for the async operation, this will only execute once the operation completes.
- **Line 20** &mdash; The final callback instructs Express via the [Response Object](#response-api) to respond to the request with the final `applicationState` object in the response body.

    > **Aside**
    >
    > Calling `.json()` will automatically set the `Content-Type` header on the response to `application/json` before sending it.

- **Line 24** &mdash; Despite being registered on the middleware chain for that endpoint, **this middleware will never be invoked** because there is no code path where `next` is called in the previous middleware.

A request to this endpoint using `curl` might produce the following output:

```
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 3000 (#0)

> GET /path/to/resource HTTP/1.1
> Host: localhost:3000
> User-Agent: curl/7.58.0
> Accept: */*
>

< HTTP/1.1 200 OK
< X-Powered-By: Express
< Content-Type: application/json; charset=utf-8
< Content-Length: 25
< ETag: ...
< Date: Mon, 24 Feb 2020 10:12:03 GMT
< Connection: keep-alive
<

* Connection #0 to host localhost left intact
{"foo":"bar","baz":"qux"}
```
## Middleware Execution Order Caveat

For every request that is processed by the service, Express will always pass the request to the first request handler whose route matches the path and method of the incoming request.

Express will determine a list of registered middleware that will be applied sequentially to every request that is passed to a particular route. _**The order of the list of middleware corresponds directly to the order in which the middleware were registered in code**_.

## Routing

In many occasions, application developers want to have a middleware execute for more than one endpoint. To support this behaviour, Express provides a hierarchical approach to registering middleware:

1. Middleware registered for an individual endpoint will be included in the middleware chain for only that specific endpoint.
2. Middleware can be registered at the application-level using `app.use([...middleware])`. These middleware will be present in the middleware chains for all requests handled by the service.

    > **Caveat**
    >
    > Middleware added at the application level with `app.use` _after_ an endpoint has been defined will still be added to the end of the middleware chain for that endpoint.
    >
    > **The level at which a middleware is defined does not affect their execution order for any given endpoint.**

3. [Express Routers](https://expressjs.com/en/4x/api.html#router) can be used to define a sub-application that can be mounted onto an application as a middleware. Consider the following example:

    ```javascript
    const router = express.Router()

    router.use((req, res, next) => { /* DO SOMETHING */ })

    app.get('/other', /* HANDLERS */)
    router.get('/path/to/resource/1', /* HANDLERS */)
    router.get('/path/to/resource/2', /* HANDLERS */)
    router.get('/path/to/resource/3', /* HANDLERS */)

    app.use('/sub', router)
    ```

    In this example, the middleware that is mounted on line 3 is only applied to the endpoints defined within the router (i.e. `/sub/path/to/resource/(1 | 2 | 3)`) and not those defined elsewhere (i.e. `/other`).

## Error Handling

When an error occurs inside an Express middleware, the default behaviour is for Express to automatically send a `500 Internal Server Error` response with the error text in the response body; however this only happens when the error occurs synchronously. When the error occurs asynchronously, the error must be handled explicitly or it will compromise the entire process. Consider the following example:

<figure>
    <center>
        <img src="./middleware-async-error-handling.png" alt="express async error handling" width="500"/>
        <figcaption>Figure: Express Handling Async Errors</figcaption>
        <br/>
    </center>
</figure>

In this example the asynchronous action invoked on line 2 has the potential to throw an error.

- If the action were to complete successfully then the result would be sent back to the client in the body of `200 OK` response.
- If the action were to fail without a `catch` callback being defined then the application would emit an _"unhandled promise exception"_ and the request would go unhandled; eventually the request would time-out on the client side.

In Express this is handled by passing the error object as a parameter to the `next` callback. This will signal to Express than an error has occurred and all remaining middleware will be skipped, except for [those set up to handle errors](#default-error-handler).

> **Further Reading**
>
> [Error Handling (official guide)](https://expressjs.com/en/guide/error-handling.html)

## Default Error Handler

For backend applications it is often a good idea to create one unified place where errors are handled; to support this, Express allows a special middleware to be registered to catch all errors thrown in the middleware chain. This middleware is called the _default error handler_ and has a slightly different method signature to other middleware:

```javascript
// 1. Setup Express and plugins
// 2. Define endpoints

// ...

// 3. Default Error Handler
app.use((err, req, res, next) => {
  // Handle your error here
})

// 4. Listen
```

There are two important differences from typical middleware:

- The default error handler takes **four parameters** instead of the usual three. This is how Express will determine whether a default error handler is in fact a default error handler. Even if all four parameters are not used they must still be defined in the method signature.
- The default error handler should be the last middleware defined before calling `app.listen`. This is to ensure that all other middleware are registered before the error handler.

Default error handlers are typically used to standardise log reporting. For large, highly parallel applications this will typically involve sending logging output to a cental, write-only repository for later analysis. Deployment schemes such as Heroku already take care of this for us and all standard output of the running application is visible through their online interface.

> **Caveat**
>
> When the `NODE_ENV` environment variable is set to `production` the default error handler that is included with Express is disabled to prevent potentially leaking sensitive information. **Applications deployed in production mode will exit in the event there are unhandled errors.**

# Express APIs

Aside from the callback function, every middleware is invoked with two objects: the request and the response.

> **Further Reading**
>
> Offical documentation for the [Request](https://expressjs.com/en/4x/api.html#req) and [Reponse](https://expressjs.com/en/4x/api.html#res) APIs.

## Request API

The request object initially contains all information that was read from the request, including request headers, the path, host information, the body and additional parameters. As middleware are executed, the request object is also typically used to transfer state between middleware when handling a request. This is most apparent when considering something like [sessions](#express-session), the purpose of which is to ensure the presence of `req.session` for use in downstream middleware.

> **Further Reading**
>
> [Express Session README.md (NPM)](https://www.npmjs.com/package/express-session)

Some properties of the request object:

- `req.query` &mdash; The query parameters for the request as a JavaScript object.
- `req.method` &mdash; The method of the request; i.e. GET, POST, PUT, etc.
- `req.params` &mdash; An object containing the named route parameters of the request. i.e. A route defined as `/user/:name` will yield a property `req.params.name` which will be the corresponding value supplied in the path.
- `req.body` &mdash; By default this is undefined and is only populated when used in combination with external body-parsing middleware such as `body-parser`. As of Express `v4`, body parsing middleware are also included within Express itself. Consider the following example:

    ```javascript
    const express = require('express')

    const app = express()

    // Content-Type: application/json
    app.use(express.json())
    // Content-Type: application/x-www-form-urlencoded
    app.use(express.urlencoded({ extended: true }))

    app.post('/path/to/resource', (req, res, next) => {
      console.log('BODY', req.body)
      res.json(req.body)
    })
    ```

## Response API

The response object conversely does not provide extensive readable information, instead it provides many APIs for configuring the response before finally sending it.

Some methods provided by the response object:

- `res.status(int)` &mdash; Set the status code of the response.
- `res.json(object)` &mdash; Configure and send a JSON response. If not explicitly set beforehand, this will configure the `Content-Type` header to `application/json` and the status code to `200 OK`.
- `res.redirect(string)` &mdash; Configure and send a `302` redirect to the specified path.
- `res.send(string)` &mdash; Send a text response. If not explicitly set beforehand, this will configure the status code to `200 OK`.
- `res.sendFile(string)` &mdash; Send the file described by the path specified. If not explicitly set beforehand, this will configure the status code to `200 OK` and will make a best guess for the `Content-Type` header.

## Morgan Logger (Optional)

Express does not come preconfigured with any logging frameworks that allow the result of requests to be seen by the user. A package called Morgan provides a simple, configurable utility for logging out request and response information to the console.

Morgan simply defines a middleware that is mounted onto the application in its early stages. At its simplest, Morgan can be completely setup and with a pre-configured in one line:

```javascript
app.use(morgan('tiny'))
```

When requests are made to the service, they will be recorded in the applicaton `stdout` as follows:

```
GET /index.html 200 23 - 1.066 ms
```

The "tiny" configuration of Morgan logs out, from left to right, the request method, the path, the status code of the final response, the size of the response in bytes, a dash separator and finally the total time taken to handle the request. This information can be useful later to evaluate the health and efficiency of your service endpoints in relation to eachother.

> **Further Reading**
>
> [Offical README.md (NPM)](https://www.npmjs.com/package/morgan)