# What is jQuery

jQuery is a JavaScript library first released in January 2006 by John Resig and is the most widely used JavaScript library ever written. Until that point, JavaScript was called explicitly from within HTML tags themselves by specifying event listeners as attributes to the HTML element itself. John's vision for jQuery was to separate the HTML and JavaScript code to increase legibility and make the code cleaner and more understandable.

jQuery is a JavaScript library designed to simplify querying and manipulation of HTML documents (DOM) and associated CSS styles, ease event handling and facilitate AJAX requests; all from within JavaScript code, without having to specify function calls inline in HTML elements. Despite having since gone into "maintenance mode" (as of ) and the introduction of more sophisticated front-end web application frameworks, [jQuery is estimated to still retain a 35% market share of the top 1M websites](https://www.datanyze.com/market-share/frameworks-and-libraries--66/Alexa%20top%201M/jquery-market-share). [Other estimates](https://trends.builtwith.com/javascript/jQuery) go as high as 80%, registering ~70M live websites still actively using jQuery.

> **Maintenance Mode**
>
> jQuery, and other "mature" libraries enter maintenance mode when there are no further planned features for those libraries. The library is still maintained, which involves publishing bug fixes, patches to security vulnerabilities and efficiency improvements, but otherwise there are no additional features planned for the library or framework. Libraries and frameworks that transision to maintenance mode are typically maintained due to lots of existing (legacy) infrastructure still relying on those resources.

jQuery introduced significantly improved performance compared to existing APIs provided by browsers at the time, due to the introduction of a "cached" or "virtual DOM". This feature enabled its selector engine to make relatively efficient (depending on the query), complex lookups of elements in the DOM which later inspired browser manufacturers to standardise around the [_Selector API_](https://developer.mozilla.org/en-US/docs/Web/API/Document_object_model/Locating_DOM_elements_using_selectors), which provided native implementations for the same and even using the same syntax.

As front-end developers, it is impossible to not consider jQuery, as we will often be called to maintain existing web infrastructure and is likely to involve jQuery in some way.

# Dependency Management: Importing jQuery

To start using jQuery, one must first somehow include it into your HTML web page. There are a few ways to do this, with varying levels of complexity:

1. **Load from a remote source** &mdash; You can include jQuery as a script tag while specifying the source to be a Content Delivery Network (CDN). For example:

    ```html
    <html>
        <body>
            <h1>Hello, World!</h1>
        </body>

        <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
    </html>
    ```

    > **Hint**
    >
    > Regardless of what is being loaded, it is usually a good idea to load your external resources (scripts, etc.) at the bottom of your web page. This ensures that your users aren't staring at a blank web page while they're waiting for your jQuery and your other `.js` files to load from the web server.

2. **Load from your local file system** &mdash; You can download the jQuery script and serve it alongside your `.html` file. For example:

    ```html
    <html>
        <body>
            <h1>Hello, World!</h1>
        </body>

        <script src="/js/jquery/jquery-3.4.1.min.js"></script>
    </html>
    ```

    > **Caveat**
    >
    > When deploying a website that loads a script locally, the script itself also needs to be served to be accessible from within the deployed HTML page.

3. **Load from NPM** &mdash; Many dependencies for frontend projects can be managed using NPM, including jQuery. Scripts and libraries included in this manner are typically served the same as above, however there is usually some automated script that copies the dependencies from where they're installed in `/node_modules/` to the final output folder for the web page.

4. **Bundled as part of the build artifact (recommended)** &mdash; Tools such as [Webpack](https://webpack.js.org/) exist to process source material across multiple higher order languages, file formats, libraries and purposes, to _transpile_ them into one common representation, which is usually JavaScript. Webpack will store the resulting output into large monolithic files that also contain all dependencies and everything needed to run the application. The output is also then _minified_ to optimise the total file size of the resulting artifact and reduce loading times.

    The output of Webpack does not respect the folder structure of its input, but the output is self-contained and ready to be loaded by a browser and executed. Consider the following folder structure before and after Webpack has been used to create the build artifact:

    **Before Webpack:**

    ```
    src
    ├── App.js
    ├── api
    │   ├── index.js
    │   ├── setAccessToken.js
    │   └── setUserInfo.js
    ├── component
    │   └── UserInfoCard.js
    ├── container
    │   ├── AuthHandler.js
    │   └── UserInfoContainer.js
    ├── index.css
    ├── index.js
    ├── reducer
    │   ├── accessTokenReducer.js
    │   ├── index.js
    │   └── userInfoReducer.js
    ├── serviceWorker.js
    ├── setupTests.js
    └── store.js

    4 directories, 15 files
    ```

    **After Webpack:**

    ```
    build
    ├── asset-manifest.json
    ├── favicon.ico
    ├── index.html
    ├── logo192.png
    ├── logo512.png
    ├── manifest.json
    ├── precache-manifest.25f0388276224975da91796cef3d3e55.js
    ├── robots.txt
    ├── service-worker.js
    └── static
        ├── css
        │   ├── 2.47e06e2e.chunk.css
        │   ├── 2.47e06e2e.chunk.css.map
        │   ├── main.5ecd60fb.chunk.css
        │   └── main.5ecd60fb.chunk.css.map
        └── js
            ├── 2.c451e83d.chunk.js
            ├── 2.c451e83d.chunk.js.LICENSE.txt
            ├── 2.c451e83d.chunk.js.map
            ├── main.abda32d5.chunk.js
            ├── main.abda32d5.chunk.js.map
            ├── runtime-main.59b8810f.js
            └── runtime-main.59b8810f.js.map

    3 directories, 20 files
    ```

    Once Webpack has been configured for a project then dependencies from `/node_modules/` can be accessed through the ES6 module syntax:

    ```javascript
    // Load jQuery from node_modules and assign to a new variable called "$"
    import $ from 'jquery'

    // Instruct Webpack to bundle Bootstrap styles as well.
    import 'bootstrap/dist/css/bootstrap.min.css'
    ```

    The remaining complexity is handled by Webpack.

    > **Pandora's Box**
    >
    > Webpack is a complicated system that has evolved out of necessity as front-end development has become more layered. Most frameworks that are considered as part of this course provide their own ready-to-use Webpack configurations which actively discourage modification by the users of the frameworks themselves. For this reason we advise treating Webpack like [Pandora's Box](https://en.wikipedia.org/wiki/Pandora%27s_box) and not interacting with it directly unless there is no other alternative. To this end, for cases where a webpack template is not provided, [we provide our own template](https://gitlab.com/noroff-accelerate/javascript/jquery-skeleton) for your use with Webpack and accompanying build scripts preconfigured.
    >
    > We do not explicitly cover Webpack configuration as part of this course.

# Selectors

One of the main contributions of jQuery is the introduction of their selector query syntax that enables more efficient querying of the DOM. jQuery selector syntax allows the selection of zero or more DOM elements by:

- `id`
- HTML tag names
- HTML attributes
- CSS class
- CSS selectors

> **Further Reading**
>
> [jQuery Selectors Table (W3Schools)](https://www.w3schools.com/jquery/jquery_ref_selectors.asp)

These are now discussed:

## `id`

Elements in our HTML can be defined in terms of a unique identifier, specified as the `id` attribute of that element. Consider the following example:

```html
<html>
    <body>
        <div id="app"></div>
        <div id="other"></div>
    </body>
</html>
```

The `div` element with the `id` value of `app` may be selected as follows:

```javascript
const element = $('#app')
```

That element may now be operated on, without also operating on the element identified by `other`.

## HTML tag names

Selections can be made on the type or name of the elements themselves; these are also referred to as "tags" (XML). Consider the following example:

```html
<html>
    <body>
        <div></div>
        <div></div>
    </body>
</html>
```

All `div` elements in the page can be selected using the following query:

```javascript
const element = $('div')
```

## HTML attributes

Selections can also be made on arbitrary attributes on elements in the DOM. Consider the following example:

<html>
    <body>
        <div foo="bar"></div>
        <div></div>
    </body>
</html>
```

The first `div` could be selected using any of the following queries:

```javascript
const element = $('[foo]')
const sameElement = $('[foo="bar"]')
```

## CSS Class

Styles can be defined and attached to multiple elements at once by specifying styles in terms of a class in CSS, however classes can also be specified without having any associated styles for the purposes of convenient selection. Consider the following example:

```html
<html>
    <body>
        <div class="foo"></div>
        <div class="foo"></div>
    </body>
</html>
```

The `div` elements can be selected together using their common class as follows:

```javascript
const elements = $('.foo')
```

The result is returned as an array of elements, which can be iterated over and operated on individually, or the same operation can be applied to all of them by invoking directly from the query result.

## Composition

Selectors may also be combined to refine selections. These compositions are evaluated from left to right. Consider the following example:

```html
<html>
    <body>
        <div class="foo"></div>
        <div class="bar"></div>
        <div class="foo bar"></div>
        <p class="foo"></p>
    </body>
</html>
```

The last of the `div` elements can be selected with the following query:

```javascript
const element = $('.foo.bar')
```

Alternatively, all the elements inside `body` could be selected with the following query, which could be read as `.foo || .bar`:

```javascript
const element = $('.foo,.bar')
```

Query selectors may also be composed of different kinds of selectors; for example, the following query will select only the `p` element inside `body` with a class named `foo`:

```javascript
const element = $('body > p.foo')
```

## CSS Selectors

jQuery also supports the typical selectors that are provided in CSS syntax as well. Consider the following example:

```html
<html>
    <body>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </body>
</html>
```

CSS selectors can be used to specify which of the `div` elements should be selected, despite the elements not having any qualifying attributes; for example, the following query can be used to select the last of the four `div` elements:

```javascript
const element = $('body > div:last')
```

Similarly, the following query could be used to select all the evenly-indexed components:

```javascript
const element = $('body > div:even')
```

# Events

Once elements have been selected, then event handlers may be subscribed to those events. If a particular event is triggered, and there exists a callback to handle that particular event, then the callback function is called and event object is passed to it as a parameter. Consider the following example:

```html
<html>
    <body>
        <button id="fooButton">Click Me!</button>
    </body>
</html>
```

In this example we define a button that can be easily selected with a unique `id`; once selected we can subscribe an event listener to the button for the `click` event, which will execute our callback at that time.

```javascript
$('#fooButton').on('click', e => alert('Hello, World!'))
```

This works as expected for all manner of possible events, for all possible selectable elements of the DOM.

> **Further Reading**
>
> See the full list of DOM events [here (MDN)](https://developer.mozilla.org/en-US/docs/Web/Events)

## jQuery General Use Caveats

When selecting elements and subscribing to events with jQuery, it is important to first wait for the document to be ready before attempting to interact with it. This is because jQuery indexes and caches the DOM as soon as the script begins to execute. Use of jQuery requires this cache to be in a ready state.

Thus, when doing operations with jQuery, the start of those operations should be within a callback to the `ready` event on the `document` object:

```javascript
$(document).ready(() => {
    // Start your jQuery operations here
})
```

Due to jQuery's strict requirement of waiting until it is ready before starting to listen for events or make changes, one might be tempted to use standard browser APIs to make changes immediately at the start of the document loading. This is also not advised, as this will lead to inconsistencies between the DOM and the virtual DOM that is cached by jQuery. **jQuery will only track changes that are made to the DOM using its own APIs.**

## Reading Input

There are two ways to read the contents of an HTML `input` element; either explicitly or by subscribing to the `change` event on that element. Consider the following example:

```html
<html>
    <body>
        <label for="foo">Foo</label>
        <input type="text" id="foo" name="foo">
    </body>
</html>
```

In this example, we can read the contents of the `input` element by first selecting it and then calling `.val()` on the element, as demonstrated below:

```javascript
const value = $('#foo').val()
alert(`FOO: ${value}`)
```

... or we can subscribe a listener to the `change` event. The provided callback will be called for every time the user makes a change to the contents of the text box.

```javascript
$('#foo').on('change', fooCallback)
```

When we subscribe to the `change` event, the parameter that is passed to the event handler (callback) is not the value itself, but rather an event object that contains event data &mdash; which includes the updated value of the `input`.

```javascript
function fooCallback (event) {
    const value = event.target.value
    alert(`FOO: ${value}`)
}
```

There are many types of events with many other useful fields; the full list for both can be seen [here (MDN)](https://developer.mozilla.org/en-US/docs/Web/API/Event).

> **Caveat**
>
> Sometimes we would like to use the event-based method but we don't necessarily want to call the callback for _every single letter_ that is typed into the `input`. For occations like this you can use helper functions [`_.debounce`](https://lodash.com/docs/#debounce) or [`_.throttle`](https://lodash.com/docs/#throttle) from a library called `lodash`. The differences between these two functions are well explained by [this article](https://css-tricks.com/debouncing-throttling-explained-examples/) by David Corbacho.

# AJAX Requests

Before jQuery, the only way to make requests from JavaScript was to use the `XMLHttpRequest` API provided by the browser. One of the major improvements that led to jQuery's popularity was the addition of its AJAX API which greatly simplified the process.

> **Further Reading**
>
> For those who are interested, [MDN has an article showing how to use the XMLHttpRequest API](https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/Using_XMLHttpRequest).

Consider the following example:

```javascript
$.ajax('https://pokeapi.co/api/v2/pokemon-species/1').then(result => {
    console.log('RESULT', result)
    // => { base_happiness: 70, capture_rate: 45, ... }
})
```

In this example, a simple GET request is made to a REST API endpoint which is expected to return some JSON. The request is asynchronous by default and if callbacks are not provided then a promise-compatible object is returned. There are other ways to consume the results of a request asynchronously which may be seen [here (API Documentation)](https://api.jquery.com/jquery.ajax/).

GET requests are the default for AJAX requests made with jQuery but these requests are fully configurable. The request method, body and headers can all be configured by passing in a settings object to the request as a second parameter:

```javascript
$.ajax('https://api.example.com/api/v1/foo', {
    type: 'POST', // a.k.a. "method"
    data: {
        bar: 'baz',
    },
    headers: {
        Authorization: `Bearer ${token}`,
    },

}).then(result => {
    // Do something with result
})
```

> **Further Reading**
>
> The various parameters and options that can be configured as part of the settings object may be seen in the [official documentation](https://api.jquery.com/jquery.ajax/).

## Error Handling

Network requests, even those over a local network, are unreliable and, even if all other possibility of failure due to malformed input or insufficient privileges have been eliminated, there is still the potential for requests to fail due to infrastructure problems. All requests made must always have some kind of error recovery strategy, or clearly define who is responsible for handling the error. At the very least, you should include a `.catch()` callback for all requests, even if you throw the error further back afterwards.

# DOM Manipulation

jQuery can also be used to add, remove and modify elements within the DOM and is frequently used in combination with [AJAX requests](#ajax-requests) to populate a web page using data loaded from a "headless API" (an API without a front-end interface).

> **Further Reading**
>
> APIs other than those mentioned here exist to accomplish the same tasks within jQuery, these can be seen in the [official documentation](https://api.jquery.com/category/manipulation/).

## Insertion

Elements should be added to the DOM using one of the following APIs:

1. `append(value)` &mdash; Appends the elements described by `value` within each of the selected elements, after the last existing child.
2. `prepend(value)` &mdash; Prepends the elements described by `value` within each of the selected elements, before the first existing child.
3. `after(value)` &mdash; Appends the elements described by `value` to each of the selected elements, having the new elements appear as siblings after the selected elements respectively.
1. `before(value)` &mdash; Prepends the elements described by `value` to each of the selected elements, having the new elements appear as siblings before the selected elements respectively.

## Removal

Elements should be removed from the DOM using one of the following APIs:

1. `empty()` &mdash; Remove all children from the selected elements in the DOM.
2. `remove()` &mdash; Remove all the selected elements from the DOM.
3. `replaceWith(value)` &mdash; Replace each matched element with the elements described by `value`.

## Modification

jQuery provides a number of convenience methods to modify existing DOM elements, for example:

1. `attr(key, value)` &mdash; Get the value of the attribute specified by `key` for the first element in the matched set, or set the attribute to the `value` provided for all elements in the matched set.
2. `css(properties)` &mdash; Get or set one or more CSS properties for the set of matched elements.
3. `addClass(classes) | removeClass(classes)` &mdash; Add or remove the specified CSS classes respectively.
4. `hide() | show()` &mdash; Set `display: none` for all matched elements or restore it to its previous values respectively.

## CSS

In most cases, there is very seldom a reason to dynamically generate CSS styles for an HTML page. It is recommended that the majority of styles are defined statically beforehand and are instead _applied_ to elements as necessary using the `addClass(classNames)` and `removeClass(classNames)` API, or manipulate the attributes so that existing styles apply.

# Rendering Dynamic Data

[AJAX requests](#ajax-requests) and [DOM manuipulation](#dom-manipulation) can be combined to load and render dynamic content in static webpages. This is done by simply making a request for some data and then manipulating the DOM in the callback where the data is consumed. Consider the following example:

```html
<html>
    <body>
        <button id="loadButton">Click Me!</button>
        <div id="content"></div>
    </body>
</html>
```

```javascript
let counter = 1

$(document).ready(() => {
    $('#loadButton').click(() => {
        $.ajax(`https://pokeapi.co/api/v2/pokemon/${counter++}`).then(result => {
            const { name, sprites: { front_default: src } } = result
            $('#content').append(`<div><h3>${name}</h3><img src="${src}" alt="${name}"></div>`)
        })
    })
})
```

After one press of the button the document will be updated to look like this:

```html
<html>
    <body>
        <button id="loadButton">Click Me!</button>
        <div id="content">
            <div>
                <h3>bulbasaur</h3>
                <img src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png" alt="bulbasaur">
            </div>
        </div>
    </body>
</html>
```

...and after one more:

```html
<html>
    <body>
        <button id="loadButton">Click Me!</button>
        <div id="content">
            <div>
                <h3>bulbasaur</h3>
                <img src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png" alt="bulbasaur">
            </div>
            <div>
                <h3>ivysaur</h3>
                <img src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/2.png" alt="ivysaur">
            </div>
        </div>
    </body>
</html>
```