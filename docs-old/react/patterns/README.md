# Advanced React

While the basics of React are easily understood, there are some more advanced concepts within React that will help in specific situations.

# Context

React having adopted the component model &mdash; where all objects within the systems are instances of a component class &mdash; all stateful instances must also be components if they are expected to be responsive to other components. This effectively means that React cannot support the [singleton service pattern](https://en.wikipedia.org/wiki/Singleton_pattern). The recommended approach to having something _like_ singletons in a React application is to use the Context API.

> **Further Reading**
>
> [Context API Documentation](https://reactjs.org/docs/context.html).

The Context API functions by introducing a Provider component, whose purpose is to inject the relevant state into a tree of React components and trigger updates in that tree when the state is changed. This is the same as how an update is triggered when a component's props are changed. The Provider component is defined and then wrapped around the highest level of the tree and may then be consumed either by any component in the tree that is configured to do so, or by a special Consumer component. For the former case, the component that is consuming the state is configured to do so from a particular Provider and does not need to have state passed in as props.

The API itself is relatively simple to use. First, a context must be created:

```javascript
const MyContext = React.createContext(placeholderValue)
```

This creates a context object. When React renders a component that subscribes to this context, it will read the current value from the closest matching Provider above it in the component tree.

> **Caveat**
>
> The `placeholderValue` is only supplied to a subscribed component when there are _no matching providers_ above it in the component tree. If an actual value is not supplied to the context provider then the value provided to subscribers will be `undefined` regardless of what is supplied here.

Next, we mount the context Provider in the component tree above where it will be used:

```javascript
<MyContext.Provider value={/* some value */}>
  {/* ... */}
</MyContext.Provider>
```

Components may subscribe to changes on this value in one of three ways:

1. Class-based components may set their `contextType` property on the component class, which will result in the context being available as `this.context` in the class methods.

    ```javascript
    class MyComponent extends Component {
      static contextType = MyContext

      render () {
        const value = this.context

        // ...
      }
    }
    ```

2. They may wrap their render output in a context Consumer.

    ```javascript
    function MyComponent (props) {
      return (
        <MyContext.Consumer>
          {value => {
            // Return something based on the value
            return (
              <span>{value}</span>
            )
          }}
        </MyContext.Consumer>
      )
    }
    ```

3. Functional components may subscribe to a context using the [`useContext` hook](https://reactjs.org/docs/hooks-reference.html#usecontext).

    ```javascript
    function MyComponent (props) {
      const value = useContext(MyContext)
      // ...
    }
    ```

> **Further Reading**
>
> There are instances where one might want to consume multiple context states within a single component. For these cases, a [higher order component](#higher-order-components) should be created that wraps multiple consumers (as in the second case above) and provides those context states to their children as props. This is discussed and shown in detail in the [offical documentation](https://reactjs.org/docs/context.html#consuming-multiple-contexts).

## Updating Context

As with any other child-to-parent component communication, functions may be passed as part of the context state. These functions may be used to define events that may be invoked from the context subscribers in exactly the same way as discussed in the [Custom Events](../../react/events/#custom-events) section of these notes. This is also discussed in the [official documentation](https://reactjs.org/docs/context.html#updating-context-from-a-nested-component).

> **Caveat**
>
> The React Engine uses shallow comparison to determine whether a component should rerender or not based on changes to props, state or context. To avoid unnecessary rerenders, the context value should be passed by reference and not defined inline. Consider the following example:

```javascript
class App extends React.Component {
  render() {
    return (
      <MyContext.Provider value={{something: 'something'}}>
        <Toolbar />
      </MyContext.Provider>
    )
  }
}
```

> In this example, all consumers of the provider will rerender every time the provider rerenders because the a new object is always created for `value`. This, and solutions to this problem, are discussed further in the [official documentation](https://reactjs.org/docs/context.html#caveats).

## When to use Context?

The Context API is not a replacement for normal state management and events in your application. The use of context reduces the reusability of components and has the potential to cause the rerendering of large parts of the application. Context should be used sparingly to conserve developer time and the users' devices' resources. Here are some general guidelines for when the use of context may be appropriate:

- It is appropriate to use context when more than one smart component in a tree require access to the state when those components are not directly related to eachother &mdash; i.e. are not direct parents or children. (TODO add image).
- It is approproate to use context when smart components in different sub-trees require access to the state when those components are not siblings to eachother &mdash; i.e. they should not share a common parent. (TODO add image).
- It is never appropriate to use context with any presentational, or "pure" component.

> **Further Reading**
>
> This is discussed further in the [official documentation](https://reactjs.org/docs/context.html#before-you-use-context).

# Higher Order Components

Higher order components (HOC) are a convenient way to abstract and reduce duplicate code in React applications &mdash; particularly for components involving similar use of side-effects such as `componentDidMount` or `componentDidUpdate`. Consider the following example:

```javascript
function withSubscription(WrappedComponent, queryCallback) {
  // Returns a new anonymous component class that wraps the supplied class
  return class extends Component {
    constructor (props) {
      super(props)

      this.handleChange = this.handleChange.bind(this)
      this.state = {
        // Note: DataSource is an imported global variable
        data: queryCallback(DataSource, props)
      }
    }

    componentDidMount() {
      // Subscribe to changes
      DataSource.addChangeListener(this.handleChange)
    }

    componentWillUnmount() {
      // Gracefully unsubscribe
      DataSource.removeChangeListener(this.handleChange)
    }

    handleChange() {
      this.setState({ data: queryCallback(DataSource, this.props) })
    }

    render () {
      // Return the wrapped component with fresh data as a prop
      // Other props are passed through so the wrapper is close to invisible
      return <WrappedComponent data={this.state.data} {...this.props} />
    }
  }
}
```

> **Caveat**
>
> HOCs should always be a pure function that produces a new component class. It is technically possible to permanently modify the component class that is passed into the HOC but one should resist the temptation to do so.

In this example, all of the complexity of handling the state updates from the external `DataSource` object are handled by the HOC and the resulting data is passed to the wrapped component as a prop. This HOC is used as follows:

```javascript
// CommentList.js
class CommentList extends Component {
  render () {
    const { data, style } = this.props

    return (
      <List>
        {data.map(item => <ListItem key={...}>{item.message}</ListItem>)}
      </List>
    )
  }
}

export default withSubscription(
  CommentList,
  (DataSource, props) => DataSource.getBlogPost(props.postId).getComments(),
)
```

The single, most useful application of HOCs is to include external state (i.e. from event emitters or to generalize callbacks from native APIs) in React applications.

> **Further Reading**
>
> HOCs, their associated conventions, and caveats are discussed in detail in the [official documentation](https://reactjs.org/docs/higher-order-components.html).

# Error Boundaries

Since React 16 (circa September 2017), the term "Error Boundary" has taken on two meanings:

1. React 16 introduces a type of component and an associated hook that is officially known as an "Error Boundary", which are discussed in the [official documentation](https://reactjs.org/docs/error-boundaries.html).
2. Error Boundaries are the term previously used to describe the typical behaviour exhibited by a catch block in all aspects of application development, including for asynchronous calls.

Both of these options for error boundaries are now explained:

## React Error Handling

React 16 introduces two new hooks, which provide the functionality equivalent to a catch block within React:

- `static getDerivedStateFromError(error)` &mdash; Produces a state update object to be merged in with the existing state when an error is caught. **Note: this is a static method.**
- `componentDidCatch(error, errorInfo)` &mdash; Defines side-effect behaviour that happens when an error is caught from a child component.

The purpose of these is to stop the application from failing further and instead to attempt recovery from the error or at least display feedback to the user about the error. Consider the following example of their use:

```javascript
class ErrorBoundary extends Component {
  constructor(props) {
    super(props)
    this.state = { error: false }
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { error: true }
  }

  componentDidCatch(error, errorInfo) {
    // You can also log the error to an error reporting service
    logErrorToMyService(error, errorInfo)
  }

  render() {
    const { error } = this.state
    const { children } = this.props

    if (error) {
      // You can render any custom fallback UI
      return <h1>Something went wrong.</h1>
    }

    return children
  }
}
```

Any unhandled errors arising from child components &mdash; not from this component itself &mdash; will cause the `error` flag to be set to `true`, and thus will display the UI elements pertaining to error handling.

> **Further Reading**
>
> This is discussed in the [official documentation](https://reactjs.org/docs/error-boundaries.html).

## Other Error Boundaries

JavaScript provides multiple language features which create error boundaries; most notably the [try-catch block](../../javascript/errors/README.md#try--catch-block) and [`Promise.catch()`](../../javascript/future-values/README.md#error-handling). The recommended way to handle most errors with React components is to make use of the [React Error Handling](#react-error-handling) described above; however there are scenarios that remain unaccounted for.

Any function that does not run directly as part of a component's render function or hooks are not handled by the React error handling. Most notably this includes all asynchronous code and event handlers such as those set to respond to user input (i.e. `onClick`, etc.). In these instances errors must be handled explicitly using one of the language features provided by JavaScript; however these do not help with providing feedback to users.

To handle errors and display feedback to users, we can make use of the following pattern:

```javascript
class MyContainer extends Component {
  constructor (props) {
    super(props)
    this.state = { loading: true, error: null }
    this.handleSomething = this.handleSomething.bind(this)
  }

  componentDidMount() {
    doSomethingAsyncOrSubscribe()
      .then(data => this.setState({ loading: false, data, error: null }))
      .catch(error => this.setState({ loading: false, error }))
  }

  handleSomething () {
    try {
      // ...
      this.setState({ loading: false, data, error: null })

    } catch (error) {
      this.setState({ loading: false, error })
    }
  }

  render () {
    const { loading, error, data } = this.state

    if (loading) {
      return <p>Loading...</p>
    }

    if (error) {
      return (
        <>
          <p>Something went very wrong...</p>
          <pre>{error.message}</pre>
        </>
      )
    }

    return (
      <MyComponent data={data} onTest={this.handleSomething} />
    )
  }
}
```

This separates the display components into three possible outputs: the output shown while pending a result from the asynchronous API call, when the API call fails, and when the API call is successful. This same pattern can be adapted to a variety of different applications (i.e. event emitters or sockets), or combined with [HOCs](#higher-order-components) or [context providers](#context).

# Virtualization

A common problem for web developers arises when attempting to display large amounts of data. Modern hardware and internet connections &mdash; particularly those of first-world countries &mdash; are capable of delivering vast amounts of content to a user's device; much more than the devices are capable responsibly rendering. With [use of mobile devices accounting for nearly 60% of global web use](https://netmarketshare.com/device-market-share?options=%7B%22dateLabel%22%3A%22Trend%22%2C%22attributes%22%3A%22share%22%2C%22group%22%3A%22deviceType%22%2C%22sort%22%3A%7B%22share%22%3A-1%7D%2C%22id%22%3A%22deviceTypes%22%2C%22dateInterval%22%3A%22Monthly%22%2C%22filter%22%3A%7B%7D%2C%22dateStart%22%3A%222019-06%22%2C%22dateEnd%22%3A%222020-05%22%2C%22segments%22%3A%22-1000%22%7D), these issues of scale are becoming increasingly important.

In React applications, if 1M data entries were served to a device, most developers would be tempted to produce a mapper function to convert each into a React component. While this would result all the elements being rendered, it would cause a noticable delay to do so, even though most of those components are not visible at the time. These kinds of delays affect the perceieved responsiveness of the website and will cause users to become frustrated. Furthermore, every time the component is rerendered, the same delay would be incurred, further aggravating the web application's users.

<figure>
    <center>
        <img src="./large_list.jpg" alt="large list" width="450"/>
        <figcaption>Figure: A list with many elements</figcaption>
        <br/>
    </center>
</figure>

Virtualization is a technique that specifically deals with these problems. Instead of converting all 1M entries into React components on every call to render, virtualization proposes rendering only what is currently required to fill the current display and some additional margin or "buffer" on either side.

<figure>
    <center>
        <img src="./virtual_list.jpg" alt="virtualized list" width="450"/>
        <figcaption>Figure: A virtualized list can handle many more elements</figcaption>
        <br/>
    </center>
</figure>

As the the user interacts with a virtualized list, they scroll into the buffer of elements that are already mounted and rendered. As the elements are already mounted and pre-rendered, the scrolling happens smoothly. This UX is perceived postively, even on modest devices. While the user is scrolling into the buffer, some additional effects are triggered to maintain the responsiveness of the list.

<figure>
    <center>
        <img src="./virtual_list_moving.jpg" alt="virtualized list moving" width="450"/>
        <figcaption>Figure: A virtualized list after the viewport has moved</figcaption>
        <br/>
    </center>
</figure>

As the user scrolls in one direction, the buffer of rendered elements in that direction shrinks and the buffer on the other end grows. As the user scrolls further away from an element, it will eventually fall outside of the desired size of the margin and will be unmounted and deallocated to conserve resources. Conversely, as the buffer is consumed on the other side and the margin falls below the desired size then more elements are taken from the data source and mounted _before they come into the user's viewport._ The result is a list that appears to scroll normally, however behind the scenes only a minimal amount of resources are being consumed and rerenders do not need to iterate over the whole data set, no matter how large it is.

## React Virtualized

[React Virtualized](https://bvaughn.github.io/react-virtualized/#/components/List) is a well-supported and maintained open-source library written to provide the functionality described above with a convenient API.

> **Further Reading**
>
> The `react-virtualized` [documentation](https://github.com/bvaughn/react-virtualized/blob/master/docs/README.md) and [demo](https://bvaughn.github.io/react-virtualized/#/components/List) show how the utility can be used to efficiently render huge data sets.

Making use of list virtualization requires some support from other parts of the application; for example, providing query parameters on the backend for a pagination mechanism to continuously fetch data as it is needed. Most of the necessary support mechanisms are highly contextual and will change with the circumstances.

> **Suggestion**
>
> Splitting responsibilities within applications is a difficult task. When dealing with large amounts of data, consider using a global state management library such as [Redux](https://redux.js.org/). This will separate the responsibilities of fetching the data and triggering rerenders from the responsibility of displaying the virtualized list content, making each individually more manageable.

# Input Debouncing

Although applications are becoming increasingly Internet-dependent, mobile devices are often constrained by factors such as battery life, bandwidth and storage &mdash; both temporary and permanent. Making HTTP requests is known to be a significant contributor to the use of the above resources and &mdash; while necessary &mdash; cannot be done freely.

Sometimes, when using older or lower quality touch hardware to interact with a web application, multiple distinct touches are registered when a user only means to do so once. If not explicitly accounted for, web applications may at this point initiate multiple distinct requests, over multiple distinct connections to a remote server. Each of these requests consume resources unnecessarily when this might be easily prevented using a technique called _input debouncing_.

A popular pure, functional, utility library called [Lodash](https://lodash.com/) provides a function called [`_.debounce`](https://lodash.com/docs/#debounce) which can be used to achieve this. The function is easily integrated into existing React component structures alongside necessary [function binding](../../javascript/functions/README.md#bind-method). The function will wrap an provided function, and will only run the provided function after the wait time has elapsed. If the function is called again while waiting, the timer is reset. Consider the following example:

```javascript
import _ from 'lodash'

class MyComponent extends Component {
  constructor (props) {
    super(props)
    this.handleChange = _.debounce(this.handleChange.bind(this), 200)
    this.state = { loading: true, data: null }
  }

  async handleChange (...args) {
    const data = await veryExpensiveOperation(args)
    this.setState({ data, loading: false })
  }

  // ...
}
```

The `this.handleChange` shown in the above example can then be passed as a callback to other components freely while already being wrapped in the debounce function. From the perspective of the child components, the debounce function wrapper is invisible and no difference will be observed, other than the response arriving at least 200ms later than it would normally. This is often better than having to rerender a component tree multiple times when doing so is expensive.

# Concurrency API

> **Notice**
>
> The Concurrency API is currently experimental for React development with unregulated breaking changes happening frequently. Once the API has stabilized this section will be revisited. The Concurrency API promises to provide new features to assist in keeping React apps responsive and graceful. For those who are curious, you can see the [official documentation](https://reactjs.org/docs/concurrent-mode-intro.html) on the topic.