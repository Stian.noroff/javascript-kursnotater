# React

React is currently the most popular of the three frontend frameworks discussed in this course. Some noteworthy features in relation to other frameworks include:

- Unopinionated. React does prescribe component types, or how projects should be structured.
- Simple. Everything in React is a component which, at their simplest, are just functions that produce JSX. React components have a low barrier to entry and have a simple API that becomes familiar very quickly with repeated use.

Much like [Vue](../../vue/basics/README.md), React also provides a "one-minute" setup scenario involving a CDN and `<script></script>` tags; however unlike Vue, React was not designed to do this from the start, this capability was added much later. Thus, we will be using React in the way which it was originally intended and starting with generating a project from a template with the CLI.

> **Further Reading**
>
> Those who want to use React as a drop-in to an existing website, or would like to compare the functionality to that of [Vue](../../vue/basics/README.md) can read about it [here (Official Guide)](https://reactjs.org/docs/add-react-to-a-website.html).
>
> These notes will largely follow the [official React tutorial](https://reactjs.org/tutorial/tutorial.html) with some minor deviations.

# CLI

To start, install the Create React App CLI tool:

```bash
npm i -g create-react-app
```

This will install the CLI tool for generating new projects with React. The tool can be used in the following ways:

```
Usage: create-react-app <project-directory> [options]

Options:
  -V, --version                            output the version number
  --verbose                                print additional logs
  --info                                   print environment debug info
  --scripts-version <alternative-package>  use a non-standard version of react-scripts
  --template <path-to-template>            specify a template for the created project
  --use-npm
  --use-pnp
  --typescript                             (this option will be removed in favour of templates in the next major release of create-react-app)
  -h, --help                               output usage information
```

To start, we'll generate a project as follows:

```bash
create-react-app --use-npm hello-react
```

This will create a project with some core dependencies pre-installed, including:

- `react` &mdash; The React framework core dependency.
- `react-dom` &mdash; Tools related to the use of React inside a web browser environment

    > **Aside**
    >
    > Out of the three frameworks in this course, React is the only framework that comes with [support for developing native smartphone applications](https://reactnative.dev/), in which case `react-dom` is replaced by `react-native`.

- `react-scripts` &mdash; A meta-dependency that contains all necessary scripts and the _entire React development toolchain_ as dependencies. This makes management of dependencies for React projects _significantly_ easier.

    > **Aside**
    >
    > If you wanted to manage all of the dependencies manually (i.e. without relying on `react-scripts`) then you can "eject" the application by running `npm run eject` in the project root directory.
    >
    > Ejecting cannot be reversed so if you want to try this then consider doing so on a freshly generated project to start.

Some other dependencies are also included (`jest`, etc.) related to unit tests, however these won't be covered in this course.

# Project

The freshly generated project will have the following structure:

```
./hello-react
├── README.md
├── package-lock.json
├── package.json
├── public
│   ├── favicon.ico
│   ├── index.html
│   ├── logo192.png
│   ├── logo512.png
│   ├── manifest.json
│   └── robots.txt
└── src
    ├── App.css
    ├── App.js
    ├── App.test.js
    ├── index.css
    ├── index.js
    ├── logo.svg
    ├── serviceWorker.js
    └── setupTests.js

2 directories, 17 files
```

Projects created using `create-react-app` are generated using an almost identical toolchain to that of Vue. Webpack and Babel are used to package and transpile respectively and additional configuration may be imported to enable the use of CSS pre-processors and Typescript. React projects also share the convention that the contents of the `public` folder are copied verbatim into the build output, and the contents of `src` are interpreted and transpiled into their respective build artifacts.

## Folder Structure

There is no offical prescription for the structure of React projects, however there are recommendations for the most optimal structure:

- `src/components` &mdash; Although the concept of modular components carries across all the frontend frameworks explored in this course, React takes this concept further by mandating that _everything_ involved in React development must be a component.
- `src/containers` &mdash; By convention, the [smart components](../../vue/components/README.md#smart-components) need to be somehow distinguished from the [pure components](../../vue/components/README.md#smart-components). One way to do this is to name them differently, however another option is to keep them in a separate folder called `containers`.
- `src/components/my-component` &mdash; Unlike in Vue, components are not contained within a single file; the styles and tests are normally imported from a file in the same folder level as where the component is defined. For this reason, components are kept in a folder of their same name.
  - The same folder will contain all the relevant files for that component; i.e. `MyComponent.css`, `MyComponent.test.js` and `MyComponent.js`.

      > **Aside**
      >
      > Sometimes files that define components will be named with the extension `.jsx`; the choice to do so is entirely up to the individul but neither `.js` or `.jsx` is wrong. The only difference is that when importing with the ES6 module syntax, the `.js` may be omitted but `.jsx` may not.

  - Optionally you can include a file called `index.js` inside the component folder which re-exports the component itself. This saves having to import the component file explicitly and other information can be provided as named exports.

    ```javascript
    // src/components/my-component/index.js
    export { default } from './MyComponent' // .js omitted
    // OR
    export { default } from './MyComponent.jsx'
    ```

- `src/views` &mdash; Some teams will also separate components that are mounted onto the [Router](../router/README.md) into a separate folder from both `components` and `containers`. Others will also use a `views` folder but as a replacement for the `containers` folder.

# Initialization

The entry point for a React application is in `src/index.js`. As with the other frameworks, this file contains the logic for mounting the application into the DOM:

```javascript
import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'

ReactDOM.render(<App />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
```

> **Aside
>
> By default, React will generate a skeleton [Service Worker](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API) file for you to register should your application require it. We do not cover service workers in this course, however they are an integral part for ensuring offline access in [Progressive Web Applications (PWA)](https://web.dev/).

This default starting point can be stripped down to an absolute minimum for further demonstration of the React basics:

```javascript
import React from 'react'
import ReactDOM from 'react-dom'

ReactDOM.render(
  <h1>Hello, World!</h1>,
  document.getElementById('root')
)
```

This will result in the HTML `h1` element being embedded into the DOM of the webpage where there is an element with an ID of "root".

# JSX

The above example clearly deviates from standard JavaScript in one particular way; the inclusion of code that looks like HTML:

```html
<h1>Hello, World!</h1>
```

React makes extensive use of a higher order JavaScript language called JSX. JSX is the standard way for React applications to "render" their components and typical HTML elements. The JSX code itself looks and acts similar to HTML, however React is almost purely a JavaScript framework. Unlike other frameworks which create templates for later interpolation, the elements that are created and rendered with JSX are compiled to pure JavaScript statements and not at all to HTML.

> **Caveat**
>
> Notice that `React` is being imported but not used anywhere? Whenever JSX is used in a JavaScript file, the `React` variable must be in scope within that file to interpret the JSX, even if the variable is not used for anything else.

One side effect of this major difference is a drastic simplification of the template-like concepts. Within JSX, any JavaScript statement can be included anywhere in the template within a single pair of curly braces (`{ }`). If the statement returns some value then that value is inserted into the DOM at that point in the template. Consider the following example:

```javascript
import React from 'react'
import ReactDOM from 'react-dom'
import MyImage from './path/to/img.png'

const greeting = name => `Hello, ${name}!`

ReactDOM.render(
  <div>
    <h1>{greeting('Foo')}</h1>
    <img src={MyImage} alt="my image!" />
  </div>,
  document.getElementById('root')
)
```

> **Caveat**
>
> When importing images using the ES6 module syntax, Webpack will automatically include the image in the final build artifact and produce an appropriate URI to provide to the `src` attribute.

JSX can also be easily composed, returned from functions and stored in typical JavaScript variables. Consider the following example:

```javascript
import React from 'react'
import ReactDOM from 'react-dom'
import MyImage from './path/to/img.png'

const greeting = name => <h1>Hello, {name}!</h1>

ReactDOM.render(
  <div>
    {greeting('Foo')}
    <img src={MyImage} alt="my image!" />
  </div>,
  document.getElementById('root')
)
```

This will produce a similar result to that of the above, albeit with a slightly different structure but the final result will look identical.