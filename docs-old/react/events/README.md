# Events

Props allow components to be passed variables from outside, and state allows components to keep their own variables based on some external source of input (i.e. outside of React). The remaining functionality necessary to build applications is to pass variables outside the current component to a parent. In React, this is done using events.

React defines two events of particular interest:

- `onClick` &mdash; this event will fire when a particular component, such as an anchor tag (`<a></a>`) or a button is clicked.
- `onChange` &mdash; this event fires when the value of the element changes. This is used typically for dropdown lists (`<select></select>`) and form inputs of all kinds.

Both of these events accept functions that will be called with an event object when the event occurs. React also supplies other events, and many libraries will defined their own [Custom Events](#custom-events) which may be subscribed to; these can be found in their relevant documentation.

> **Further Reading**
>
> The exact content of React Synthetic DOM events can be seen [here](https://reactjs.org/docs/events.html). They are mostly compatible with actual DOM events with some minor differences.

Simple events can be used as follows:

```javascript
function handleClick(e) {
  e.preventDefault()
  console.log('The link was clicked')
}

return (
  <a href="#" onClick={handleClick}>
    Click Me!
  </a>
)
```

When retrieving a value from the event, this can be done as follows:

```javascript
function handleChange(e) {
  console.log('The input value is:', e.target.value)
}

return (
  <input type="text" href="#" onChange={handleChange} />
)
```

In this example, the handler is executed once for every letter the user types into the form input. The value is passed to the handler as part of the event, as can be seen above.

> **Caveat**
>
> React Synthetic Events are short lived; their resources will be freed when the program reaches the end of the handler function, even if the handler is not asynchronous. This means that events cannot be passed around whole and must instead have pertinent information retrieved as soon as possible before continuing execution.

# Function Binding

In class-based components, methods can be used, but care must be taken when considering the context of the invocation. It is a common pattern to have event handlers be registered on a component as a method; if the event handler makes use of `this` inside the method then the context must be "bound" to have the intended behaviour.

```javascript
class Toggle extends Component {
  constructor(props) {
    super(props)
    this.state = { isToggleOn: true }

    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick() {
    this.setState(state => ({
      isToggleOn: !state.isToggleOn
    }))
  }

  render() {
    return (
      <button onClick={this.handleClick}>
        { this.state.isToggleOn ? 'ON' : 'OFF' }
      </button>
    )
  }
}
```

# Custom Events

In React, defining a custom event is as simple as passing a function to be called into a child component as a prop. If the event is defined and its context bound correctly then the function will be callable in the context of the component itself. Consider the following example:

```javascript
function MyParent (props) {
  return (
    <MyChild onSubmit={({ foo }) => console.log('FOO', foo)} />
  )
}

class MyChild extends Component {
  constructor (props) {
    super(props)
    this.state = { foo: '' }
    this.submit = this.submit.bind(this)
  }

  submit () {
    if (typeof this.props.onSubmit === 'function') {
      this.props.onSubmit(this.state)
    }
  }

  render () {
    return (
      <>
        <input type="text" onChange={e => this.setState({ foo: e.target.value })} />
        <button onClick={this.submit}>
          Submit
        </button>
      </>
    )
  }
}
```

In this example; the contents of the form input are kept within the `MyChild` component, until the user presses the "Submit" button. Once pressed, the callback function is invoked with the current component's state being passed in as a parameter. Thus, the parent component can then access the state at that point in time.