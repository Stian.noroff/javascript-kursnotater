# React Router

Frontend frameworks produce a class of application called _single page web applications_ (SPA) that run inside the browser. Once built into their final artifact, they comprise of exactly one `.html` file: `index.html`. Unlike static web content, they're not intended to navigate from one page to another. Instead, these applications introduce the concept of **Routers** which produce the illusion of navigating from one page to another.

React comes with many differnet routing options, all of which are maintained as separate packages published via NPM. The most popular of these packages is the React Router package, which can be installed with the following terminal command:

```bash
npm install react-router-dom --save
```

Unlike some other frameworks, React Router is further separated into many packages with different dependencies for depending on where the application is being deployed. For example, the `react-router-native` package is intended to be used with React Native, a framework for developing native cross-platform smartphone applications using React. Both `react-router-dom` and `react-router-native` rely on the `react-router` package which contains the common, core logic of the React Router framework. This common package is automatically included as a dependency of the other two and does not need to be included explicitly.

# How it works

The React Router package reads and manipulates the browser's [`history` (See MDN)](https://developer.mozilla.org/en-US/docs/Web/API/History) object. The history API controls the browser session history stack for that particular tab or frame but _does not cause any navigations_ when it is manipulated. When some state is pushed onto the stack, the history is modified and the URL bar will be updated accordingly, however the page described won't be loaded or even check that the page even exists. Router frameworks produce the expected effect of "navigation" within the SPA by subscribing to changes in the browser's history and displaying the corresponding view.

# Adding React Router

Everything in React is a component of some kind; the same is true for React Router. React Router uses what is called a ["Provider Pattern"](../patterns/README.md#context) to store state about which components or "routes" should be displayed. React Router can be registered on the application as follows:

```javascript
import React from 'react'
import {
  BrowserRouter as Router,
} from 'react-router-dom'

export default function App() {
  return (
    <Router>
      <h1>Hello, World!</h1>
    </Router>
  )
}
```

The inclusion of the `BrowserRouter` component (here aliased to `Router`), makes no visible changes to the application. The component is simply a container for the router state for subsequent `Route` components lower in the component hierarchy. From this point, components can be wrapped in `Route` components to be conditionally rendered based on the state of the `history` object.

# Rendering Routes

Routes in React Router are also components that will conditionally render the specified contents if the routing conditions are met. Consider the following example:

```javascript
import React from 'react'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'

export default function App() {
  return (
    <Router>
      <h1>Hello, World!</h1>
      <Route path="/foo">
        <h1>Foo</h1>
      </Route>
      <Route path="/foo/bar">
        <h1>Bar</h1>
      </Route>
      <Route path="/baz" exact>
        <h1>Baz</h1>
      </Route>
      <Route path="/baz/qux">
        <h1>Qux</h1>
      </Route>
    </Router>
  )
}
```

The above example will render `Hello, World!` regardless of the state of `history`; however if the current path starts with `/foo` then the Router will display the text `Foo`. This is the core mechanism of any routing framework and operates similarly to [Vue Router](../../vue/vue-router/README.md); however they are not identical.

> **Further Reading**
>
> There is another alternative to `BrowserRouter` called `HashRouter` which is not explicitly discussed here. More details can be found in the [official documentation](https://reacttraining.com/react-router/web/api/HashRouter).

If the current path starts with `/foo/bar`, it is logical to expect `Bar` to be displayed on the page; however where React Router differs from others is that `Foo` also remains in the display output. The output in question would look something like this:

```html
<h1>Hello, World!</h1>
<h1>Foo</h1>
<h1>Bar</h1>
```

This happens for two reasons:

1. When a path is specified on a Route, React Router will match anything that _starts with_ the specified path; thus a route path of `/foo` will match any paths in that hierarchy, as long as it starts with `/foo`. To disable this behaviour the `exact` flag may be specified to ensure that the path will not match its children as well.

    For the same example above, if the current path starts with `/baz`, then `Baz` to be displayed on the page. When the path changes to `/baz/qux` then `Qux` becomes visible on the page and `Baz` is removed. This is due to the `/baz` route being specified as `exact` which will no longer match.

2. Routes are designed to be completely independent of each other; each component in turn evaluates whether to display their contents based on the state of the `history` object. Most developers coming from other forms of web development find this concept counter-intuitive, **however this method enables embedded conditional rendering _anywhere_ on the page**, and not just in the main viewport. Routes can also be nested within other routes. For instances where it is desirable to display _only the first_ route that matches, React Router provides the `Switch` component.

# Switch

The `Switch` component is a simple component that wraps a set of `Route` components and will only render the first route whose path matches the current `history` state. Consider the following example:

```javascript
import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom'

export default function App() {
  return (
    <Router>
      <h1>Hello, World!</h1>
      <Switch>
        <Route path="/foo">
          <h1>Foo</h1>
        </Route>
        <Route path="/foo/bar">
          <h1>Bar</h1>
        </Route>
        <Route path="/baz" exact>
          <h1>Baz</h1>
        </Route>
        <Route path="/baz/qux">
          <h1>Qux</h1>
        </Route>
      </Switch>
    </Router>
  )
}
```

In this example, if the current path is `/foo` then the page will display `Foo` as normal, however it is worth noting that the remaining routes are not even evaluated. All logic is short-circuited once a match is made successfully.

When the current path is `/foo/bar`, the result is less intuitive; the page will still display `Foo` and not display both `Foo` and `Bar` as it did previously. The correct behaviour in this case would be to display only `Bar`, however it should be noted that the route for `/foo` is not declared as being `exact`. Although the route for `/foo/bar` would be the most correct, the route for `/foo` is still the first path to match, which satisfies the conditions of the `Switch` and stops further evaluation.

Conversely, if the current path is `/baz/qux`, the resulting page will correctly display `Qux`. The route for `/baz` is declared as `exact`, meaning that it will not match for the path `/baz/qux`, thus the next option is evaluated normally and the expected route is displayed.

There are many ways to deal with this particular problem; one could mark the offending route as `exact`, however this has other implications in how the routes are resolved. Another option which has less impact on the overall application behaviour is to adjust the order in which the routes are presented. Inside a `Switch`, it is a good idea to have more specific routes &mdash; those that are deeper or marked as `exact` &mdash; appear before those that are more general.

Consider the corrected form of the above example:

```javascript
import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom'

export default function App() {
  return (
    <Router>
      <h1>Hello, World!</h1>
      <Switch>
        <Route path="/baz" exact>
          <h1>Baz</h1>
        </Route>
        <Route path="/baz/qux">
          <h1>Qux</h1>
        </Route>
        <Route path="/foo/bar">
          <h1>Bar</h1>
        </Route>
        <Route path="/foo">
          <h1>Foo</h1>
        </Route>
      </Switch>
    </Router>
  )
}
```

The order of these components is justified as follows:

1. `Baz` is specified as `exact`; regardless of the depth of the path specification, all exact paths are equally specific.

2. Either `Qux` and `Bar` could appear next as both are non-exact routes with paths that are equally deep.

3. Whichever of `Qux` or `Bar` did not appear above will appear after.

4. `Foo` appears last as it is the least specific path in the set.

## Default Routing

A common pattern for switches is to have a final route specified with a very general path (i.e. `/`) that will catch any malformed URLs. This is how a "Page not found" error page could be displayed.

```javascript
<Switch>
  <Route path="/baz" exact>
    <h1>Baz</h1>
  </Route>
  <Route path="/baz/qux">
    <h1>Qux</h1>
  </Route>
  <Route path="/foo/bar">
    <h1>Bar</h1>
  </Route>
  <Route path="/foo">
    <h1>Foo</h1>
  </Route>
  <Route path="/" component={PageNotFoundView} />
</Switch>
```

# Navigation

As with other SPA routing frameworks, using anchor tags (`<a></a>`) will cause the user agent to navigate away from the current page instead of manipulating the `history` state to cause internal navigation. To provide a similar user experience, React Router provides `Link` and `NavLink` components that enable easy manipulation of the router state. Consider the following example:

```javascript
import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Link,
} from 'react-router-dom'

export default function App() {
  return (
    <Router>
      <nav>
        <ul>
          <li>
            <Link to="/foo">Show Foo</Link>
          </li>
          <li>
            <Link to="/foo/bar">Show Foo &amp; Bar</Link>
          </li>
          <li>
            <Link to="/baz">Show Baz</Link>
          </li>
          <li>
            <Link to="/baz/qux">Show Qux</Link>
          </li>
        </ul>
      </nav>
      {/* ... Routes ... */}
    </Router>
  )
}
```

The `Link` components will produce clickable "links" that behave similarly to anchor tags, but is tightly integrated with React Router. The only condition for `Link` components to work is they must be placed within the children of the `BrowserRouter`.

> **Hint**
>
> You can also pass a [location object](#location) into the `to` prop of a `Link`. This is useful to pass state without resorting to query parameters.

## NavLink

The `NavLink` component operates almost identically to the `Link` component with one exception. If the specified target path of a NavLink matches the current path in the router state then the `NavLink` will automatically apply the CSS class called `active` to itself. This provides a convenient way to provide feedback to the user as to which page they are currently viewing.

The rules for "matching" paths are the same as those [discussed above](#switch) in the context of routes. `NavLink` components also similarly have an `exact` prop that behaves in the same way as it does on a `Route` component.

# Rendering Components

The above examples of React Router's use all use one of the available render methods of displaying routed components; which is to include them as the child elements of the `Route` instance. Routes can render their contents in one of three ways:

1. `<Route>{components}</Route>`
2. `<Route component />`
3. `<Route render /> function`
4. `<Route children /> function`

> **Further Reading**
>
> For exact usage specifications, see the `Route` component [API reference](https://reacttraining.com/react-router/web/api/Route).

## Default Render

The default render method for routes is to simply include them as the children of `Route` component. In this case, if the path does not match that specified for the current route, then `null` is returned in the render method. When the path does match, the specified contents are mounted into the DOM and rendered. The examples of React Router thus far have used this method.

## Render Component

A route can be specified with a component from which takes a reference to a component type. When the current path matches the route then a new instance of the given component is created and mounted within. Consider the following example usage:

```javascript
<Route path="/foo/bar" component={MyComponent} />
```

Components mounted in this way will be initialized lazily (i.e. only when they are first needed), however they will not be reinitialized between different routings. If a previously inititialized component is set to be displayed by the route then the existing component is instead remounted into the virtual DOM. This helps make the routes more efficient, however it may also produce some unexpected behaviour in the component lifecycle (specifically regarding `componentDidMount`, `componentDidUpdate` and `componentWillUnmount`).

## Render Function

To avoid the remounting behaviour [described above](#render-component), a render function may be specified force a complete reinitialization of the route contents every time it is rendered. Consider the following example:

```javascript
<Route path="/foo" render={props => <MyComponent {...props} />}>
```

This is primarily used to produce [higher order components](../patterns/README.md#higher-order-components) such as the following:

```javascript
function AnimatedRoute({ component: WrappedComponent, ...rest }) {
  return (
    <Route {...rest} render={props => (
      <Animation>
        <WrappedComponent {...props} />
      </Animation>
    )} />
  )
}
```

Because this method will forcefully reinitialize components every time the route is updated, it will consume a lot of resources if not used sparingly. To avoid negatively impacting the user experience of the application, this method should only be used with [Pure or Presentational Components](../../frameworks/README.md#pure-components).

> **Caveat**
>
> Note that `component` takes precedence over `render`. These two render methods shouldn't be used at the same time (on the same `Route`).

## Render Children Function

Providing a function to the `children` prop on a route works similarly to the `render` function, however the function specified will be called regardless of whether the path matches or not. Otherwise it functions exactly the same as the `render` function.

The primary use of this render method is to adjust your UI based on whether the route matches or not. Consider the following example:

```javascript
<Route
  children={({ match, ...rest }) => (
    <Animate>
      {match && <Something {...rest}/>}
    </Animate>
  )}
/>
```

In this example we are always rendering the `Animate` component so that we can use component lifecycles to animate its child in and out of view.

> **Caveat**
>
> Note that `children` takes precedence over both `component` and `render`. These render methods shouldn't be used together on the same `Route`.

# Reading Router State

All of the above render methods produce props which will automatically be passed into the rendered component:

1. `history` &mdash; A reference to the page's history object for manipulating the navigation.

2. `location` &mdash; Contains current path information of the application and any additional state passed during navigation.

3. `match` &mdash; Contains information about how a `Route` matched a URL and any defined route parameters.

> **Further Reading**
>
> The exact specification of these props are discussed in the [official documentation](https://reacttraining.com/react-router/web/api/Route/route-render-methods).

## History

The history object is typically used to cause navigation events without explicit interaction from the user. This is typically done with the following methods:

- `history.push(path, [state])` &mdash; Push a new entry onto the history stack. This will cause all `Routes` that are affected to rerender.
- `history.goBack()` &mdash; Navigate to the immediate previous application route. Equivalent to clicking the "back" button on the browser.
- `history.goForward()` &mdash; Navigate to the immediate next application route, if one is available. Equivalent to clicking the "forward" button on the browser.
- `history.location` &mdash; An object that is described [in the next section](#location).

> **Further Reading**
>
> The full list of methods available and their descriptions can be seen [here](https://reacttraining.com/react-router/web/api/history).

Consider the following example use:

```javascript
<Route path="/foo" render={({ history }) => (
  <Button onClick={() => history.push('/bar')}>Navigate To Bar</Button>
)} />
```

## Location

The location object contains information that is read from the current application routing state; the object has the following form:

```javascript
{
  key: 'ac3df4', // not with HashHistory!
  pathname: '/somewhere',
  search: '?some=search-string',
  hash: '#howdy',
  state: {
    [userDefined]: true
  }
}
```

This is useful to get the exact route information on routes that have very general path specification; for example, if a route is specified with `path="/foo"` (without `exact`) then the route will still match if the actual path is `/foo/bar`. In this case, `location.pathname` will correctly reflect the current path as being `/foo/bar`.

## Match

The match object provides detailed information on _how_ a `Route` matched to the current URL. Match objects contain the following properties:

- `params` &mdash; An object containing path variables pulled from the URL. For example, a route that matches on `path="/:foo/:bar"` will produce a params object of the form `{ foo, bar }`.
- `isExact` &mdash; A boolean indicating whether the path matched exactly. This does not require the `exact` flag to be specfied on the route to be used.
- `path` &mdash; The path pattern that was used to match. This appears as it is specified on the `Route` component, and not with the actual current path as with `location.pathname`. This is useful for building nested routes.
- `url` &mdash; The matched portion of the URL. This is useful for building dynamic `Link` components.

> **Caveat**
>
> Sometimes accessing the router state from a component that is far removed from the `Route` component is necessary. To this end, any descendant of a `Route` component may be wrapped in the `withRouter` [higher order component](../patterns/README.md#higher-order-components) to have the router state variables described above injected into their props. Consider the following example:
>
>    ```javascript
>    import { withRouter } from 'react-router-dom'
>
>    // A simple component that shows the pathname of the current location
>    function ShowTheLocation ({ match, location, history }) {
>      return <div>You are now at {location.pathname}</div>
>    }
>
>    const ShowTheLocationWithRouter = withRouter(ShowTheLocation)
>    ```

# Hooks

React Router has recently been extended to include a number of hooks to support the new functional style of writing components. As follows convention, the hooks are named the following:

- `useHistory`
- `useLocation`
- `useParams` (Only the `params` property of the `match` object)
- `useRouteMatch` (The remaining properties of the `match` object)

These hooks correspond to behaviour that is described above and will not be repeated here. These hooks are discussed in detail in the [official API documentation](https://reacttraining.com/react-router/web/api/Hooks). Hooks as a concept are [discussed in a previous chapter](../hooks/README.md).