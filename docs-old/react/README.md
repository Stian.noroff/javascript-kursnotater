# React

> Module Overview

## Lessons

1. [**Basics**](./basics/README.md)

    &nbsp;

1. [**Components**](./components/README.md)

    &nbsp;

1. [**Hooks**](./hooks/README.md)

    &nbsp;

1. [**Events**](./events/README.md)

    &nbsp;

1. [**React Router**](./react-router/README.md)

    &nbsp;

1. [**Advanced Patterns**](./patterns/README.md)

    &nbsp;