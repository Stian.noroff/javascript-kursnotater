# Components

All elements of a React application are components, even services, or logic that does not display anything. This is a requirement of the React engine as only a component can cause an update to another component; even if indirectly.

At their simplest, a Component is simply a function that accepts `props` &mdash; which may be thought of as HTML attributes &mdash; and produces some kind of JSX output. Consider the following example:

```javascript
import React from 'react'

function Welcome(props) {
  return (
    <h1>Hello, { props.name }!</h1>
  )
}
```

Most uses of React since the introduction of the [Hooks API](../hooks/README.md) use functional components like the one above; however there are sometimes legitimate reasons to consider the alternate form: class-based components. Function components and class-based components both produce identical components given identical logic; neither is more correct than the other but both have places where they are easier to use than the other. [There are no plans to deprecate class based components in favour of hooks in the forseeable future.](https://reactjs.org/docs/hooks-intro.html#gradual-adoption-strategy) Consider the following example, which contains an equivalent component to the one described above.

```javascript
import React, { Component } from 'react'

class Welcome extends Component {
  render () {
    const { name } = this.props

    return (
      <h1>Hello, { name }!</h1>
    )
  }
}
```

> **Caveat**
>
> Remember, anywhere JSX is used requires the `React` object to be present.

Both of these components described above may be rendered as follows:

```javascript
ReactDOM.render(
  <Welcome name="Greg" />,
  document.getElementById('root'),
)
```

Components may also refer to other components in their outputs, allowing application logic to be abstracted for any level of detail. Consider the following example:

```javascript
function Welcome(props) {
  return <h1>Hello, { props.name }!</h1>
}

function App() {
  return (
    <div>
      <Welcome name="Greg" />
      <Welcome name="Dewald" />
      <Welcome name="Craig" />
    </div>
  )
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
)
```

# Fragments

One strict rule for React components is that the render function of individual components must return exactly one other component. While this requirement is easy to work around by simply wrapping a list of components in a `<div>` or a `<span>`, this may cause complications as those components become rendered in the DOM. Fragments were added to React to provide a kind of invisible component that does not get rendered in the DOM, while still being able to wrap and contain other components. Consider the following example:

```javascript
function App() {
  return (
    <>
      <Welcome name="Greg" />
      <Welcome name="Dewald" />
      <Welcome name="Craig" />
    </>
  )
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
)
```

In this example a fragment is being used to return three other elements which will be mounted at the root level where `App` is rendered. This example will produce the following HTML:

```html
<div id="root">
  <h1>Hello, Greg!</h1>
  <h1>Hello, Dewald!</h1>
  <h1>Hello, Craig!</h1>
</div>
```

Any non-essential components &mdash; i.e. those that are not contributing to the structure of the resulting page &mdash; can be removed and replaced with fragments to simplify the styles of the application.

# Passing Props

A common pattern in React applications is for state to be stored in a [stateful "smart components"](../../vue/components/#smart-components), referred to as containers, which then pass small pieces of the state to child components for further processing and display. This state is typically passed as part of the component's props. Any valid JavaScript value may be passed to a component as props and JSX provides some special syntax to help with this. Consider the following examples:

Components can accept props as per the HTML attribute syntax (seen above):

```javascript
const element = <Welcome name="Foo" />
```

For values that are not strings, the value can be enclosed in curly braces (`{}`):

```javascript
const number = <Welcome name={42} />
const boolean = <Welcome name={true} />
```

These will pass the values as their intended type without converting them to strings as would be necessary in an HTML attribute. This same syntax will also work for Strings:

```javascript
const string = <Welcome name={"Foo"} />
```

The same syntax can also be used to pass JavaScript functions, objects and arrays:

```javascript
function Welcome(props) {
  const { name } = props.profile

  return (
    <h1>Hello, { name }!</h1>
  )
}

const element = <Welcome profile={{ name: 'Foo' }} />
```

> **Aside**
>
> This same technique may also be applied to specify inline styles for a particular component. See [here](https://reactjs.org/docs/dom-elements.html#style) for more information.

Any arbitrary statement may be placed inside the curly braces (`{}`) and will be evaluated to pass the result as the named property.

Objects can also be destructured onto the component props:

```javascript
function Something(props) {
  const { foo, bar } = props

  return (
    <>
      <p>{foo}</p>
      <p>{bar}</p>
    </>
  )
}

const props = {
  foo: 'baz',
  bar: 'qux',
}

const element = <Something {...props} />
```

## Props are Read-Only

Props passed into a component can never be modified; instead those values should be modified at their source. Functional programming has the concept of ["purity"](https://en.wikipedia.org/wiki/Pure_function) where "pure functions" have do not change their inputs (also known as not having "side effects") and for the same inputs they produce the same output. For React components, the strict rule is that all components should act like pure components with respect to their props.

> **Further Reading**
>
> This concept is discussed in the React [documentation](https://reactjs.org/docs/components-and-props.html#props-are-read-only).

# State

In addition to props, Components may also store state. The mechanism for doing so is different for class-based and functional components. Functional components use the newer [Hooks API](../hooks/README.md) which will be described later.

Class-based components have a method called `setState()` which allows them to update their local state variable and causes the component to rerender. The initial value of the component state can be set in the constructor. Consider the following example:

```javascript
class Clock extends Component {
  constructor (props) {
    // Necessary
    super(props)

    this.state = { date: new Date() }
  }

  render () {
    const { date } = this.state

    return (
      <div>
        <h2>It is {date.toLocaleTimeString()}.</h2>
        <button onClick={() => this.setState({ date: new Date() })}>Update</button>
      </div>
    )
  }
}
```

> **Caveat**
>
> Note that we pass `props` to the super constructor of Component; this is necessary and React will complain if you don't do this.

The state of a component must be updated with `setState` so that the React engine will know to run the lifecycle of the component. Changing the value of the current state by assignment will cause errors and undefined behaviour in React applications. The above component will render with a button that will update the state with the current time when pressed.

> **Caveat**
>
> A common mistake that is made by novice React developers is to cause a state update directly inside logic that is called when a component is rendered. If the state is updated inside of the render function this will cause an **infinite loop** where the component is continuously rerendered. State updates should generally only be triggered in response to external events such as loading data from an external service or processing user input.
>
> In the above component, the state update is only triggered when the button is pressed which is acceptable.

# React Component Lifecycle

When React first wishes to display a component, an instance of the component is created using the component constructor. The component then begins the process of being registered in the React engine, which ends with the component being mounted into the React virtual DOM. During this process, the component goes through a number of phases or states before it is finally mounted. Each phase has methods that will "hook into" the React component lifecycle; where those methods will be invoked during the corresponding phase. We have already used one of these methods; `render` is the only required lifecycle hook for all components and it is the last method that is called as part of the "render phase". The React component lifecycle is summarised in the following diagram:

<figure>
    <center>
        <img src="./react_lifecycle.png" alt="react component lifecycle" width="650"/>
        <figcaption>Figure: React Component Lifecycle</figcaption>
        <br/>
    </center>
</figure>

The above diagram contains a large amount of information. Some important features are noted below:

1. The `constructor` is only invoked at the start of the component life span and never again. During the constructor invocation, the component does not yet have any relationship with the React virtual DOM. For this reason, the constructor is treated as an exception when assigning directly to the object itself; the initial state may be set using the assignment operator because no other components depend on the React engine to notify them of changes yet. For the same reason, this is also an inappropriate place to invoke calls to asynchronous APIs; without being mounted into the virtual DOM, there will be no way for this component to notify dependent components of any changes in state.

2. The `getDerivedStateFromProps` function is an _optional_ lifecycle method that consumes the incoming props and initial or updated state to produce a new state object based on those inputs. While component props are always immutable, it is possible to use this function to assign derived values as local component state. This is useful for memoization of complex calculated variables.

3. The `render` function is mandatory for all components, even if to produce an empty response. The render function must return some valid JSX for the component to be displayed in the virtual DOM. Render is also special compared to other lifecycle methods because it is directly related to the performance and reactivity of the whole React application. Render must not directly depend on any asynchronous actions to produce its output because that will cause the application execution to be blocked while those actions are resolved. Additionally, render must not directly invoke an update to the component state, as this will trigger a rerender and result in an **infinite loop** of component updates.

4. The `componentDidMount` function is an _optional_, open-ended component lifecycle hook that is executed after the component is ready to receive input. The method is open-ended, meaning that there are no functions executed after it; this means that there is no critical logic following this lifecycle hook that would be blocked by asynchronous actions. **Asynchronous actions and subscription logic that must run as soon as possible should be invoked here.**

5. The `shouldComponentUpdate` function is an important lifecycle method for which most components rely on its default behaviour. React components will trigger an update if either the component props (from its parent) change or the component state is updated with `setState()`. Complex components can consume a lot of resources when they update, so unnecessary updates should be minimised. In the event that the props or component state change in a way that would not strictly require a rerendering of that component, this method can be overriden to return `false` to stop the render function from being called.

6. The `getSnapshotBeforeUpdate` function is an _optional_, read-only utility function that allows the virtual DOM to be read before the differences between that that and the actual DOM are reconciled. This is typically used by advanced component caching techniques and will not be discussed further in this course.

7. The `componentDidUpdate` function is an _optional_, open-ended component lifecycle hook that is executed after the component is ready to receive input following an update. This method is also typically used to trigger asynchronous actions.

8. The `componentWillUnmount` function is an _optional_, open-ended component lifecycle hook that executes just before the resources held by a component are freed. This is typically used to undo any subscriptions started following the component mount procedure and to otherwise gracefully stop processes used by this component.

# Adding Lifecycle Methods

Defining logic to take advantage of these lifecycle events is as simple as declaring a method with the corresponding name; consider the following example:

```javascript
class Clock extends Component {
  constructor (props) {
    super(props)

    this.state = { date: new Date() }
  }

  componentDidMount () {
    // Executes the given function every ~1000ms
    this.updateTimer = setInterval(() => {
      this.setState({ date: new Date() })
    }, 1000)
  }

  componentWillUnmount () {
    // Stops the timer
    clearInterval(this.updateTimer)
  }

  render () {
    const { date } = this.state

    return (
      <div>
        <h2>It is {date.toLocaleTimeString()}.</h2>
      </div>
    )
  }
}
```

The component lifecycle executes normally and will consult the component instance for methods that match the name of the current lifecycle event. If a function with the appropriate name (as described above) is defined then that function is called.

> **Caveat**
>
> Rendering the above component will have the desired effect of showing the text representation of the current time on the page and will update approximately every second (1000ms). When the component is removed by some upstream logic, the timer is gracefully cancelled and will no longer attempt to make calls to an instance that has already had its resources deallocated. **Failure to gracefully deregister callbacks when the component is being unmounted will lead to catastrophic errors.**