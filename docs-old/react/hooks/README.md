# Hooks

Hooks are a recent addition to React that allow the use of state and component lifecycle methods in functional components. In React, there are two main hooks: `useState` and `useEffect`; from which more advanced, custom hooks can be built.

> **Further Reading**
>
> This section briefly summarises the use of hooks in React applications; however, the React documentation contains [a separate section dedicated to the use of hooks](https://reactjs.org/docs/hooks-intro.html). It is highly recommended to read this and experiment further.

# `useState` Hook

The `useState` hook produces a state object and update method that are identical to `this.state` and `this.setState` inside class-based components. To discuss the use of the state hook, we will consider the following example:

```javascript
class Clock extends Component {
  constructor (props) {
    super(props)
    this.state = { date: new Date() }
  }

  render () {
    const { date } = this.state

    return (
      <div>
        <h2>It is {date.toLocaleTimeString()}.</h2>
        <button onClick={() => this.setState({ date: new Date() })}>Update</button>
      </div>
    )
  }
}
```

The creation of the `date` state value is a prime target to be rewritten as a functional component to improve legibility. Component state variables may be declared with the `useState` hook inside a functional component as follows:

```javascript
const [stateVariable, stateUpdateMethod] = useState(initialValue)
const [date, setDate] = useState(new Date())
const [num, setNum] = useState(42)
const [obj, setObj] = useState({ foo: 'bar' })
```

The state can contain a value of any kind; whatever is passed in as the `initialValue` and to the `stateUpdateMethod` is what becomes available as the `stateVariable` after the component lifecycle is complete. The example class-based component above can be rewritten to be a functional component using the `useState` hook:

```javascript
import React, { useState } from 'react'

function Clock (props) {
  const [date, setDate] = useState(new Date())

  return (
    <div>
      <h2>It is {date.toLocaleTimeString()}.</h2>
      <button onClick={() => setDate(new Date())}>Update</button>
    </div>
  )
}
```

These two forms of the `Clock` component are written differently, however the resulting components they produce are near identical &mdash; with particular reference to how they are treated inside the React engine. Although slightly unintuitive, this functional component will not redeclare the `date` variable each time the function is called when the component is rendered. Instead, when the component is mounted, the hook-related logic will be removed from the render logic for the component. The newer functional syntax hides the details surrounding the definition and maintenance of the state variable declared with the `useState` function.

> **Caveat**
>
> It is important to note that the components above are _almost identical_; just because a functional component _looks_ like it has fewer moving parts does not mean it is automatically any more performant than a class-based component.

&nbsp;

> **Further Reading**
>
> `useState` is discussed in detail in the relevant [API documentation page](https://reactjs.org/docs/hooks-state.html).

# `useEffect` Hook

The `useEffect` hook is a single hook function that merges the behaviour of `componentDidMount`, `componentDidUpdate`, and `componentWillUnmount`. This hook allows a callback function to be called after React has commited current changes to the DOM; which includes both when the component is first mounted and when the component is updated. Effects may also optionally specify how they are cleaned up, which corresponds to the behaviour that would normally be included in `componentWillUnmount`. Consider the following example:

```javascript
import React, { useState, useEffect } from 'react'

function FriendStatus(props) {
  const [isOnline, setIsOnline] = useState(null)

  const handleStatusChange = status => setIsOnline(status.isOnline)

  // Runs when the component is mounted or updates
  useEffect(() => {
    ChatAPI.subscribeToFriendStatus(props.friend.id, handleStatusChange)

    // Runs when the component is unmounted
    return () => {
      ChatAPI.unsubscribeFromFriendStatus(props.friend.id, handleStatusChange)
    }
  })

  if (isOnline === null) {
    return 'Loading...'
  }

  return isOnline ? 'Online' : 'Offline'
}
```

> **Further Reading**
>
> `useEffect` is discussed in detail in the relevant [API documentation page](https://reactjs.org/docs/hooks-effect.html).

# Custom Hooks

Customs hooks can be defined as a way to reuse stateful logic without using [Higher Order Components](../patterns/README.md#higher-order-components). In the above `useEffects` example, a listener for `FriendStatus` updates could be used across many different components if refactored into a custom hook. Consider the following example:

The first step is to extract the subscription logic into a custom hook called `useFriendStatus`:

```javascript
import React, { useState, useEffect } from 'react'

function useFriendStatus(friendID) {
  const [isOnline, setIsOnline] = useState(null)

  function handleStatusChange(status) {
    setIsOnline(status.isOnline)
  }

  useEffect(() => {
    ChatAPI.subscribeToFriendStatus(friendID, handleStatusChange)
    return () => {
      ChatAPI.unsubscribeFromFriendStatus(friendID, handleStatusChange)
    }
  })

  return isOnline
}
```

Now the custom hook can be used in many different components:

```javascript
function FriendStatus(props) {
  const isOnline = useFriendStatus(props.friend.id)

  if (isOnline === null) {
    return 'Loading...'
  }
  return isOnline ? 'Online' : 'Offline'
}
```

```javascript
function FriendListItem(props) {
  const isOnline = useFriendStatus(props.friend.id)

  return (
    <li style={{ color: isOnline ? 'green' : 'black' }}>
      {props.friend.name}
    </li>
  )
}
```

> **Caveat**
>
> Note that custom hooks reuse stateful logic, but do not reuse the state variables themselves; independent instances of the components using the hooks will each have their own copies of the `isOnline` state variable declared.

&nbsp;

> **Further Reading**
>
> Custom hooks are discussed further in the relevant [API documentation page](https://reactjs.org/docs/hooks-custom.html).

# Other Hooks

React provides many hooks without requiring additional libraries. These can be seen in the [Hooks API Reference](https://reactjs.org/docs/hooks-reference.html) documentation. These include &mdash; but are not limited to &mdash; the following:

- `useContext` &mdash; The Hooks equivalent to the Context API discussed in the [next section](../patterns/README.md#context).

- `useReducer` &mdash; An alternative to `useState` that accepts a reducer function of type `(state, action) => newState`. This concept is used extensively in [Redux](https://redux.js.org/).

- `useMemo` &mdash; [Memoizes](https://en.wikipedia.org/wiki/Memoization) a value generated from a computation function with respoect to a set of dependencies. If the parent component renders with the same variables (dependencies) twice, the value returned from the initial call will be returned instead of invoking the callback itself. This can be used to avoid doing expensive computations unnecessarily.

- `useCallback` &mdash; `useCallback(callback, deps)` is the equivalent of `useMemo(() => callback_fn, deps)`.

- etc.