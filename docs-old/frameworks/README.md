# JavaScript Frontend Frameworks

Frontend web development has progressed significantly since the days of using jQuery to subscribe to events and manipulate the DOM. Modern JavaScript frameworks such as React, Vue.JS and Angular are faster, more efficient, more idiomatic, more pluggable, provide a higher optimised resource usage and provide more features than jQuery or backend template engines ever did. These frameworks, and many others, all evolved from a growing demand for a more application-like development experience for the web; a concept that was eventually formalised into [progressive web applications](https://developers.google.com/web/progressive-web-apps).

> **Further Reading**
>
> [Progressive Web App Checklist (Google)](https://developers.google.com/web/progressive-web-apps/checklist)

# Trends

According to the [_StackOverflow Developer Survey of 2019_](https://insights.stackoverflow.com/survey/2019#most-popular-technologies), the most popular frontend web frameworks for building single-page applications among professional web developers (~55000 respondents) were:

- jQuery, with **48.3%** of the respondents indicating active use.
- Angular and Angular.JS, with **32.4%** of respondents indicating active use.
- React.JS, with **32.3%** of respondents indicating active use.
- Vue.JS, with **15.5%** of respondents indicating active use.

> **Aside**
>
> While unrelated to frontend technologies, ASP.NET (.NET Core), Express.JS and Java Spring were also mentioned here with **27.2%**, **19.5%** and **17.2%** of respondents indicating active use respectively.

In the same survey, when asked [which frameworks they _most loved_](https://insights.stackoverflow.com/survey/2019#technology-_-most-loved-dreaded-and-wanted-web-frameworks) &mdash; meaning they have expressed an interest in _continuing_ to develop in that framework &mdash; the respondents indicated the following:

- React was the most loved framework web framework with **74.5%**.
- Vue appeared very close behind React with **73.6%**. Vue also took first place as the framework with the fastest growing userbase.
- Angular and jQuery fell toward the bottom of the list with **57.6%** and **45.3%** respectively.

jQuery and Angular also topped the list of the most dreaded web frameworks &mdash; meaning those who currently work with the technology but have expressed an interest in _not continuing_ to do so &mdash; with **54.7%** and **42.4%** respectively.

In terms of interest in learning new technologies, as always, React, Vue and Angular topped the list with **21.5%**, **16.1%** and **12.2%** of respondents expressing a desire to learn to develop with those frameworks respectively.

> **Aside**
>
> One other new frontend framework that deserves an honourable mention for their rapid growth is [Svelte](https://svelte.dev/), however they are not yet considered for this course as they do not have sufficient market share.

## The Big Three

React, Vue, and Angular are the clear winners of the race for most popular web frameworks and thus are the chosen frameworks which are explored as part of this course. jQuery is also included owing to the fact that it remains at the top of the list, being the primary technology used before SPA JavaScript frameworks and therefore a large part of maintaining legacy systems. Observation of the search trends for these four frameworks tells an interesting story:

<figure>
    <center>
        <img src="./GoogleWebTechTrends5y.png" alt="google web tech trends" width="900"/>
        <figcaption><a href="https://trends.google.com/trends/explore?date=today%205-y&q=%2Fm%2F012l1vxv,%2Fg%2F11c6w0ddw9,%2Fm%2F0268gyp,%2Fg%2F11c0vmgx5d">Figure: Google Global Trends for React, Vue, Angular and jQuery over five years</a></figcaption>
        <br/>
    </center>
</figure>

Over the last five years of web development, there have been some clear observable trends in web development; the most notable of which is the continuous decline in use of jQuery. As new applications are created and old applications rewritten or otherwise replaced, jQuery finds itself in less demand than ever before. Conversely, Angular and Vue remain at a strong constant and React shows clear and continuous growth over the last five years.

Regardless of the framework, there is also a notable drop in searches related to these frameworks between Christmas and New Year's Eve every year.

# Framework Philosophy

JavaScript, HTML5 and CSS3 are the core technologies behind modern web applications and modern web frameworks can be classified on a scale of their level of integration between JavaScript and HTML. On this scale, React and Angular appear at opposite ends with contradictory development philosophies:

> Figure: JavaScript-HTML Integration Scale

In the above figure:

- Angular takes the hard line of defining web applications in terms of HTML templates and binding JavaScript functionality within those templates with specific attributes. This takes the procedural, event-based model used with jQuery and improves on it by binding parts of the HTML within the application to a specific object in an object-oriented fashion. It is the HTML that requires and invokes the corresponding JavaScript; we refer to this as being a **"JavaScript in HTML"** approach.
- React does the exact opposite and instead defines applications directly in terms of components,  written in JavaScript, whose purpose are to _generate_ the HTML that is necessary for displaying to the user. React components are also highly object-oriented and all elements that are displayed in the application are instances of JavaScript classes within a "Virtual DOM" maintained by React. When writing display logic in React, a JavaScript-based language called JSX is used to emulate the look of HTML while retaining programming concepts of JavaScript such as variable scope and higher-order functions; we refer to this method of frontend development as being an **"HTML in JavaScript"** approach.
- Vue.JS sits passively in the middle, between these two extremes and offers the user the opportunity to use either approach. By default, Vue is configured to build up a virtual DOM of components, each of which have their own templates which are written in HTML. When the components are prompted to render, the templates are then interpolated (populated with data) and written to the DOM to be displayed to the user. Vue.JS reintroduces some of the ease of working with template engines while still remaining a powerful, pluggable frontend framework that is optimised for a generative, object-oriented approach to web application development.

# Components

Despite the obvious differences in the way the different frontend frameworks work behind the scenes, they all have one obvious point of commonality: the abstract component.

Composable components bring object-oriented programming to web frameworks by creating a hierarchical structure of JavaScript objects that mirrors the structure of HTML elements. Where components differ significantly from a browser's DOM event subscription mechanic used by jQuery, is that components contain all the display logic (HTML), program logic (JS), display logic (CSS) and other components that pertain to how that component is intended to be used. Components are class-like structures that accept input, store state and produce output. This is a hard contrast to web development of the previous decade, which mostly featured, global state and monolithic definitions of style, structure and interaction logic.

Use of components in web applications tends to conform to a common pattern, where components are categorized into two varieties: [Pure Components](#pure-components) and [Smart Components](#smart-components).

## Pure Components

Pure Components (also sometimes called "Dumb Components") are named after the functional programming concept of purity. Pure functions are written deterministically, such that given the same input multiple times, they should always produce the same output. Components written in this pattern accept data as parameters and exclusively contain logic related to the display and style of the resulting HTML elements, as well as passing user input back to the [smart components](#smart-components).

One of the major benefits offered by the functional purity of the component is to easily determine whether a component needs to be updated or not. If the inputs have not changed, then no update is required, which means that resources that would have been wasted on such an update can instead go toward servicing smart components instead. The separation of concerns offers a significant improvement in the efficiency of web applications.

In Vue and React, these are simply called "components" but are often referred to as being "pure" or "dumb" components to distinguish them from the other type. In Angular these are also simply referred to as "components", however the concept of a "smart component" is called a "service" so the word is not as overloaded as with React and Vue.

## Smart Components

Smart Components are so called because they contain all the application state and API interaction logic for the application, none of the display logic and event handlers are defined to handle data gathered from user input. Once data eventually becomes available, the smart components invoke and _contain_ many [pure components](#pure-components) to handle the rendering thereof. When considering the HTML structure of a rendered web page, these smart components are often completely invisible.

In React, smart components are often referred to as "containers" because they "contain" state as well as other components. In Angular, components are defined and expected to handle display logic (as per "pure components"), whereas "services" are defined to handle state and API interaction. Services in Angular are a more formalized kind of "headless" component which forces the separation of display logic and API interaction logic, whereas in React and Vue, what separates the components is only convention.

# Client-side Applications

Another point of commonality with frontend frameworks is that, unlike template engines like Razer, Django and Mustashe.JS, the big three frontend frameworks are all client-side applications that contain the logic necessary to populate themselves with data from an external source.

Each of these applications are written, bundled into a build artifact and served from a web service. When a client connects to the web service, they begin by first downloading the frontend application and starting to execute it. Once loaded and running, the application then interacts with the external source (i.e. a REST API) using subsequent requests to fetch some data to display to the user.

<figure>
    <center>
        <img src="../web/express/WebService.png" alt="web service" width="700"/>
        <figcaption>Figure: Web Service</figcaption>
        <br/>
    </center>
</figure>

There are two results of interest with this model of web application:

1. Web services are highly efficient at serving static files with minimal overhead costs. The efficiency of downloading the build artifact can be further optimised with proxy caching and content delivery networks, as the application logic itself is not expected to change very often.
2. Interpolating data into a template is a highly expensive operation. In the case of backend-rendered template engines, it is the responsiblity of the server to interpolate data into web pages before serving them to the client. The resulting web pages are much larger, placing more strain on the network connection between the server and the client, and the interpolation takes considerable processing time, resulting in longer wait times for clients.

    By contrast, a frontend web application which interpolates all data on the client side makes use of dedicated resources (i.e. the user's phone or computer) to accomplish the same thing resulting in more responsive applications. Servers only have to be concerned about business logic and loading data from a database, resulting in a more optimised server load and therefore reduced server costs. The amount of data that is being sent between server and client is reduced, resulting in faster load times.

While frontend web applications are more complicated to use than a simple template engine on a backend service, the improved efficiency coupled with reduced costs and more features offered by [Vue](../vue/README.md), [Angular](../angular/README.md) and [React](../react/README.md) make the effort worth it.