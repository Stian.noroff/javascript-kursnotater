# Projects

The developers of Vue.JS pride themselves on providing a framework that is a "drop-in solution" that can be used alongside any others. This is both accurate and useful in terms of migrating existing projects from legacy frameworks (i.e. jQuery), however for larger, more complicated projects, some additional structure and integration with a toolchain is required.

For these cases, Vue also provide a Command Line Interface (CLI) interface for creating and managing Vue projects.

> **Further Reading**
>
> [Getting Started with Vue CLI](https://cli.vuejs.org/guide/)

# Installation

Before using the CLI, it must be installed globally. This is achieved as follows:

```bash
npm i -g @vue/cli @vue/cli-service-global
```

This will install the base CLI tool for generating projects as well as the Webpack Development Server for rapid prototyping. The latter package integrates with the former so no further configuration is necessary.

> **Caveat**
>
> These notes assume a Linux environment and commands may not be compatible with Windows environments. When encountering problems of this nature, Windows users are encouraged to search for solutions with their chosen search engine.

You can check that the installation is working by running:

```bash
vue --version
```

If the above fails then you can also try prepending `npx` to the command:

```bash
npx vue --version
```

If the command works with `npx` but not without then this means that your Node.JS global cache has not been added to your OS' `PATH` variable correctly. Consult [this article](https://stackoverflow.com/questions/18383476/how-to-get-the-npm-global-path-prefix) for more details.

# CLI

To create a new project, run:

```bash
vue create <project-name>

# i.e.
vue create hello-world
```

Your terminal will begin to prompt you for information; firstly to select a template, and later to select which technologies should be applied. For this course, the default template (`default (babel, eslint)`) is recommended, as well as the following technologies:

- Babel &mdash; The Vue component transpiler. Required.
- TypeScript &mdash; Optional for those who enjoy statically typed languages (this is only discussed in this course later in terms of [Angular](../../angular/README.md)).
- Router &mdash; For client-side routing and creating logical "views" inside your single-page web application. Not recommended while learning the framework basics but recommended for everything thereafter.
- CSS Pre-processors &mdash; CSS is not explicitly covered as part of this course but pre-processors such as SCSS or LESS make working with it much easier. For candidates' own experimentation.
- Linter / Formatter &mdash; When working on a project with a team of developers, linting is a good way to ensure the same standardised style between multiple programmers. This is recommended but will probably be considered an annoyance until one experiences working on badly styled production code.
- Unit Testing / E2E Testing &mdash; Unfortunately not covered as part of this course. For candidates' own experimenation.

Generating a project with only _Babel_ and _Linter / Formatter_ will create the following directory structure:

```
./hello-world
├── README.md
├── babel.config.js
├── package-lock.json
├── package.json
├── public
│   ├── favicon.ico
│   └── index.html
└── src
    ├── App.vue
    ├── assets
    │   └── logo.png
    ├── components
    │   └── HelloWorld.vue
    └── main.js

4 directories, 10 files
```

The README contains instructions for how to get the project serving initially. This is usually as simple as running the following commands:

```bash
# install dependencies (once)
npm install

# start webpack dev service
npm start
```

> **Caveat**
>
> Don't forget to update your `README.md` with information that is relevant to your project. The default seldom gives a good first impression of a project.

# Folder Structure

There are two folders in the project root: `src` and `public`. The former contains all the source code for the application and assets that will be bundled by Webpack; including CSS, images and other content. The latter contains all assets that are _not_ going to be processed by Webpack, and are instead copied over to the final build output as they appear in the folder itself. Generally most project files belong inside the `src` directory.

The `main.js` file inside `src` is the entry point for the Vue application. It is here where `new Vue(...)` is called and mounted into the designated element in `public/index.html`. All other files, including all Vue components are imported using the ES6 module syntax starting from this point onward.