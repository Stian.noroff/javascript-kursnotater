# Vue

> Module Overview

## Lessons

1. [**Getting Started**](./basics/README.md)

    &nbsp;

1. [**Directives**](./basics/README.md)

    &nbsp;

1. [**Projects**](./projects/README.md)

    &nbsp;

1. [**Components**](./components/README.md)

    &nbsp;

1. [**Vue Router**](./vue-router/README.md)

    &nbsp;