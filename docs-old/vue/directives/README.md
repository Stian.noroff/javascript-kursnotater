# Directives

In Vue.JS, directives are used inside HTML templates to instruct Vue how the DOM should be manipulated. There are five types of directives, four of which are discussed here:

- Attribute Binding Directives
- Conditional Directives
- Looping Directives
- Events
- Two-Way Binding

Custom events and passing data between components will be discussed [elsewhere](../components/README.md#custom-events).

# Binding Attributes

While the [mustache syntax](../basics/README.md#templating) enables embedding variables into an HTML template, it fails when attempting to interact with HTML attributes. To bind variables to attributes, we prefix the name of the attribute with `v-bind`; this instructs Vue to treat the value of the attribute as the name of the variable to be bound.

```html
<div id="app">
  <span v-bind:title="message">
    Hover your mouse over me for a few seconds
    to see my dynamically bound title!
  </span>
</div>
```

This will produce the following initial HTML:

```html
<div id="app">
  <span title="Hello, World!">
    Hover your mouse over me for a few seconds
    to see my dynamically bound title!
  </span>
</div>
```

The `v-bind` directive is one of many directives that Vue introduces to inform the template engine how the DOM should be manipulated. The next most common forms of directive introduced by Vue are for conditionals and loops.

# Conditional Directive

A variable supplied to the template can be used to toggle the presence of that element and its children within the DOM. The directive to achieve this is: `v-if`. Consider the following example:

```html
<div id="app">
  <span v-if="seen">Hello, World!</span>
</div>
```

```javascript
var app = new Vue({
  el: '#app',
  data: {
    seen: true
  }
})
```

As `seen` is set to `true`, the contents of the `span` element are displayed to the user, however if `seen` is set to `false`, then that element is removed.

> **Aside**
>
> Unlike with jQuery, Vue and other frameworks don't simply hide parts of the application that aren't being displayed; those parts of the application don't get initialized and added to the DOM at all until they are required to be displayed.
>
> The exact behaviour for this is configurable meaning it is possible to preload components into the DOM before they are displayed if that is the desired behaviour.

# Loop Directive

Vue can also create elements dynamically for each item in a collection. Consider the following example:

```html
<div id="app">
  <ol>
    <li v-for="todo in todos">
      {{ todo.text }}
    </li>
  </ol>
</div>
```

In this template, the `v-for` directive is used to specify that multiple instances of the given element should be rendered; once for ever element of the collection called `todos`. In the directive, the value `todo` is declared as being the current element within the collection (as per a [`for ... of` loop](../../javascript/flow-control/README.md#for--of-loop)). The value of `todos` is supplied normally as part of the `data` option.

```javascript
var app = new Vue({
  el: '#app',
  data: {
    todos: [
      { text: 'Learn JavaScript' },
      { text: 'Learn Vue' },
      { text: 'Build something awesome' }
    ]
  }
})
```

The resulting HTML after interpolation might look as follows:

```html
<div id="app">
  <ol>
    <li>
      Learn JavaScript
    </li>
    <li>
      Learn Vue
    </li>
    <li>
      Build something awesome
    </li>
  </ol>
</div>
```

# Events

As in all web application development, interaction with user interface components is communicated using events. In Vue, we are able to define logic that is executed in terms of subscribing a callback to an event; we achieve this using the `v-on` directive. Consider the following example:

```html
<div id="app">
  <button v-on:click="counter += 1">Add 1</button>
  <p>The button above has been clicked {{ counter }} times.</p>
</div>
```

In this example we subscribe the inline JavaScript statement `counter += 1` to be executed whenever the event called `click` is fired within the DOM. This updates the `counter` variable and triggers a rerender, displaying the updated value to the user.

A similar syntax can be used to subscribe methods to events. Consider the following example:

```html
<div id="app">
  <button v-on:click="greet">Greet</button>
</div>
```

```javascript
var app = new Vue({
  el: 'app',
  data: {
    name: 'foo'
  },
  methods: {
    greet () {
      console.log(`Hello, ${this.name}!`)
    }
  }
})

// you can invoke methods in JavaScript too
app.greet() // => 'Hello, foo!'
```

In this example, the value of the `v-on` directive can also be the name of a defined method. Methods can also be called, explicitly passing parameters, using an inline JavaScript statement; for example: `<button v-on:click="greet('foo')">Greet</button>` will pass the parameter `foo` to the method when the button is clicked.

> **Further Reading**
>
> Event subscriptions allow subscriptions to be modified and filtered. They also expose DOM event objects to methods if one is available. For more information, see the official [event handling guide](https://vuejs.org/v2/guide/events.html).

# Two-Way Binding

HTML attributes are bound using the `v-bind` directive. This creates a one-way binding where if the bound variable is updated then the value of the attribute is also updated. If the attribute of the corresponding element were changed within the DOM, this would not cause the value of the bound variable to be updated. In cases where this is necessary &mdash; i.e. when handling user input &mdash; we must use a two-way binding.

To achieve a two-way binding in Vue, we use the `v-model` directive. Consider the following example:

```html
<div id="app">
  <p>{{ message }}</p>
  <input v-model="message">
</div>
```

```javascript
var app = new Vue({
  el: '#app',
  data: {
    message: 'Hello, World!'
  }
})
```

Here the contents of the `p` element will be updated every time the variable `message` is updated. The same variable is bound to the `input` element; whenever the user makes a change inside the rendered text field, it causes the contents of the bound variable to be also updated. As usual, if the contents of `message` is updated programatically (i.e. with `app.message = 'foo'`) then the value in both the text field and the mustache template will be updated.

This is called a two-way binding because updates can come from either the HTML side or from the JavaScript side and will be reflected in both.