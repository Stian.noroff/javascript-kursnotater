# Vue Router

All the JavaScript frontend frameworks produce a class of application called _single page web applications_ (SPA) that run inside the browser. Once built into their final artifact, they comprise of exactly one `.html` file: `index.html`. Unlike static web content, they're not intended to navigate from one page to another. Instead, these applications introduce the concept of **Routers** which produce the illusion of navigating from one page to another.

For Vue.JS, the standard router package is maintained in the separate NPM package: Vue Router. This is installed with the following terminal command:

```bash
npm install vue-router --save
```

Once installed, Vue Router is registered in the application in `src/main.js`, before the Vue constructor is called:

```javascript
import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
```

The application will have not changed significantly but will be built including Vue Router.

# How it works

The Vue Router package reads and manipulates the browser's [`history` (See MDN)](https://developer.mozilla.org/en-US/docs/Web/API/History) object. The history API controls the browser session history stack for that particular tab or frame but _does not cause any navigations_ when it is manipulated. When some state is pushed onto the stack, the history is modified and the URL bar will be updated accordingly, however the page described won't be loaded or even check that the page even exists. Router frameworks produce the expected effect of "navigation" within the SPA by subscribing to changes in the browser's history and displaying the corresponding view.

# Using Vue Router

To use the router, two conditions must be satisfied:

1. You must include the `<router-view></router-view>` component somewhere in your application. This may be placed deep within the application meaning that an "application frame" may be built around the area where the routed information is displayed.

2. You must define at least one route for your router and navigate to it appropriately.

> **Caveat**
>
> Anchor tags (`<a></a>`) must not be used within SPAs; these will result in navigation events instead of history manipulations. Instead, with Vue Router, you should use `<router-link to="/page">Link</router-link>`. The other frameworks have similar components.

&nbsp;

> **Aside**
>
> The contents of `router-link` automatically have the CSS class named `.router-link-active` applied to it when the current path matches that defined by the link. This can be used to give the user additional feedback as to which view they're currently looking at.

The routes are injected into the application by providing them to the `VueRouter` constructor. This is done as follows in `src/main.js`:

```javascript
import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

// Views
import Page from './components/PageView.vue'
import User from './components/UserView.vue'

// Define routes
const routes = [
    { path: '/page', component: Page },
    { path: '/user/:id', component: User },
]

// Init app
const app = new Vue({
    router: new VueRouter({ routes }),

}).$mount('#app')
```

In this example, the routes are simply defined in terms of a path and component that should be rendered when that path is matched. There are many different ways to have a path be matched for a particular URL:

- [Dynamic Route Matching (with route parameters and regular expressions)](https://router.vuejs.org/guide/essentials/dynamic-matching.html)
- [Nested Routes (a view within a view)](https://router.vuejs.org/guide/essentials/nested-routes.html)
- [Redirects and Aliases](https://router.vuejs.org/guide/essentials/redirect-and-alias.html)

> **Caveat**
>
> Sometimes a URL could be matched to multiple routes, however only the first one to match will be displayed. Vue Router "short circuits" any additional route checking; after the first match no additional routes are even checked.

# `$route` Property

Being rendered as a view enables Vue Router's injection of a number of properties onto the rendered components; the first of which is the `$route` property.

The route property provides component with access to information about the current route. For example, route parameters:

```javascript
// routes: [{ path: '/user/:id', component: User }]
created () {
    this.userId = this.$route.params.id
}
```

> **Further Reading**
>
> Additional route properties such as query parameters, the fragment, the full path and exact match information can be seen in the [Official API Reference](https://router.vuejs.org/api/#route-object-properties).

# `$router` Property

The `$router` property is another property that is automatically added to components whose display is controlled using Vue Router.

The router property primarily enables programmatic navigation. Consider the following example:

```javascript
methods: {
  goBack () {
    // Go back once.
    this.$router.go(-1)
  },
  onButtonClick () {
    // Navigate to a new page
    this.$router.push('/user/foo')
  },
}
```

> **Further Reading**
>
> [Router Properties and Methods (Official API Reference)](https://router.vuejs.org/api/#router-instance-properties) and [Programmatic Navigation (Official Guide)](https://router.vuejs.org/guide/essentials/navigation.html).

# Handling Asynchronicity

Sometimes data must be fetched when a route is activated; i.e. when fetching a user's profile before their profile page is rendered. This can be accomplished in two ways:

1. Fetching after navigation. Display a loading state while data is being fetched then update state to display data once it becomes available.
2. Fetching before navigation. Fetch data before navigation happens by intercepting the navigation with a [route guard](https://router.vuejs.org/guide/advanced/navigation-guards.html).

These two methods accomplish the same things but deliver different user experiences. The former will respond immediately but display a loading state, and the latter will be delayed but will navigate straight into visible data.

## Fetching after Navigation

In this approach we render the target component immediately and handle the request logic inside the `created` hook of that component. Consider the following example:

```html
<template>
  <div class="post">
    <div v-if="loading" class="loading">
      Loading...
    </div>

    <div v-if="error" class="error">
      {{ error }}
    </div>

    <div v-if="post" class="content">
      <h2>{{ post.title }}</h2>
      <p>{{ post.body }}</p>
    </div>
  </div>
</template>
```

```javascript
export default {
  data () {
    return {
      loading: false,
      post: null,
      error: null
    }
  },

  created () {
    // fetch the data _after_ the view is created
    // fetching before `created` will cause errors
    this.fetchData()
  },

  watch: {
    // call again the method if the route changes
    '$route': 'fetchData',
  },

  methods: {
    async fetchData () {
      const id = this.$route.params.id

      // reset state
      this.error = this.post = null
      this.loading = true

      try {
        const res = await fetch(`.../api/post/${id}`)
        this.post = await res.json()

      } catch (err) {
        this.error = err.toString

      } finally {
        this.loading = false
      }
    }
  },
}
```

## Fetching before Navigation

This approach enables inserting asynchronous requests in between the navigation event and the rendering of the resulting view. This is done by subscribing the Vue Router-defined hook `beforeRouteEnter`. The primary advantage to doing this is the reduction of state tracking logic inside of the components themselves. Consider the following example:

```javascript
export default {
  data () {
    return {
      post: null,
      error: null
    }
  },

  beforeRouteEnter (to, from, next) {
    fetch(`.../api/post/${to.params.id}`)
      .then(response => response.json())
      // Here the component doesn't exist yet;
      // we can't reference `this` so `vm` is passed
      // into the `next` callback instead.
      .then(post => next(vm => vm.setData(null, post)))
      .catch(err => next(vm => vm.setData(err)))
  },

  // when route changes and this component is already rendered,
  // the logic will be slightly different.
  beforeRouteUpdate (to, from, next) {
    fetch(`.../api/post/${to.params.id}`)
      .then(response => response.json())
      .then(post => this.setData(null, post))
      .catch(err => this.setData(err))
      .then(next)
  },

  methods: {
    setData (err, post) {
      if (err) {
        this.error = err.toString()
      } else {
        this.post = post
      }
    }
  }
}
```

> **Further Reading**
>
> Vue Router defines a number of additional hooks specifically for components that are rendered by as router views. These can be seen [here (Official Guide)](https://router.vuejs.org/guide/advanced/navigation-guards.html#in-component-guards). A full explanation of the addition lifecycle introduced by Vue Router can be seen [here (Official Guide)](https://router.vuejs.org/guide/advanced/navigation-guards.html#the-full-navigation-resolution-flow).