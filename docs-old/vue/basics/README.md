# Vue.JS

The largest advantage that Vue offers over those of React and Angular is its flexibility and ease of learning.

> TODO

&nbsp;

> **Further Reading**
>
> [Vue Tutorial](https://vuejs.org/v2/guide/#Getting-Started)

# Installation

Vue can be installed in one of two ways:

1. Linked as a script from a CDN or a local file.
2. Installed as an NPM package and bundled into the application itself &mdash; this is usually coupled with the use of the [CLI](../projects/README.md#cli) tool to generate a project from a template.

To demonstrate the simplicity of Vue, both of these options are shown and their differences discussed in a [later section](../projects/README.md).

> **CDN.JS**
>
> The URL for the most recent stable release of Vue can be found [here](https://cdnjs.com/libraries/vue). Usually selecting the option marked as the default (`vue.min.js`) is best, however there may be instances where other dependencies should be loaded.
>
> These files can either be downloaded and served from a local service, or linked straight from the CDN.

# Initialization

Once the Vue script has been linked, a new script should be created for the local application logic. Inside this script, Vue can be instantiated as follows:

```javascript
// Note: this is intentionally declared globally with `var`
var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello, World!',
    },
    computed: {
      reverse () {
        return this.message.split('').reverse().join('')
      },
    }
})
```

> **Aside**
>
> The object passed into the Vue constructor is often referred to as the `options` object and is a common pattern in JavaScript for passing "named parameters" into a function.

The parameters passed into the Vue constructor have the following meaning:

- `el` &mdash; This is the [selector string](../../web/jquery/README.md#selectors) for which HTML element the application should be mounted into.
- `data` &mdash; A collection of named parameters that are passed to the application template.
- `computed` &mdash; A collection of functions that return computed values.

It should be noted that there is no template that has been passed into the constructor options yet. This is because the template for the constructor is read from the initial value of the element where the application is being rendered. Consider the following HTML page:

```html
<div id="app">
  <blockquote class="blockquote">
    <p class="mb-0">{{ message }}</p>
    <p class="mb-0">{{ reverse }}</p>
    <footer class="blockquote-footer">Anonymous</footer>
  </blockquote>
</div>
```

For this page, the contents of the `div` element are first selected using the selector specified by `el`, then read and interpolated using the options provided in the `data` and `computed` objects. This results in the following interpolated HTML:

```html
<div id="app">
  <blockquote class="blockquote">
    <p class="mb-0">Hello, World!</p>
    <p class="mb-0">!dlroW ,olleH</p>
    <footer class="blockquote-footer">Anonymous</footer>
  </blockquote>
</div>
```

> **Further Reading**
>
> [Computed Properties (Official Documentation)](https://vuejs.org/v2/guide/computed.html)

# Templating

[The Mustache Syntax](https://mustache.github.io/) is a well known style of template engine, used extensively across many languages, and over a long period of time. Vue.JS borrows this syntax which substantially improves the developer experience for those who have used the syntax before when learning how to work with frontend frameworks. The result is that Vue.JS is effectively a "frontend template engine", retaining all the benefits and ease of development of template engines as a class, while being more efficient from the reduced or eliminated server-side rendering.

As can be seen above, properties supplied by the `data` object can be embedded into the template using the mustache template delimiters: `{{ msg }}`. This approach serves all instances where text is interpolated directly into the HTML. In the event we update our variable, the change will be shown in the rendered output:

```javascript
app.message = 'foo'
```