# Components

While Vue can be used as a simple template engine, object-oriented components can be used to create logical abstractions within an web application.

Within the Vue framework, components are created with their own templates, properties and logic, and are registered onto the Vue singleton. Consider the following example:

```javascript
Vue.component('button-counter', {
    data: () => ({
        count: 0
    }),
    template: `<button v-on:click="count++">
        You clicked me {{ count }} times.
    </button>`,
})
```

This defines an "element" that can be referenced in other templates:

```html
<div id="app">
  <button-counter></button-counter>
  <button-counter></button-counter>
  <button-counter></button-counter>
</div>
```

```javascript
var app = new Vue({ el: '#app' })
```

In this example, three different instances of the `button-counter` component are created, each with separate counters which increment when clicked.

> **Caveat**
>
> One major difference between the root instance of Vue (i.e. `new Vue(...)`) and components is that the value of `data` **must** be a function that returns an object. If this wasn't the case, clicking on one button would incremenet the counters for all the buttons in the above example.

# Passing Properties

Parent components can pass data to their children using properties; which look like HTML attributes. Consider the following example:

```javascript
Vue.component('blog-post', {
    props: ['title'],
    template: '<h3>{{ title }}</h3>',
})
```

In this example, the `props` field is used to declare an array of property names, the values for which should be made available to the internal logic and template of the component. Once registered in this way, you can pass custom data and objects to it as a custom attribute:

```html
<blog-post title="My journey with Vue"></blog-post>
<blog-post title="Blogging with Vue"></blog-post>
<blog-post title="Why Vue is so fun"></blog-post>
```

In most applications, this is usually used to render collections of data:

```javascript
new Vue({
  el: '#app',
  data: {
    posts: [
      { id: 1, title: 'My journey with Vue' },
      { id: 2, title: 'Blogging with Vue' },
      { id: 3, title: 'Why Vue is so fun' }
    ]
  }
})
```

```html
<blog-post
  v-for="post in posts"
  v-bind:key="post.id"
  v-bind:title="post.title"
></blog-post>
```

This will render the above `blog-post` component for each of the values in the `posts` array. Note that because properties are modelled as HTML attributes, the `v-bind` should be used so that any updates to the data will cause a rerender with the updated inforation.

> **Caveat**
>
> Every element in the DOM must have a unique attribute called `key`. Key is similar to `id` in that it is used to make efficient lookups in the [Virtual DOM](#virtual-dom) and it needs to be globally unique (within your application). Most of the time this property is generated for you by Vue but when rendering collections of data dynamically then you must specify this property explicitly.
>
> Try to avoid using the array index in creating a value for `key` because if the array order is changed then it leads to inconsistencies within the app.

# Custom Events

Passing properties to the child component through HTML attributes enables the parent to communicate with the child, however most applications require two-way communication. Passing data from the child component to the parent is done using [events](../directives/README.md#events) in a similar way to what was discussed before.

We can modify our above example to include the ability to alter the font size of the text. First, we add an appropriate property to our root component `data` field:

```javascript
new Vue({
  el: '#app',
  data: {
    posts: [/* ... */],
    fontSize: 1,
  }
})
```

We can then use that value to alter the style of our blog posts' text.

```html
<div id="app">
  <div :style="{ fontSize: fontSize + 'em' }">
    <blog-post
      v-for="post in posts"
      v-bind:key="post.id"
      v-bind:post="post"
    ></blog-post>
  </div>
</div>
```

We then add a button to enlarge the text inside our blog post:

```html
<div class="blog-post">
  <h3>{{ post.title }}</h3>
    <button v-on:click="$emit('enlarge-text')">
      Enlarge text
    </button>
  <div v-html="post.content"></div>
</div>
```

When the button is clicked it will fire an event we've defined as `enlarge-text`, which is handled in exactly the same way we'd handle a normal `click` event:

```html
<blog-post
  ...
  v-on:enlarge-text="fontSize += 0.1"
></blog-post>
```

## Emiting Values

The above example is equivalent to calling a function without parameters; events can also be fired with values that get passed to subscribers as parameters. Consider the following example:

```html
<!-- component partial -->
<button v-on:click="$emit('enlarge-text', 0.1)">
  Enlarge text
</button>
```

```html
<!-- root partial -->
<blog-post
  ...
  v-on:enlarge-text="onEnlargeText"
></blog-post>
```

```javascript
new Vue({
  el: '#app',
  data: {
    posts: [/* ... */],
    fontSize: 1,
  },
  methods: {
    onEnlargeText (amount) {
      this.fontSize += amount
    }
  }
})
```

## Two-Way Binding

Two way binding using the `v-model` directive is simply syntactic sugar for explitly binding in both directions. Consider the following simple example:

```html
<input v-model="search">
```

This is simply a short-hand (syntactic sugar) for this:

```html
<input
  v-bind:value="search"
  v-on:input="search = $event.target.value"
>
```

> **Aside**
>
> Using the variable `$event` in an inline JavaScript event handler refers to the first parameter passed to the handler. In most cases this will be a DOM event (if one is available) but can also be a value explitly passed to `$emit('event', value)`

Thus, to make our custom components work when using the `v-model` directive, we must simply implement the same interface. Consider the following example:

```javascript
Vue.component('custom-input', {
  props: ['value'],
  template: `
    <input
      v-bind:value="value"
      v-on:input="$emit('input', $event.target.value)"
    >
  `
})
```

As long as we define some behaviour for the value property and logic that fires the `input` event then our component can be used with the `v-model` directive:

```html
<custom-input v-model="search"></custom-input>
```

# Component Lifecycle

Vue components can define callbacks that get executed during key points in the lifecycle of the components. The Vue component lifecycle is described in the following image:

<figure>
    <center>
        <img src="https://vuejs.org/images/lifecycle.png" alt="vue component lifecycle" width="550"/>
        <figcaption>Figure: Vue Component Lifecycle</figcaption>
        <br/>
    </center>
</figure>

A few of the more important lifecycle methods are now discussed:

- `beforeCreated` &mdash; Before events and data reactivity have been set up. This lifecycle should be generally avoided.
- `created` &mdash; The data and events have been set up but the component has not renderd. In the event that a component needs to make requests to asynchronous APIs or set up subscribers then this is a good place to do it.
- `mounted` &mdash; Componenents have been rendered.
- `beforeUpdate` &mdash; After an update has been triggered this method will receive the information before the component is modified. This is a good place to alter the data before it renders.
- `updated` &mdash; Component has been rerendered. This is a good place to put caching operations that don't cause the component to be altered. **If you alter the component inside this callback then you will cause an infinite loop.**
- `beforeDestroy` &mdash; Before the component is removed from the lifecycle. This is a good place to unregister any subscribers that were setup up in `created`. **Failure to unsubscribe event listeners will produce errors if those event listeners fire after the component is destroyed.**

## Lifecycle Hooks

Lifecycle hooks are defined as follows:

```javascript
Vue.component('foo', {
  data: () => ({ remote: null }),
  created () {
    fetch('https://api.example.com/foo/bar/baz')
      .then(res => res.json())
      .then(data => {
        this.remote = data
      })
  }
})
```

> **Further Reading**
>
> [Instance Lifecycle Hooks (Official Documentation)](https://vuejs.org/v2/guide/instance.html#Instance-Lifecycle-Hooks)

# Slots

Components in Vue are modelled to be used as if they were HTML elements; they are invoked as "elements" in templates and accept properties and directives as HTML attributes. It makes sense that a component may also have its own children which are also HTML elements. In Vue, this is achieved using slots. Consider the following example:

```javascript
Vue.component('alert-box', {
  template: `
    <div class="demo-alert-box">
      <strong>Error!</strong>
      <slot></slot>
    </div>
  `
})
```

This template defines a `slot` element, which can be populated by passing children to the element when it is invoked:

```html
<alert-box>
  <h3>Something went wrong!</h3>
</alert-box>
```

Slots can also be named so content may be separated:

```html
<div class="container">
  <header>
    <slot name="header"></slot>
  </header>
  <main>
    <slot></slot>
  </main>
  <footer>
    <slot name="footer"></slot>
  </footer>
</div>
```

Providing content to the named slots is done with a `<template>` element with a `v-slot` directive with the slot name specified:

```html
<card-component>
  <template v-slot:header>
    <p>A paragraph that goes inside the "header" element</p>
  </template>

  <p>A paragraph that goes inside the "main" element</p>
  <p>... and another</p>

  <template v-slot:footer>
    <p>A paragraph that goes inside the "footer" element</p>
  </template>
</card-component>
```

The slot that is left unnamed implictly has the name "default". If you wanted to be more explicit, you could also do the following:

```html
<card-component>
  <template v-slot:header>
    <p>A paragraph that goes inside the "header" element</p>
  </template>

  <template v-slot:default>
    <p>A paragraph that goes inside the "main" element</p>
    <p>... and another</p>
  </template>

  <template v-slot:footer>
    <p>A paragraph that goes inside the "footer" element</p>
  </template>
</card-component>
```

Either way, this is rendered as follows:

```html
<div class="container">
  <header>
    <p>A paragraph that goes inside the "header" element</p>
  </header>
  <main>
    <p>A paragraph that goes inside the "main" element</p>
    <p>... and another</p>
  </main>
  <footer>
    <p>A paragraph that goes inside the "footer" element</p>
  </footer>
</div>
```

> **Futher Reading**
>
> [Scopes (Official Documentation)](https://vuejs.org/v2/guide/components-slots.html)

# Single File Components

The Webpack configuration that is shipped with the default Vue project template used by the Vue CLI enables the use of `.vue` files to define components. At its root, the file contains three XML-style tags:

- `<template></template>` &mdash; this corresponds to the `template` field of the options object when registering a component.
- `<script></script>` &mdash; contains the JavaScript to configure and register this component. Exports must be defined within these tags using the ES6 module syntax. All exports correspond to the fields of the component registration options object (i.e. `data`, `methods`, etc.).
- `<style></style>` &mdash; styles to be applied. If defined with `<style scoped>` then the styles are only applied within the scope of this template. CSS preprocessors can also be specified using the `lang` attribute if they were configured during project initialization.

In the case of the `<script>` and `<style>` tags, these contents can be split into separate files which can be referenced with the `src` attribute.

```javascript
<template>
  <MyComponent v-bind:foo="foo"></MyComponent>
</template>

<script>
import MyComponent from './MyComponent'

export default {
    name: 'ParentComponent',
    data: () => ({ foo: 'bar' })
}
</script>

<style></style>
```

To use one component inside of another, one only has to make sure the component being used is imported using the ES6 module syntax.

> **Further Reading**
>
> [Single-File Components (Official Documentation)](https://vuejs.org/v2/guide/single-file-components.html)

# Component Design

Maintaining and accessing state for a frontend application in a highly distributed manner is both difficult and expensive (in terms of resources); for this reason, when designing components, they are usually categorized into one of two types:

- Smart Components
- Pure Components

## Smart Components

The defining characteristics of smart components is that they store state, as well as the logic for modifying the state. Smart components are also sometimes referred to as containers because their "rendering logic" purely consists of invoking other containers whose purpose is to accept data and display information. The smart components are said to "contain" the other components.

## Pure Components

By contrast to smart components, pure components contain no state and very little logic that isn't directly related to displaying information. Pure components get their name from the functional programming paradigm's concept of purity. Purity, in functional programming, is defined as being without "side effects". A pure function is one that, given the same input twice, will produce the same output. Thus, a pure component is one that, given the same input twice, will render the same result.

## Composition

When designing a frontend applicaton, the application starts from a single entry point, most commonly called `App` or something similar. Inside this root component, the application is initialized and configured.

Once the application is configured then the initial content displayed is usually loaded from one or more "view". Vue (and other web frameworks) produce single-page web applications, thus navigation cannot involve navigating to another web page; instead, application libraries called "routers" are used to conditionally switch the content that is displayed based on the path specified by the user agent. These router frameworks also work in terms of components, and the components that are displayed at the top level are commonly smart components that are referred to as "views".

Within the view smart components, the state of the view is managed within itself and pure components are used to display the state contained within. In the event that the user navigates to a "deeper" part of the application, there may be additional layers of smart components that are contained within pure components.

> Figure: Pure components nested inside a smart component, inside a pure component, inside the view, inside the root (App).

Additional levels of nesting inside an application introduces complexity and often multiplies the resource consumption of the application. For most applications, there are very few cases where additional nesting is required, where a flat application structure would not suffice. In the event where deep nesting is used, then it should be used sparingly.