# Angular - Basics

Angular is a front-end framework that we use to create single-page applications. Unlike React and Vue, Angular is very prescriptive in the way code is written and projects are organized.

It does however share similarities to other frameworks, such as the concept of Components and Routing, more on this later.

Angular can be more complex than other frameworks due to the large number of files. Therefor, it requires careful planning to avoid projects becoming unmanageable. It does also requires that you learn TypeScript.

> **Further reading**
> 
> To read the official documentation on Angular you can head over to the [Angular website](https://angular.io/guide/setup-local) for an in-depth look into all the concepts covered here.

Before continuing with Angular, it should be noted that you must have a basic understanding of the following topics:
- HTML
- JavaScript
- CSS

> **Aside**
>
> Angular and AngularJS are **not** the same framework. The frameworks are *incompatible*. AngularJS has been deprecated and all active development has switched to Angular.

# Angular Concepts

The following concepts are the building blocks of any Angular application. It will be covered in more detail in each of the relevant sections. 

- Components
  - UI Building blocks for an Angular Application.
- Decorators
  - Metadata that defines the purpose of your code.
- NgModule
  - A way to organize code as well as improve re-usability. 
- Dependency Injection
  - A design pattern that allows the application to share the same instance of a class.
- Services
  - Services in Angular are used to manage and share data between components. Services are also responsible for executing HTTP Requests
- Angular Forms.
  - A library with custom form validation and two way data-binding for HTML forms in an Angular application.
- Angular Router
  - An Angular library used to navigate between components in your application.

> **Important**
> 
> Angular uses TypeScript. It is recommened that you review the basics of TypeScript before continuing with the Angular section.
> 
> Reading: [TypeScript Documentation](https://www.typescriptlang.org/)

# CLI

The Angular CLI (Command Line Interface) is used to create applications, generate new application code, run and test applications, and bundle Angular applications for deployment. 

To use to CLI it must be installed globally on your computer. The installation requires that Node.JS and npm are installed.

> **Aside**
> 
> Before you continue, ensure that you have [Node.js](https://nodejs.org/en/) installed on your system.

## Installation

To start we will install the Angular CLI:

```bash
npm install -g @angular/cli
```

The above will install the Angular CLI, globally, on your system. The Angular CLI is accessible using the **`ng`** command.

## Usage and available options

```
Usage: ng new <project-name> [options]

Options:

--help
    Shows a help message for this command in the console.

--inline-style (-s)
    When true, includes styles inline in the component TS file. By default, an external styles file is created and referenced in the component TS file.

--inline-template (-t)
    When true, includes template inline in the component TS file. By default, an external template file is created and referenced in the component TS file.
    When false, disables interactive input prompts.

--legacy-browsers
    Add support for legacy browsers like Internet Explorer using differential loading.

--prefix (-p)
    The prefix to apply to generated selectors for the initial project.

--routing
    When true, generates a routing module for the initial project.

--skip-tests (-S)
    When true, does not generate "spec.ts" test files for the new project.

--style
    The file extension or preprocessor to use for style files.

--verbose (-v)
    When true, adds more details to output logging.

```

To see a complete list of options you can run the following command from your operating system's command application:

```bash
ng new --help
```

## Other important commands

The following are the CLI commands that will most often be used:

```bash
ng serve            Build and serve a project to the browser with LiveReloading
ng generate         Create a new Module, Component, Service, etc.
```

> **Further reading**
> 
> For a full list of commands and options you can visit the [Angular CLI Documentation](https://angular.io/cli#command-overview).

# Generate a new project

Generate a new Angular project called hello-world using the ng new command.

```bash
ng new hello-world
```

The CLI will prompt you with a couple of questions before beginning the setup of your new project. 

<figure>
    <center>
        <img src="./Angular_basics_CLI_001.png" alt="Angular CLI Setup prompts" width="650"/>
        <figcaption>Figure: Setup prompts</figcaption>
        <br/>
    </center>
</figure>

> **Angular routing**
>
> To include the Angular Routing module in your application, you can type `yes` to the question.
> 
> **Stylesheet format**
>
>You can select your preferred stylesheet format using the up and down arrows. 

The Angular CLI will proceed to create the base project files and install all the required dependencies into the `node_modules` folder.

## Open your new application

To view the new application you can run the following commands:

```bash
cd hello-world
ng serve --open
```

The above command will automatically start a build process for your application and run it locally on your computer. 

<figure>
    <center>
        <img src="./Angular_basics_CLI_002.png" alt="Angular CLI Default Screen" width="650">
        <figcaption>Figure: Angular CLI startup screen</figcaption>
    </center>
</figure>

# Project

A newly generated Angular application will have the following folder/file structure.

```
./hello-world
├── e2e/
├── node_modules/
├── src/
│   ├── app/
│   │   ├── app-routing.module.ts
│   │   ├── app.component.css
│   │   ├── app.component.html
│   │   ├── app.component.spec.ts
│   │   ├── app.component.ts
│   │   └── app.module.ts
│   ├── assets/
│   ├── environments/
│   ├── favicon.ico
│   ├── index.html
│   ├── main.ts
│   ├── polyfills.ts
│   ├── styles.css
│   └── test.ts
├── package-lock.json
├── package.json
├── angular.json
├── README.md
├── tsconfig.app.json
├── tsconfig.base.json
├── tsconfig.jsonO
├── tsconfig.spec.json
└── tslint.json

3 directories, 13 files
```

## Worth noting

A default Angular project includes the following:
- Built in end-to-end testing using Karma
- An assets folder for images and fonts
- environment folder for managing development and production specific variables
- Built in [polyfills](https://developer.mozilla.org/en-US/docs/Glossary/Polyfill)
- Global `style.css` file

# Core files

Investigating the core files individually can give a better understanding of how an Angular application is structured. There are 7 core files in a default Angular project.

## Index
 
```html
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>HelloWorld</title>
  <base href="/">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
</head>
<body>
  <app-root></app-root>
</body>
</html>
```
<center>
    <figcaption>src/index.html<figcaption>
</center>

This is the single page of your application. All the components are mounted into the `<app-root>` component. You may add external CSS or JavaScript libraries in the `<head>` of the HTML file.

## Main

```typescript
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

// Conditionally enable production mode.
if (environment.production) {
  enableProdMode();
}

// The AppModule is bootstrapped by Angular
platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
```
<center>
    <figcaption>src/main.ts<figcaption>
</center>

The first important thing to note is that Angular uses ES6 Import syntax.

The `main.ts` is the bootstrap of our application. An Angular application begins with the main.ts file and loads the `AppModule`.

The `environment` file is also imported and conditionally enables or disables Angular's built in [production mode](https://angular.io/guide/deployment#enable-runtime-production-mode).

## Environments

```typescript
export const environment = {
  production: true
};
```
<center>
    <figcaption>src/environments/environment.ts<figcaption>
</center>

This simple file can be used to determine variable values based on the environment in which the application is running.

The Angular CLI automatically switches out the contents of the `environment.ts` file with the contents of the `environment.prod.ts` file when the `ng build --prod` command is run from the terminal.

Custom properties can be added to this object. 

> **Aside**
> 
> The object in the `environment.ts` and `environment.prod.ts` must have identical properties. The values may differ.

## App Module

```typescript
1    import { BrowserModule } from '@angular/platform-browser';
2    import { NgModule } from '@angular/core';
3  
4    import { AppRoutingModule } from './app-routing.module';
5    import { AppComponent } from './app.component';
6 
7    @NgModule({
8      declarations: [
9        AppComponent
10     ],
11     imports: [
12       BrowserModule,
13       AppRoutingModule
14     ],
15     providers: [],
16     bootstrap: [AppComponent]
17   })
18   export class AppModule { }
```
<center>
    <figcaption>src/app/app.module.ts<figcaption>
</center>

The `AppModule` is imported by the `main.ts` file. The AppModule contains the components and NgModules that this application will need to start. The    `bootstrap` property on line 16 in the above code snippet provides the Root Component of the application to the `AppModule`. The AppComponent will be loaded into the `index.html` file.

> **Aside**
> 
> Every Angular application must have at least one `NgModule`. 

A class can be identified as a `NgModule` due to the `@NgModule` decorator as seen on line 7 of the above code snippet.

# Decorators

The `@NgModule` is the first `Decorator` you will see. A `Decorator` in the simplest terms; is an object that contains metadata about the current class. A decorator can be identified by the presence of the `@` symbol. 

Common decorators:
- `@NgModule`
  - Defines a class as a NgModule
- `@Component`
  - Defines a class as an Angular Component
- `@Injectable`
  - Defines a class that can be dynamically injected into other classes using Dependency Injection.

Each decorator accepts an object with its own specific properties, or *metadata*. The `@Component` and `@Injectable` decorators will be discussed in more detail later.

# NgModule 

NgModules are used to organize Angular applications.

## The NgModule metadata

- `declarations`
  - A list of declarable classes, (components, directives, and pipes) that belong to this module.
- `imports`
  - A list of modules which should be folded into this module. Folded means it is as if all the imported NgModule's exported properties were declared here.
- `exports`
  - Components that must be exposed when this module is being imported.
- `providers`
  - A list of dependency-injection providers.
- `bootstrap`
  - A list of components that are automatically bootstrapped. Usually there's only one component in this list, the root component of the application.

> **Further reading**
> 
> For detailed information on NgModules see the [Angular.io documentation](https://angular.io/guide/ngmodule-api)

## App Component

```typescript
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'hello-world';
}
```
<center>
    <figcaption>src/app/app.component.ts<figcaption>
</center>

The AppComponent is the first Component in the Angular application. A component usually consists of three files. 

- app.component.html
  - The HTML or View for the component.
- app.component.ts
  - The Logic for the component.
- app.component.css 
  - Styles for the component. Can also be SCSS if chosen during setup.

The most important part for now is the @Component decorator. It contains 3 main properties.

### selector

The selector defines how this Component will be accessed in the HTML of your application. The AppComponent's selector is `app-root` and can be found in the `index.html` file. This will tell Angular to render any HTML found in this Component to the `index.html` file.

### templateUrl

The `templateUrl` points to the `html` or View of this Component. This will be the UI part of the component.

### styleUrls

The `styleUrls` array contains the CSS (or SCSS if you chose during setup) for this component. 

> **Aside**
> 
> By default, CSS is scoped to this component only. Therefor styles applied to this component will not be available outside of itself.
> 
> Global styles should be written in the `styles.css` file found in the root of the `src` folder.

## App Routing Module

```typescript
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
```
<center>
    <figcaption>app-routing.module.ts<figcaption>
</center>

The `app-routing.module.ts` file contains the code that manages which component should currently be displayed in the browser based on the path in the address bar.

This concept will be discussed in more detail in the Angular Router section.

> **Aside**
> 
> It is important to note that this file is imported into the `AppModule` file under the `imports` property of its `NgModule` decorator.

# Writing HTML

In Angular all HTML is written in one of two places.

1. In a separate HTML file
2. In the Component's template property 


## 1. Writing HTML: In a separate file

For demonstration purposes of the Angular basics, clear out all the code in the `app.component.html` file.

```html
<h1>Welcome to Angular</h1>
```
<center>
<figcaption>app.component.html</figcaption>
</center>

Add an `h1` tag with the text `Welcome to Angular`. When you hit save the Angular live server will recompile the changes and display it in the browser. This is the most common why of writing HTML in Angular applications.

## 2. Writing HTML: Inline HTML

If you are writing a very basic component that contains very few lines of HTML, you can write the HTML directly in the component's typescript file.

Using the `app.component.ts` as an example, removing the `templateUrls` property from the component's decorator and replacing it with the `template` property. 

The ES6 backtick ( ` ) allows us to write inline HTML without requiring a separate file for writing HTML in the component.

```typescript
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  // Inline HTML in a Component
  template: `
    <h1>Hello Angular World!</h1>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'hello-world';
}
```
<center>
<figcaption>app.component.ts</figcaption>
</center>

> **Aside**
> 
> For the above to work the `ng serve` command must currently be running.

# Writing CSS

Similarly to the HTML, CSS in Angular applications can  be written in two places.

1. CSS in a separate CSS file
2. CSS in the component's TypeScript file

## 1. Writing CSS: In a separate CSS file

We will continue to use the App component to demonstrate the basics. 

Open the `app.component.css` file and add some CSS to modify the appearance of the `h1` created in the previous section.

```css
h1 {
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    font-weight: bold;
    text-align: center;
    color: #333;
}
```
<center>
  <figcaption>app.component.css</figcaption>
</center>


You can write standard CSS using all the standard selectors like tag names, classes, id's etc. The Angular CLI's Live server will recompile the changes and reload the application in the browser for you to see the changes. 

<figure>
    <center>
        <img src="./Angular_basics_CSS_002.png" alt="Angular CLI Default Screen" width="650">
        <figcaption>Figure: Angular CSS</figcaption>
    </center>
</figure>

> **Aside**
> 
> Keep in mind that CSS written in the Component is scoped to itself. The CSS will not affect components outside of itself.

## 2. Writing CSS: Inline CSS

The next option for writing CSS is useful should you have very few lines of CSS. This is great for reducing the number of files in your application, especially if the current component is very basic.

Open the `app.component.ts` file and remove the `styleUrls` property from the decorator and add the following code.

```typescript
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <h1>Hello Angular World!</h1>
  `,
  // Inline Styles
  styles: [
    `
      h1 {
        font-family: -apple-system, BlinkMacSystemFont, 
                'Segoe UI', Roboto, Oxygen, 
                Ubuntu, Cantarell, 'Open Sans', 
                'Helvetica Neue', sans-serif;
        font-weight: bold;
        text-align: center;
        color: #333;
    }
    `
  ]
})
export class AppComponent {
  title = 'hello-world';
}
```
<center>
  <figcaption>app.component.ts</figcaption>
</center>

The above will have the exact same result as using a separate file but it will reduce our component from 3 files to a single file. It is important to keep this in mind when developing an Angular application. If a component can be contained in a single file, you should do so. 

# Render data - Interpolation

Angular uses the mustache template syntax, or the double curly-braces to display data rich content in our HTML templates.

```html
<h1>{{ title }}</h1>
```
<center>
<figcaption>Mustache Template Syntax</figcaption>
</center>

Use the `app.component.ts` file to illustrate this concept. 

In the `app.component.ts` file there is a title property declared in the `AppComponent` class. Using the mustache template syntax, we can display the string value in our HTML inside a `<p>` tag.

Modify the `app.component.ts` file to reflect the following.

```typescript
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  // 1. Add the <p> with the {{ title }}
  template: `
    <h1>Hello Angular World!</h1>
    <p>{{ title }}</p>
  `,
  // 2. Add the CSS for the <p> tag
  styles: [
    `
      h1 {
        font-family: -apple-system, BlinkMacSystemFont, 
                'Segoe UI', Roboto, Oxygen, 
                Ubuntu, Cantarell, 'Open Sans', 
                'Helvetica Neue', sans-serif;
        font-weight: bold;
        text-align: center;
        color: #333;
    }
    p {
      text-align: center;
    }
    `
  ]
})
export class AppComponent {
  // Note: The property value will be interpolated into the HTML.
  title = 'hello-world';
}
```
<center>
  <figcaption>app.component.ts</figcaption>
</center>

The `<p>{{ title }}</p>` piece of code will now be interpolated by Angular and display the string value declared for the `title` property in the `AppComponent` class.

The style ```css p { text-align: center; }``` will ensure that the text aligns with the `h1` tag.

> **Aside**
> 
> We can add multiple lines of CSS in the `styles` property of the decorator.

# Structural directives

Structural directives are an integral part of any Angular application. Often data needs to be displayed conditionally or listed. The Angular framework provides structural directives to help achieve this.

Structural directives do not require mustache syntax. 

## Conditional: `NgIf`

```html
<p *ngIf="condition"></p>
```

```html
<section *ngIf="user.loggedIn === true">
  <h1>Welcome {{ user.name }}</h1>
  <p>{{ user.email }}</p>
</section>
```

## Loops: `NgForOf`

The NgForOf loop is designed to iterate over the iteratable object. The most common usage is to iterate of an array.

```html
<section *ngFor="let item of items">
  {{ item }}
</section>
```
<center>
  <figcaption>Code: Iterating over an array called items.</figcaption>
</center>


### Local variables

The ngFor loop has a collection of variables that can be aliased and accessed in the loop body.

```html
<section *ngFor="let item of items; index as i">
  {{ item }} number {{ i }}
</section>
```
<center>
  <figcaption>Code: Accessing the index in the loop.</figcaption>
</center>

Here is a list of available Local variables that can be aliased.

```
Local variables

index: number -  The index of the current item in the iterable.
count: number - The length of the iterable.
first: boolean - True when the item is the first item in the iterable.
last: boolean - True when the item is the last item in the iterable.
even: boolean - True when the item has an even index in the iterable.
odd: boolean -True when the item has an odd index in the iterable.
```

> **Further reading**
> 
> If you would like to read more about the ngFor loop you can head over to the official [Angular documentation](https://angular.io/api/common/NgForOf)

# Angular and HTML Events

Any useful application relies on input from the user. Generally the user can click on elements in the app or type information into text boxes. Angular provides User Input events to listen for HTML events.

## Click event

The common HTML events, like onclick, onchange, etc. are provided in the HTML template using parenthesis `()`. Note that the 'on' is dropped from the event name, i.e. `onclick` will be called `click` and so forth.

The click event can be tied to any element in a HTML template.

```html
<button (click)="onClicked()">Click me</button>
```

The above snippet show how to attach a click listener to a button. To execute code on the click event, a event handler must be written in the logic file of a component.

```typescript
...
export class AppComponent {
  onClicked() {
    // Handle the (click) event of the button.
  }
}
```

> **Aside**
> 
> It should be noted that events provided in the template forward an `$event` object or value.

Below is a snippet of passing the element event to a handler function.

```html
<input (keyup)="onKeyUp($event)" />
```
<center>
    <figcaption>Code: onkeyup sending $event to handler function.</figcaption>
</center>

### Note
The event object is then accessible in the handler function of the component. 

```typescript
...
// Event Handler function
onKeyUp(event) {
  // Get the current value of the input.
  const { value } = event.target;
}
```
<center>
    <figcaption>Code: Handler function in Angular component.</figcaption>
</center>

> **Further reading**
> 
> For detailed information about user input events, you can follow the [Angular Official Documentation](https://angular.io/guide/user-input).