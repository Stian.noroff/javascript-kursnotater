# Angular Services

Unlike other frameworks, Angular provides custom libraries to manage HTTP Requests.

## Dependency Injection

Dependency injection is a application design pattern used to provide a class with a service or object required to fulfil its purpose. In Angular it is typically used to design applications. 

> **Aside**
> Angular has its own Dependency Injection framework.

To get started with Dependency injection in Angular, it is common to create a service.

## Creating a Service

The Angular CLI can generate a new service. 

> ## Activity - Create a service
> 
> Create a new service called `ContactService` using the CLI in a folder called `services`.

```bash
ng generate service services/contact
```

The above command will create a new skeleton service called `ContactService`.


```typescript
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor() { }
  
}
```

<center>
  <figcaption>Code: Angular Service Skeleton</figcaption>
</center>

## @Injectable Decorator

Notice the `@Injectable` decorator added to the service. This decorator is used to indicate that a class maybe be used for Dependency Injection in Angular. The `providedIn` property defines where or how, this class will be injected. The default setting, `root` means the service will be available on application level and can be injected into any class.

> **Further reading** 
> To read more about the Injectable decorator you can head over to the [Official Angular Documentation](https://angular.io/api/core/Injectable).

It is common practice to use services to provide and share data between components. In the Components section, it mentioned that only sibling components may communicate with `@Input()` and `@Output()` decorators. 

If two completely unrelated components must share data, a service can be used.

> ## Activity - Move contacts to service.
> 
> To move the contacts to the service some restructuring of the application is needed.

### 1. Creating an interface

In TypeScript interfaces can be used to represent custom data types. The `Contact` is a custom data type. Interfaces also help the code editor to improve intellisense in your code.

> ## Activity - Create a Contact interface
> Create a new file and name it `contact.model.ts` in a folder  `src/app/models`. This file will export an interface that defines the structure of a `Contact`.

Unlike components and services, there is no CLI command to generate an interface. The file must be manually created.

```typescript
export interface Contact {
    id: number;
    firstName: string;
    lastName: string;
    phone: string;
    email: string;
}
```

<center>
  <figcaption>Code: TypeScript Interface for a Contact</figcaption>
</center>

### 2. Create a mock file

Mock files in Angular are used to provide placeholder data to applications. This is often used when prototyping or building a front-end before the back-end has been completed.

> ## Activity - Create a Mock File
> 1. Create a new file called `contacts.mock.ts` in a folder `src/app/mocks`. 
> 2. Move the contacts array from the `app.component.ts` and export it in the new file, `contacts.mock.ts`

```typescript
import { Contact } from '../models/contact.model';

export const contacts: Contact[] = [
    {
      id: 1,
      firstName: 'Han',
      lastName: 'Solo',
      phone: '555-2902',
      email: 'han.solo@starwars.com'
    },
    {
      id: 2,
      firstName: 'Boba',
      lastName: 'Fett',
      phone: '-',
      email: 'bountyfett@starwars.com'
    },
    {
      id: 3,
      firstName: ' Admiral',
      lastName: 'Ackbar',
      phone: '555-1123',
      email: 'admiral.ackbar@starwars.com'
    },
    {
      id: 4,
      firstName: ' C-3PO',
      lastName: '',
      phone: '100-1011',
      email: 'c3po@starwars.com'
    },
    {
      id: 5,
      firstName: ' Lando',
      lastName: 'Calrissian',
      phone: '100-1011',
      email: 'lando.calrissian@starwars.com'
    },
  ];
```

The above code snippet shows a contact list in the `contacts.mock.ts` file. Note that the array type has now been define as `Contact` and no longer `any`.

> **Aside**
> 
> When using TypeScript, in general, one must avoid using `any`. It is best practice to create an `interface` and define what type an object is.

The next step would be to add the contacts as a property in the new `ContactService`. There are two steps required.


  - Importing the mock data - We will import the data into the service.
  - Creating a function to provide the mock data to other classes - After importing the data into the service, a method must be created to expose the data to any class that injects this service.

### 3. Importing Mock data to a Service

To enable a service to share data, it needs to be imported into the service class. As an example, the mock data can be easily imported into a service.


> ## Activity - Prepare ContactService
> Import the mock data into the service and create a method to provide the data to any class.

```typescript
import { Injectable } from '@angular/core';
// Get the contact Mock data
import { contacts } from '../mocks/contact.mock';
// Get the Interface to define a custom Contact type.
import { Contact } from '../models/contact.model';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor() { }

  // Method to expose the contacts array.
  getContacts(): Contact[] {
    return contacts;
  }
  
}
```

<center>
  <figcaption>Figure: ContactService with Mock Data.</figcaption>
</center>

In the above code snippet, the contacts mock data was imported and can be accessed using the `getContacts` method. The method has a signature to defines itself as returning an array of `Contact` objects. 

Defining a method signature with a custom type will enable intellisense in any class that has injected the service.

## Inject a service into a component

Once a service is ready, it can be easily injected into a `Component` using the its constructor function. Angular will provide a single instance of the `ContactService` to the entire application. 

This way when data in a service is updated, it will be shared across all other components that access that data.

> ## Activity - Inject the ContactService
> Inject the ContactService into the AppComponent and assign the contacts to the value from the service. The template will be unchanged.

To inject a service into a class, it must be added as an argument to the class' `constructor` function. Demonstrated below is the `ContactService` being injected into the `AppComponent`.

```typescript
import { Component, OnInit } from '@angular/core';
// 1. Import the ContactService
import { ContactService } from './services/contact.service';
// 2. Import the custom Contact type.
import { Contact } from './models/contact.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  // 3. Declare a contacts array to access in the template
  contacts: Contact[] = [];

  // 4. Inject the ContactService as an argument in the constructor().
  constructor(private contactService: ContactService) { }

  ngOnInit(): void {
    // 5. Assign the contacts property to the contacts from the service.
    this.contacts = this.contactService.getContacts();
  }

}
```

<center>
  <figcaption>Figure: Inject ContactService into AppComponent .</figcaption>
</center>

Any class that injects the  `ContactService` will also have access to the `getContacts` method. The `getContacts` method is currently synchronous. This would however not be the case in a real application. Data is fetched from an external remote location. The next section discusses the `HttpClient` and how to retrieve data from remote resources.

# Http Client

Unlike other frameworks that rely on the `fetch` api or libraries like `Axios`, Angular provides a built in module to make Http requests.

The `HttpClientModule` contains all the features required to communicate with remote resources. 

## Enable Http Services in Angular

To enable the `HttpClient`, it must first be imported and "registered" in the root `AppModule` of the application.

> ## Activity - Enable HTTP
> Import the `HttpClientModule` and add it to the `imports` array of the `AppModule`.

The first step is to import the `HttpClientModule` in the `app.module.ts` file, thereafter it must be added to the `imports` array of the `AppModule`.

```typescript
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// 1. Import the HttpClientModule
import { HttpClientModule } from '@angular/common/http';

// other imports
...

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    ContactItemComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // 2. Add the HttpClientModule to the Imports array.
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

```

The Angular application is now ready to make Http requests and can inject the `HttpClient` service into any class.

## Setting up a test server

To simulate communication to a remote service we can use the `json-server` tool to create a testing server in seconds.

### Downloads

- Download a copy of the project from Github: [Json Server](https://github.com/typicode/json-server)
- Download a copy of the Contacts data in JSON format: [contacts.json]('./contacts.json)

> ## Activity - Test Server
> Follow the video to setup a test server.

<center>
  <video width="620" height="348" controls>
    <source src="./Angular_Services_Setup_JsonServer.mkv" type="video/webm">
  </video>
  <figcaption>Video: Setup Json-Server</figcaption>
</center>

## Get contacts with HttpClient

We can make a `GET` request using the `HttpClient` by executing the built in `get()` method.

### Steps:

The `ContactService` must be updated to use the `HttpClient`. Here are the 6 steps required.

1. Import the `HttpClient`
2. Import the RxJS `Observable`
3. Specify the url of the json-server 
   - The default value is `http://localhost:3000`. Check the terminal output of the `json-server` application to confirm.
4. Inject the `HttpClient` into the constructor of the `ContactService`
5. Define the `getContacts` method signature
6. Define `HttpClient`'s `get` method return type.

```typescript
import { Injectable } from '@angular/core';
import { Contact } from '../models/contact.model';
// 1. Import the HttpClient service.
import { HttpClient } from '@angular/common/http';
// 2.  Import the Observable object from RxJS
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  // 3. Specify the Endpoint for the resource.
  contactsUrl = 'http://localhost:3000/contacts';

  // 4. Inject HttpClient into the ContactService
  constructor(private http: HttpClient) { }

  // 5. Use the HttpClient to fetch the contacts.
  getContacts(): Observable<Contact[]> {
    // 6. Define the get method return type.
    return this.http.get<Contact[]>( this.contactsUrl )
  }
  
}

```
<center> 
  <figcaption>Code: Use HttpClient to fetch Contacts.</figcaption>
</center>

## HttpClient methods - return values

All the `HttpClient` methods return an RxJS `Observable` of some type. In the `getContacts` method of the `ContactService`, it is defined as returning a RxJS `Observable` with an array of `Contacts`. Take note that it must be written in two places. 

1. As the method signature

```typescript
getContacts(): Observable<Contact[]> { ...
```

2. and as the return type for the `get` method of the `HttpClient`

```typescript
return this.http.get<Contact[]>( this.contactsUrl )
```

> **Aside**
> 
> In general, an Observable could return multiple values over time. However, the Observable returned from the `HttpClient` will only run once and complete.

## RxJS

RxJS is built into Angular and it's features can be used for a variety of tasks.

> **Further reading**
> 
> Detailed information about RxJS and its features can be found on the [Official RxJS website](https://rxjs.dev/guide/overview)

## Displaying Observable data

A component that consumes data as an `Observable` must use the `async pipe` to display asynchronous data. This is because data that is requested from a server is not immediately available. It takes time for an Http request to complete.

## Async pipe

In the `app.component.html` file there is currently a `*ngFor` loop that displays the contacts using a `ContactItemComponent`. However, after changing the `ContactService` to return an Observable with an array of contacts this will no longer work.

The solution for the template is to make use of the `async pipe`.

```html
<!-- Add the async pipe to the *ngFor loop -->
<app-contact-item *ngFor="let contact of contacts | async" [contact]="contact"></app-contact-item>
```
<center>
  <figcaption>Code: Add the async pipe to the <code>app.component.html</code>.</figcaption>
</center>

The `AppComponent` is still expecting the `contacts` property to be of type `Contact[]`, however the `ContactService` returns an `Observable`.

```typescript
... 

export class AppComponent implements OnInit {
  
  /* Change the type of the contacts property to match 
     the ContactService return type. */
  contacts: Observable<Contact[]>;
  
  ...
```
<center>
  <figcaption>Code: Change <code>contacts</code> property type to an <code>Observable</code> in <code>app.component.ts</code></figcaption>
</center>

The Angular `AppComponent` template now knows the data it receives from the logic file is of type `Observable` and will wait to render the data until it is emitted from the service.

> **Aside**
> 
>  Both the Angular application and the `json-server` application must be running.

## Handle Http Errors

When making Http requests errors will occur. The resource might be unavailable, an unauthorized request might have been initiated or a variety of other Http errors.

> **Aside**
> 
> Review Http Status codes on the Mozilla Developer Network; [Http Status Codes](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)

## RxJS - pipe, catchError, of

`pipe`

The pipe operator exists on `Observable` objects and can be used to chain multiple RxJS operators to accomplish errorHandling, data mutation or even logging.

```typescript
// contact.service.ts ...

getContacts(): Observable<Contact[]> {
  return this.http.get<Contact[]>( this.contactsUrl )
    .pipe(
      
    )
}

...
```
<center>
  <figcaption>Code: <code>contact.service.ts</code> - Add the <code>.pipe()</code></figcaption>
</center>

`catchError`

The RxJS operator `catchError` will catch any errors that have occurred during the HTTP Request. It provides an error object and the `Observable` object.

```typescript
// contact.service.ts imports
import { catchError } from 'rxjs/operators';

getContacts(): Observable<Contact[]> {
  return this.http.get<Contact[]>( this.contactsUrl )
    .pipe(
      catchError( )
    )
}

...
```

<center>
  <figcaption>Code: <code>contact.service.ts</code> - Add the <code>catchError()</code></figcaption>
</center>

## Generic error handler

It is good practice to write a generic error handler method rather than rewriting unique `catchError` code for each request being made.

```typescript
// contact.service.ts imports
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

// Generic error handler method.
handleError<T>(operation = '', result?: T) {
  // Return Observable of type T.
  return (error: any): Observable<T> => {
    
    // TODO: Send Error to remote location.
    console.log(`Error occurred on ${operation}`);

    // TODO: Execute a error message function to show user.

    // Forward result as Observable
    return of(result as T);
  }
}
```

<center>
  <figcaption>Code: <code>contact.service.ts</code> - Add the <code>handleError()</code> method.</figcaption>
</center>

`handleError<T>(operation = '', result?: T)`

The above method signature has some complex looking code but it is fairly simple once you understand what it means. 

## Generic type - `<T>`
The `<T>` means that this method could return many different types. It could be an array of contacts, perhaps a single contact or any other type. This means the method is reusable for almost any Http request in the application, irrespective of the data type being expected.

## Arguments - `operation, result?: T`

The `operation` argument is a simple string that can be used to identify the current method that was executed when the error occurred.

The `result` is an optional argument, as indicted by the use of the `?`. The `T` means that is must be the same type that will be returned by the method.

## Return value - `of(result as T)`

The RxJS `of` operator emits a variable amount of values and the emits a complete notification to close the Observable.

## Update `getContacts`

Add the `handleError` method to the `catchError` operator of the `getContacts` method.

```typescript
// contact.service.ts

getContacts(): Observable<Contact[]> {
  return this.http.get<Contact[]>( this.contactsUrl )
    .pipe(
      // Define the operation and provide empty array as a default result
      catchError( this.handleError<Contact[]>('getContacts', []) )
    )
}

...
```

<center>
  <figcaption>Code: <code>contact.service.ts</code> - Implement the <code>handleError</code> method.</figcaption>
</center>

In the above code snippet the `handleError` method is being implemented in the `catchError` and provides `getContacts` as the operator and an empty array as the default value that will be returned at the end of the `handleError` method.

## Closing on Services

The above content illustrates one way to architecture an application. This is not the only approach and must be adjusted to suit the needs of the application that is currently being worked on. 

More complex projects could require the use of `BehaviorSubject`, `Subscription` and any many other types of RxJS functions.

> ## Further reading - RxJS
> 
> To learn more about the RxJS library in Angular you can read the [Official Angular Documentation](https://angular.io/guide/rx-library) on [angular.io](https://angular.io/guide/rx-library)