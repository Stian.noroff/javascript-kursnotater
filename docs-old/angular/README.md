# Angular

> Module Overview

## Lessons

1. [**Basics**](./basics/README.md)

    &nbsp;

2. [**Components**](./components/README.md)

    &nbsp;

3. [**Services & Data**](./services/README.md)

    &nbsp;

4. [**Forms**](./forms/README.md)

    &nbsp;

5. [**Angular Router**](./router/README.md)

    &nbsp;