# Angular Router

Angular is used to build Single Page Applications. This means that what the user sees is parts of your application, or components that display a specific feature. 

In traditional applications, the browser would make a request to the server to fetch the new `HTML` and then display the response. In Single Page Applications, we use the idea of a `Router` to display a specific "View" or "Parent" component. 

We bind a URL path to a specific Angular Component. 

The current application will have a file called `app-routing.module.ts` found in the `src/app` folder. This file will keep track of which component belongs to which path. 

The current app that we built during the course currently have 3 components, excluding the `AppComponent`.

We will take each component, and tie it to its own path in our application.

```typescript
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './components/profile/profile.component';

const routes: Routes = [
  {
    path: 'profile',
    component: ProfileComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
```
<center>
    <figcaption>Code: Defining a route in the <code>app-routing.module.ts</code> file.</figcaption>
</center>

### Array of Routes - `routes: Routes = []`
The `routes` variable in the `app-routing.module.ts` contains the registered routes in the application. In the above code snippet, there is only one route defined for the `ProfileComponent`. 

###  `path: 'profile'`
The `path` property tells the router which path the component will be linked to. Whenever the browser points to `/profile` the Anguar Router will render the linked component.

> **Important** 
> 
> Do *not* add a `/` to the start of the path when declaring routes.

### `component: ProfileComponent`
The `component` property tells the router which component the `path` must render.

## Activating the router
For the application to use the router, we need at least one `router-outlet` defined in the application.

This must be added in our `app.component.html` template file. Remove *all* other `HTML` in the `app.component.html` file.

```html
<router-outlet></router-outlet>
```
<center>
<figcaption>Code: Defining a router-outlet for the application in the app.component.html</figcaption>
</center>

After this change, you will see the `ProfileComponent` render when you point the path of the browser to `/profile.

If you are using the default port for the Angular server, this will be `http://localhost:4200/profile`.

## Automatic Redirect
When deploying an application, you can not expect your users to type in your website address with a specific path. The application should automatically redirect to the path that must be displayed.

Angular provides this feature using the `redirectTo` property. 

```typescript
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './components/profile/profile.component';
import { CreateContactComponent } from './components/create-contact/create-contact.component';

const routes: Routes = [
  {
    path: 'profile',
    component: ProfileComponent
  },
  // Automatic redirect to /profile
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/profile'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
```
<center>
    <figcaption>Code: Automatic redirect using <code>redirectTo</code></figcaption>
</center>

### `pathMath`
In the above code snippet, the property `pathMatch: 'full'` tells the router only to execute the redirect if the path is *exactly* and empty path. In reality, all paths have an empty string at the start, therefore the `pathMatch` property ensures that it only executes when the path is exactly and empty string.

> **Further Reading**
> 
> You can read more about the router and its properties on the [Official Angular Documentation](https://angular.io/api/router/Route)


## Defining multiple paths
An application does not have a single path, therfore multiple entries in the `routes` variable is needed.

Before we continue, we need to consider that all the "View" code for our application was currently written in the `app.component.ts` file. The contacts subscription lived there and the `CreateContactComponent` was rendered in the `app.component.html`. 

## Create a `ContactListComponent`

We must create a seperate "View" or "Root" component for the `ContactItemComponent` to be rendered in. 

```bash
ng generate component components/contact-list
```
<center>
<figcaption>Command: The above command will generate a new <code>ContactListComponent</code></figcaption>
</center>

Generating a new component to render the `ContactItemComponent` is an application design decision. We need at least a "root" component the `Router` will use as a container and the `ContactItemComponent` will be rendered repeatedly for each contact in our "database".

Add the following to the `contact-list.component.html` file.

```html
<h1>Contacts</h1>

<!-- Display all the contacts using the ContactItem component with unique data-->
<app-contact-item *ngFor="let contact of contacts | async" [contact]="contact"></app-contact-item>
```
<center>
<figcaption>Code: Render multiple ContactItem components in the contact-list.component.html</figcaption>
</center>

```typescript
// contact-list.component.ts
import { Component, OnInit } from '@angular/core';
// Required Imports
import { Observable } from 'rxjs';
import { Contact } from 'src/app/models/contact.model';
import { ContactService } from 'src/app/services/contact.service';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {

  // Contacts Observable
  contacts: Observable<Contact[]>;

  // Injecting the ContactService
  constructor(private contactService: ContactService) { }

  ngOnInit(): void {
    // Fetching the contacts.
    this.contacts = this.contactService.getContacts();
  }

}
```
<center>
<figcaption>Code: Moving the logic from the <code>app.component.ts</code> to the <code>contact-list.component.ts</code></figcaption>
</center>

## Linking another path to a component
To link another path to a component, it can simply be added to the `routes` array in the `app-routing.module.ts`.

```typescript
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './components/profile/profile.component';
import { CreateContactComponent } from './components/create-contact/create-contact.component';
import { ContactListComponent } from './components/contact-list/contact-list.component';

const routes: Routes = [
  {
    path: 'profile',
    component: ProfileComponent
  },
  // Multiple Routes
  {
    path: 'contacts',
    component: ContactListComponent
  },
  {
    path: 'contacts/create',
    component: CreateContactComponent
  },
  // Redirect Must be last
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/profile'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
```
<center>
<figcaption>Code:  Multiple routes <code>contact-list.component.ts</code></figcaption>
</center>

The above code is written in the `app-routing.module.ts` file. There are three main routes, `profile`, `contacts` and `contacts/create`. Each of theses routes render a specific component in the application. 

These `components` are often referred to as `parent`, `view` or `container` components. 

> **Important note:**
> The `redirectTo` path must be written last, unless there is a `NotFound` component.

## Handling unknown routes
If you user types in an unknown route, your application can do one of two things. One, it can show a `404 Not found` page or two, it can simply redirect to a valid page. 

The first option is generally preferred as it could be a place to add some humour to your application. There are some very creative `404` pages that can be found on the internet.

### Defining a Not Found component.

```bash
ng generate component components/not-found
```

The above snippet will generate a new `NotFoundComponent` which can be used to define a new route.

```typescript
// app-routing.component.ts
...
{
    path: '**',
    component: NotFoundComponent
}
...
```

In the code snippet above, you will find an example of a `404` not found route. The `**` means any route that was not met in the previously declared routes. 

> **Important note:**
> This `NotFound` route must always be last. It should only execute if *none* of the paths in the `routes` array was found or matched.

You can create your own `404` page content.

## Navigating with the Router
The application currently has no way for a user to navigate between paths. There are two main options in Angular. 

### RouterLink navigation
The `routerLink` is a property that can be added to `<a>` anchor tags or other `HTML` elements.

We can easily see this by adding a new link in our `ProfileComponent`'s `HTML` template.

```html
<br />
<a routerLink="/contacts">View Contacts</a>
```
<center>
<figcaption>Code: Anchor tag with the routerLink attribute in <code>profile.component.html</code></figcaption>
</center>

In the above code snippet, it illustrates how to setup navigation between components using the `routerLink` attribute in Angular.

The `Router` will be triggered, redirect the website to the path specified, `/contacts` in this case and try to find this path in the `routes` array declared in `app-routing.module.ts`.

This in turn, will find the component that is linked to that path, and attempt to render it inside of the `router-outlet` added to the `app.component.html` file.

### Programmatic navigation
The Angular `Router` also provides and `injectable` service that allows for programmatic navigation in the app. 

The `Router` service must be injected to the `Component` that requires it. 

```typescript
// create-contact.component.ts
// Imports ...
import { Router } from '@anular/router';

...
// Inject Router service to CreateContactComponent.
constructor(private contactService: ContactService, private router: Router) { }
...
```

A good place to setup a programmatic navigation is once an action has been successfully performed. In our example application, adding a contact is a perfect scenario.

```typescript
// create-contact.component.ts
... 
  onFormSubmit(form) {
    this.contactService.createContact(
      this.contactForm.value
    ).subscribe((result: Contact) => {
      console.log("Contact Added");
      // Programmatically navigate using the Router
      this.router.navigateByUrl('/contacts');
    });
  }
...
```
<center>
<figcaption>Code: Programmatically navigate using the Router service in <code>create-contact.component.ts</code></figcaption>
</center> 

## Route parameters

The Angular `Router` also provides the ability to pass arguments or, `Router Params` between components. 

A route param is a part of the URL in the browser. 

Basic construction of a URL.

> | Protocol        | Domain        | Path          | Route Param (/:id) |
> | :-------------  | :-----------  | :-----------  | :----------- |
> | http(s)://      | mywebsite.com | /profile      | /1           |

Route parameters are designed to pass simple data link numbers or short strings. Route params should and can not be used to pass objects and complex data structures.

## Defining a Route param

> ## Activity - Before you continue
> To create a scenario where a route parameter can be used, a new component can be generated.

```bash
ng generate component components/contact-detail
```
<center>
<figcaption>Command: Generate a new component named <code>ContactDetailComponent</code></figcaption>
</center>

The `ContactDetailComponent` can be used to display detailed information about a single `Contact`. A very common way to identify a object or record in a data set is by using its `id` property or `key`.

## Router setup 
The first place we must start with implementing route paramters is in the  `app-routing.component.ts`. 

```typescript
// app-routing.component.ts
... 
  // routes
  
  {
    path: 'contacts/:id',
    component: ContactDetailComponent
  }

...
```
<center>
<figcaption>Code: Path with a route parameter named <code>id</code></figcaption>
</center>

Adding this to the `routes` array in the `app-routing.module.ts` will tell the `Router` that it requires a `Route Parameter` called `id`.

## Accessing the route param: `id`

Angular provides a couple of ways to access route parameters, but the simplest way is using the `ActivatedRoute`'s snapshot property.

The `ActivatedRoute` service must be injected to the component that requires to access the route parameter.

```typescript
// contact-detail.component.ts
import { Component, OnInit } from '@angular/core';
// 1. Import the ActivatedRoute service from angular.
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.css']
})
export class ContactDetailComponent implements OnInit {

  // 2. Component property to store contact id.
  private readonly contactId: number;

  // 3. Inject the ActivatedRoute service
  constructor(private route: ActivatedRoute) {
    // 4. Use the snapshot to access the route param - id
    this.contactId = Number(this.route.snapshot.paramMap.get('id'));
  }

  ngOnInit(): void {
  }

}
```
<center>
<figcaption>Code: Using the <code>ActivatedRoute</code> service to access route parameters.</figcaption>
</center>

If you look back at component LifeCycles, you will recall that the `constructor` executes before the `ngOnInit`. Therefore, it is a safe practice to assign the `id` route parameter to the `contactId` property in the `ContactDetailComponent` to later use this value in the `ngOnInit` LifeCycle hook.

## Updating the `ContactService`.
Having access to the Route parameter is great, except, it is not being used. The `json-server` allows adding arguments to only fetch a specific record. 

> ### **Further reading**
> For more on the options that come with the `json-server` you can take a look at the `Github` repository.
> Read more: [Json Server Github](https://github.com/typicode/json-server)

Add the following method, `getContact(id: number)` to the `ContactService`.

```typescript
// contact.service.ts
...
  
  // Use the HttpClient to get a Single Contact based on its id
  getContact(id: number): Observable<Contact> {
    return this.http.get<Contact>( `${this.contactsUrl}/${id}` )
      .pipe(
        catchError( this.handleError<Contact>('getContact:id', null) )
      )
  }

...
```
<center>
<figcaption>Code: <code>getContact()</code> method to fetch a single contact in the <code>ContactService</code></figcaption>
</center>

## Implement `getContact` method in Component

The implement the `getContact` method in our `ContactDetailComponent` we must first inject the service and create a property to store the contact data.

```typescript
import { Component, OnInit } from '@angular/core';
// Importing the Activated Route Service
import { ActivatedRoute } from '@angular/router';
import { ContactService } from 'src/app/services/contact.service';
import { Contact } from 'src/app/models/contact.model';

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.css']
})
export class ContactDetailComponent implements OnInit {

  // Component property to store contact id.
  private readonly contactId: number;
  // Property to store the current contact.
  contact: Contact;

  // Inject the ActivatedRoute service
  constructor(private route: ActivatedRoute, private contactService: ContactService) {
    // Use the snapshot to access the route param - id
    this.contactId = Number(this.route.snapshot.paramMap.get('id'));
  }

  ngOnInit(): void {
    // Subscribe to the service and set the contact.
    this.contactService.getContact( this.contactId ).subscribe(contact => {
      this.contact = contact;
    });
  }
}

```
<center>
<figcaption>Code: <code>ContactDetailComponent</code> setup to use a Route param to fetch data using a service.</figcaption>
</center>

The code is now setup to fetch the contact based on a unique `id` from the Angular `Router`'s parameter.  You can use the template file `contact-detail.component.html` to display the information for the contact.

```html
<!-- contact-detail.component.html -->
<a routerLink="/contacts">Back to contacts</a>
<br><br>
<div *ngIf="contact">
   <app-contact-item [contact]="contact"></app-contact-item>
</div>
```
<center>
<figcaption>Code: Render the contact in the Contact detail page</figcaption>
</center>

The template reuses the existing `ContactItemComponent` to avoid rewriting any code. 

### `*ngIf` 
The if is required at this point because the `contact` property is initially undefined. It will only be set once the `.subscribe()` on the `getContact` method has completed. 

## Conclusion

This concludes the section on the Angular router. There are many more advanced concepts and implementations of the router like Lazy Loading and Child Routes. These concepts can be studied on the official Angular documentation.

> ### Further reading
> To learn more about the Angular Router you can read the [Official Angular Documentation](https://angular.io/guide/router) or the Router.

