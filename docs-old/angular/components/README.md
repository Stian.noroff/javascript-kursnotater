# Angular - Components

> ## Activity - Before you start
>
> Create a new Angular project using the CLI called **address-book**. You can use this project to follow along with the notes.
>
>During the setup please select yes **enable Angular Routing** and select **CSS** as your styling format.

Run the following command to create a Angular project
```bash
ng new address-book --skipTests
```
<center>
    <figcaption>Angular CLI: New Project</figcaption>
</center>


Components are a core building block of the majority of modern JavaScript frameworks. 

Components can be seen as reusable parts of the application user interface. Depending on the size and frequency of usage, Components can be as small as a button or as large as a full page. 

In Angular, all applications must have at least one *root component*. This *root component* connects the entire application. Multiple other components can be rendered in the *root component*. 

As an example the *root component* could contain a navbar and footer that is the same on all of your application's pages.

The Angular CLI generates an `AppComponent` during the initial setup of a new project, which defaults to the *root component* in a new project.

## Types of components

Components in an Angular application can be divided into two categories. 

### Root Component
A root component can be viewed as any component that houses other components. Root components are often referred to as "Pages" in a project.

### Child Component
A child component is any component that will be used inside of Root Component. Depending on the hierarchy, a *child component* can also be considered a *root component*, if it hosts other components. The components hosted in it would then be called the child components. These child components are also sometimes called *Grandchild Components*.

> **Aside**
> 
> The Role of a component may change depending on it's position in relation to other components.

### Component hierarchy

<center>
    <img src="./Angular_Components_hierarcy.png" alt="Component tree" width="300">
    <figure>
        <figcaption>Figure: Component hierarchy</figcaption>
        <small><i>Source: https://angular.io/guide/architecture-components</i></small>
    </figure>
</center>

# Creating new components

New components are creating in Angular using the Angular CLI. The command can be divided into 3 parts.

<center>
    <figure>
        <img src="./Angular_Components_Generate_CLI.png" alt="Ng Generate Parts" width="680">
        <figcaption>
            Figure: Angular CLI - ng generate
        </figcaption>
    </figure>
</center>

## Activity - Before you continue

Generate a new component called `Profile` in a `components` folder using the Angular CLI

```bash
ng generate component components/profile
```

Remember, you can use shorthand to generate components too.

```bash
ng g c components/profile
```

## Dissecting components

In Angular, a component is made up of three parts. 

<center>
    <figure>
        <img src="./Angular_Components_parts.png" alt="Angular Component 3 Parts" width="420">
        <figcaption>Figure: 3 Angular Component parts</figcaption>
    </figure>
</center>

## Logic - TypeScript
The logic of a component is written in a TypeScript class. The Logic file provides data to the view, or template that it should display. Data should be prepared for the view to be easily displayed. All public methods and properties in the component class are accessible by the view.

In TypeScript classes you define properties inside of the class code block. Below is an example of defining 2 public properties and one private property.


```typescript
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  // Public properties
  fullName: string = 'Luke Skywalker';
  about = {
    occupation: 'Jedi',
    powers: ['telekinesis', 'mind control'],
    father: 'Anakin Skywalker'
  };

  // Private properties
  private headquarters: string = 'Unknown'

  constructor() { }

  ngOnInit(): void {
  }

}
```

<center>
    <figcaption>Code: Component public and private properties</figcaption>
</center>

When defining a property without an access modifier (public, private or protected) it defaults to public. This can be seen on lines 10 and 11. The property `headquarters` has the `private` modifier and is therefore not accessible in the template. 

> **Aside**
> 
> It should be noted that private properties are accessible during development but will cause build errors when you try to build a project where the view accesses private properties from a component's class.

## Template / View - HTML
The template or view, contains the HTML or "Front-end" for a component. Each Angular component has its own HTML that will be rendered whenever it is used within your application.

Component views should not contain complex logic and display data is provided by the logic file. 

To display the properties defined in the *Logic Section* above we can use `Interpolation`.

```html
<!-- profile.component.html -->
<div class="profile">

    <h1 class="title">{{ fullName }}</h1>

    <h4 class="subtitle">About</h4>
    <ul class="profile-about">
        <li class="profile-about_item">Occupation: {{ about.occupation }}</li>
        <li class="profile-about_item">Father: {{ about.father }}</li>
    </ul>

    <h4 class="subtitle">Powers</h4>
    <ul class="profile-powers">
        <li class="profile-power" *ngFor="let power of about.powers">
            {{ power }}
        </li>
    </ul>

</div>
```

<center>
    <figcaption>
    Code: Rendering component properties - profile.component.html
    </figcaption>
</center>

In the above code snippet, the component will display the property values of the component in the HTML. Angular uses Interpolation to replace the property names with the property values.

## Style - CSS
Each component contains its own styling. The styles defined in an Angular component are scoped to the component only. Therefore, styling in your components won't bleed out to other components.

```css
/* profile.component.css */
.profile {
    font-family: Arial, Helvetica, sans-serif;
    padding: 3em;
}

.title {
    font-size: 3em;
    margin: 0 0 .5em;
}

.subtitle {
    font-size: 2.2em;
    margin: .5em 0;
}

.profile-about,
.profile-powers {
    list-style-type: none;
    margin: 0;
    padding: 0;
}
```

<center>
    <figcaption>Code: Component CSS - profile.component.css</figcaption>
</center>

> **Further reading**
> 
> There are options available to alter the behaviour of styles in Angular components by changing the ViewEncapsulation property. 
> If you would like to read more about this head over to the [ViewEncapsulation documentation](https://angular.io/api/core/ViewEncapsulation).


## Render your component

Can you remember how to render your component?

> ## Activity - Try to render the ProfileComponent
> 
> Try to add the profile component to the AppComponent's HTML of your application. This is the expected result of the application.

&nbsp;

<center>
    <figure>
        <img src="./Angular_Components_RenderedProfile.png" width="650">
        <figcaption>Figure: Rendered Profile Component</figcaption>
    </figure>
</center>
&nbsp;

# Component LifeCycle Hooks

Lifecycle hooks in Angular start when the Component initialises with its child components. Once a component is initialised the hooks continue with change detection and finally ends when Angular destroys the component. 

To respond to a Lifecycle hook we must add it to the component class' implements arguments and write out the relevant method associated with the Lifecycle Hook. Below you can see a component that implements 2 lifecycle hooks, the OnInit and AfterViewInit lifecycle hooks. 

Angular automatically executes these hooks during the initialisation of the component. 

```typescript
import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor() {
    console.log('1. Constructor executed');
  }

  ngOnInit(): void {
      console.log('2. OnInit Lifecycle Hook Executed');
  }

  ngAfterViewInit(): void {
    console.log('3. AfterViewInit Lifecycle Hook Executed');
  }

}
```

<center>
    <figcaption>Code: LifeCycle Hooks in an Angular Component</figcaption>
</center>

Below you can find a couple of Lifecycle hooks in an Angular component. 

> | Hook Method     | Purpose      | Timing       |
> | :-------------  | :----------  | :-----------  |
> | `ngOnChanges()` | Respond when Angular sets or resets data-bound input properties. | Called before `ngOnInit()` and whenever one or more data-bound input properties change. |
> |  `ngOnInit()` | Initialize the directive or component after Angular first displays the data-bound properties and sets the directive or component's input properties.   | Called once, after the first `ngOnChanges()`.    |
> | `ngAfterViewInit()`   | Respond after Angular initializes the component's views and child views, or the view that contains the directive. | Called once after the first `ngAfterContentChecked()`. |

<center>
    <figcaption>Source: https://angular.io/guide/lifecycle-hooks</figcaption>
</center>

> **Further reading**
> 
> More detail on lifecycle hooks in Angular can be found in the [Official documentation](https://angular.io/guide/lifecycle-hooks) 

# Component communication

Components have two ways to communicate with each other; Inputs and Outputs. Both are a type of Decorator and require to be imported from `@angular/core`.

Component communication, using Input and Output, is only available between parent-to-child and child-to-parent. Sibling communication is not possible and a different approach is required to accomplish sibling component communication.

<center>
    <figure>
        <img src="./Angular_Components_comms.png" alt="Communication Flow in Angular" width="650">
        <figcaption>Figure: Component communication flow.</figcaption>
    </figure>
</center>

## Input

Components can receive data from its direct parent using an `Input`.

`Input` is a type of decorator in Angular and must be imported from the `@angular/core` package.

The parent passes information to the child component using square brackets `[]` in the template. The value inside the square brackets should match the name of the `@Input()` decorator in the child component. The value in the quotes should match the property name of the value that must be sent to the `@Input`.

You may have multiple Inputs in components.

> ## Activity - Before you continue
> To illustrate Input/Output, create a new component. 
> 
> Generate a new component called `ContactItem` in the components folder.

Use the Angular CLI to generate the component. 

```bash
ng g c components/contact-item
```

There will now be two components in your project. A `ProfileComponent` and a `ContactItemComponent`. To help explain component communication we will make use of the `ContactItemComponent`.

> **Aside**
> 
> When naming your components you should think of how your application might grow. A good name for a component that displays contact information in a list, might have been just that, Contact. 
> 
> However, what if you needed to add a component that displays a single Contact's information? This too could be named Contact. In this scenario, ContactItem for the list and Contact for a details page might make more sense. 

## Child Component Logic

To prepare a component to receive data, we must first declare an Input decorator and give it a name. Open the `contact-item.component.ts` and add the @Input decorator called `contact`;

> **Aside** 
> 
> Remember to check for the import of Input from `@angular/core`.

```typescript
import { Component, OnInit, Input } from '@angular/core';
// NOTE: Remember to import Input decorator

@Component({
  selector: 'app-contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.css']
})
export class ContactItemComponent implements OnInit {

  // Input decorator to receive data
  @Input() contact;

  constructor() { }

  ngOnInit(): void {
  }

}
```


<center>
    <figcaption>@Input() for contact-item.component.ts</figcaption>
</center>

Next is to display the data from the `@Input` in the template. An `Input` decorator behaves like a normal property, with the one exception that it is only available after the `ngOnInit` lifecycle.

```html
<div class="contact" (click)="onClicked()">
    <div class="contact-name">
        <span>{{ contact.firstName }} {{ contact.lastName }}</span>
    </div>
    <address>
        Phone: {{ contact.phone }} <br>
        Email: {{ contact.email }}
    </address>
</div>
```

<center>
    <figcaption>@Input() for <code>contact-item.component.html</code></figcaption>
</center>

The above code has now enabled the `ContactItemComponent` to receive data from its immediate parent. The next step is to send the data from the parent component. In this case, the `AppComponent`.


## Parent Component

The first step to sending data is, as you might have guessed, to have data. The parent component is responsible for hosting data being sent to its child components. 

Modify the `app.component.ts` file to add some data to your application.

```typescript
import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    // An array of contacts
    contacts: any[] = [
        {
            "id": 1,
            "firstName": "Han",
            "lastName": "Solo",
            "phone": "555-2902",
            "email": "han.solo@starwars.com"
        },
        {
            "id": 2,
            "firstName": "Boba",
            "lastName": "Fett",
            "phone": "-",
            "email": "bountyfett@starwars.com"
        }
    ];


    ngOnInit(): void {
    }

}

```

<center>
    <figcaption>Contacts array in app.component.html</figcaption>
</center>

Change the `app.component.html` to match the following.

```html
<div>
    <aside>
        <app-profile></app-profile>
    </aside>
    <main>
        <!-- Display 2 contacts using the ContactItem component with unique data-->
        <app-contact-item [contact]="contacts[0]"></app-contact-item>
        <app-contact-item [contact]="contacts[1]"></app-contact-item>
    </main>
</div>
```

<center>
    <figcaption>Display 2 contacts in app.component.html</figcaption>
</center>

> **Caveat**
> 
> The @Input property of a component is only available in the `ngOnInit` lifecycle hook. 


## Output

Components may also send events and data to its parent component using an `Output`. The `Output` uses the parenthesis `()` as oppose to the square brackets to identify it as an output.

Using the @Output decorator is slightly more complex than the Input as it requires the use of an EventEmitter and a handler function.

Declaring an Output must be done in the Child Component that wants to "send a message" to its parent.

**Preparing the Child Component**

```typescript
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
// NOTE: Remember to import Input, Output and Event Emitter.

@Component({
  selector: 'app-contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.css']
})
export class ContactItemComponent {

  @Input() contact;

  // Output decorator to send data/events
  @Output() show: EventEmitter<number> = new EventEmitter();

  // Handle the click event on a contact.
  onClicked() {
    // Send a notification to the Parent component.
    this.show.emit( this.contact.id );
  }
}

```
<center>
    <figcaption>@Output() - contact.component.ts</figcaption>
</center>

Once the logic in the TypeScript file has been setup, a final step remains in the Child component. The HTML should listen for an event, like a click event, in the DOM. This click event will then be tied to the onClicked handler in the TypeScript file.

```html
<!-- Add a click listener to the component -->
<div class="contact" (click)="onClicked()">
    <div class="contact-name">
        <span>{{ contact.firstName }}</span>
        <span>{{ contact.lastName }}</span>
    </div>
    <address>
        Phone: {{ contact.phone }} <br>
        Email: {{ contact.email }}
    </address>
</div>
```
<center>
    <figcaption>@Output() - contact.component.html</figcaption>
</center>


**Parent Component Template**
```html
<div>
    <app-profile (edit)="handleEditClick()"></app-profile>
</div>
```
<center>
    <figcaption>app.component.html</figcaption>
</center>

**Parent Component Logic**
```typescript
// Import the Input decorator from the @angular/core package.
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  stylesUrls: ['./app.component.css']
})
export class AppComponent {

    handleEditClick() {
        // Handle the event from the ChildComponent (Profile).
    }
    
}
```
<center>
    <figcaption>app.component.ts</figcaption>
</center>

## Event Emitter

The `EventEmitter` is a special object provided by Angular. It is used to send events, with or without data, from a child component to a parent component.

### EventEmitter Types - Examples

The `EventEmitter` must have a defined type. Below are samples using the `EventEmitter` and how to emit an event to its parent.


**Emitting an event without data**
```typescript
export class ContactComponent {

    // Define EventEmitter sending no data
    @Output() clicked: EventEmitter<void> = new EventEmitter();

    onClicked() {
        this.clicked.emit();
    }
}
```
<center>
    <figcaption>Code: EventEmitter - No data</figcaption>
</center>

```typescript
export class ProfileComponent {
    // EventEmitter sending a string
    @Output() statusChanged: EventEmitter<string> = new EventEmitter();
    
    onDeleteClicked() {
        this.statusChanged.emit("Taking a rest");
    }
}
```
<center>
    <figcaption>Code: EventEmitter - String</figcaption>
</center>


```typescript
export class ProfileComponent {
    // EventEmitter sending a boolean
    @Output() seen: EventEmitter<boolean> = new EventEmitter();
    
    onViewMessage() {
        this.seen.emit(true);
    }
}
```
<center>
    <figcaption>Code: EventEmitter - Boolean</figcaption>
</center>

```typescript
export class ProfileComponent {
    // EventEmitter sending all types
    @Output() delete: EventEmitter<any> = new EventEmitter();
    
    onDeleteClicked() {
        this.delete.emit({ userId: 1 });
    }
    
}
```
<center>
    <figcaption>Code: EventEmitter - Object</figcaption>
</center>

> **Further reading**
> To read more detail about the @Input and @Output decorators head over to the [Official Angular Documentation](https://angular.io/guide/inputs-outputs).

> ## Activity - Display all contacts
> 
> Try to use the Contact component along with the ngFor structural directive to display all the contacts in the AppComponent.

## Solution

```html
<div>
    <aside>
        <app-profile></app-profile>
    </aside>
    <main>
        <!-- Display all the contacts using the ContactItem component with unique data-->
        <app-contact-item *ngFor="let contact of contacts" [contact]="contact"></app-contact-item>
    </main>
</div>
```

<center>
    <figcaption>Code: Loop over contacts app.component.html</figcaption>
</center>
