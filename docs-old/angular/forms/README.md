# Angular Forms

When building an application that works with data it is almost inevitable that data input will be required at some point. In web development, HTML forms are used to capture data and send the data off to a remote database.

Angular provides the powerful `FormsModule` and `ReactiveFormsModule`. This section will continue on from the `Services`. 

## Topics covered

This lesson will be divided into too main topics, Angular `FormsModule` and `ReactiveFormsModule`.

### FormsModule and NgModel
You will learn how to incorporate the `FormsModule` and use the `ngModel` to enable two-way data binding in an Angular application. 

### ReactiveFormsModule and form validation
You will also learn how to use the powerful `ReactiveFormsModule` with declarative and programatic form setup. It will also show how to access the `ReactiveFormsModule`'s built in form validation and error handling in your web forms. 

## Two-Way binding

Two way binding in Angular is provided with the `ngModel` directive. The `ngModel` directive creates an instance of a `FormControl` which tracks the value of the input and provides input validation.

## Enable FormsModule

To use the `FormsModule` in an Angular application it must be imported into the `AppModule`.

1. First add the import from `@angular/forms` at the top if the `app.module.ts` file.

```typescript
import { FormsModule } from '@angular/forms';
```
2. Secondly, include the `FormsModule` in the `imports` array of the `AppModule`.

```typescript
// app.module.ts ...
imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    // Add the FormsModule
    FormsModule
],

...

```
<center>
    <figcaption>Code: Add <code>FormsModule</code> to the <code>AppModule</code>'s import array.</figcaption>
</center>

The above will enable the use of the `ngModule` in forms of an Angular application. 

## Goal

The goal is to be able to create new contacts to our current application. The form must be able to accept the basic information of a contact like first and last name, email and phone number.

> ## Activity - CreateContact Component
> Before we can add a form we need a component that allows us to create a form. Generate a new component using the Angular CLI to generate a component called `CreateContact`.

```bash
ng generate component components/create-contact
```

## Creating a Form

Creating a form in Angular is very similar to creating a form in normal HTML. It uses the same basic syntax as regular HTML with some additional attributes, or Angular directives.

## 1. Create a basic HTML form

```html
<form #contactForm="ngForm" (ngSubmit)="onFormSubmit(contactForm)">

</form>
```
<center>
    <figcaption>Code: A basic Angular Form with submit listener</figcaption>
</center>

In the above code sample you can see how we declare a basic Angular form. The form accomplishes to important tasks. 

### `#contactForm`
Gives the form a name and accessible property in the template. 

### `(ngSubmit)="onFormSubmit(contactForm)"`
This creates a submit listener for the form. This method will be written in our logic file, `create-contact.component.ts`, later.

The `$event` argument will pass the form object to the handler function, `onFormSubmit`.

## 2. Adding inputs with the `ngModel`

## 2.1 Binding properties

To store and access data entered in the form we must make use of the `ngModel` directive in Angular.

```html
<form #contactForm="ngForm" (ngSubmit)="onFormSubmit(contactForm)">

<input ngModel [(ngModel)]="firstName" type="text" placeholder="First name" name="firstName" required />

</form>
```

The above code snippet defines a new input in the form and links it to a `firstName` property using the `ngModel`. 

### `[(ngModel)]`
The `ngModel` uses the "Banana in a box" syntax. The Two parenthesis in the square brackets look like two bananas in a box. `[()]`. 

_Or so I am told._

The reason for using both square brackets and parentheses leads back to the topic on the `@Input` and `@Output` lesson.

If you can recall, `@Input` used square brackets when sending information to a component `[]`. While the `@Output` used parentheses `()` to emit data from a component to a parent. You can see the `ngModel` syntax as a combination of the two. 

This provides the form with Two-Way binding.

> **Important**
> 
> You _MUST_ add the name attribute when using the `ngModel` or the two-way binding will not work.

> ## Caveat
> To make use of the *banana in a box* syntax, we do also have to create a property in the logic file to match the name of the `ngModel`. This is not a major problem for small forms but when validation becomes more complex this pattern is not sufficient.

## 2.2 Binding to form properties

The other option is making use of the `ngModel` directive and creating a form variable in the template. 

The form refers to the `#contactForm="ngForm"` attribute on the `form` tag.

### Using the ngModel directive

```html
<input ngModel firstName="ngModel" type="text" placeholder="First name" name="firstName" #firstName />
```

`ngModel=firstName`

In the above code snippet the *Banana in the box* syntax is replaced with a normal Angular directive syntax. Here, the form variable name is `firstName` and it is defined as a `ngModel`. 

`#firstName`

The variable name works the same way the `contactForm` name. The `#` symbol tells the Angular Framework that this is a property that is declared in the template. It will automatically be attached to the `contactForm`. 


## 3. Complete the form inputs

To complete the form, all the required properties of a contact must be added. It is required to add an input, select or any other HTML form element to capture data for all the properties of an object.

```html
<form #contactForm="ngForm" (ngSubmit)="onFormSubmit(contactForm)">

    <input ngModel firstName="ngModel" type="text" placeholder="First name" name="firstName" #firstName />

    <input ngModel lastName="ngModel" type="text" placeholder="Last name" name="lastName" #lastName />

    <input ngModel #phone="ngModel" type="text" placeholder="Phone number" name="phone" #phone />

    <input ngModel #email="ngModel" type="text" placeholder="Email address" name="email" #email />
</form>
```

You can also add some basic `css` in `create-contact.component.css` to arrange the form in a more readable manner.

```css
:host {
    margin-bottom: 1em;
    display: block;
}
input {
    display: block;
    margin-bottom: .5em;
    font-size: 1em;
}
```
<center>
    <figcaption>Code: Basic CSS for the <code>CreateContactComponent</code></figcaption>
</center>


If you try to run the application at this point, you will receive a couple of errors. 

```bash
 TS2339: Property 'onFormSubmit' does not exist on type 'CreateContactComponent'.

<form #contactForm="ngForm" (ngSubmit)="onFormSubmit(contactForm)">
```

One of the reasons for this is that the method `onFormSubmit` has not been defined in the logic file, `create-contact.component.ts`.

## 4. Link form to Component Logic
The final step of the form setup is to link an `event handler` for the `submit` event of the form.


### 4.1 Submit Handler

The submit the data of a form to a remote database or server, it must be submitted once the user has clicked on a button. In the above code snippets the `ngSubmit` listener will catch a form submission and pass it to the handler in the `create-contact.component.ts` file.

Add a submit button to the bottom of the form in the `create-contact.component.html` file.

```html
    ...
    <button type="submit">Create contact</button>
 </form>
```
<center>
    <figcaption>Code: Form submit button in <code>create-contact.component.html</code></figcaption>
</center>

The next step in form submission is to have a handler function that captures submissions. It has already been defined in the form's `ngSubmit` listener as `onFormSubmit`. 

This handler function for a form must be declared in the logic file of the component.

```typescript 
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-contact',
  templateUrl: './create-contact.component.html',
  styleUrls: ['./create-contact.component.css']
})
export class CreateContactComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  // Form submission handler
  onFormSubmit(form) {
    // Boolean, is the form valid
    console.log( form.valid );

    // Form input values - Matched to form properties.
    console.log( form.value );
  }

}

```

At the moment this form will not be visible on the application. It needs to be rendered by the `AppComponent`.

```html
<div class="app">
    <aside>
        <app-profile></app-profile>
    </aside>
    <main>
        <!-- Display the CreateContactComponent. -->
        <app-create-contact></app-create-contact>
        <!-- Display all the contacts using the ContactItem-->
        <app-contact-item *ngFor="let contact of contacts | async" [contact]="contact"></app-contact-item>
    </main>
</div>
```
<center>
    <figcaption>Code: <code>app.component.html</code></figcaption>
</center>

```css
h1 {
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    font-weight: bold;
    text-align: center;
    color: #333;
}

* {
    box-sizing: border-box;
}

.app {
    display: grid;
    grid-template-columns: 4fr 8fr;
}

.app-main {
    padding: 2em 1em;
}
```
<center>
    <figcaption>Code: <code>app.component.css</code></figcaption>
</center>

The above code is very basic CSS that minimally organises the application layout. If the content of the `CSS` is unclear you can read this external link that explains it in detail: [CSS-Tricks: A Complete Guide to Grid](https://css-tricks.com/snippets/css/complete-guide-grid/)

The result of the above will give you a type of "Dashboard" page that displays a `ProfileComponent`, a `ContactList` and the `CreateContactComponent`.

<center>
    <figure>
        <img src="./Angular_Forms_CreateContact_in_AppComponent.png" alt="Create Contact Component displayed in AppComponent">
        <figcaption>Figure: Application "Dashboard".</figcaption>
    </figure>
</center>

On form submission you can see the `form` values being printed in the console. 

<center>
    <figure>
        <img src="./Angular_Forms_CreateContact_ConsoleLogForm.png" alt="Create Contact Component Console Log of Form">
        <figcaption>Figure: Create contact console log of form submission.</figcaption>
    </figure>
</center>

## 4.2 Two-Way binding

Two way binding can easily be demonstrated by adding some code to the `HTML` of the `create-contact.component.html` file.

Add the following code to the bottom of the  `create-contact.component.html` before the closing `form` tag.

```html
    ...
   <button type="submit">Create contact</button>

    <!-- Test Two Way Binding with NgModel -->
    {{ firstName }} - {{ lastName }} - 
    {{ phone }} - {{ email }}
</form>
```

Angular will interpolate the values of the form into the properties of the `CreateContactComponent` class. Whenever the value is changed in the input, the value with also change in the property. This is essentially two-way binding in Angular. 

<center>
  <video width="620" height="348" controls>
    <source src="./Angular_Forms_Interpolation_with_NgModel.mkv" type="video/webm">
  </video>
  <figcaption>Video: NgModel with Interpolation.</figcaption>
</center>

## 4.3 Basic validation

In the form there is a special variable that was called `#contactForm`.

```html
<form #contactForm="ngForm" (ngSubmit)="onFormSubmit(contactForm)">
```

In Angular this is a variable declared in the `HTML` template and can only be accessed in the `HTML`. This variable is not accessible in the logic file (`create-contact.component.ts`).

### Making all inputs required

Using normal `HTML` attributes we can make all the fields required in the form. To do this, simply add the `required` attribute to each input element.

```html
... <!-- create-contact.component.html -->

<input [(ngModel)]="firstName" type="text" 
        placeholder="First name" name="firstName" 
        #firstName required />

<input [(ngModel)]="lastName" type="text" 
        placeholder="Last name" 
        name="lastName" 
        #lastName required />

<input [(ngModel)]="phone" type="text" 
        placeholder="Phone number" 
        name="phone" 
        #phone required />

<input [(ngModel)]="email" type="text" 
        placeholder="Email address" 
        name="email" 
        #email required />
        
...
```
<center>
    <figcaption>Code: Required inputs for <code>create-contact.component.html</code></figcaption>
</center>

Once the required attribute has been added to the inputs, the `#contactForm` object will automatically take this into consideration and apply the validation to itself. 

### `contactForm.valid`

The `contactForm.valid` property returns a boolean. If the form meets all the requirements based on the attributes (`required`, `minlength`, etc) it will return `true`. If not all fields meet the requirements, the `.valid` property will return `false`.


```html
<form #contactForm="ngForm" (ngSubmit)="onFormSubmit(contactForm)">

    <input ngModel firstName="ngModel" type="text" placeholder="First name" name="firstName" #firstName required />

    <input ngModel lastName="ngModel" type="text" placeholder="Last name" name="lastName" #lastName required />

    <input ngModel #phone="ngModel" type="text" placeholder="Phone number" name="phone" #phone required />

    <input ngModel #email="ngModel" type="text" placeholder="Email address" name="email" #email required />

    <button type="submit" [disabled]="!contactForm.valid">Create contact</button>

</form>
```

`[disabled]="!contactForm.valid"`

With the above code snippet by only adding a single check for the required fields, it can be assured that all fields will have a value before the user may submit the data entered in the forms.


# Reactive Forms

Angular provides a more customizable form module called `ReactiveFormsModule`. The main motivation for using `ReactiveForms` is the ability to completely customize input validation. 

As an example if you have a database that requires unique usernames for your application, `ReactiveForms` has the ability to do an asynchronous Http request to a remote location and validate an input based on the result of the request.

> **Caveat**
> 
> One caveat with using `ReactiveForms` is that it requires more setup than the `ngModel` pattern. If the form requires complex validation, it is recommended to use `ReactiveForms`.

## Enable Reactive Forms

Similar to the `FormsModule` the `ReactiveFormsModule` must be imported in the root `AppModule` and added to the `imports` array of the `AppModule`.

```typescript
// app.module.ts imports
import { ReactiveFormsModule } from '@angular/forms';
```

The `ReactiveFormsModule` must be imported from the `@angular/forms` package.

```typescript
// ... app.module.ts imports
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    ContactItemComponent,
    CreateContactComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    //------- Add ReactiveFormsModule to the imports.------- //
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

## Converting the `CreateContactComponent`'s form

This section will show how to create a `ReactiveForm` by changing the `CreateContactComponent`'s form. 

## `FormGroup` Instance

The `ReactiveForm` requires an instance of a `FormGroup`. This can be defined in the logic file (`create-contact.component.ts`).

```typescript
import { Component, OnInit } from '@angular/core';
// 1. Import the FormGroup 
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-create-contact',
  templateUrl: './create-contact.component.html',
  styleUrls: ['./create-contact.component.css']
})
export class CreateContactComponent implements OnInit {

  // 2. Declare a FormGroup instance named contactForm.
  contactForm: FormGroup = new FormGroup({
    // Form Controls 
  });

  constructor() { }

  ngOnInit(): void {
  }

  onFormSubmit(form) {
    console.log( form.valid );
    console.log( form.value );
  }

}
```
<center>
    <figcaption>Code: FormGroup instance in <code>create-contact.component.ts</code></figcaption>
</center>

In the above code snippet a new instance of a `FormGroup` called `contactForm` was created. It is **important** to know that this currently is not related to the `#contactForm` property in the template.

## `FormControl` 

The `ReactiveForm` instance requires an instance of a `FormControl` for every input that needs to be validated or captured. Each `FormControl` instance can have its own validation.

### Defining a `FormControl`

```typescript
// create-contact.component.ts
...

contactForm: FormGroup = new FormGroup({
    firstName: new FormControl('', [ Validators.required ])
});

...
```

### `FormControl('', [])`
The first argument of the `FormControl` is the default value for the form input. 

In this case, the default value is an empty string. 

### `[ Validators.required ]`
Angular has a collection of built-in validators. 

> **Further reading**
> 
> This content will cover some basic validators, if you would like to read more in depth you can head over to the [Official Angular Documentation](https://angular.io/api/forms/Validators).


### Complete the `FormControl` instances

Add all the remaining form controls required for the `CreateContactComponent`'s form.

```typescript
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-contact',
  templateUrl: './create-contact.component.html',
  styleUrls: ['./create-contact.component.css']
})
export class CreateContactComponent implements OnInit {
  
  // FormGroup with FormControls.
  contactForm: FormGroup = new FormGroup({
    firstName: new FormControl('', [ Validators.required ]),
    lastName: new FormControl('', [ Validators.required ]),
    phone: new FormControl('', [ Validators.required, Validators.minLength( 8 ) ]),
    email: new FormControl('', [ Validators.required, Validators.minLength( 6 ) ])
  });

  constructor() { }

  ngOnInit(): void {
  }

  onFormSubmit(form) {
    console.log( form.valid );
    console.log( form.value );
  }

}
```
<center>
    <figcaption>Code: A FormGroup with FormControl instances.</figcaption>
</center>

The above code introduces a new `Validator`, `minLength`. The `minLength` validator accepts 1 argument. This defines the minimum number of characters or numbers required for the input to be valid.

## `FormControl Getters`

Before the template can be connected, the form control instances must be easily accessible by the template. For this we can use a typescript feature called `getters`.

```typescript
// create-contact.component.ts

  // Expose the firstName FormControl instance.
  get firstName() {
    return this.contactForm.get('firstName');
  }

  ...
```

The above code snippet makes the `firstName` instance of the `FormControl` easily accessible as `firstName` in the template. It returns an `AbstractControl` which enables validation in the template.

### Complete `Getters`

```typescript
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-create-contact',
  templateUrl: './create-contact.component.html',
  styleUrls: ['./create-contact.component.css']
})
export class CreateContactComponent implements OnInit {

  contactForm: FormGroup = new FormGroup({
    firstName: new FormControl('', [ Validators.required ]),
    lastName: new FormControl('', [ Validators.required ]),
    phone: new FormControl('', [ Validators.required, Validators.minLength( 8 ) ]),
    email: new FormControl('', [ Validators.required, Validators.minLength( 6 ) ])
  });

  constructor() { }

  ngOnInit(): void {
  }

  // Getters for FormControls
  get firstName(): AbstractControl {
    return this.contactForm.get('firstName');
  }

  get lastName(): AbstractControl {
    return this.contactForm.get('lastName');
  }

  get phone(): AbstractControl {
    return this.contactForm.get('phone');
  }

  get email(): AbstractControl {
    return this.contactForm.get('email');
  }

  onFormSubmit(form) {
    console.log( form.valid );
    console.log( form.value );
  }

}
```

## Connecting a template to a FormGroup

Once the FormGroup has been setup in the logic file, it must be connected to the template. 

The current form must be altered to connect it to the new `FormGroup`. All the references to the `ngModel` can now be removed and the template variables declared with the `#` symbol can also be removed. 

```html
<!-- create-contact.component.html -->
<form [formGroup]="contactForm" (ngSubmit)="onFormSubmit(contactForm)">

    <input type="text" placeholder="First name" name="firstName" required formControlName="firstName" />

    <input type="text" placeholder="Last name" name="lastName" required formControlName="lastName" />

    <input type="text" placeholder="Phone number" name="phone" required formControlName="phone" />

    <input type="text" placeholder="Email address" name="email" required formControlName="email" />

    <button type="submit" [disabled]="!contactForm.valid">Create contact</button>

</form>
```
<center>
    <figcaption>Code: HTML Form connected to the FormGroup</figcaption>
</center>

### Some notable changes
All `#` symbols has been removed. 

### `[formGroup]="contactForm"`
The `FormGroup` in the `create-contact.component.ts` file has been connected to the `form` using the `[formGroup]` input directive.

### `formControlName="firstName"`
Each input has the `formControlName` attributed with the value of the `FormControl` it represents. In combination with the above `formGroup` assignment, the controls now form part of the `contactForm`.

### Fallback validation
It is important to add the same validation to the HTML as is declared for each `FormControl`. If something were to be unsupported it must fallback to the `HTML5` validation. 

You can add the `minlength` attributes to the `phone` and `email` inputs.

```html
    <input type="text" placeholder="Phone number" name="phone" required formControlName="phone" minlength="8" />

    <input type="text" placeholder="Email address" name="email" required formControlName="email" minlength="6" />
```


## Displaying validation errors

The `contactForm` will now actively be "watching" for validation on each of the inputs. Angular provides various helper properties on the `FormControl` instances that make displaying errors easy.


```html
<div *ngIf="firstName.invalid && ( firstName.dirty || firstName.touched )">
    <div *ngIf="firstName.errors.required">
        First name is required
    </div>
</div>
```
<center>
    <figcaption>Code: Displaying validation errors in a template</figcaption>
</center>

### `firstName.invalid`
This property is a `boolean` that, as the name suggests, is true when it does not meet the validation criteria defined for the `FormControl`.

### `firstName.dirty`
The `dirty` property is a boolean and is flagged to true if the input value has changed.

### `firstName.touched`
Whenever an input is `focussed` or clicked, it flags the `touched` property to true. 

The above checks ensure that the input has been interacted with before displaying any errors. It is bad `UX` to display that a field is required before a user has interacted with it. 

<center>
<figure>
    <img src="./Angular_Forms_Required_Validation.gif" alt="FormControl Required validation">
    <figcaption>Figure: Required validation of FormControl</figcaption>
</figure>
</center>

### Complete validation for all inputs

All the `FormControl` inputs require validation. Add a validation error for each. The `phone` and `email` will require multiple validation messages. One for the `required` validator and another for the `minlength` validator.

```html
<form [formGroup]="contactForm" (ngSubmit)="onFormSubmit(contactForm)">

    <input type="text" placeholder="First name" name="firstName" required formControlName="firstName" />

    <div *ngIf="firstName.invalid && ( firstName.dirty || firstName.touched )">
        <div *ngIf="firstName.errors.required">
            First name is required
        </div>
    </div>

    <input type="text" placeholder="Last name" name="lastName" required formControlName="lastName" />

    <div *ngIf="lastName.invalid && ( lastName.dirty || lastName.touched )">
        <div *ngIf="lastName.errors.required">
            Last name is required
        </div>
    </div>

    <input type="text" placeholder="Phone number" name="phone" required formControlName="phone" minlength="8" />

    <div *ngIf="phone.invalid && ( phone.dirty || phone.touched )">
        <div *ngIf="phone.errors.required">
            Phone is required
        </div>

        <div *ngIf="phone.errors.minlength">
            The phone number must be at least 8 numbers
        </div>
    </div>

    <input type="text" placeholder="Email address" name="email" required formControlName="email" minlength="6" />

    <div *ngIf="email.invalid && ( email.dirty || email.touched )">
        <div *ngIf="email.errors.required">
            Email is required
        </div>

        <div *ngIf="email.errors.minlength">
            The email must be at least 6 characters
        </div>
    </div>

    <button type="submit" [disabled]="!contactForm.valid">Create contact</button>

</form>
```

<center>
<figcaption>Code: Form validation display</figcaption>
</center>

```html
<div *ngIf="email.invalid && ( email.dirty || email.touched )">
    <div *ngIf="email.errors.required">
        Email is required
    </div>

    <div *ngIf="email.errors.minlength">
        The email must be at least 6 characters
    </div>
</div>
```

### Multiple validators

In the above code snippet there are multiple validator messages displayed. 
Both `Validator` checks must be nested within the initial `*ngIf` check. 

The `FormControl` provides an error object that dynamically changes based on the current 
validation state. It will only contain the errors specified during setup in the logic file.

```typescript
new FormControl('', [ Validators.required, Validators.minlength(8) ])
```

The above snippet will dynamically added the `error.required` and `error.minlength` properties *IF* it is invalid.

> **Further reading**
> 
> To learn more about Validators and setting up custom validators, you can read the [Official Angular Documentation](https://angular.io/guide/form-validation)

## Submitting Form Data with Services

> ## Continuation of Services
> 
> The next section links directly to the `Services` Module. In the `Services` module you only covered `GET` requests using the `HttpClient`. 
> 
> This will discuss how to create a `POST` request and update the `ContactItemComponent` list with the new `Contact`.

With all of the above out of the way, the data captured in the form can be sent to a server
using the `HttpClient`.

The `json-server` setup in previous lessons have the ability to add and delete entries into the `.json` file.
No extra code is needed to enable this feature.

### 1. Create a `POST` method in the `ContactService`

Before sending the data to the `json-server`, a method is required in the `ContactService` to execute a `POST` request.

```typescript  
  // services/contact.service.ts
  ...

  createContact(contact: Contact): Observable<Contact> {
    return this.http.post<Contact>( this.contactsUrl, {
        ...contact
    }).pipe(
      catchError( this.handleError<Contact>('createContent', null) )
    )
  }

  ...
```
<center>
<figcaption>Code: A createContact method that sends a POST Http request to add a new contact.</figcaption>
</center>

The above code executes a post request using the Angular `HttpClient`
and sends the `contact` argument as the body of the request.

### 2. Execute `createContact` on form submission

The last step is to link the `createContact` method with the `onFormSubmit`
handler in the `CreateContactComponent`.

**Remember**: Before using a `Service` it must be imported and `Injected` into the class by adding it to the class
`constructor`.

Import the `ContactService` and `Contact` model to the `create-contact.component.ts` file.

```typescript
// create-content.component.ts
import { ContactService } from 'src/app/services/contact.service';
import { Contact } from 'src/app/models/contact.model';

...
// Inject the service into the constructor.
constructor(private contactService: ContactService) { }

// Execute the createContact method in the form submit handler.
onFormSubmit(form) {
    this.contactService.createContact(
      this.contactForm.value
    ).subscribe((result: Contact) => {
      console.log("Contact Added");
    })
  }
```

### `this.contactForm.value`
Using the `.value` property of a `FormGroup` will generate a `Object` containing the form inputs as properties, with their respective values.

In this case, it will generate a `Contact` object.

```javascript
{
    firstName: "",
    lastName: "",
    phone: "",
    email: ""
}
```
<center>
    <figcaption>Code: Object generated by the .value property of a FormGroup</figcaption>
</center>

## Improvements 

There are many improvements that can be implemented. 
- The first thing that comes to mind is loading notifications or spinners.
- Another problem that will occur is that the ContactItem component will have no way of knowing about the existence of a new `Contact` after this method has finished executing.

## Improving Communication

To improve the communication a couple of things needs to change in the application. The way `Observables` are handled is the main change. The program needs to keep track of the `Contacts` therefor the `Service` will need to have a property that can be updated.

## 1. An `Contacts` list that can be updated

Step one to achieve this is to create an array of `Contacts` in the `ContactService`. This can be used to push new contacts and remove when contacts are deleted.

### `BehaviorSubject`

Another commonly used `RxJS` 

In the `ContactService` define a new `BehaviorSubject` of type `Contact[]`.

The `BehaviorSubject` is a special type of Observable that can emit data whenever the `.next()` method is executed. Incorrect use of the `BehaviorSubject` will cause subscriptions to be active in the app even when it is no longer used. This problem will be addressed in the `AppComponent`.

```typescript
// services/contact.service.ts - Imports
...
// 1. Import BehaviorSubject and tap 
import { Observable, of, BehaviorSubject } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  ...

  // 2. Create a PRIVATE contacts$ property of type BehaviorSubject
  private contacts$: BehaviorSubject<Contact[]> = new BehaviorSubject([])

  getContacts(): Observable<Contact[]> {
    this.http.get<Contact[]>( this.contactsUrl ).pipe(
        catchError( this.handleError<Contact[]>('getContacts', []) ),
        // 3. Add the tap operator with .next()
        tap(contacts => this.contacts$.next( contacts ))
      ).subscribe();

      return this.contacts$.asObservable();
  }

  createContact(contact: Contact): Observable<Contact> {
    return this.http.post<Contact>( this.contactsUrl, {
        ...contact
    }).pipe(
      catchError( this.handleError<Contact>('createContent', null) ),
      // 4. Add the tap operator and append new contact to current contacts.
      tap(contact => this.contacts$.next( [...this.contacts$.value, contact ] ))
    )
  }  

  ...
}
```
<center>
  <figcaption>Code: Adding the contacts$ property and BehaviorSubject and tap operator.</figcaption>
</center>

### 1. Import BehaviorSubject and tap
Add the `BehaviorSubject` import from the `rxjs` package. The `tap` operator must also be imported from the `rxjs/operators` package.

### 2. Declare the contacts$ property
The service will keep track of the current contacts in the application. The `Service` in your application is a `Singleton` instance, therefore the `contacts$` property will be persistent until the browser is refreshed.

The `$` at the end of the property name is a common naming scheme for `Observable` objects. It indicates that a property is an `Observable` type at a glance. 

### 3. The `tap` operator and `.next()`
The `tap` operator literally *taps* into the observable without modifying the response. It allows us as developers to access the data for logs or in this case, triggering a emission on the observable, `contacts$`.

The `.next()` method accepts an argument, that is the next value the `Observable` should emit to all of its subscribers.

This is useful when multiple components subscribe to an observable. 

**Important** - In the `getContacts` method, it no longer returns the `HttpClient` observable but the `BehaviorSubject` observable. This is crucial to make this design pattern work.

### 4. Accessing values using `contacts$.value` 
In the createContact, it is required to force and emission on the Observable using the `.next()` method. This is *crucial* so that the Subscribers to the `contacts$` Observable may receive the latest value for the contacts.

## Subscribing to the BehaviorSubject

The last step in the *conversion* process is to update our `app.component.ts` file. 

```typescript
import { Component, OnInit } from '@angular/core';
import { ContactService } from './services/contact.service';
import { Contact } from './models/contact.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  // 1. Change contacts to an Observable of type Contact[]
  contacts: Observable<Contact[]>;

  constructor(private contactService: ContactService) { }

  ngOnInit(): void {
    this.contacts = this.contactService.getContacts();
  }

}
```
<center>
  <figcaption>
    Code: Change the contacts property to and Observable of type Contact[]
  </figcaption>
</center>

### The `Observable<Contact[]>`
The contacts property is now a type of `Observable` and subscribes to the `BehaviorSubject` that exists in the `ContactService`. When ever the `ContactService`'s `BehaviorSubject` is updated using `.next()`, all subscriptions will receive a notification with the latest data.