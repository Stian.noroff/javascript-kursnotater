# Frontend JavaScript Notes

<img src="https://www.noroff.no/images/docs/vp2018/Noroff-logo_STDM_vertikal_RGB.jpg" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![web](https://img.shields.io/static/v1?logo=icloud&message=Online&label=web&color=success)](https://noroff-accelerate.gitlab.io/javascript/course-notes/)

> Course Notes for the Noroff Accelerate Frontend Course in JavaScript - Translated to Norwegian

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)

## Background

TODO

## Install

The handbook uses [mdBook](https://rust-lang.github.io/mdBook/) to render Markdown to static HTML. mdBook can be built from source, or [downloaded as precompiled binaries](https://github.com/rust-lang/mdBook/releases). At the current time, there are no standardized docker images for mdBook.

### Windows Prebuilt Binary

1. Visit the [Github Releases](https://github.com/rust-lang/mdBook/releases) page for mdBook and download the `windows-msvc` zip file for the latest version.
2. Unzip `mdbook.exe` to `C:\Users\<username>\AppData\Local\Programs\mdbook\mdbook.exe` and add the same folder to your `PATH` environment variable. The path environment variable can be modified as follows:

   > _Right clicking This PC → Properties → Advanced System Settings → Environment Variables → Select `PATH` → Click Edit → Click New → Input the above path without `\mdbook.exe`_

3. Restart your computer
4. Open Powershell and run `mdbook.exe -V`

If you receive a non-error response then mdbook is installed and configured correctly.

### Linux Prebuilt Binary

```sh
# 1. Download the latest release from the Github Releases page
wget https://github.com/rust-lang/mdBook/releases/download/vX.Y.Z/mdbook-vX.Y.Z-x86_64-unknown-linux-gnu.tar.gz

# 2. Untar the archive
tar -xvf mdbook-vX.Y.Z-x86_64-unknown-linux-gnu.tar.gz

# 2a. Optionally clean up the used tar archive
rm mdbook-vX.Y.Z-x86_64-unknown-linux-gnu.tar.gz

# 3. Make the binary executable
chmod u+x mdbook

# 4. Move to a location on the path
mv mdbook ~/.local/bin

# 5. Verify the application runs
mdbook -V
```

### macOS Prebuilt Binary

As per [Linux Prebuilt Binary](#linux-prebuilt-binary) but use the macOS binary instead of the Linux one.

### Docker

Currently no standard docker image exists for mdBook. As Docker is our preferred means of distribution, we will likely create an image of our own in the future when infrastructure permits.

## Usage

mdBook renders standard Markdown into HTML. Changes to the content can be done as necessary in the `src` folder. All changes should be tested locally and inspected for artifacts before being submitted for review.

mdBook can render the project locally by running:

```sh
# Linux in Bash
mdbook serve -n 127.0.0.1 -p 3000 -o

# Windows in Powershell
mdbook.exe serve -n 127.0.0.1 -p 3000 -o
```

This should open a browser window to http://localhost:3000 automatically.

## Maintainers

[Chantal van Wyk](https://gitlab.com/Chantal.wyk)

[Livinus Nweke](https://gitlab.com/livinusnweke)

[Marius Solheim](https://gitlab.com/masteesol)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

All Rights Reserved © 2022 Noroff Accelerate AS.

