# JavaScript-introduksjon

Denne leksjonen vil dekke de grunnleggende konseptene om JavaScript-språket.

## Hva er JavaScript?

På teknisk nivå er JavaScript et skriptspråk, eller et **[tolket programmeringsspråk](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/00_Introduction.html#interpreted-vs-compiled-code)** , som ble designet for å implementere kompleks logikk på nettet. Den ble opprinnelig opprettet av Brendan Eich i 1995 for Netscape-nettleseren, som til slutt ble grunnlaget for Mozilla Foundation og deres nettleser, Mozilla Firefox.

<aside>
ℹ️ JavaScript har hatt en rekke navn siden opprettelsen, inkludert: Mocha, LiveScript, JavaScript og til slutt ECMAScript . Selv om spesifikasjonen for språket nå er annerledes, blir språket fortsatt vanligvisreferert til som JavaScript.

</aside>

## JavaScript-motorer

![https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/javascript-engines.jpg](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/javascript-engines.jpg)

### Hvorfor?

Kode trenger et runtime-miljø for å kjøre, og en JavaScript-motor gir den kjøretiden (nettleser eller node). Hver av de store nettleserne har sin egen innebygde JavaScript-motor: Google Chrome bruker V8-motoren, Mozilla Firefox bruker Spider Monkey-motoren og Safari bruker JavaScript Core Webkit.

### Hva?

JavaScript-motoren analyserer, tolker og kjører JavaScript-koden du har skrevet.

### Hvordan?

Nettlesere har forskjellige implementeringer og kjøretider kan gi forskjellige APIer. Nettlesere har for eksempel **[Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)** der NodeJS ikke gjør det. JavaScript-motoren har en parser som setter kildekoden inn i datastrukturer ved å bruke **[Abstract Syntax Tree (AST)](https://en.wikipedia.org/wiki/Abstract_syntax_tree)**. Derfra genererer en tolk bytekode fra AST.

## Syntaks Parser

Syntaks - **[parseren](https://en.wikipedia.org/wiki/Parsing)** er et program som leser kode og sikrer at syntaksen er gyldig. Den sjekker koden for "stavefeil" eller syntaksfeil. Den oppretter AST (Abstract Syntax Tree) for tolk. Syntaks-parseren kan endre koden, eller legge til ny kode i skriptet.

## Tolk

**[Tolken](https://en.wikipedia.org/wiki/Interpreter_(computing))** kan kjøre kode uten å ha blitt kompilert til maskinkode tidligere. JavaScript-tolken bruker Just-in-time kompilering, hvor koden kompileres etter hvert som den kjøres.

# Tolket vs. kompilert kode

![Figur: Et JavaScript-program](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/InterpreterContextProgram.png)

Figur: Et JavaScript-program

![Figur: En JavaScript til "byte-kode"-kompilator](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/InterpreterContextCompiler.png)

Figur: En JavaScript til "byte-kode"-kompilator

På et tolket språk kjøres kode fra topp til bunn, og resultatet av å kjøre hver setning returneres umiddelbart. Hovedkarakteristikken til et tolket språk er at de krever et annet program, kalt **[runtime](https://en.wikipedia.org/wiki/Runtime_system)**, for å tolke og kjøre kildekoden.

Hovedfordelen med å bruke et tolket språk er at kildekoden kan kjøres uten å måtte gjøre antagelser om miljøet den kjøres i, så lenge det er en kjøretid tilgjengelig. Når det gjelder JavaScript, gir alle de store nettleserne (Safari, Chrome, Firefox og Edge) og NodeJS JavaScript-kjøringstiden som tolker kildekoden og avslører APIer som kildekoden kan bruke.

<aside>
ℹ️ Mens nettleserprodusenter anstrenger seg for å tilby et standardsett med APIer til det kjørende JavaScriptet på tvers av de forskjellige nettleserne; det skal bemerkes at NodeJS ikke gjør det. Dette vil bli diskutert videre nedenfor.

</aside>

Hver gang kjøretiden begynner å laste noe JavaScript-kode, blir kilden analysert ved hjelp av en tolk og redusert til en mellomrepresentasjon kjent som "bytekode". Når kilden er tolket, begynner kjøretiden å utføre bytekodeinstruksjonene og oversette dem til opprinnelige instruksjoner.

![Figur: Et JavaScript-program kjøres på en tolk i sanntid](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/InterpreterDiagrams-Interpreter.png)

Figur: Et JavaScript-program kjøres på en tolk i sanntid

Noen språk - som C# og Java, også tolkede språk - har et kompileringstrinn der kilden er kompilert til en bytekode "binær". Dette fjerner kostnadene ved å kjøre kompilatoren ved å kjøre den på forhånd, som tar en brøkdel av tiden å laste enn å reprodusere. Selv om disse språkene fortsatt er "kompilert", er de kompilert til bytekode som deretter tolkes på en kjøretid.

Andre språk kompilerer koden direkte til *opprinnelige instruksjoner*. Disse programmene krever ikke et kjøretidsprogram for å kjøre, de kan i stedet tolkes direkte av hardware (ved hjelp av operativsystemet).

<aside>
ℹ️ Språk som C# og Java har blitt mye mer effektive med årene ved å bruke just-in-time (JIT) kompilatorer. Dette er programmer som tolker bytekodeinstruksjoner og kompilerer dem direkte til opprinnelige instruksjoner, i sanntid, før de utføres.

Programmer som disse er fortsatt ikke like effektive som innfødte programmer, men er en enorm forbedring av tolkede språk. De fleste moderne JavaScript-tolkere bruker denne teknikken i en eller annen form.

</aside>

![Figur: En JIT-kompilator kjører et program som tidligere har blitt kompilert til et mellomformat kjent som "bytekode"](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/InterpreterDiagrams-JIT.png)

Figur: En JIT-kompilator kjører et program som tidligere har blitt kompilert til et mellomformat kjent som "bytekode"

![Figur: A C-programmet er kompilert direkte til maskinvareinstruksjoner og grensesnitt direkte med operativsystemet](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/InterpreterDiagrams-Compiled.png)

Figur: A C-programmet er kompilert direkte til maskinvareinstruksjoner og grensesnitt direkte med operativsystemet

## Leksikalsk scoping

**[Leksisk scoping](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures#lexical_scoping)** i henhold til`MDN`, er hvordan parseren løser variabelnavn når funksjoner er nestet.

```jsx
function init() {
  var name = 'Mozilla'; // name is a local variable created by init
  function displayName() { // displayName() is the inner function, a closure
    alert(name); // use variable declared in the parent function
  }
  displayName();
}
init();
}
```

Kilde: [MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures#lexical_scoping)

![Global Scope innkapsler alle andre scope](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/global-scope-circle.png)

Global Scope innkapsler alle andre scope

### Illustrerer leksikalsk scoping eller scope-innkapsling med kode

```jsx
// Global Scope
let globalVar = "Global";
// can NOT access funcAVar or funcBVar

function funcA() {
  // funcA Scope
  // can access globalVar
  // can NOT access funcBVar
  let funcAVar = "FuncA";

  function funcB() {
    // funcB Scope
    // can access globalVar
    // can access funcAVar
    let funcBVar = "FuncB";
  }
}
```

### Funksjoner med Scope-innkapsling

Funksjonomfangsvariabler kan dele navn med variabler som har *globalt* omfang. Den globale variabelen forblir uendret. Funksjonen vil imidlertid bruke variabelen som er deklarert i sitt omfang i løpet av funksjonens levetid.

```jsx
function foo() {
  console.log(baz); // What will the value of baz be?
}

function bar() {
  // Function scope variable
  let baz = "BAZ2";
  foo();
}

// Globally scoped variable
let baz = "BAZ1";
bar();
```

### Function Scoped variabler

Variabler deklarert med `var`nøkkelordet har funksjonsomfang. Funksjonsomfangede variabler vil eksistere til funksjonen der den ble erklært avsluttes. Når du erklærer en funksjonsomfangsvariabel inne i en kodeblokk som en `if`setning, `while loop`eller `for loop`vil føre til at variabelen vedvarer i minnet til funksjonsomfanget er avsluttet. Det anbefales derfor å bruke blokkomfangede variabler.

```jsx
var myVar = "var1";

if (true) {
  // Function scoped variable using var
  var myVar2 = "var2";
}

console.log(myVar2); // Output: "var2"
```

### Block scoped variabler

Variabler som er deklarert med nøkkelordene `let`og `const`, har blokkomfang. Dette betyr at variabelen vil bli fjernet fra minnet når kodeblokken avsluttes. Dette inkluderer if, while og even for loops.

Variabler med blokkomfang er bare tilgjengelige i blokk der de er deklarert. Å få tilgang til en variabel med blokkomfang utenfor kodeblokken der den ble deklarert, vil resultere i at en ReferenceError blir kastet. Det anbefales på det sterkeste å bruke block scoped variabler og er flott for minneadministrasjon.

```jsx
let myVar = "var1";

if (true) {
  // Block scoped variable using let
  let myVar2 = "var2";
}
// myVar2 will be removed from memory after the if block

console.log(myVar2); // ReferenceError: Can't find variable: myVar2
```

### Minnets livssyklus

Minnets livssyklus består av tre trinn:

- Allokering,
- Bruk
- Utgivelse.

Når en variabel, funksjon eller objekter opprettes, tildeler JavaScript-motoren minne for å lagre verdien av variabelen og frigjør den når den ikke lenger er nødvendig gjennom en automatisk prosess kalt søppelinnsamling (GarbargeCollection - GC). Søppelinnsamling bruker avanserte algoritmer for å avgjøre om en variabel fortsatt er nødvendig eller ikke.

![Minnets livssyklus *Kilde: [felixgerschau.com](https://felixgerschau.com/javascript-memory-management/)*](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/memory-life-cycle.png)

Minnets livssyklus *Kilde: [felixgerschau.com](https://felixgerschau.com/javascript-memory-management/)*

## Utførelseskontekst

Når du kjører en JavaScript-fil, er det to faser under utførelsesprosessen.

- Opprettelsesfasen
- Utførelsesfasen

### Opprettelsesfasen

Under opprettelsesfasen leses JavaScript-kode linje for linje. Alle variabler opprettes i minnet og lagres som `undefined`. Når funksjoner påtreffes, lagres hele koden for denne funksjonen i minnet. Funksjonen vil bli utført under utførelsesfasen. Funksjoner må også gjennom begge fasene for å skape en ny utførelseskontekst.

### Opprettelsesfasen oppsummert

- Oppretter variabler som *undefined*
- Lagrer funksjoner i minnet
- Utfører IKKE noen operasjoner
- Påkaller IKKE noen funksjoner

<aside>
⚠️ Det er viktig å huske at ingen operasjoner, oppgaver eller funksjoner utføres under opprettelsesfasen.

</aside>

### Utførelsesfasen

Under fase to, eller utførelsesfasen, kjøres kode og funksjoner påkalles. Koden utføres også linje for linje. Den kan inneholde kode som du ikke skrev. Kan legges til av "Stavekontrollen" eller Syntaks-parseren

### Utførelsesfasen oppsummert

- Den tildeler verdier til variabler
- Den utfører operasjoner
- Den påkaller funksjoner
- Påkalte funksjoner oppretter en ny kontekst og må starte på nytt med fase én

### Global utførelseskontekst

*Global Execution Context* opprettes første gang koden din kjøres. Vanligvis er det toppnivået til enhver fil som ikke er en JavaScript-modul. (Mer om dette senere)

- JavaScript kjører i en utførelseskontekst
- Wrapper eller container for gjeldende kode
- Globalt objekt levert for oss (vindu eller dette)
- Den ytre mest utførelseskontekst
- Ingenting utenom dette
- I nettlesere refererer "vinduet" eller "dette" til nettleservinduet
- I nettlesere gir "dokument"-objektet API-er for samhandling med HTML

### Funksjonsutførelseskontekst

Opprettes hver gang en funksjon påkalles

## JavaScript Event Loop

- JavaScript kan bare gjøre én ting om gangen
- Prioriterer kjøring av synkron kode
- Andre språk kan gjøre mange ting samtidig ved å bruke tråder
- For å administrere kjøring av asynkron kode bruker JavaScript Event Loop
- Laster av asynkrone oppgaver til Event Loop
- Utnytter nettleser-API-er til multitasking
- fetch, setTimeout, setInterval, etc.

```jsx
// 1. Synchronous - Prints first
console.log("Before the Timeout")

// 3. Event Loop - Prints third
setTimeout(function() {
  console.log("Inside the first Timeout")
}, 0)

// 2. Synchronous - Prints second
console.log("After the Timeout")

// 4. Event Loop - Prints four
setTimeout(function() {
  console.log("Inside the second Timeout")
}, 0)
```

<aside>
💎 **JavaScript Event Loop av Philip Roberts**

I denne videoen forklarer Philip Roberts i detalj hvordan JavaScript-hendelsesløkken fungerer

Kilde: **[JavaScript Event Loop av Philip Roberts](https://youtu.be/8aGhZQkoFbQ)**

</aside>

## Nettleserkonsollen

Hvis du har jobbet med andre programmeringsspråk som Python og Java, kan du være vant til å få utdataene dine gjennom terminalen på IDE-en. I Python og andre backend programmeringsspråk har du funksjon som skriver ut utdataene til terminalen/konsollen. I Python bruker du `print()`funksjonen og i Java bruker du `System.out.print()`metoden, og i JavaScript `console.log`tjener den et lignende formål. Når du bygger et statisk nettsted `console.log()`, kan du finne utdataene dine i nettleserkonsollen. For å åpne nettleserkonsollen, høyreklikk og klikk deretter "Inspiser". Du kan teste ut konsollen selv. Skriv`console.log("Write your message here")`inn i konsollen og nettleseren vil logge meldingen og skrive den ut til deg. Nettleserkonsollen brukes av webutviklere for å se om koden deres fungerer som den skal (som du ser på bildet nedenfor). Her prøver vi å referere til en variabel som ikke eksisterer, og nettleseren forteller oss hva som er galt i en feilmelding, uthevet i rødt.

![Nettleserkonsoll i Chrome](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/js-fundamentals-console.JPG)

Nettleserkonsoll i Chrome

## Hvor du skal skrive JavaScript

JavaScript kan skrives direkte i `<script>`taggene på en html-side, men den foretrukne måten er å skrive JavaScript-kode i en egen `.js`fil. I koden nedenfor kan du se en enkel `console.log()`utskrift av strengen "Hei fra JavaScript" til konsollen.

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>My website</title>
  </head>
  <body>
    <!-- Website HTML Goes here -->

    <!-- Create a script tag to write JavaScript -->
    <script>
      // Write JavaScript here
      console.log("Hello from JavaScript!");
    </script>
  </body>
</html>
```

Filen legges ved HTML-dokumentet ved å bruke `src`attributtet til `<script>`taggen.

```html
<script src="index.js"></script>
```

### Hvorfor skrive på slutten av `[<body>](https://noroff--accelerate-gitlab-io.translate.goog/javascript/course-notes/javascript/00_Introduction.html?_x_tr_sl=en&_x_tr_tl=no&_x_tr_hl=no&_x_tr_pto=wapp#why-write-at-the-end-of-the-body)`?

Nettlesere leser en analysere HTML fra topp til bunn, linje for linje. Plassering av en `<script>`tag i `<head>`dokumentet vil bety at JavaScriptet som er skrevet der, ikke vil ha tilgang til dokumentet, siden det `<body>`ennå ikke har blitt analysert av nettleseren.

## Variabler

### Hva er en variabel

En variabel er på en enkel måte midlertidig lagring av verdier som er tilordnet minnet. Det er som en beholder som kan lagre alle slags datatyper. Når du erklærer en variabel bruker du enten nøkkelordet `var`, `let`eller `const`nøkkelordet, som for eksempel `const number = 54`. Variabelnavnet er nummer og verdien er 54 . Variablene `var` og `let` kan endres slik at du kan tilordne dem på nytt. Med `const` variabelen kan du ikke gjøre dette fordi den er uforanderlig. Du kan i teorien bruke `let` nøkkelordet for å deklarere alle variablene dine, men dette anses som dårlig praksis. La oss se på de tre variabeltypene i detalj.

### JavaScript-variabler

### `let`

Dette er den moderne måten å deklarere variabler på, sammen med `const`nøkkelordet. Variablene som er deklarert med `let`nøkkelordet kan tilordnes på nytt, i motsetning til variablene som er deklarert med `const`nøkkelordet.

Disse variablene har blokkomfang, noe som betyr at de vil bli slettet fra minnet når kodeblokken er fullført.

```jsx
let hello = "Welcome to JavaScript";

console.log(hello); // Output: "Welcome to JavaScript"

hello = "Goodbye";

console.log(hello); // Output: "Goodbye"
```

### `const`

`const`-variabler kan bare tilordnes én gang. Som nevnt kan du utelukkende bruke `let`-variabler, men dette anses som dårlig praksis. I leksjonen om HTML snakket vi om semantisk markup der koden i seg selv formidler et budskap til andre utviklere.

Når du erklærer en variabel ved å bruke `const`nøkkelordet, forteller du alle som ser på koden din at verdien av denne variabelen skal forbli den samme og ikke skal deklareres på nytt.

Sammen med dets semantiske formål, vil deklarering av variabler med `const`nøkkelordet gjøre koden din mindre utsatt for feil ved å redusere risikoen for å omdeklarere en eksisterende variabel, en som ikke var ment å bli omdeklarert.

Du kan se i koden nedenfor at vi prøver å omklarere variabelen `hello`. Hvis denne variabelen ble deklarert med `let`nøkkelordet, ville det ikke være noe problem. Siden vi bruker `const`søkeordet, får vi en feilmelding som sier at `const` ikke kan tilordnes på nytt.

```jsx
const hello = "Welcome to JavaScript";

console.log(hello); // Output: "Welcome to JavaScript"

hello = "Goodbye"; // Throws an error, const may not be reassigned
```

### `var`

`var`er den eldre metoden for å deklarere variabler og brukes ikke mye i nyere applikasjoner. Det er ikke blokkomfang, men *funksjonsomfang* , som nevnt tidligere. Å gjenta; dette betyr at enhver variabel som er deklarert inne i en funksjon er tilgjengelig fra et annet sted innenfor samme funksjon. Så hvis du for eksempel deklarerer en funksjon i en `if`kodeblokk, kan du fortsatt få tilgang til denne variabelen fra utenfor `if`kodeblokken. I tilfellet nedenfor tilordner vi verdien fra "Bob" til "John", som fungerer helt fint ved å bruke en `var`variabeldeklarasjon.

```jsx
"use strict"

function printName() {
    if(true) {
        var firstName = "Bob";
        console.log("Inside of code block: ", firstName);
    }
    firstName = "John";
    console.log("Outside of code block: ", firstName);
  }

printName();
```

Output:

```
Inside of code block:  Bob
Outside of code block:  John
```

La oss prøve å tilordne verdien på nytt `firstName`når du bruker `let`søkeordet i stedet. Du kan se at vi legger til strengen `"use strict"`. Denne strengen aktiverer noe som kalles *strict* modus. I strict modus blir den dårlige syntaksen rotet ut og gir en feil, i motsetning til hva som skjer i "slurvet modus". I "slurvet modus" ville ikke koden nedenfor ha resultert i en feil. Å skrive riktig JavaScript er viktig for å sikre feilfri kode i hele applikasjonen. Les mer om strict modus **[her](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode)** .

```jsx
"use strict"

function printName() {
    if(true) {
        let firstName = "Bob";
        console.log("Inside of code block: ", firstName);
    }
    firstName = "John";
    console.log("Outside of code block: ", firstName);
  }
//Invoking the function
printName();
```

Output:

```
Uncaught ReferenceError: assignment to undeclared variable firstName
```

## Operatører

Når du skriver kode i JavaScript eller et hvilket som helst annet programmeringsspråk, vil du bruke **[operatører](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators)** til å utføre operasjoner, slik navnet *operatører* antyder.

Operatører er vant til å...

- Skape bivirkninger
- Tilordne verdier
- Endre verdier

Det er 4 hovedtyper:

- Aritmetikk
- Sammenligning
- Logisk
- Oppdrag

<aside>
💎 **MDN-dokumentasjon for operatører**

MDN-dokumentasjonen har et fantastisk sammendrag av operatørene tilgjengelig i JavaScript.

Kilde: **[https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators)**

</aside>

### Aritmetikk

Aritmetiske operatorer brukes til å utføre vanlige matematiske operasjoner. Tabellen nedenfor viser vanlige aritmetiske operasjoner:

[Untitled](https://www.notion.so/027d7aa86ca344969ecfa558c6f6b4fd)

Modulo-operatoren `%`beregner **resten** . Når `a = 5`deles med `b = 3`, er resten 2. Det er mest brukt med **heltall** (integers).

Nedenfor kan du se noen eksempler på hvordan du bruker de aritmetiske operatorene.

```jsx
let numberOne = 4;
let numberTwo = 2;

const added = numberOne + numberTwo;
const multiplied = numberOne * numberTwo;
const subtracted = numberOne - numberTwo;
const divided = numberOne / numberTwo;

//string concatenation
console.log("numberOne = " + numberOne + ", numberTwo = " + numberTwo)

//creating a string using a template literal. Formatting is preserved, as you see in the output
let results = "Results: " +  
  "Addition = " + added + 
  "Multiplication = " + multiplied + 
  "Subtraction = " + subtracted +
  "Division = " + divided;

//changing the value of the variables up and down by 1
numberOne++;
numberTwo--;

const incremented = numberOne;
const decremented = numberTwo;

//redeclaring existing variable
results = "Results: " +
  "Incremented numberOne = " + incremented + 
  "Decremented numberTwo = " + decremented;
```

Output:

```
numberOne = 4, numberTwo = 2

Results:
Addition = 6.
Multiplication = 8.
Subtraction = 2.
Division = 2.

Results:
Incremented numberOne = 5. 
Decremented numberTwo = 1
```

Når vi logger resultatet kan du se at det er to forskjellige metoder som brukes.

Den første kalles **[strengsammenkobling](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/Strings)** ved å bruke `+`operatøren til å koble sammen strenger og variabler.

Den andre metoden er å bruke [*template literals](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals)* der du bruker bakre haker i stedet for anførselstegn, og bruker `${expression}`som plassholder for variabler. Du vil se at denne metoden blir brukt mye. En fordel med denne metoden er at formateringen blir bevart. Så hvis du inkluderer linjeskift i malen din, vil utdataene inkludere linjeskift. Du vil få flere eksempler på bruk av malbokstaver senere.

### Tildeling

Tilordningsoperatorer (assignment operators) brukes til å tilordne verdier til variabler. Vi har allerede sett den grunnleggende `=`operatøren som tildeler verdier.

[Untitled](https://www.notion.so/83a664e9a3c344b5b3fca9e8bc200609)

La oss se på noen eksempler på hvordan du bruker noen av tilordningsoperatørene.

Først og fremst kan vi lage en enkel funksjon som logger resultatet til konsollen, slik at vi ikke trenger å skrive `console.log()`hele tiden.

```jsx
function logResult(result) {
    console.log("Result: " + result)
}
```

Bruker `+=`for strengsammenkobling:

```jsx
//Reassigning the value of customerName to "John"
let customerName = "Bob";
customerName = "John";

logResult(customerName);

//Using the += operator to add a string onto existing string.
customerName += " Lennon";

logResult(customerName);
```

Output:

```
Result: John
Result: John Lennon
```

La oss se hvordan operatøren `+=`og fungerer med tall. `-=`I dette eksemplet bruker vi en `while loop`, som du vil lære mer om i neste leksjon. Det grunnleggende om det er så lenge betingelsen inne i parentesen ikke er oppfylt, vil looping fortsette og koden i kodeblokken til `while loop`vil utføres. I dette tilfellet er betingelsen som følger: så lenge verdien av `num`er mindre enn 10, vil verdien av `num`øke med 2 til den når 10. Med den aritmetiske operatoren øker du `++`den nåværende verdien alltid med én, men med `+=`øke verdien med et hvilket som helst tall. Begge `++`og `+=`virker som aritmetiske operatorer fordi de fungerer på samme måte, men`+=`er en stenografisk måte å skrive a = a + b, som er en tilordning av en verdi.

```jsx
let num = 0;while(num < 10) {
    num += 2;
    logResult(num);
}
```

Produksjon:

```
Result: 2
Result: 4
Result: 6
Result: 8
Result: 10
```

Nå er verdien på `num`10, la oss lage en annen `while loop`, men denne gangen benytte `-=`operatoren som vil resultere i å redusere verdien fo num til betingelsen i parentesen er oppfylt.

```jsx
while(num > 2) {
    num -= 2;
    logResult(num);
}
```

Output:

```
Result: 8
Result: 6
Result: 4
Result: 2
```

### Sammenligning

Sammenligningsoperatorer brukes til å sammenligne to verdier.

[Untitled](https://www.notion.so/967e3b63d2ce417c8c99ed011feea0dd)

Hvis du er kjent med andre programmeringsspråk som Python eller Java, er du vant til å se "er lik" operatører skrevet `==`. I JavaScript har du to måter for dette: `==`og `===`. De er litt forskjellige i *hvor* lik verdier må være for at sammenligningen skal være sann. `==`vil sammenligne to verdier for å sjekke om de er like, men ignorerer datatypen til variabelen, der `===`bare vil være sant hvis både verdiene og datatypene deres er de samme. Beste praksis er å bruke `===`slik at sammenligningen din blir mer presis og for å unngå uforutsigbar oppførsel.

```jsx
console.log("1" == 1) // returns true
console.log("1" === 1) // returns false
console.log(1 === 1) // returns true
```

Sammenligningsoperatorer brukes til å lage betingelser, for eksempel innenfor parentesen til **`[if... else`utsagn](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/if...else)** eller ved looping (som vi gjorde med `while loop`). Dette er en del av det vi i programmering kaller **[kontrollflyt](https://developer.mozilla.org/en-US/docs/Glossary/Control_flow)** . Du vil lære mer om ulike typer kontrollflytstrukturer i neste leksjon, men du kan få en liten forhåndsvisning av det grunnleggende i en `if... else`erklæring nå, da det vil hjelpe med å illustrere formålet med sammenligningsoperatørene.

I `testLength()`funksjonen i eksemplet nedenfor sender vi inn en streng som en parameter og tester om strenglengden er mellom 10 *og* 15 tegn eller ikke. Hvis betingelsen er oppfylt, logger vi en suksessmelding, og hvis ikke, logger vi en beskrivende feilmelding. Du kan kombinere flere betingede operatorer ved å bruke den logiske operatoren `&&`, som for eksempel:

```jsx
const numOne = 14;

if(numOne >= 10 && numOne <= 20) {
  console.log("The number is between 10 and 20");
}
```

Igjen kan du se at vi bruker mal-literaler med plassholdere som inneholder variabelnavnene. Noe som kan være nytt for deg i denne kodebiten er **`[length](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/length)`**egenskapen, som teller antall enheter i en streng, inkludert mellomrom og symboler. `console.log("Oslo".length);`vil logge 4, da «Oslo» inneholder fire enheter.

```jsx
const minValue = 10;
const maxValue = 15;

function testLength(address) {
    if(address.length <= maxValue && address.length >= minValue) {
        console.log("Success: length criteria met");
    } else {
        console.log(`Error: address should be between ${minValue} and ${maxValue} characters long. The character length of your input is ${address.length}`); 
    }
};

// testing four street names to see if they pass the validation done by the testLength() function.

testLength("Davygate 2");
testLength("Whernside Ave 123");
testLength("Trinity Ln 10");
testLength("Vine St 8");
```

Output:

```
Success: length criteria met
Error: address should be between 10 and 15 characters long. The character length of your input is 17
Success: length criteria met
Error: address should be between 10 and 15 characters long. The character length of your input is 9
```

<aside>
⚠️ NB! Vær veldig forsiktig så du ikke forveksler `===` sammenligningsoperatøren med `=` oppdragsoperatøren, da disse tjener helt andre formål.

</aside>

### Logisk

Logiske operatorer brukes til å bestemme logikken mellom variabler eller verdier.

[Untitled](https://www.notion.so/dfb685b77fe848b282b8a7ec1cd2d6dc)

Vi har sett på den `&&`logiske operatoren i forrige kodeeksempel. En annen logisk operator som ofte brukes i programmering er den logiske *eller* , skrevet med lignende `||`.

La oss si at vi ønsker å validere et passord. Hvis passordet enten inneholder strengen "passord" *eller* lengden på strengen er mindre enn 8 tegn, logger vi en melding med en beskrivende feilmelding. Så hvis en av disse to uønskede betingelsene er sanne, vil koden i `if`kodeblokken bli utført.

```jsx
const userPassword = "password123#";

if(userPassword.includes("password") || userPassword.length < 8) {
    console.log("Password is not strong enough");
}
```

Operatoren logisk *ikke(*`!`) vil returnere usann hvis resultatet er sant. Det returnerer også sant hvis resultatet er usant. I eksemplet nedenfor sjekker vi først om et tall er partall, og hvis betingelsen er sann, logger vi "Number is even". Du kan snu dette og sjekke om tallet *ikke* er partall. Dette kan noen ganger være nyttig, for eksempel hvis du vil se om et brukerens passord samsvarer med passordreglene.

```jsx
const number = 5;

//if number is even, execute the code in the if(){...} code block

if(number % 2 === 0) {
    console.log("Number is even");
} else {
    console.log("Number is odd");
}

//reversing the condition
//If the number is NOT even, execute the code in the if(){...} code block
if(!number % 2 === 0) {
    console.log("Number is odd");
} else {
    console.log("Number is even");
}
```

En ganske fin ting du kan gjøre med den `!`logiske operatoren er å reversere en **`[boolean](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean)`**verdi.

I eksemplet nedenfor har vi en `toggled`variabel som inneholder verdien av `false`. La oss si at vi ønsker å endre dette hver gang vi starter funksjonen. Du kan gjøre dette ved å skrive `toggled = !toggled;`. Hvis vi bryter dette ned, betyr det at `toggled`variabelen blir tildelt det motsatte av sin startverdi `boolean`hver gang funksjonen påkalles, så `false`blir `true`. Vi starter funksjonen igjen og verdien endres igjen fra `true`tilbake til `false`.

```jsx
let toggled = false;

function changeToggled() {
    toggled = !toggled;
    console.log(toggled);
}

//invoking the function

changeToggled() //toggled = true
changeToggled() //toggled = false
changeToggled() //toggled = true
```

<aside>
ℹ️ Det er en ekstra type operatør kalt **Bitwise-operatorer**. Brukes til å utføre operasjoner på individuelle biter, det er ikke vanlig, men har verdi når det er aktuelt.

</aside>

### Operatørprioritet

Operatorprioritet bestemmer rekkefølgen som operatorene i et uttrykk evalueres i. Se følgende kodelinje:

```jsx
const result = 12 - 4 * 2;
```

Hva blir `result`?

Svaret er `4`.

Aritmetiske operatorer er enkle å forstå ettersom de følger matematiske regler. I dette tilfellet har multiplisering `*`høyere prioritet enn `+`, og utføres først.

Parenteser `()`overstyrer prioritet. Tvinge operasjonene i parentes til å bli evaluert først. Ta følgende kodelinje:

```

const result = (12 - 4) * 2;

```

Hva blir `result`?

Svaret er `16`.

Dette er fordi parentesene har høyest prioritet og utføres først, `(12 - 4) = 8`. Dette multipliseres deretter med `2`for å gi `16`.

Tabellen nedenfor kan brukes som referanse:

[Untitled](https://www.notion.so/5bbe063bdcee4d1c9b336d1d30b9cf82)

<aside>
ℹ️ **MERK** : `()` er ikke oppført da den alltid er den høyeste.

</aside>

<aside>
ℹ️ **MERK** : Alle Bitwise relaterte prioriteter ble utelatt.

</aside>

<aside>
💎 [**MDN-operatørprioritet**](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Operator_Precedence)

</aside>

## Typekonverteringsoperatorer (Coercion operators)

Bruk av operasjoner eller sammenligninger med to uventede typer (f.eks. a `number`og a `string`) vil bli **[tvunget](https://developer.mozilla.org/en-US/docs/Glossary/Type_coercion)**. Dette betyr at JavaScript konverterer en **[datatype](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures)** til en annen.

For eksempel:

- `*false*` kan konverteres til,`0,` `*true*` til `*1`
- `“1”`kan konverteres til `*true*`, eller heltallet `1`

Hvordan det fungerer avhenger av operatøren som brukes:

- `+`vil prøve å konverte om til et tall eller en streng
- `-` vil prøve å konvertere om til et tall

La oss se på noen kodeeksempler:

```jsx
// Strings
const addition = 1 + 2 // Output: 3
const concatenation = "Hello " + "World" //Output: "Hello World"
const coerced = "1" + 2 // Output: "12"
const alsoCoerced = "true" + true // Output: "truetrue"

// Numbers
const minus = "10" - 5 // Output: 5

// Booleans
const truthyCoerced = true == 1 // Output: true
const alsoTruthyCoerced = false == 0 // Output: true

// Avoiding Coercion with === 
const notTruthyCoerced = true === 1 // Output: false
const alsoNotTruthyCoerced = false === 0 // Output: false
```

## Funksjoner, funksjonelle konstruktører og hoisting

Her er noen nøkkelaspekter ved JavaScript-funksjoner:

- Funksjoner inneholder kode som kan gjenbrukes flere steder
- Vi bruker funksjoner for å lage konstruktører
- Funksjonserklæringer *hoistes* (heises)
- Funksjonsuttrykk behandles som variabler

### Funksjonserklæringer

Funksjonserklæringer bruker `function`nøkkelordet for å definere en funksjon med et navn.

```jsx
// Defining the function.
function myFunctionDeclaration() {
  console.log("I am a function declaration");
}

// Invoking the function
myFunctionDeclaration();
```

### Hoisting

Funksjonserklæringer kan påberopes før de deklareres på grunn av **[hoisting](https://developer.mozilla.org/en-US/docs/Glossary/Hoisting)** .

Hoisting er en hendelse som skjer i JavaScript. I løpet av kontekstopprettingsfasen blir variabler tildelt minneplass, men funksjoner plasseres i minnet i sin helhet. Verdien blir tildelt når koden kjører linje for linje under utførelsesfasen. Det er spesielt viktig når vi snakker om funksjoner og variabler deklarert med var.

Ta det forrige eksempelet:

```jsx
// Invoking the function before it is declared.
myFunctionDeclaration();

// Defining the function.
function myFunctionDeclaration() {
  console.log("I am a function declaration");
}
```

Eksempelet ovenfor vil ikke resultere i en feil på grunn av heising. Begrepet heising beskriver prosessen i [Opprettelsesfasen](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/00_Introduction.html#creation-phase) av JavaScript hvor hele funksjonen legges til minnet, men IKKE utføres.

Hvis vi prøver å referere til en variabel før dens erklæring vil det resultere i en referansefeil.

```jsx
// Defining the function.
const myFunctionExpression = function() {
  console.log("I am a function expression");
}

// Invoking the function.
myFunctionExpression();
```

> **Husk**: Alle variabler er i utgangspunktet satt til udefinert under opprettelsesfasen av JavaScript-kjøring.
> 

### Funksjonsuttrykk

Du kan tilordne en funksjon til en variabel som dens verdi ved å bruke et funksjonsuttrykk. Funksjonsuttrykk påkalles identisk med funksjonserklæringer.

```jsx
// Defining the function.
const myFunctionExpression = function() {
  console.log("I am a function expression");
}

// Invoking the function.
myFunctionExpression();
```

Funksjonserklæringer er den tradisjonelle måten å skrive en funksjon på i JavaScript. Det krever funksjonsnøkkelordet, et navn på funksjonen og en valgfri liste med argumenter.

### Funksjon Returverdier

Funksjoner returnerer kanskje ikke en verdi. Når en funksjon ikke returnerer noen verdi, anses den å være en ugyldig funksjon.

```jsx
// Defining a void function.
function printAwesome() {
  console.log("Awesome")
  // No return - Therefore, it is void.
}
```

Funksjonen ovenfor har ingen retursetning, og kalles derfor en *void-*funksjon.

```jsx
// Defining a function that returns a number
function sumTheAnswer() {
  // return the sum of 4 and 2.
  return 4 + 2;
}
```

I eksemplet ovenfor returnerer funksjonen summen av 4 og 2, derfor returnerer den et tall. Fordi JavaScript ikke har noe typesystem, kan du ikke angi returtypen naturlig. JavaScript-utviklere er ofte avhengige av verktøy som JSDocs for å "skrive" koden deres.

```jsx
/**
 * Returns the sum of 4 and 2
 * @returns {number}
 */
function sumTheAnswer() {
  return 4 + 2;
}
```

Eksempelet ovenfor bruker JSDoc for å fortelle redaktøren din hvilken type returverdi funksjonen har. Det er viktig å merke seg at dette ikke legger til skriving til JavaScript, men gir hjelp til koderedigereren din for å bedre forstå koden din og gi mer passende "hint" mens du koder.

<aside>
ℹ️ **Mer om JSDoc**

For å lære mer om JSDoc kan du se den offisielle dokumentasjonen.

JSDoc-dokumentasjon: **[https://jsdoc.app/](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://jsdoc.app/)**

</aside>

## Funksjonelle konstruktører

Funksjonelle konstruktører brukes til å lage **[objekter](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)** , som du vil lese om litt lenger ned. Funksjonelle konstruktører fungerer som en blåkopi for å lage objekter, og du "bygger" en ny forekomst av den "strukturen" (objektet) ved å bruke `new`nøkkelordet.

Det brukes ofte til å representere noe logisk eller fysisk, som detaljene/spesifikasjonene til en person, ansatt, bil eller fly, som du ser i eksemplet nedenfor.

```jsx
// Declare a functional constructor
function Person() {
  this.name = "Luke";
  this.surname = "Skywalker";
  this.email = "luke.skywalker@starwars.com";
}

// Instantiate a Person using "new" keyword
const luke = new Person();

// Creates an object from properties attached to this
// Output: { name: "Luke", surname: "Skywalker", email: "luke.skywalker@starwars.com" }
```

I eksemplet ovenfor opprettes den funksjonelle konstruktøren ved å bruke *PascalCase* og ikke ved å bruke *camelCase* .

> Funksjonelle konstruktører har kanskje ikke et returnøkkelord.
> 

### Funksjonelle konstruktørargumenter

Som nevnt ovenfor bruker vi funksjonelle konstruktører som en "blåkopi" for å lage objekter, så for å gjøre konstruktørene gjenbrukbare og ha flere brukstilfeller, kan vi legge til våre egendefinerte argumenter for de interne egenskapsverdiene, med andre ord ekstra informasjon kan sendes til en funksjon for å gjøre data dynamiske og unike per instans. Funksjonsargumentene sendes gjennom parentesen, som du ser nedenfor.

```jsx
// Declare a functional constructor with arguments
function Person(name, surname, email) {
  this.name = name;
  this.surname = surname;
  this.email = email;
}

// Instantiate unique Persons using "new" keyword
const luke = new Person("Luke", "Skywalker", "luke.skywalker@starwars.com");
const han = new Person("Han", "Solo", "han.solo@starwars.com");
```

## Objekter

I stedet for å bruke en funksjonell konstruktør for å lage objektene dine, kan du lage dem direkte. Objekter er en samling nøkkelverdipar formatert slik: *nøkkel* : *verdi* . Så du bruker `:`operatørene til å tilordne en verdi til nøkkelen. Verdiene du tilordner nøkkelen kan være en hvilken som helst gyldig JavaScript-type, så ikke bare strengdatatyper, men verdiene kan være en matrise, en funksjon eller til og med andre objekter, som gjør objektene nestede.

Nøkler med primitiver som verdier kalles *Properties* , og når nøklene har en funksjon som verdier kalles det en **[metode](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Basics)** .

```jsx
// Declare a functional constructor with arguments
function Person(name, surname, email) {
  this.name = name;
  this.surname = surname;
  this.email = email;
}

// Instantiate unique Persons using "new" keyword
const luke = new Person("Luke", "Skywalker", "luke.skywalker@starwars.com");
const han = new Person("Han", "Solo", "han.solo@starwars.com");
```

I eksemplet ovenfor opprettes objektet "person" ved hjelp av krøllparenteser. Dette objektet trenger ikke å bli instansiert som den funksjonelle konstruktøren, men det må dupliseres for hvert nytt objekt som opprettes.

### `this`i Objektmetoder

I en objektmetode refererer `this`nøkkelordene til selve objektet. Ta en titt på objektet nedenfor for å se hvordan vi kan få tilgang til egenskapene til et objekt i dens metoder

```jsx

// Creating an Object
const person = {
  // Properties
  name: "Luke",
  // Methods
  printInfo() {console.log(
      this.name // Will access the name property's value "Luke".
    );
  }
};
```

---

Copyright 2022, Noroff Accelerate AS