# Web Development Context

## JavaScript Context & History

### Evolution of JavaScript

The JavaScript languages was the brain-child of Brendan Eich during his time at Netscape Communications Corporation. The language was required to match the syntax of the Java language. Eich was given ten days to create the first prototype for the Netscape browser.

The language has undergone several renamings, initially Mocha, LiveScript and then finally JavaScript when it was officially released with the Netscape browser.

### What is ECMAScript or ES

ECMA International (formally known as European Computer Manufacturers Association) is a non-profit body that develops standards and specifications for computer programming languages, hardware and communications. The ECMA-262 Specification, is a a set of outlines for ECMAScript.

ECMAScript is based on several originating languages, largely JavaScript (Netscape) and Jscript (Microsoft). It outlines the specifications for a general usage scripting language. JavaScript is based on ECMAScript but deviates enough to be called its own language.

> 📄 **ECMA Specification Documentation**
>
> For deeper insight to the language, you may review the official specification documentation.
>
> [ECMA Script Specification](https://262.ecma-international.org/12.0/)

### Front-End (Browser) vs Back-End (Server)

JavaScript can be executed in many environments. The two core platforms for web development is JavaScript that runs in the browser or on the server. When refering to backend and frontend we often use the terms *server-side* and *client-side* of a website. 

#### Back-End - On the Server

In the server-side of a webpage/web application is where your website "lives" and is a layer of the website that the user doesn't see (in contrast to the client-side). The server-side is responsible for sending information to the client-side as well as receiving and responding to other requests from user interactions. The information/data the server receives is stored in a database, like MySQL which is used for, among others, Wordpress sites. 

#### Front-End - In the Browser

In the browser, also called the client-side, is where the user views and interacts with your website. Here you have forms that the user can fill out to send information to the server, search bars to query the database for specific information like product information, and the user views the content which is sent from the server and rendered to HTML, for example posts on a news channel. 

### Tools for Back-End development

#### NodeJS with Frameworks like Express

You have many different tools, programming languages and frameworks used for creating the backend of a website. Among the most popular programming language for backend programming is JavaScript in the form of Node.js. One big advantage of using Node.js is the syntax, which is regular JavaScript. This means that you don't need to spend time learning the syntax, rules and quirks of a new language. According to MDN, Node.js is "(...) an open-source, cross-platform runtime environment that allows developers to create all kinds of server-side tools and applications in JavaScript." [MDN docs](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/Introduction). 

In this course you will learn the Node.js [framework](https://en.wikipedia.org/wiki/Software_framework) Express, which makes developing and maintaining a web server simpler and more efficient. 


#### Networking

According to the MDN docs, "HTTP messages are how data is exchanged between a server and a client. There are two types of messages: requests sent by the client to trigger an action on the server, and responses, the answer from the server." [MDN docs](https://developer.mozilla.org/en-US/docs/Web/HTTP/Messages)

You will need to learn how to use HTTP requests and responses in order to properly communicate and exhange data between the backend and the browser.

#### Databases and Query Languages

The data received and sent to the client-side is stored and retrieved from a database. There many different programming languages used to comminucate with a database, some of the most popular being [SQL](https://developer.mozilla.org/en-US/docs/Glossary/SQL) (Structured Query Language), MySQL and [ProstgreSQL](https://aws.amazon.com/rds/postgresql/what-is-postgresql/). 

Like any programming language, query languages have their own syntax which differ from what you are used to in C based langauges like JavaScript, as you can see in the little code snippet below. This is just a simple query to the database. 

```sql
SELECT column1, column2
FROM   table_name
WHERE  CONDITION;
```

When using SQL for querying a database you need to be very careful to [sanitize/validate](https://developer.mozilla.org/en-US/docs/Learn/Forms/Form_validation) the user input, as unsanitized input is susceptible to [injection attacks](https://developer.mozilla.org/en-US/docs/Glossary/SQL_Injection), which is a very common hacking method to gain access to restricted information and even user credentials. 

#### ORM

Object Relational Mapping like [Sequelize](https://sequelize.org/docs/v6/getting-started/), which is a Node.js module, enables developers to work with [relation databases](https://www.oracle.com/database/what-is-a-relational-database/) easier. In practice it means that you can use JavaScript syntax to query the database and you mostly avoid using SQL queries. 

#### Server configs

A tool in backend development is `.env` files for [environment variables](https://en.wikipedia.org/wiki/Environment_variable). This will be covered in more details later in the course. 

### Tools for Front-End development

#### UI

The tools for creating the [user interface](https://developer.mozilla.org/en-US/docs/Glossary/UI) (UI) are HTML and CSS, which you have learned about in the previous lessons. We briefly covered popular CSS pre-processors like SASS/SCSS and LESS, which needs to be compiled to regular CSS for the browser to understand it. 

#### Scripting

In order to create interactivity, we use JavaScript (ES5, ES6+) for this. You will find that there's an abundance of frameworks and libraries, but one thing to keep in mind is that these frameworks all need to be compiled into plain old Vanilla JavaScript for the browser. In other words, everything you can do using framework, you can accomplish using Vanilla JavaScript. Frameworks are there to simplify the process and make your code clearer, well structured and scalable. 

[Typescript](https://en.wikipedia.org/wiki/TypeScript) is a common superset of JavaScript that uses stricter syntax in order to avoid errors and make the code clearer.

#### Configuration and Build Tools

NPM is a package manager that comes with Node.js and is linked to a node repository. Installing modules is very simple with the npm. To install a modules, simply `npm install` and the name of the package you want to install. 

Read more about NPM in the [NPM Docs](https://docs.npmjs.com/cli/v6/commands/npm).

Other useful tools include:

* [Babel](https://babeljs.io/docs/en/)
* [Webpack](https://webpack.js.org/)

#### JavaScript Frameworks

<center>
        <figure>
            <img src="./img/js-frameworks-everywhere.jpeg" alt="js-frameworks-meme" width="760">
        </figure>
        <figcaption>Source: makeameme.org
        </figcaption>
</center>

There are so many JavaScript frameworks out there that it's a common theme for frontend developer memes. The most common ones are Vue, React and Angular. React was developed and is being maintained by Facebook, Angular was developed by Google and Vue is an open-source framework maintained by local developers. 

As mentioned, frameworks are there to simplify the development process. One thing you got to keep in mind is that a person or people developed these frameworks based on their own ideas, methodologies and abstractions. You, yourself, could create a framework based on how you like to work, but this is time consuming and requires a lot of skill. Choosing a framework is about finding if these ideas, methodologies and abstractions are aligned with your own and your end goal. 

If you look at job posting online for frontend development you will most likely see a list of technologies, including frameworks, that they use in their web stack. So, knowing some of the more popular frameworks will give you an edge when applying for jobs. 


#### Design Tools

You already have some experience using Figma to develop wireframes and user interfaces. 

#### Communication with Back Ends

Like mentioned earlier, the client-side in most cases needs to comminucate and exchange data with the server-side, and this is done using HTTP Requests and [web sockets](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API).

## Text Editors

### What is a text editor?

When writing code you will use some type of text editor. [Source code editors](https://en.wikipedia.org/wiki/Source-code_editor) are designed especially for coding and will assist you in writing and editing code. These text editors are limited in features, but can be extended with plugins. Take the Visual Studio Code plugin. If you want an easy way to create a local server for development, you can install the [live server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) extension. If you want to easilly compile your SASS/SCSS files into CSS, you can install the [live sass](https://marketplace.visualstudio.com/items?itemName=ritwickdey.live-sass) compiler extension. 

### Popular text editors

Visual Studio Code
Sublime Text
Atom
Notepad++

### Visual Studio Code

Some of its features:
* Windows, MacOS and Linux
* Good for small to medium teams
* Free, Open Source
* Built with Node.js (JavaScript)
* Plenty of plugins
* Available online
* Maintained by Microsoft

## Integrated Development Environments (IDE)

Some of the features of an IDE:

* Swiss army knife (dlots of functionality)
* Text Editor + Lots more
* Source control
* Databases
* Code generation
* Advanced refactoring

### Popular IDE's

JetBrains Webstorm is the IDE we would recommend.

You can read more JetBrains WebStorm vs VS Code [here](https://swimm.io/blog/vscode-vs-webstorm-a-detailed-comparison/).

## Browsers and WWW

### What is a Browser?

A browser is a software application built to access information over the world wide web. It is built for data retrieval from some remote computer/server on the internet.

You can read more in-depth about browsers in the [MDN docs](https://www.mozilla.org/en-US/firefox/browsers/what-is-a-browser/)

### What is the World Wide Web

A collection of documents, graphics and other media that is shared between a network of connected computers.

## How do browsers work?

### 4 Steps of Display a web page

1. The URL
2. The Name Resolution
3. The Fetch
4. The Display

#### Step 1 The URL

The URL or Uniform Resource Locator of a website is a human friendly name for a website. It would be a tremendous hassle to remember a bunch of numbers for our favourite websites. The URL is entered in the address bar of your browser. The browser will then move onto step two and use various resources to resolve the name of the website.

<center>
    <figure>
        <img src="./img/browser-step1-url.png" alt="Entering a URL in a browser" width="760">
    </figure>
    <figcaption>Step 1 - The URL</figcaption>
</center>

#### Step 2 The Name Resolution

* Find out where the website lives.
* Resolve the address against the domain.
* A website site “Address” is called the I.P. Address (Internet Protocol Address).

<center>
    <figure>
        <img src="./img/browser-step2-dns.png" alt="Simplified DNS Diagram" width="760">
    </figure>
    <figcaption>Step 2 - The Name Resolution</figcaption>
</center>

#### Step 3 The Fetch

* Request and response
* Browser requests information
* Server responds with content

<center>
    <figure>
        <img src="./img/browser-step3-requests.png" alt="HTTP Requests to get HTML Documents" width="760">
    </figure>
    <figcaption>Step 2 - The Name Resolution</figcaption>
</center>

#### Step 4 The Display

* Browser receives content
* Temporarily stores it (Caching)
* Browser interprets content
* Displays content in browser
* CAT MEMES!

## Browser choices

## Tools used in this course

### NodeJS

Node is a JavaScript runtime that allows JavaScript to be executed outside of a web browser. Node is used to run JavaScript on servers. Allowing connections to databases and creating real time applications using web sockets.

> ℹ️ **Download NodeJS**
>
> You should **always** download the LTS\* version of Node for web development.
>
> [NodeJS](https://nodejs.org/en/)
>
> \*LTS - Long Term Support

### Node Package Manager (npm)

NPM is a package manager (Node Package Manager)

> We will cover NodeJS and npm in more detail in the Node module.

## Recommended text editor

You are free to use any text editor of your choice, however we recommend using Visual Studio Code.

## Useful Extensions (Visual Studio Code)

Below you can find recommended Visual Studio Code extensions to help improve your coding experience.

### LiveServer

Run HTML websites directly in the browser and have it automatically update when making changes to your code

[Live Server - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)

### Visual Studio IntelliCode

Code assistance to assist your code writing.

[Visual Studio IntelliCode - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=VisualStudioExptTeam.vscodeintellicode)

### ESLint

Find problems, fix problems = Consistent code
\*Requires locally or globally installed ESLint package

[ESLint - Visual Studio Code Marketplace](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

### Auto Rename Tag

While renaming HTML and JSX tags, the extension will rename both the opening and closing tag.

[Auto Rename Tag - Visual Studio Code Marketplace](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag)

### IntelliSense for CSS classes

Let Visual Studio Code give CSS class hints in HTML.

[IntelliSense for CSS class names in HTML - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=Zignd.html-css-class-completion)

### Prettier

Format your code to adhere to ESLint rules.

[Prettier - Visual Studio Code Marketplace](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

### Path Intellisense

Better imports for paths in JavaScript and NPM.

[Path Intellisense Visual Studio Code Marketplace](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense)

---

Copyright 2022, Noroff Accelerate AS