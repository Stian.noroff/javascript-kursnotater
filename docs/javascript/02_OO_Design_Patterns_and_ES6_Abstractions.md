# JavaScript OO Designmønstre og ES6 Abstraksjoner

# JavaScript OO, Designmønstre og ES6 Abstraksjoner

## Objektorientering (OO)

Oversikt over objektorienteringskonsepter:

### Objektarv

Er delingsegenskaper mellom objekter

### Funksjonsprototyper

Er deling av funksjonalitet mellom instanser

### Prototypearv

Er deling av atferd mellom objekter

### JavaScript-klasser

En mer tradisjonell måte å lage objekter på som utviklere fra andre språk vil kjenne igjen.

## Objektarv

Du tenker kanskje at konseptet med arv er nytt for deg, men faktisk har du jobbet med arv når du skriver CSS. Ta eksempelet nedenfor der du angir en stil for alle `p`-elementene. Klassene `.primary` og `.secondary` arver stilene satt ved hjelp av `p` og legger til sine egne stiler til de som er arvet

```jsx
<p class="primary">First paragraph</p>
<p class="secondary">Second paragraph</p>
```

I CSS-stilarket:

```css
p {
    color: darkgray;
    font-family: Sans-serif;
}

.primary {
    font-size: 2rem;
}

.secondary {
    font-size: 1.5rem;
}
```

Objektarv i JavaScript følger mye av det samme prinsippet, og i eksemplet nedenfor, arver `Employee()` et par egenskaper fra basiskonstruktøren `Person()` og legger til en egen egenskap.

Du kan se at inne i `Employee()`-funksjonen bruker vi `[.call()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/call)`metoden på `Person`objektet for å arve egenskapene.

Metoden `.call()`godtar to argumenter, som er:

1. gjeldende utførelseskontekst (Employee)
2. argumenter til Person-konstruktøren (navn og etternavn).

Medarbeideren kan nå få tilgang til navn og etternavn fra `Person()`. Når en ny forekomst er opprettet med `Employee()`som en konstruktør, vil det opprettede objektet inneholde egenskapene fra både `Person()`og `Employee()`.

### Tilgangsegenskaper

```jsx
// Base "Class"
function Person(name, surname) {
    // Person properties
    this.name = name
    this.surname = surname 
}

function Employee(name, surname, jobTitle) {
    // Inherit Persons properties
    Person.call(this, name, surname)

    // Employee properties
    this.jobTitle = jobTitle
}

const oliver = new Employee(
    'Oliver',
    'Dog',
    'Best friend'
)
console.log(oliver.name) // Output: Oliver
consol.log (oliver.surname) // Output: Dog
console.log(oliver.jobTitle) // Output: Best friend
```

### Prøv det selv

1. Gjenskap eksemplet fra forrige slide i CodePen
2. Legg til en ny egenskap for basiskonstruktøren kalt "armour" og sett verdien til 100
3. Legg merke til hvordan både spilleren og fiendens objekter endres.

## Funksjonsprototype

### .prototype

En prototype er en spesiell type objekt hver funksjon og objekt har som standard. Det lar deg gjøre visse ting, som å legge til nye egenskaper til en eksisterende funksjonell konstruktør senere i programmet.

Vi kan enkelt legge til en annen egenskap til `Employee`prototypen. Syntaksen for å legge til en annen egenskap til en eksisterende prototype:

```jsx
//adding another property to the prototype
Employee.prototype.jobStatus = "employed";

//logging the jobStatus property inside the dewald object
console.log( dewald.jobStatus ) // Output: "employed"
console.log( oliver.jobStatus ) // Output: "employed"
```

La oss prøve å logge `Employee.prototype` til konsollen og se hva vi får.

```jsx
console.log(Employee.prototype);
```

Output:

```jsx
constructor: function Employee(name, surname, jobTitle)
jobStatus: "employed"
```

Vi får en liste over egenskaper inne i `Employee`objektet. Den første egenskapen, `constructor`, ble automatisk lagt til som en egenskap av `Employee()`konstruktørfunksjonen.

`jobStatus`egenskapen var ikke der i utgangspunktet, og denne egenskapen kalles en prototype som er inne i `Employee`objektet. Selv om `jobStatus` ikke er inne i `Employee()`konstruktøren, som vi tydelig ser av den loggede outputen, kan vi få tilgang til egenskaper for prototypen på samme måte som vi ville få tilgang til egenskaper, for eksempel `dewald.name`.

Å bruke prototypen til å lage metoder vil sikre at det ikke er noen kodeduplisering i minnet. Metoder som er knyttet til prototypen, oppretter en enkelt referanse i minnet, og alle forekomster som har tilgang til den metoden, vil referere til den samme metodekoden.

I stedet for å bare legge til en egenskap og tilordne den en verdi, kan vi gjøre noe mer nyttig, som å legge til en egenskap med verdien av en funksjon. Nedenfor ser du akkurat det. Vi legger `.getFullName`egenskapen til `Employee`prototypen med verdien av en funksjon. Siden `this`nøkkelordet refererer til `Employee`prototypen, kan vi få tilgang til både `name`og `surname`gjennom `this`nøkkelordet. Det er ganske kult.

```jsx
function Employee(name, surname, jobTitle) {
    // Inherit Person properties
    Person.call(this, name, surname)
    //Employee properties
    this.jobTitle = jobTitle

}

// Create a new function on Employee prototype
Employee.prototype.getFullName = function() {
    return this.name + '  ' + this.surname
}

// Both instances share .getFullName()
const oliver = new Employee('Oliver', 'Dog', 'Best friend')
console.log( oliver.getFullName() )
// Output: 'Oliver Dog'

const dewald = new Employee('Dewald', 'Els', 'Teacher')
console.log( dewald.getFullName() )
// Output: 'Dewald Els'
```

## Prototypearv

### Demonstrasjon av prototypearv

```jsx
function Controllable() {
    this.direction = 'forward';
    this.isMoving = false;
}

Controllable.prototype.move = function() {
    this.isMoving = true;
}

function Player() {
    this.health = 100;
    this.inventory = [];
}

// Let the player inherit the prototype from the Controllable class.
Player.prototype = Object.create(Controllable.prototype);

const player = new Player();
player.move();
console.log(player);
```

Demonstrasjon av prototypearv

```jsx
class Person {
    // invoked with new Person()
    constructor(name, email) {
        this.name = name
        this.email = email
        this.sayHello = () => {
        }
    }

    // Attached to the Prototype
    printInfo() {
        console.log(this.name, this.email);
    }
}
```

Hvordan prototypekjeden fungerer: BILDE MANGLER

### Prøv det selv

1. Naviger til [https://codepen.io/sean-noroff/pen/WNoqBde](https://codepen.io/sean-noroff/pen/WNoqBde)
2. Fest en funksjon til prototypen av Guitar-funksjonen kalt "play" og la den endre Sound-egenskapen for Guitar-objektet til "Sweet sounding chords”
3. Lag en annen funksjon kalt ElectricGuitar. Den har en enkelt egenskap kalt "pickups" som kan tildeles en verdi på "EMG 81/85"
4. Bruk prototypekjeden til å la ElectricGuitar arve prototypen til Guitar, instansiere et nytt ElectricGuitar-objekt, kalle play()-metoden og logge verdien av objektet til konsollen.

## JavaScript-klasser

I programmering kan vi lage klasser for å representere et objekt. Den bør ha et navn og den kan ha egenskaper og metoder.

JavaScript-klassen bruker prototype-arvmodellen internt. Når egenskaper og metoder brukes i selve klassen, krever det bruk av `this`nøkkelordet.

### Erklære en JavaScript-klasse

```jsx
class Person {

    // Invoked with new Person()
    constructor(name, email) {
        // Attach properties to the class
        this.name = name
        this.email = email
    }

    // Methods are attached to the Prototype
    sayHello() {
        console.log("Hello " + this.name);
    }

    printInfo() {
        console.log(this.name, this.email);
    }
}

const luke = new Person("Luke Skywalker", "luke@therebellion.galaxy");
luke.printInfo();
// Output: "Luke Skywalkler", "luke@therebellion.galaxy"
```

### Arv med klasser

Syntaktisk arv med klasser er mye enklere enn prototypemetoden. Nøkkelordet `extends` brukes til å dele både egenskaper og metoder fra en basisklasse.

```jsx
class Person {
    ...
}

// Create a class the inherits methods and properties from the Person
class Employee extends Person {
    constructor(name, email, jobTitle) {
        // Required super() Inherits Prototype
        super(name, email)
        
        // Employee specific properties
        this.jobTitle = jobTitle
    }
    // Override Parent Method
    printInfo() {
        console.log(this.name, this.email, this.jobTitle);
    }
}
const luke = new Employee("Luke Skywalker", "luke@therebellion.galaxy", "Rebel Pilot")
luke.printInfo();
// Output: "Luke Skywalker", :luke@therebellion.galaxy", "Rebel Pilot"
```

### Private felt

Alt som er knyttet til `this`nøkkelordet i en klasse, vil være offentlig tilgjengelig på alle forekomster som er opprettet fra den klassen. Dette vil tillate utviklere å endre enhver egenskap, noe som kan føre til uventet oppførsel.

JavaScript støtter private egenskaper som bruker `#`-symbolet i klasser. Dette vil sikre at egenskapen merket som privat ikke kan endres utenfor selve klassen.

For å opprette et privat felt i JavaScript, må feltene deklareres og tildeles i klassen.

```jsx
class Employee extends Person {

    #baseLocation;

    constructor(name, email, jobTitle) {
        // Inherits Prototype
        super(name, email)
        this.jobTitle = jobTitle
        this.#baseLocation = "Corellia"
    }

    // Override Parent Method
    printInfo() {
        console.log(this.name, this.email, this.jobTitle);
    }
}

const luke = new Employee("Luke Skywalker", "luke@therebellion.galaxy", "Rebel Pilot")
// Try to Access Private property will throw an Error
console.log(luke.#baseLocation);
// Uncaught SyntaxError: Private field '#baseLocation' must be declared in an enclosing class
```

### Private metoder

På samme måte som JavaScript-klasser støtter private felt, kan det også lage private metoder. Disse metodene er kun tilgjengelige innenfor klassen og kan ikke brukes på forekomsten av en klasse.

Private metoder opprettes også ved hjelp av `#`symbolet.

```jsx
class Employee extends Person {
    // Private field
    #baseLocation;

    constructor(name, email, jobTitle) {
        // ...
    }

    // Private method
    #updateBase() {
      this.#baseLocation = "Somewhere Else";
    }

    // Override Parent Method
    printInfo() {
        // Accessible in class only
        this.#updateBase();
        console.log(this.#baseLocation); 
        // Output: Somewhere Else
    }
}
```

## Prøv det selv - JavaScript-klasser

1. I CodePen oppretter du en ES6-klasse som representerer en gitar
2. En gitar bør ha et merke, modell og produksjonsår knyttet til seg. Klassen bør også ha en metode som returnerer alle detaljene til gitaren
3. Lag en subklasse kalt ElectrskGitar, som arver fra gitar
4. Instantier et ElektriskGitar-objekt og print detaljene til konsollvinduet

## ES6-funksjoner

### Oversikt over noen ES6-funksjoner som dekkes her

Du kan skrive elegant og konsis JavaScript-kode ved å dra nytte av noen ES6-funksjoner, f.eks

- Gjør mye mer med mye mindre kode (Arrow Functions).
- En bedre måte å sette sammen strenger (Template Literals)
- Trekk ut verdier på elegant måte (destrukturering).
- Stenografisk måte å skrive navn/verdipar (Object Literals)
- Håndter funksjonsargumenter konsist ved å bruke hvileparametere og spredningsoperatorer (Funksjonsargumenter).
- Øk modulariteten til koden din (moduler)

### Arrow-funksjoner

Syntaks for en **[arrow-funksjon](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions)**: `() => { ... }`

Arrow-funksjoner kan sees på som en kortversjon for vanlige funksjoner, der pilen `=>`er en slags kortversjon for `function`nøkkelordet. Som du ser i syntaksen, er parentesen for funksjonsargumentene plassert foran `=>`, som kan se litt rart ut til å begynne med. En arrow-funksjon inneholder sin egen erklæring eller kodeblokk og kan være med eller uten krøllparenteser.

Med krøllparenteser må du bruke `return`nøkkelordet hvis du vil at funksjonen skal returnere en verdi. Hvis du ikke bruker `{}`for kodeblokken, vil verdien eller uttrykket automatisk returneres. Arrow-funksjoner opprettholder utførelseskonteksten der den ble skrevet.

### Variasjoner av funksjonssyntaks i JavaScript

Hvordan lage en funksjon før ES6:

```jsx
// Create a function to calculate the square
function square(n) {
    return n* n;
}
```

Opprette en funksjon ved hjelp av arrow-funksjonens syntaks:

```jsx
// Changing to an arrow function.
const square = (number) => {
    return number * number;
}
```

Fjerne tilleggstegn som ikke er påkrevd for en enkelt returerklæring:

```jsx
// A single return statement does not require curly braces
const square = (number) => number * number;
```

> Du kan i tillegg også fjerne `()` rundt argumentlisten når det bare er et enkelt argument som sendes til funksjonen.
> 

Enkeltargument:

```jsx
// Get only completed items
const completedItems = array.filter(item => item.completed);
```

### Arrow-funksjoner i objekter

Det er viktig å huske at arrow-funksjoner ikke skal brukes for “Object of class”-metoder. arrow-funksjonen arver utførelseskonteksten der den ble deklarert, derfor vil du miste betydningen av denne når den brukes på et objekt eller en klasse. I noen tilfeller kan dette være akseptabelt, men vær oppmerksom på bivirkningene som oppstår ved bruk av arrow-funksjoner.

### Prøv det ut - arrow-funksjoner

1. I CodePen oppretter du en arrow-funksjon som beregner arealet til et rektangel
2. Funksjonen vil ta inn to argumenter, lengde og bredde

## Template Literals

[Template Literals](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals) er en spesiell type streng som er definert med venstreaksent i stedet for anførselstegn.

Å bruke plassholdere i en Template Literal er et godt alternativ til strengkonkatenering siden det ser smidigere og ryddigere ut. Ta en titt på disse eksemplene:

Bruke strengkonktatenering:

```jsx
const fname = 'Dewald';
const custNumber = '45'
const message = "Hello " + name + ", welcome to Pizza Planet!\nYou are customer number " + custNumber + "!";
```

Bruke template literal:

```jsx
const name = 'Dewald';
const custNumber = '45'
const message = `
Hello ${name}, welcome to Pizza Planet! 
You are customer number ${custNumber}!
`;
console.log(message);
```

Output:

```
Hello Dewald, welcome to Pizza Planet!
You are customer number 45!
```

Man ser veldig enkelt hvor bedre dette ser ut. Output er nøyaktig den samme, men med mindre kode og ingen `+`operatorer eller `\n`for linjeskift.

Det er mulig å lage HTML ved å bruke template literals, selv om `document.createElement()`metoden er foretrukket. For å gjøre dette kan du ganske enkelt skrive markup som du vanligvis ville gjort, og bruke plassholderne dine til å angi verdier til egenskaper og attributter som `innerText`, `src` og så videre. Når dette er gjort, må du bruke `append()`metoden for å legge til "malen" du har laget til et eksisterende HTML-element som du har forespurt fra DOM.

### Eksempel

```jsx
//saving the HTML element from the DOM in a variable
const elProfile = document.querySelector(".profile-container");

const name = 'Dewald';
const bio = `Hello ${name}, welcome to the future!`;
const imgSrc = "users/dewald.png";
const template = `
    <div class="profile">
        <h4>${name}'s Profile</h4>
        <p>${bio}</p>
        <img src="${imgSrc}" alt="Dewald Avatar" />
    </div>`;

//appending the template to the HTML from the DOM
elProfile.append(template);
```

### Prøv det selv - Template Literals

1. Naviger til [https://codepen.io/sean-noroff/pen/GRNbaVa](https://codepen.io/sean-noroff/pen/GRNbaVa)
2. Ved å bruke template literals, logg følgende til konsollvinduet ved å bruke variablene som følger med:
3. "James Hetfield er medlem av Metallica. De ga ut sitt debutalbum i 1983."

## Destrukturering

### Hva er destrukturering?

Destrukturering tillater oss å hente flere verdier fra et objekt eller en array.

Ta dette objektet som inneholder en mengde egenskaper:

```jsx
const car = {
    brand: "Nissan",
    model: "Leaf",
    year: "2019",
    new: false,
    serviceDates: [
        {
            date: "2020.02.02",
            issue: "breaks"
        },
        {
            date: "2021.02.03",
            issue: "battery" 
        }
    ]
}
```

Nå ønsker vi å lagre verdien til noen av objektene ovenfor i nye variabler. Som du ser får du fire linjer med kode, og du må bruke `const`nøkkelordet for å deklarere hver variabel. Vi kan gjøre denne kodebiten mye kortere og samtidig gjøre den ryddigere og klarere ved å bruke destrukturering.

```jsx
const brand = car.brand;
const model = car.model;
const year = car.year;
const serviceDate = car.serviceDate
```

Som du ser i dette eksemplet nedenfor, trenger vi ikke bruke `const`søkeordet mer enn én gang, og alle de variable erklæringene havner på samme linje. Denne kodebiten gjør akkurat det samme som den ovenfor. Det er bare syntaksen som har endret seg. Vi erklærer og instansierer automatisk verdier på *én linje* , og dermed blir det veldig nyttig når vi trenger verdiene til mer enn én egenskap fra et objekt.

```jsx
const { brand, model, year, serviceDate } = car;
```

### Når skal du bruke {} og []

- Ved bruk på objekter, `{ }`
- Ved bruk på Arrays, `[ ]`

Nå kan vi se på mer dyptgående eksempler for bruk av destrukturering på objekter og matriser.

### Objektdestrukturering

```jsx
const project = {
    name: 'Project Pineapple',
    dateStart: '2023-02-26',
    dateDue: '2023-04-25',
participants: {
    count: 4,
    team: ['ewoks', 'anikin', 'han', 'luke']
},
tasks: {
    count: 0,
    items: []
    }
}

// Destructure items from Object.
const { name, dateStart } = project;
const { team } = project.participants
const { items } = project.tasks;
```

Foreløpig har vi tilordnet verdiene for objektegenskapene til variabler med samme navn som egenskapsnøklene, men du kan gi variablene et hvilket som helst navn. I eksemplet nedenfor tildeler vi verdien av nøkkelnavnet `a`til en variabel kalt `total`og på samme måte `b`til variabelnavnet `max`.

```jsx
// Assign to new variable names
const obj = { a: 123, b: 4243, c: '1233' };
const { a: total, b: max } = obj;
```

### Array-destrukturering

Som nevnt kan du bruke destrukturering på objekter og arrays.

La oss først se på tilnærmingen du allerede er kjent med. Vi ønsker å tilordne verdien av hver indeks i matrisen til en variabel. Som med objekter, må vi deklarere hver variabel separat med `const`nøkkelordet og tildele den verdien til den bestemte indeksen i arrayet ved å bruke array-navnet med `[]`. Igjen, mye kode. Hvis dette er hva du ønsker å oppnå - tilordne verdien av *hver* indeks til en separat variabel, så vil destrukturering gjøre en bedre jobb med dette.

```jsx
// Get values from array. 
const colours = ['#23131', '#316fe2', '#31e29e', '#dfe231']

const red = colours[0];
const blue = colours[1];
const green = colours[2];
const yellow = colours[3];

console.log(red); // -> #e23131
console.log(blue); // -> #316fe2
console.log(green); // -> #31e29e
console.log(yellow); // -> #dfe231
```

Bruk av destrukturering:

```jsx
// Get values from array. 
const colours = ['#23131', '#316fe2', '#31e29e', '#dfe231']

const red = colours[0];
const blue = colours[1];
const green = colours[2];
const yellow = colours[3];

console.log(red); // -> #e23131
console.log(blue); // -> #316fe2
console.log(green); // -> #31e29e
console.log(yellow); // -> #dfe231
```

I tilfellet ovenfor ønsker vi å tilordne hver indeksert verdi til sin egen variabel, men det er ikke alltid det du ønsker. Noen ganger vil du bare ha verdien fra indeks 0 og indeks 3, for eksempel. I dette tilfellet er det den første og siste indeksen til arrayet. Dette er mulig ved hjelp av destrukturering, men det blir fort uhåndterlig.

```jsx
const places = ["first", "second", "third", "fourth"];
const [first, second, third, fourth] = places;
console.log(first, second, third, fourth)
//Output: "first" "second" "third" "fourth"
```

Alt vel, ikke sant? Ok, la oss fjerne `second`og `fourth` variabelnavnene i `[]` og se hva som skjer med outputen.

```jsx
const places = ["first", "second", "third", "fourth"];
const [first, second, third, fourth] = places;
console.log(first, second, third, fourth)
//Output: "first" "second" "third" "fourth"
```

Dette skjer fordi indeksene innenfor destruktureringsparentesene direkte samsvarer med indeksene i arryet vi destrukturerer. For å løse dette problemet kan vi legge til kommaer som tomme plassholdere. Dette løser problemet, men skaper et nytt. Tenk deg alle kommaene du trenger å holde styr på om du vil ha verdien av indeks 3 og indeks 100. 

```jsx
const places = ["first", "second", "third", "fourth"];
const [first, , , fourth] = places;
console.log(first, fourth)
//Output: "first" "fourth"
```

En annen måte å tilordne verdier til variabler kan gjøres som i eksemplet nedenfor.

```jsx
// Assign separate from declaration
let username, email;
[username, email] = ['dewaldels', 'dewaldifels@gmail.com'];

console.log(username); // -> dewaldels
console.log(email); // -> dewaldifels@gmail.com
```

Du kan bytte verdiene til variabler på denne måten, hvis det er nødvendig:

```jsx
// Swapping variables
[username, email] = [email, username];
console.log(username); // -> dewaldifels@gmail.com
console.log(email); // -> dewaldels
```

### Prøv det selv - Destrukturering

1. Bruk destrukturering for å få noen vitser fra dette joke API: [https://sv443.net/jokeapi/v2/](https://sv443.net/jokeapi/v2/)
2. Display det til konsollen

## Object Literals

### Hva er Object Literal?

Enkelt sagt, du trenger ikke å navngi noe som allerede har samme navn.

### Eksempel - Object Literals

```jsx
const data = {
    username: 'schwifty82',
    hair: 'grey',
    age: 'old',
    job: 'Crazy scientist'
}
// Old way
http.post('https://myapi.com',{
    data: data
});
// New way
http.post('https://myapi.com',{
    data
});
```

### Prøv selv - Object Literals

1. Naviger til [https://codepen.io/sean-noroff/pen/XWNLLRq](https://codepen.io/sean-noroff/pen/XWNLLRq)
2. Refaktorer CodePen til å bruke object literals

## Funksjonsargumenter

### Hvileparametere

**[Hvileparametere](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/rest_parameters)** lar oss definere et ubestemt antall argumenter som et array.

```jsx
function masterJavaScript(...args) {
    // Arguments will be an array
    args.forEach(arg => {
        // Do something with argument
    })
}
// Pass in any number of arguments
masterJavaScript("Learn ES6", "Learn about Functions", "Loops");
```

## Spredningsoperatør

Utvid et gjentakbart objekt eller array til **[funksjonsparametere](https://developer.mozilla.org/en-US/docs/Glossary/Parameter)**.

I dette eksemplet har vi en funksjon `sum`som tar tre argumenter, `x`, `y`, `z`, og returnerer summen av disse. Vi vil at parameterne skal være verdiene som er lagret i et array kalt `numbers`. I dette tilfellet vil vi normalt få verdien ved å referere til den bestemte indeksen i aarrayet, som `numbers[0]`, `numbers[1]`, og `numbers[2]`. Igjen, hvorfor ikke gjøre det på en enklere måte?

```jsx
function sum(x, y, z) {
    return x + y + z;
}
// Array we want to sum
const numbers = [1, 2, 3];

// Pass in the values from the array manually
const result = sum(numbers[0], numbers[1], numbers[2]);

console.log(result); // -> 6
```

**[Spredningsoperatoren](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax)** gjør det enkelt å sende inn argumentet som en funksjonsparameter.

```jsx
// Use spread operator to easily pass
const result = sum(...numbers);

console.log(result); // -> 6
```

### Spread Operation - Arrays

```jsx
const sheep = ["🐑", "🐑", "🐑"];
const fakeSheep = sheep;
const clonedSheep = [...sheep];

// Will ONLY change clonedSheep.
clonedSheep.push('"🐑"');
console.log("clonedSheep: ", clonedSheep)
console.log("Sheep using spread: ", sheep)

// Will change fakeSheep AND sheep
fakeSheep.push('🐺');

console.log("fakeSheep: ", fakeSheep)
console.log("Sheep not using spread: ", sheep)
```

Output:

```jsx
clonedSheep:  
Array(4) [ "🐑", "🐑", "🐑", "🐑" ]

sheep using spread:  
Array(3) [ "🐑", "🐑", "🐑" ]

fakeSheep:  
Array(4) [ "🐑", "🐑", "🐑", "🐺" ]

sheep not using spread:  
Array(4) [ "🐑", "🐑", "🐑", "🐺"  ]
```

### Objektkopiering ved hjelp av spredning

Du kan bruke spredningsoperatoren til å lage kopier av et objekt, men det vil bare lage [overfladiske kopier](https://developer.mozilla.org/en-US/docs/Glossary/Shallow_copy)**,** noe som betyr at objekter i objekter vil bli kopiert ved referanse. Hvis målet ditt er å lage en **[dyp kopi](https://developer.mozilla.org/en-US/docs/Glossary/Deep_copy)** hvor du kopierer verdien og deler referansen til den opprinnelige kilden, må du bruke en annen metode. Som en funksjonell konstruktør med `new`nøkkelordet.

```jsx
const person = {
    name: 'Dewald', 
    surname: 'Els',
    email: 'dewald.els@noroff.no'
}

// Copied by Value not Reference
const copyOfPerson = {
    ... person,
    jobTitle: 'Teacher'
}
```

### Prøv selv - Spredningsoperatør

1. Naviger til [https://codepen.io/sean-noroff/pen/poNXXpW](https://codepen.io/sean-noroff/pen/poNXXpW)
2. Skriv den manglende funksjonen `minusFromThirty` som tar inn et ubestemt antall argumenter og trekker fra verdiene deres fra 30

## Moduler

### Modulær programdesign

Å bruke moduler i JavaScript gjør koden mye mer gjenbrukbar og lar deg ha bare én `script`tag. Kort sagt, en modul er ganske enkelt en frittstående `.js`fil som *eksporterer* en funksjon eller variabel ved hjelp av **[export](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/export)**-nøkkelordet. På samme måte kan du *importere* eksporterte funksjoner og variabler ved å bruke **[import](https://translate.google.com/website?sl=auto&tl=no&hl=no&client=webapp&u=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import)**-nøkkelordet

I `script`taggen er det veldig viktig å inkludere `type`attributtet og sette verdien til `"module"`. Hvis du ikke gjør dette vil du få en feilmelding og modulene vil ikke fungere som forventet.

```jsx
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My App</title>
</head>
<body>
    <main></main>
    <!--Including the type attribute -->
    <script src="index.js" type="module"></script>
</body>
</html>
```

Det er to typer eksport:

- navngitte eksporter.
- standard eksporter.

### Den navngitte eksportsyntaksen

```jsx
// export named variable
export const myVariable = "Hello";
```

I filen vil du importere `myVariable`som en navngitt variabel:

```jsx
// import named variable
import { myVariable } from "./path/to/file.js";
```

En fil kan inneholde mange navngitte eksporter, i motsetning til *standard eksporter*, som vi også skal se på.

Du kan aliase en navngitt eksport ved å bruke `as`nøkkelordet, Med andre ord endre variabelnavnet.

```jsx
import { myVariable as anotherName } from "./path/to/file.js";
```

### Standard eksportsyntaks

Hvis du har en enkelt eksport fra en modul, kan du bruke `export default`nøkkelordet.

```jsx
// export default
const myVariable = "Hello";
export default myVariable;
```

Når du importerer en standardeksport, bruker du ikke `{ }`.

```jsx
import myVariable from "./path/to/file.js";
```

### Navigere i kataloger

Når du arbeider med moduler, må du vite hvordan du navigerer mellom mappene i filstrukturen du har opprettet.

Gå tilbake en mappe:

- `"../"` - gå tilbake (eller opp) en katalog (mappe) fra gjeldende katalog.
- `"../subfolder/path/to/file.js"`

Du kan bruke flere av disse. nedenfor går vi tilbake tre mapper:

- `"../../../"`
- `"../../../root/subfolder/path/to/file.js"`

Se i gjeldende mappe:

- `"./"` - se etter banen i gjeldende mappe
- `"./path/to/file.js"`

### Enkelt, praktisk eksempel

Husker du koden vi brukte for å lage en mal ved hjelp av malstrenger? La oss sette dette inn i en funksjon inne i en modul og eksportere det.

1. Vi oppretter først en mappe inne i hovedkildemappen vår kalt "components". Inne i mappen "components" lager vi en fil som heter for eksempel "template.js".
2. Vi oppretter bare en funksjon inne i denne filen som heter "template", limer inn koden vi vil at funksjonen skal returnere og eksporterer den deretter med `export default`. Enkelt, ikke sant?

I `profile.js`modulen:

```jsx
function createProfile(name, imgSrc) {
    const container = document.createElement("div");
    const bio = `Hello ${name}, welcome to the future!`;
    const template = `
        <div class="profile">
            <h4>${name}'s Profile</h4>
            <p>${bio}</p>
            <img src="${imgSrc}" alt="Dewald Avatar" />
        </div>`
    container.append(template);
    return container;
}

export default createProfile;
```

Nå ønsker vi å importere denne funksjonen til hovedfilen vår `.js`ved å bruke `import`nøkkelordet.

I `script.js`modulen:

```jsx
//importing the default export
import createProfile from "./components/profile";

// getting the main element from the DOM
const main = document.querySelector('main');

const newUser = createProfile("Dewald", "./img/profile.png")
main.append(newUser);
```

Du kan ha så mange importer du vil i `.js` hovedfilen. Den modulære måten å strukturere appene dine på er sentral i rammeverk som React.

### Flere eksempler som eksporterer standard og spesifikke funksjoner

```jsx
// tools.js 
function Tool(type) {
    this.type = type;
    this.useTool = function() {
        return 'The ${this.type} tool is being used...'
    }
}
export default Tool;

// utils.js
export function cube(size) {
    return size * size * size;
}

export function sum(...numbs) {
    let total = 0;
    nums.forEach(num => total += num);
}

export function reverse(string) {
    return string.split('').reverse().join('');
}
```

### Importere de eksporterte modulene

```jsx
// Default imports
import Tool from './tool.js';

const hammer = new tool('hammer');

cube(5);
sum(10, 20, 5);
reverse('JavaScript Rules!');
```

### Prøv det selv - ES6-moduler

1. Lag en modul som inneholder en funksjon som bestemmer om et tall er oddetall eller partall
2. Importer denne modulen i en egen kodefil og kall opp funksjonen
3. IKKE bruk CodePen til denne øvelsen, men skriv den i et tekstredigeringsprogram (VS Code anbefales)

## Designmønstre

Gjenbrukbare løsninger på vanlige designproblemer for programvare

Du trenger ikke å finne opp hjulet på nytt hver gang

- Designmønstre er gruppert i
    - Kreasjon – håndtere mekanismer for oppretting av objekter
    - Strukturell – identifisere måter å definere objektrelasjoner på
    - Atferdsmessige – håndtere kommunikasjon mellom objekter

Denne leksjonen dekker kun modul- og fabrikkmønstre.

### Moduldesignmønster

### Innkapsling

Etterligne private og offentlige egenskaper

Opprett settere og gettere for utvalgte egenskaper

Dette lar oss beskytte visse variabler/funksjoner

Og eksponere andre egenskaper som man må få tilgang til

### Hva er IIFE?

IIFE står for Immediately Invoked Function Expression. Funksjonen er pakket inn i en funksjon som kjøres umiddelbart, og gir den navnet Immediately Invoked.

```jsx
(function() {
    // my first IIFE
    let hiddenValues = 'Dewald!';

    return {
        getValue: function() {
            return hiddenValues;
        }
    }
})(); // Wrapped function immediately executes.
```

Den avgjørende delen av dette er returerklæringen `return {...}`. Alle variabler som er opprettet innenfor IIFE-scopet, vil kun være tilgjengelige innenfor dette scopet. Derfor kan du ikke endre noen av de deklarerte variablene.

<aside>
ℹ️ Module Design Pattern ble opprettet før de private feltene i JavaScript-klasser ble introdusert.

</aside>

Nedenfor finner du et eksempel på modulmønsteret som brukes til å lage en bjørn. Lydvariabelen er scoped til modulen og kan ikke endres utenfor dette scope.

Metoder for å selektivt endre verdier kan eksponeres i retursetningen.

```jsx
const bear = (function(){

    let sound = 'Grrrr';
    return {
        speak: function() {
            return `The bear says ${sound}`
        },
        setSound: function(newSound) {
            sound = newSound;
        }
    }
})(); IIFE

console.log(bear.sound); // -> undefined
console.log(bear.setSound('Meow'));
console. log(bear.speak()) // -> the bear says Meow
```

### Prøv det selv - Module Design Pattern

1. Gjenskap eksemplet fra forrige lysbilde i CodePen
2. Lag en ny atferd kalt "attack" som beskriver at bjørnen forsvarer seg selv
3. Sørg for at den er innkapslet
4. Logg resultatet av den nye atferden til konsollen

## Fabrikkdesignmønster

### Komposisjon

Komposisjon i programmering er avhengig av å lage små gjenbrukbare funksjoner. I stedet for å lage klasser som representerer objekter, er objekter i stedet konstruert ved hjelp av små funksjoner for egenskapene. På en måte "komponerer" vi objekter.

### Use Case

For å illustrere hvordan komposisjon til tider kan passe bedre enn arv, kan vi bruke følgende eksempel: Et programvaresystem for en pizzasjappe ble laget. Kjøkkenet serverer kun pizza og salat.

Følgende klasseoppsett ble opprettet for de første forretningskravene.

Klassen `Pizza`

```jsx
class Pizza {
    prepare() {}
    bake() {} // Pizza is baked
    serve() {}
}
```

Klassen `Salad`

```jsx
class Salad {
    prepare() {}
    toss() {} // A salad is tossed
    serve() {}
}
```

Siden det ble introdusert en duplisering i metodene `prepare`og `serve`, lages en ny baseklasse.

```jsx
class Food {
    prepare() {}
    serve()  {}
}
```

De to første klassene kan nå arve fra `Food`klassen.

```jsx
class Pizza extends Food {
    bake() {}
}

class Salad extends Food {
    toss() {}
}
```

### Uforutsigbare endringer

Som en spesial har eieren av kiosken bestemt seg for å lage to spesialpizzaer, Stuff Crust pizza og Folded Pizza. Det opprettes to nye klasser som arver fra Pizza.

```jsx
class CrustPizza extends Pizza {
    stuff() {}
}
class FolderPizza extends Pizza {
    fold() {}
}
```

Til slutt, de to spesialene gjorde det så bra, eieren bestemte seg for å kombinere de to til en Frankenstein-kreasjon, The Folded Stuffed Crust Pizza. Plutselig begynner tradisjonell arv å skape noen problemer. For å lette denne nye pizzaen, må koden dupliseres. Det er ingen måte å arve fra både Pizzaen og de to nye "spesielle" pizzaene.

For å kontrast til dette lar bruk av komposisjon utviklere enkelt lage små fokuserte "fabrikker" eller funksjoner som vi kan bruke til å lage et hvilket som helst antall objektkombinasjoner uten funksjonsduplisering.

### Base factories (Basefabrikkering - atferd)

Vi kan lage *factories* ved å bruke små fokuserte funksjoner som returnerer et JavaScript-objekt med en enkelt fokusert funksjon. Denne tilstanden kan valgfritt oppgis hvis den trenger å oppdatere noen verdier.

```jsx
// Factories
const preparation = (state) => ({
    prepare: () => {}
});

const server = (state) => ({
    serve: () => {}
});

const tossing = (state) => ({
    toss: () => {}
});

const baker = (state) => ({
    bake: () => {}
});

const folder = (state) => ({
    fold: () => {}
});

const stuffer = (state) => ({
    stuff: () => {}
});
```

Lage gjenstander med factories 

```jsx
// Defining objects using factories
const Pizza = (size, type) => {
    let state = {
        size,
        type,
        status: ""
    }
    // Return a new object, attaching the factory function methods to the returned value.
    return Object.assign(
        {}, // Create a new object
        preparation,
        baker,
        server,
    );
};

const Salad = (size, type) => {
    let state = {
        size,
        type
    };

    return Object.assign(
        {},
        preparation,
        tossing,
        server
    );
}

const FoldedStuffedPizza = (size, type) => {
    let state = {
        size, type
    }

    return Object.assign(
        {},
        preparation,
        baker,
        folder,
        stuffer,
        server
    );
}
```

`Object.assign()`er lignende syntaks som å bruke spredningsoperatoren for å kopiere, ikke referere, faktoriene inn i det nye objektet. Nedenfor kan du se et eksempel på bruk av spredningsoperatoren for å oppnå samme resultat.

```jsx
return {
    ...preparation,
    ...baker,
    ...server,
};
```

### Bruker factories

Når de komponerte objektene er ferdige, er det veldig enkelt å lage en ny forekomst av et objekt.

```jsx
const spicyPizza = Pizza("Large", "Spicy Diablo");
// The factory functions are now available on the object. 
spicyPizza.prepare();
spicyPizza.bake();
spicyPizza.serve();

const smallSalad = Salad("Small", "Greek");
const theSpecial = FoldedStuffedPizza("Medium", "Ham and Cheese");
```

### Prøv det selv - Lag dine egne factories

1. Gjenskap eksemplet fra de forrige lysbildene i CodePen.
2. Skriv noen `console.log`-setninger i funksjonene slik at du kan se output i konsollen.

## Konklusjon

- **Funksjonsprototyper** , **objektarv** og **klasser** vil gjøre koden din mer modulær
- **Designmønstre** (f.eks. modul og factory) vil gjøre koden din mer konsistent, gjenbrukbar og enkel å vedlikeholde
- Disse ES6-abstraksjonene lar deg gjøre mer med mindre kode:
    - Arrow-funksjoner gir en renere måte å uttrykke funksjoner på
    - Template literals gir en kortfattet måte å konkatenere strenger
    - Destrukturering lar deg trekke ut flere egenskaper i en enkelt setning
    - Object literals gir en kortfattet måte å skrive navn/verdipar på
    - Bruk *rest*-parametere og spredningsoperatorer for å håndtere funksjonsargumenter på en kortfattet måte
    - Innkapsle og gjenbruk atferd ved å importere og eksportere moduler

### Se fremover

- En leksjon til i denne modulen, før du går videre til frontend-rammeverkene, React og Angular
- I neste leksjon vil du lære hvordan
    - servere statiske HTML-, CSS- og JS-filer ved hjelp av Node med Express
    - få tilgang til forskjellige ressurser gjennom ruting med Express
    - utvide funksjonaliteten til Express-appen din med middleware

---

Copyright 2022, Noroff Accelerate AS