# Node og Express

# Introduksjon til Express.JS

> Leverer HTML-, CSS- og JavaScript-innhold.
> 

### Prøv det selv

1. I denne leksjonen vil du stort sett gjenskape og kjøre alle kodeeksemplene i et tekstredigeringsprogram istedenfor CodePen.
2. Åpne og bruk følgende:
    - Et tekstredigeringsprogram for å skrive koden.
    - CLI-verktøy for å generere prosjektet og kjøre koden lokalt.

## Komme i gang med Node.js

> Gjøre JS til et fullstack programmeringsspråk og ikke bare for front-end scripting.
> 

### Hva er Node.js?

[Node.js](https://nodejs.org/en/) er tradisjonelt JavaScript fokusert på skripting på klientsiden, noe som gjør det enklere å bytte mellom front-end og back-end oppgaver. Nettlesere gir en motor for å tolke og kjøre JS-kode. Node.js gir et runtime-miljø for å kjøre JavaScript på en server.

Vi kan bruke Node.js som et fullverdig backend-språk, men det er fortsatt ikke like modent som andre språk.

> **Merk**: På kurset lærer du hvordan du setter opp en webserver ved hjelp av [Node](https://nodejs.org/en/) og [Express](https://expressjs.com/).
> 

### Når du ikke skal bruke Node.js

Det er visse tilfeller der Node.js ikke vil være ditt førstevalg av programmeringsspråk, og disse er:

- Intensiv databehandling på serversiden
- Webapplikasjoner som bruker relasjonsdatabaser
- Video/bildebehandling

![Hvordan Node fungerer](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/express-node-server.jpg)

Hvordan Node fungerer

## Komme i gang med Express.js

> Gjør det enklere å kode i Node.js
> 

### Hva er Express.js?

Ren Node.js krever mye koding fra bunnen av, og det er her Express.js kommer inn. Express.js er et Node.js-rammeverk for å gjøre utvikling av webapplikasjoner raskere.

Express kan brukes til å:

- Bygge Rest API-er
- Bygge webservere for å levere statisk innhold som HTML, CSS og JS

Dette kurset fokuserer på bruk av Express.js for å levere innhold.

### Gir oss verktøy

Express.js gir oss flere verktøy for å hjelpe med tviklingen.

Her er noen av dem:

- Automatisk parsing til og fra JSON
- Enkel ruting (POST, GET, etc.).
- Semantisk kode for endepunkter
- Avansert ruting
- Enkel integrasjon med Node ved hjelp av NPM

Det er enkelt å komme i gang med Express, ved å bruke **[npm](https://developer.mozilla.org/en-US/docs/Glossary/Node.js)** , som står for Node Package Manager. npm kan brukes til å kjøre skript og installere **[pakker](https://docs.npmjs.com/about-packages-and-modules)** .

Hvis du bruker VS Code, kan du gå til Terminal sin dropdown-meny og klikke "Ny terminal". Det er her du vil kjøre alle npm-kommandoene dine. Du kan også bruke CMD for Windows eller Mac-terminalen hvis du er Mac-bruker.

Initialiser node-appen din – Oppretter en package.json-fil:

```
npm init -y
```

Bruk npm for å installere express:

```
npm install express
```

### Konfigurere en grunneleggende Express-instans

Når vi har installert Express kan vi prøve å konfigurere en grunnleggende Express-instans.

```jsx
const express = require('express')
const app = express()
const { PORT = 3000 } = process.env

//Routes go here

app.listen(PORT, () => console.log(`Server is running on port ${PORT}`))
```

### Lage et startskript i package.json

Inne i den opprettede package.json-filen legger vi til denne kodebiten kjent som et **[startskript](https://docs.npmjs.com/cli/v8/using-npm/scripts)** . Les mer om npm-skript på [npm docs](https://docs.npmjs.com/cli/v7/using-npm/scripts) .

```jsx
{
    ...
    "scripts":  {
        "start": "node index.js"
    }
    ...
}
```

## Levere innhold med Express.js

> Viser HTML, CSS og JS
> 

### Krav

Når vi leverer innhold med Express, trenger vi følgende:

- En HTML-fil
- En rute å peke mot, slik at vi kan bruke [GET](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/GET)-metodeforespørselen.
- Bruke express sendFile-funksjonen

Prosjektstrukturen skal se ut som det vi ser på bildet nedenfor.

![Express prosjektstruktur](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/express-project-structure.png)

Express prosjektstruktur

I `index.html`filen du har i `public`mappen, lim inn følgende markup:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hello Express</title>
</head>
<body>
    <h1>Hello world!</h1>
    <p>I'm being served using Node and Express!</p>
</body>
</html>
```

Vi vil sende denne filen til nettleseren med Express ved å bruke `sendFile()`metoden.

```jsx
const express = require('express')
const app = express()
const { PORT = 3000 } = process.env

app.get('/', (req, res) => {
    res.sendFile( path.join( __dirname, 'public', 'index.html' ) )
})

app.listen(PORT, () => console.log(`Server is running on port ${PORT}`))
```

Vi er nå klare til å teste koden og levere HTML-filen til nettleseren ved å bruke node index.js. Som du husker, vil `star`-skriptet i `package.json`filen kjøre `node index.js`.

```
npm start
```

Resultat:

![HTML-output ved hjelp av Express](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/express-output-hello-world.png)

HTML-output ved hjelp av Express

### Levere innhold med Express.js

Som standard vil ikke CSS og JS være tilgjengelig for HTML-filene dine, så du må tillate Express å betjene disse filene.

> `__dirname` refererer til gjeldende mappe
> 

### Vise statisk innhold med app.use()

```jsx
const express = require('express')
const app = express()
const { PORT = 3000 } = process.env
const path = require('path')

//Tell express to allow access to static content
app.use( express.static( path.join(__dirname, 'public', 'static') ) )
```

### HTML – Få tilgang til statisk innhold

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Express</title>
    <!-- Static files -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Include the script tag -->
    <script src="js/index.js" defer></script>
</head>
<body>
    <h1>Hello world!</h1>
    <p>I'm being served using Node and Express!</p>
</body>
</html>
```

### Prøv det selv

Lever innhold med Express.js

1. Eksperimenter med forskjellige font- og bakgrunnsfarger i `public/static/css/styles.css` filen.

## Ruting med Express.js

> Definere ruter for å håndtere URL-er
> 

Les om grunnleggende ruting i [Express.js docs](https://expressjs.com/en/starter/basic-routing.html).

""Routing refers to determining how an application responds to a client request to a particular endpoint, which is a URI (or path) and a specific HTTP request method (GET, POST, and so on)." -(Routing refererer til å bestemme hvordan en applikasjon svarer på en klientforespørsel til et bestemt endepunkt, som er en URI (eller bane) og en spesifikk HTTP-forespørselsmetode (GET, POST, og så videre)

### Ruter

Vi må knytte et HTTP-verb (GET, POST, PUT, etc.) med en URL, og skrive kode som håndterer funksjoner

Syntaks:`app.method(path, handler)`

- app – forekomst av Express.js
- method – HTTP-forespørselsmetode (get, post, etc.).
- path – bane på serveren
- handler – funksjon som utføres når ruten er matchet

Vi kan definere flere ruter når vi leverer nettstedinnhold i express.

- `app.get(“/route”, callback)`

Når vi leverer HTML, kan vi lage ruter for hver av sidene våre.

Ruter kan også defineres for å sende inn skjemadata.

```jsx
const path = require("path")

app.get("/", (req, res) => {
    return res.sendFile( path.join(__dirname, "public", "index.html") )
})

app.get("/", (req, res) => {
    return res.sendFile( path.join(__dirname, "public", "contact.html") )
})

app.get("/", (req, res) => {
    return res.sendFile( path.join(__dirname, "public", "about.html") )
})

app.get("/", (req, res) => {
    return res.sendFile( path.join(__dirname, "public", "portfolio.html") )
})
```

### Hver side har en rute

Ovenfor kan du se at det er definert 4 unike ruter.

- • `/`, `/contact`, `/about` og `/portfolio`

Hver av disse rutene betjener en annen HTML-side og Express vil automatisk betjene denne siden når du prøver å få tilgang til den fra en nettleser. Dette betyr at når du går til "http://localhost:3000/about", vil den vise "about.html"-siden i nettleseren.

- **MERK**: Du bruker kanskje en annen port enn port 3000.

### Prøv det selv

1. Prøv å replikere lysbilde 26 ved å lage de forskjellige HTML-sidene i den offentlige mappen
2. Legg til en h1-tag med sidetittelen for hver relevante side
3. Du skal kunne navigere til hver rute (about, contact og portfolio) og se h1-tittelen du opprettet for hver side

### Poste informasjon med skjemaer

Du kan sende skjemadata til Express-appen din fra et HTML-skjema ved å bruke **[POST-metoden](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST)** .

- `<form method=“POST”>`

I Express kan du få de innsendte dataene i **[forespørselsteksten](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://developer.mozilla.org/en-US/docs/Web/API/Request/json)** til en POST-forespørsel.

Disse dataene kan brukes til for eksempel å sende en e-post eller lagre dataene i en database.

Eksempel på håndtering av POST-forespørsler:

```jsx
app.post('/contact', (req, res) => {
    const { formData } = req.body;
    sendEmail(formData)
    res.status(201)
        .json({ sentEmail: true })
})
```

```jsx
<form method="POST" action="/contact">
    <input type="text" name="name" />
    <input type="tel" name="phone" />
    <textarea name="message" rows="30" cols="10"></textarea>
    <button type="submit">Send message</button>
</form>
```

### [Forespørselsmetoder](https://noroff--accelerate-gitlab-io.translate.goog/javascript/course-notes/javascript/03_introduction_to_express.html?_x_tr_sl=en&_x_tr_tl=no&_x_tr_hl=no&_x_tr_pto=wapp#request-methods)

Vi lærte om vanlige metoder for ruting- og innstillingsforepørsler per rute

- `app.get()`, `app.post()`, `app.put()`, `app.delete()`

Dette er de vanligste forespørselsmetodene i et API.

Få en fullstendig liste her i [Express.js docs](https://expressjs.com/en/4x/api.html#app.METHOD)

### De vanligste forespørselsmetodene

```jsx
app.get('/users', (req, res) => {
    // Get list of users
})

app.post('/users', (req, res) => {
    // Create a new user
})

app.put('/users', (req, res) => {
    // Replace a user
})

app.patch('/users', (req, res) => {
    // Update parts of a user
})

app.delete('/users', (req, res) => {
    // Delete a user
})
```

## Express.js middleware

> Funksjoner som kjøres under serverforespørsels- og responslivssykluser.
> 

### Hva er middleware

middlewarebestår av funksjoner som kjøres under serverforespørsels- og responslivssykluser, og har tilgang til HTTP-forespørsler (req) og respons (res).

Middleware kan:

- endre req & res objekter
- avslutte req-res syklus
- kalle neste middleware-funksjon

![Middleware](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/express-nmiddleware.png)

Middleware

### Body parsing i Express.js

Middleware brukes til å automatisk utvide gjeldende funksjoner i et bibliotek/pakke.

Legg til følgende linjer for å aktivere body-parsing i Express.js

- `app.use(express.json());`
- `app.use(express.urlencoded({ extended: false }));`

Dette lar oss enkelt parse *Request body* og trekke ut POST-data.

Det var tidligere en egen pakke kalt 'body-parser' som nå er en del av Express.js.

### Legge til middleware i Express.js

```json
// Add JSON Middleware to you express App
app.use( express.json() )
// Enable nested form data to be parsed
app.use( express.urlencoded({ extended: true }))
```

### UrlEncoded - Utvidet True vs False

```json
// If the query string is "person[name]=bobby&person[age]=3"

app.use(express.urlencoded({ extended: true }))
// Will be parsed as
// { person: { name: 'bobby', age: '3'} }

app.use(express.urlencoded({ extended: false }))
// Won't be parsed correctly
```

### Nå kan vi sende JSON

```json
// req -> HttpsRequest received from client
// req -> HttpResponse sent to client
app.get('/users', (req, res) => {
    // Data to return to client making request
    const response = {
        name: 'James',
        surname: 'Bond',
        code: '007'
    }

    // respond with data in JSON
    res.status(200).json(response)
})
```

### Nøkkelpunkter

- Du kan enkelt levere statisk HTML, CSS og JavaScript ved å bruke Node.js med Express.js
- Du kan få tilgang til forskjellige ressurser gjennom ruting med Express.js
- Du kan utvide funksjonaliteten til Express.js-apper med middleware

---

Copyright 2022, Noroff Accelerate AS