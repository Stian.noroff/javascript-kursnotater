# Grunnleggende kodingskonstruksjoner

## Funksjoner

Den følgende delen vil introdusere kodemønstre som er avhengige av funksjoner.

> Hold koden din modulær med blokker med organisert, gjenbrukbar kode
> 

### Rekursjon

**[Rekursjon](https://developer.mozilla.org/en-US/docs/Glossary/Recursion)** skjer når en funksjon påkaller *seg selv*

Vi må legge til en sjekk for å hindre at det skjer uendelig, vanligvis ved å bruke en **[betinget setning](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Building_blocks/conditionals)** som `if... else`(som vi vil dekke i et avsnitt litt lenger ned).

```jsx
function doPushups(numberOfPushups) {
    const remainingPushups = numberOfPushups - 1;

    if(remainingPushups > 0) {
        doPushups(remainingPushups)
    }

    return 'Completed pushups';
}
```

## Closure

### Lagre verdier i en kontekst

En *[closure](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures)* er ganske enkelt en funksjon som returnerer en funksjon. Dette gjør du ved å *neste* funksjoner. La oss se på et enkelt eksempel:

```jsx
function outer() {
    console.log("Inside the outer function")

    function inner() {
        console.log("Inside the inner function");
    }
    //invoking the inner function
    inner();
}

//invoking the outer function
outer();
```

Output:

```jsx
Inside the outer function
Inside the inner function
```

Variabler definert for omfanget av ytre funksjoner er også tilgjengelige innenfor omfanget for indre funksjoner, så la oss teste dette.

```jsx
function outer(name) {
    //defining a variable inside the outer function
    const lastName = "Bobbington";
    console.log(`Inside the outer function: ${name} ${lastName}`)

    function inner() {
        console.log(`Inside the inner function: ${name} ${lastName}`);
    }
    //invoking the inner function
    inner();
}

//invoking the outer function with a string as a parameter
outer("Bob");
```

Output:

```jsx
Inside the outer function: Bob Bobbington
Inside the inner function: Bob Bobbington
```

Hint: lukkede funksjoner kan også være *[returverdier](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Building_blocks/Return_values)* .

### [Lukke deler](https://noroff--accelerate-gitlab-io.translate.goog/javascript/course-notes/javascript/01_Fundamental_JavaScript_Coding_Constructs.html?_x_tr_sl=en&_x_tr_tl=no&_x_tr_hl=no&_x_tr_pto=wapp#closure-parts)

En lukking har to hoveddeler: en *ytre funksjon* og en *indre funksjon* . Den indre funksjonen kan lagre de ytre funksjonsparametrene i sin utførelseskontekst og brukes når som helst.

```

//A closure
function getDatabase(database, table) {
    return function(data) {
        database.add(table, data)
    }
}//Create a user
const createUser = getDatabase(db, 'user');
createUser('dewald');//Create todo
const createTodo = getDatabase(database, 'todo');
createTodo('Buy the milk')

```

### Prøv det selv- *Closure*

1. Naviger til [https://codepen.io/sean-noroff/pen/WNoqPZe](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://codepen.io/sean-noroff/pen/WNoqPZe)
2. Lag en ny funksjon kalt 'halv' som halverer verdien av num og viser den til konsollen i en indre funksjon.

## Bind kontekst til funksjoner

> Bind en funksjon til å kjøre innenfor en gitt utførelseskontekst
> 

Du lærte i forrige leksjon at et objekt har enten *egenskaper* eller *metoder* , som er funksjoner lagret i et objekt. Hvis du ønsker å få verdien av `name`eiendommen innenfor objektet som er lagret i `person`, er det ikke noe problem. Hvis du vil logge strengen "John Doe" fra `person`, gjør du det bare slik:

```jsx
console.log(person.name);
```

Det vil være naturlig å anta at det er like enkelt å få tilgang til verdien som returneres av `getName`metoden, men det er litt annerledes og krever bruk av `bind()`funksjonen.

Du ble introdusert for `this`nøkkelordet i forrige leksjon, som definerte slik: "I en objektmetode refererer dette nøkkelordet til selve objektet."

```jsx
const person = {
    name: 'John Doe',
    getName: function() {
        return this.name
    }
}

// Assigning getName to a variable
const unboundGetName = person.getName;

// Try to print using unboundGetName
console.log( unboundGetName() ); // => logs undefined

// Assign the correct context
const boundGetName = person.getName.bind(person);

console.log( boundGetName() ) // logs 'John Doe'
```

Når du bruker `person.getName;`, tilordner du et funksjonsuttrykk fra en Object-metode, som tvinger arv med globalt omfang.

"Overstyr" dette ved å bruke `.bind()`på en metode for å spesifisere utførelseskonteksten.

### Prøv det selv - Objekter og metoder

1. Bruk CodePen til å lage et objekt som inneholder en egenskap som lagrer en hardkodet hilsen, og en annen egenskap som lagrer en funksjon som returnerer verdien til hilsen
2. Kall opp funksjonen på objektet og vis resultatet i konsollvinduet

### Passere med referanse eller verdi

Variabler kan lagre to typer verdier. Enten *[primitiver](https://developer.mozilla.org/en-US/docs/Glossary/Primitive)* eller ved *[referanse](https://en.wikipedia.org/wiki/Reference_(computer_science))* .

Primitiver inkluderer datatyper som `undefined`, `null`, `boolean`, `number`, `string`og `Symbol`. Du jobbet med primitiver i forrige leksjon.

[Untitled](https://www.notion.so/60842b6fe3354f4590f50ce236b750a6)

En av de beste måtene å forklare forskjellen mellom primitiver og objekter på er å se på hva som skjer når du prøver å kopiere verdien variabelen lagrer.

I det første eksemplet nedenfor lager vi en kopi av `number`og lagrer den i en ny variabel kalt `numberCopy`. Når vi tilordner en ny verdi til `numberCopy`, forblir verdien til `number` den samme. Slik fungerer primitiver. Å lagre en ikke-primitiv som en verdi fungerer på en annen måte.

```jsx
let number = 42;
let numberCopy = number;

console.log(number) // => Outputs 42
console.log(numberCopy) // Outputs 42

// Let's change the value of numberCopy

numberCopy = 52;

console.log(number) // => Outputs 42  - Did not change
console.log(numberCopy) // => Outputs 52
```

I eksemplet nedenfor erklærer vi først variabelen `names`og tildeler den et *objekt* i stedet for en primitiv verdi. Vi tildeler `namesCopy`verdien av `names`, eller det virker i det minste slik. Det ser tilsynelatende ut som i det første eksemplet, men du kan se at når vi endrer verdien på objektet, endres utgangen for både `names`og `namesCopy`.

Dette er fordi ikke-primitiver er *objektreferanser*, noe som betyr at de refererer til et sted i minnet, som en adresse. Du kopierer altså ikke selve verdien, men *adressen* til verdien. Så både variablene `names`og `namesCopy`peker til samme sted i minnet.

```jsx
const names = {
    name : "Bob";
}

const namesCopy = names;

console.log(names); // => Object { name: "Bob" }
console.log(namesCopy); // => Object { name: "Bob" }

//Changing the value of the "name" property
namesCopy.name = "John";
//Adding a property with the value of "Lennon"
namesCopy["lastName"] = "Lennon";

console.log(names); // => Object { name: "John", lastName: "Lennon" }
console.log(namesCopy); // => Object { name: "John", lastName: "Lennon" }
```

### Prøv det selv - Referanse- og verdityper

1. Lag en funksjon som tar inn en primitiv og en ikke-primitiv som parametere.
2. Endre dem i funksjonen og se hvordan/om de opprinnelige variablene har endret seg.

## Samlinger

```jsx
[1, 'two', false, { number: 5 }]
```

I de fleste programmeringsspråk er grupper av data en del av de mest grunnleggende datastrukturene. Du har flere typer samlinger, og navnene deres kan variere mellom programmeringsspråkene. For eksempel bruker Python en *liste* , mens JavaScript bruker en *array* (matrise).

Det er tre hovedsamlingstyper, som er:

- **[Indekserte samlinger](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Indexed_collections)**
- **[Nøkkelsamlinger](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Keyed_collections)**
- **[DOM-samlinger / HTML-samlinger](https://developer.mozilla.org/en-US/docs/Web/API/HTMLCollection)**

Vi skal se nøye på [arrayen](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array), som er en *indeksert* samling av alle gyldige JavaScript-verdier.

Hver "posisjon" i arrayen blir referert til som *indeksen* til array. En viktig ting å huske er at indeksene til en array starter fra 0 og *ikke* fra 1. For å illustrere dette, la oss [loope](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration) gjennom en enkel array kalt `array`og logge verdien (i dette tilfellet tallet) til hver indeks, og den faktiske indeksen for den verdien.

```jsx
const array = [ 1, 2, 3, 4, 5 ];

array.forEach((number, index) => {
    console.log(`The number is ${number} and the index is ${index}`)
})
```

```
The number is 1 and the index is 0
The number is 2 and the index is 1
The number is 3 and the index is 2
The number is 4 and the index is 3
The number is 5 and the index is 4
```

Elementene i arrayet kalles *Elements* , og du kan få tilgang til ethvert element i arrayet ved å referere til indeksen, som vi vil dekke mer detaljert i neste seksjon.

Arrays kan inneholde alle gyldige JavaScript-verdier, som inkluderer primitiver, arrays, objekter og til og med funksjoner.

Arrays kommer med nyttige hjelpefunksjoner/-egenskaper, og vi har listet opp noen av dem her.

1. **[.length](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/length)**
    - returnerer lengden på en array som en heltallsverdi

```jsx
console.log(array.length) // Outputs 5
```

1. [**.push**](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/push)
    - legger til et element på slutten av en array

```jsx
array.push(6);

console.log(array) // Outputs [ 1, 2, 3, 4, 5, 6 ]
```

1. [**.pop**](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/pop)
    - fjerner det siste elementet i en array

```jsx
array.pop(); // removes 6, which is the last element
array.pop(); // removes 5, which is now the last element

console.log(array) // Outputs [ 1, 2, 3, 4 ]
```

1. [**.slice**](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice)
    - returnerer en kopi av en del eller en "slice" av den opprinnelige array og returnerer en *ny array*
    - endrer *ikke* den opprinnelige matrisen
    - syntaks:`array.slice(start, end)`
    - *start* og *end* refererer til start- og sluttindeksen
    - *end*-parameteren refererer til sluttindeksen + 1, som du vil se nedenfor

La oss først legge til noen tall til arrayet vårt.

```jsx
//a counter variable that takes the current length of the array, which is 4, and adds 1
let count = array.length + 1;

//while the length of array is under 10, we will continue to push the value of count
while(array.length < 10) {
    array.push(count);
    //incrementing count for each iteration
    count++;
}
console.log(array); //Output: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
```

La oss nå se på `.slice()`.

```jsx
console.log(array.slice(0, 4)); // Outputs [ 1, 2, 3, 4 ]
console.log(array.slice(3, 4)); // Outputs [ 4 ]
console.log(array.slice(4)); // Outputs [ 5, 6, 7, 8, 9, 10 ]
console.log(array.slice(2, 6)); // Outputs [ 3, 4, 5, 6 ]
```

1. [.splice](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/slice)
    - endre innholdet i arrayet ved å fjerne eller erstatte eksisterende elementer, eller legge til nye i stedet. *[MDN docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice)*
    - syntaks: `splice(start)`, `splice(start, deleteCount)`, `splice(start, deleteCount, item1)` fra *[MDN Docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice)*

Hvis sletteantallet er 0, kan du se at vi bare setter inn verdien ved den indeksen.

Nettleserkonsollen vil vise deg lengden på arrayet, som vi har inkludert her i kodekommentarene. `Array(11)` i konsollen betyr at den har en lengde på 11.

```jsx
array.splice(4, 0, "some value"); // Output: Array(11) [ 1, 2, 3, 4, "some value", 5, 6, 7, 8, 9, … ]
```

La oss erstatte verdien ved indeks 4 med "en eller annen verdi".

```jsx
array.splice(4, 1, "some value"); // Output: Array(10) [ 1, 2, 3, 4, "some value", 6, 7, 8, 9, 10 ]
```

Det var enkelt nok. La oss øke antallet slettinger til 2 uten å legge til et annet element. Arrayet er kortere med ett element, elementet ved indeks 5, som har blitt slettet uten å bli erstattet av noe. Vi kan *erstatte* dette elementet i stedet for bare å slette det.

```jsx
array.splice(4, 2, "some value"); // Output: Array(9) [ 1, 2, 3, 4, "some value", 7, 8, 9, 10 ]
```

Her legger vi til `"another value"`for å erstatte den forrige verdien ved indeks 5.

```jsx
array.splice(4, 2, "some value", "another value"); // Output: Array(10) [ 1, 2, 3, 4, "some value", "another value", 7, 8, 9, 10 ]
```

### Array-indekser

Som allerede nevnt starter arrays alltid på indeks 0 og det første elementet i matrisen vil derfor være på indeks 0.

La oss se på noen kodeeksempler for å forstå mer om hvordan arrays fungerer.

Erklære en array av strenger:

```jsx
const arrayOfWords = ['javascript', 'is', 'kinda', 'cool'];
console.log(arrayOfWords.length); // Output: 4
```

Erklære en array av tall

```jsx
const arrayOfNumbers = [1, 2, 4, 43, 89, 234, 42];
console.log(arrayOfNumbers.length) //Output: 7
```

Erklærer en array av blandede typer

```jsx
const arrayOfMixed = ['Hello', 42, { message: 'The answer' }];
console.log(arrayOfMixed.length); //Output: 3
```

Deklarere, legge til og fjerne et element

```jsx
// Declare an array
const itemsInMyBag = ['sandwich', 'laptop', 'pen'];

//Add an item at end of array
itemsInMyBag.push('wallet');

//Remove last item
itemsInMyBag.pop();
```

Aksessere array-elementer

```jsx
// Accessing array elements
const randomNumbers = [19, 99, 123, 5, 10334, 29, 1];

// Accessing the nth number by its index - 0 based
const fourthNumber = randomNumbers[5];
const sixthNumber = randomNumbers[7];
```

Noen kule triks! – String > Array

```jsx
// Getting words of a string into an array
const sentence = 'Javascript is a cool language';
// Use the .split() array method
const word = sentence.split(' ');

console.log(words);
//Output: ['JavaScript', 'is', 'a', 'cool', 'language']
```

### `Array.from`

```jsx
// Get characters of sentence into array
const sentence = 'JavaScript is weird';
// Use the Array object .from() method
const characters = Array.from(sentence);

console.log(characters);
/*
Output: 
    [  
       'J', 'a', 'v', 'a', 'S', 'c', 'r', 'i', 'p', 't', ' ', 
        'i', 's', ' ', 
        'w', 'e', 'i', 'r', 'd'
    ]
*/
```

### `Array.fill`

```jsx
// Create placeholder values in array
const placeholders = Array(5).fill('temp');

console.log( placeholders );
// Output: ['temp', 'temp', 'temp', 'temp', 'temp']
```

Lag en matrise med *n* lengde og fyll ut med forhåndsdefinert verdi.

### Prøv det selv - Arrays

1. Naviger til [https://codepen.io/sean-noroff/pen/GRNbeGo](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://codepen.io/sean-noroff/pen/GRNbeGo)
2. Manipuler arrayet slik at konsoll-outputen ligner den på bildet nedenfor:

```
[42, 12, 7, 66, 45]
```

### Prøv det selv - Rekursive funksjoner

1. I CodePen, skriv en rekursiv funksjon som beregner faktoren til et tall
2. Lær mer om fakultetsfunksjoner: [https://www.britannica.com/science/factorial](https://www.britannica.com/science/factorial)

## Betingede erklæringer

Noen ganger vil vi bare kjøre visse deler av koden vår, avhengig av en angitt tilstand. [Betingelser](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Building_blocks/conditionals) er en grunnleggende del av programmering og derfor viktig å forstå riktig. Heldigvis er det grunnleggende om betingelser som `if... else` utsagnet ganske enkelt.

### `if… else` erklæringer

`if... else`er den vanligste betingede erklæringen, som du vil møte ofte i din programmeringskarriere. Det brukes til å lage programmer ved hjelp av **[boolsk logikk](https://en.wikiversity.org/wiki/Introduction_to_C_programming/Lectures/BooleanLogic)** , som høres mer komplekst ut enn det er.

Syntaksen:

```jsx
if(condition) {
    // Condition is true, and we execute this piece of code
} else {
    // Condition is false, and we execute this piece of code
}
```

JavaScript kan sjekke om en betingelse er sann eller usann ved å bruke sammenligningsoperatoren (`===`). Andre operatører som `>`, `<`, eller `!` kan også brukes til å sjekke om en betingelse er sann.

```jsx
// Check the result of a calculation
console.log(2 + 2 === 4) // Output: true
console.log(1 + 2 === 4) // Output: false

// Check if the length of a word is greater than 4
console.log("JavaScript".length > 4) // Output: true

// Check if the length of a word is less than 4
console.log("JavaScript".length < 4) // Output: false
```

La oss lage en funksjon kalt `testLength()`som logger to forskjellige meldinger avhengig av om betingelsen er sann eller ikke, med andre ord, *false*.

```jsx
function testLength(name) {
    if(name.length > 4) {
        console.log(`The name ${name} is longer than 4 characters`)
    } else {
        console.log(`The name ${name} is shorter than 4 characters`)
    }
}

//Invoking the function with different names as a parameter
testLength("Dewald");
testLength("Bob");
```

Output:

```
The name Dewald is longer than 4 characters
The name Bob is shorter than 4 characters
```

Hvis du har en variabel som inneholder `true`eller `false`boolske verdier, kan du inkludere disse i betingede utsagn slik:

```jsx
// Boolean value of true
const javaScriptIsCool = true;

if (javaScriptIsCool === true) {
    console.log('JavaScript is cool!');
} else {
    console.log('This is not possible!')
}
```

Hvis en variabel eller funksjon gir ut en `boolean`verdi ( `true`eller `false`), er det en kortversjon av å skrive en slik setning. I stedet for å skrive ut betingelsen eksplisitt som vi gjorde ovenfor, kan vi skrive bare variabelnavnet inne i parentesen. JavaScript vil forstå at den fullstendige betingede erklæringen er underforstått.

```jsx
const javaScriptIsCool = true;

if (javaScriptIsCool) {
    console.log('JavaScript is cool!'); // => This code will be executed
} else {
    console.log('This is not possible!')
}
```

I noen tilfeller vil du sjekke om en betingelse *ikke er sann* (med andre ord, `false`), og du kan enkelt gjøre dette ved å legge til operatoren `!`, *logisk ikke.*

```jsx
const javaScriptIsCool = true;

if (!javaScriptIsCool) { // Note the ! operator
    console.log('JavaScript is cool!');
} else {
    console.log('This is not possible!') // => This code will be executed
}
```

### `else… if`

Noen ganger må du teste for flere betingelser, som hver kjører sin egen kodeblokk. Utførelsesstakken går fra topp til bunn, og for å gå videre til `else if`, må betingelsen i `if`-setningen være usann.

For å blande ting litt, har vi laget en funksjon som **[returnerer](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/return)** en streng i stedet for å logge den inne i kodeblokken. Vi logger fortsatt meldingen, men vi gjør det ved å påkalle `logMessage()`inne en `console.log()`, *utenfor* funksjonen `logMessage()`.

```jsx
function logMessage(assignmentGrade) {
    if( assignmentGrade > 77 ) {
        return `Score: ${assignmentGrade}. Outstanding grade!`;
    }
    else if( assignmentGrade > 50 ) {
        return `Score: ${assignmentGrade}. Nice, you passed`;
    }
    else {
        return `Score: ${assignmentGrade}. Oh boy, try again`;
    }
}

console.log(logMessage(77));
console.log(logMessage(30));
console.log(logMessage(55));
console.log(logMessage(100));
```

Output:

```
Score: 77. Nice, you passed
Score: 30. Oh boy, try again
Score: 55. Nice, you passed
Score: 100. Outstanding grade!
```

### [`s](https://noroff--accelerate-gitlab-io.translate.goog/javascript/course-notes/javascript/01_Fundamental_JavaScript_Coding_Constructs.html?_x_tr_sl=en&_x_tr_tl=no&_x_tr_hl=no&_x_tr_pto=wapp#the-switch-case)witch`

I JavaScript har vi et alternativ til `if... else if... else`setningene, dette er renere i syntaks. `switch`Uttalelsen er bra for kjøring av én linje . Det ville hjelpe hvis du unngår kompliserte kodeblokker med `switch`, da dette blir rotete og ikke er slik `switch` vanligvis brukes.

Syntaks:

```
switch (value) { 
    case: 'test'
        // Execute some code if value === 'test'
        break;
        //stop iterating through the cases
    case: 'another test'
        // Execute some code if value === 'another test'
        break;
    case: 'a third test'
        // Execute some code if value === 'a third test'
        break;
    default: 
        // Execute some code if there are no matches
    }
```

Husk alltid å inkludere `default`, siden verdien du tester kanskje ikke samsvarer med noen caser.

`switch`er bra når du bruker sammenligning med konstante verdier, se eksempel nedenfor.

```jsx
const fruit = 'banana';

switch (fruit) {
    case 'apple':
        console.log('Apples are 10.00');
        break;
    case 'apple':
        console.log('Apples are 10.00');
        break;
    case 'apple':
        console.log('Apples are 10.00');
        break;
    default: //If no case matches
        console.log('we do not sell that fruit');
}
```

### Prøv det selv

1. Bruk CodePen til å skrive et program som bestemmer om et tall er delelig med 5
2. Hvis tallet er delelig med 5, skal meldingen "x er delelig med 5" vises, med 'x' som erstatter tallet.
3. Omvendt, hvis tallet IKKE er delelig med 5, skal meldingen "x er IKKE delelig med 5" vises, med 'x' som erstatter tallet.

## Løkker (Looping)

### Gå gjennom samlinger (arrays)

Det finnes mange typer løkker, og vi vil dekke de fleste av dem i denne delen av leksjonen. Først, la oss snakke om poenget med løkker i programmeringsmessig forstand.

Ved å bruke løkker kan vi "tråkke" gjennom array-elementer ett om gangen, dette er et av de kraftigste verktøyene vi har innen programmering. Med løkker kan vi utføre repeterende kode uten å måtte skrive om logikken.

I eksemplet med oppgavekarakteren påkaller vi funksjonen *fire* ganger, som er mye repeterende kode. Tenk deg nå å gjøre det samme med hundrevis av karakterer. Det er hundrevis av linjer med kode som bare påkaller den samme funksjonen om og om igjen. Det blir veldig fort uhåndterlig. I et virkelighetsscenario ser vi ofte data som dette i form av en array, så la oss sette karakterene inn i en array og gå gjennom den.

For hver iterasjon påkaller vi `logMessage()`funksjonen og sender inn det gjeldende elementet i arrayet som en parameter. Bare se hvor lite repetisjon vi oppnår ved å gjøre dette.

```jsx
const arrayOfGrades = [ 77, 30, 55, 100 ]

arrayOfGrades.forEach(function(grade) {
    console.log(logMessage(grade));
})
```

Output:

```
Score: 77. Nice, you passed
Score: 30. Oh boy, try again
Score: 55. Nice, you passed
Score: 100. Outstanding grade!
```

I eksemplet bruker vi en **`[forEach](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach)`**array-metode, som er en **[array-metode](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)** for å gå gjennom en array (mer om dette senere når vi snakker om høyere ordrefunksjoner).

Mengden kode sett her vil ikke endre seg, uavhengig av hvor lang matrisen blir. Funksjonen `logMessage()` påkalles inne i loopen i stedet for å påkalle den manuelt og passere inn karakterene, slik vi gjorde i det første eksemplet.

Vi kan iterere gjennom arrays, men vi kan også iterere gjennom objektnøkler eller verdier – alt dette vil vi dekke i denne delen av leksjonen.

### Typer løkker

Typene løkker vi skal se på:

- `while`
- `for`
- `for... of`
- `for... in`
- `do... while`

Når du er kjent med andre C-baserte programmeringsspråk som Java, C++, C# eller PHP, kan noen av løkketypene være like, siden syntaksen er mer eller mindre den samme i JavaScript.

Før vi går videre inn i disse typene, må vi sikre at måten å få tilgang til array-elementer ved deres indeks er forstått. Igjen, ser på `arrayOfGrades`matrisen, la oss få tilgang til hvert element ved hjelp av hakeparenteser. Innenfor de hakeparentesene setter vi inn indeksen vi ønsker å få tilgang til elementet fra.

```jsx
console.log(arrayOfGrades[0]) // Outputs 77
console.log(arrayOfGrades[1]) // Outputs 30
console.log(arrayOfGrades[2]) // Outputs 55
console.log(arrayOfGrades[3]) // Outputs 100
```

Igjen, vi ønsker å unngå å gjenta koden om og om igjen, i dette tilfellet, bare endre tallet i hakeparentesene. Vi kan automatisere denne prosessen ved å bruke ulike typer løkker, som vi nå skal se på.

### `While` løkker

**`[while](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/while)`**brukes til å lage en løkke som fortsetter til en betingelse ikke lenger er sann.

En ting å merke seg er at `while` løkken ikke starter løkken hvis betingelsen *ikke* er oppfylt i utgangspunktet.

Syntaksen er ganske enkel, og det er derfor vi introduserer denne først. Som nevnt vil koden inne i `while`kodeblokken kjøres så lenge betingelsen i parentesen er sann, og løkken stopper først når tilstanden slutter å være sann. Hvis tilstanden aldri slutter å være sann, vil du ende opp med en endeløs løkke, som til slutt kan føre til at datamaskinen/nettleseren din krasjer når den går tom for ram. Vær derfor forsiktig når du bruker `while`løkker.

```jsx
while(condition) {
    //execute code
}
```

En `while`løkke kan brukes til å loope gjennom en matrise, selv om vi vil anbefale å bruke en `for`løkke for dette, som vi skal se på. I eksemplet nedenfor vil vi loope gjennom en array som gjør det klart hvordan en `while`løkke fungerer.

La oss gjenskape funksjonaliteten til `forEach`metoden vi brukte på `arrayOfGrades`, nå ved å bruke en `while`løkke.

1. Vi trenger en tellervariabel som øker seg selv hver gang vi fullfører en løkke. La oss kalle det `index`.
2. Betingelsen vi lager må være på en slik måte at loopingen avsluttes når alle resultatene er logget. Vi kan bruke `.length`-metoden for å få lengden på arrayet som et heltall. La oss sette `arrayOfGrades.length` på innsiden av parentesen. Vi ønsker også å sjekke om verdien av `index` er mindre enn verdien av `arrayOfGrades.length`, og vi vil forklare hvorfor.
3. Rett før hver fullførte løkke/iterasjon øker vi `index`variabelen ved å bruke `++` operatoren. Så, etter den første iterasjonen er `index` lik 1, og etter den andre iterasjonen er `index` lik 2, og så videre. Når verdien av `index` ikke lenger er mindre enn lengden på matrisen, slutter betingelsen å være sann og løkken avsluttes.

```jsx
let index = 0;

while(index < arrayOfGrades.length) {
    //let's log the value of index, for the sake of clarity
    console.log("Current index is: ", index);

    // logging the output of logMessage()
    //adding \n\n to get some line breaks in the output
    console.log(`${logMessage(arrayOfGrades[index])}\n\n`);
    index++;
}
```

Output:

```
Current index is:  0
Score: 77. Nice, you passed

Current index is:  1
Score: 30. Oh boy, try again

Current index is:  2
Score: 55. Nice, you passed

Current index is:  3
Score: 100. Outstanding grade!
```

Her er et annet eksempel med en array av gjøremål:

```jsx
const items = ['buy milk', 'learn javascript', 'exercise'];

let count = items.length - 1;

while (count >= 0) {
    console.log(`Current item ${items[count]}`)
    count--
}
```

### `for` løkke

 `while`er kanskje ikke det beste valget for å gå gjennom arrays, så la oss se på `for`løkken.

Hvis du ikke er kjent med `[for](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for)`løkken, kan koden se litt overveldende ut, så la oss prøve å demystifisere denne kodebiten.

La oss først gå gjennom `arrayOfGrades` ved å bruke `for`løkken. Prøv å analysere hva som skjer i kodeeksemplet nedenfor og sammenlign det med kodeeksemplet for `while`løkken.

Du vil kanskje legge merke til at `for`løkken ser ut som en konsentrert versjon av kodeeksemplet vi brukte med`while`løkken, med alle de samme påfølgende delene, nå plasssert inne i parentesen.

Den har en tellervariabel kalt `i`som starter på 0, så en betingelse som bestemmer når løkken skal avsluttes, og deretter en inkrementor for `i`.

Inne i de krøllparentesene er det kode som utføres på hver iterasjon. I eksemplet nedenfor, så lenge verdien av `i`er mindre enn lengden på `arrayOfGrades.length`, vil loopingen fortsette.

```jsx
for (let i = 0; i < arrayOfGrades.length; i++) {
    console.log(logMessage(arrayOfGrades[i]))
}
```

Eksempel med en rekke gjøremål:

```jsx
const items = ['buy milk', 'learn javascript', 'exercise'];

for (let i = 0; i < items.length; i++) {
    console.log(`item ${i} is ${items[i]}`);
}
```

En av de betydelige fordelene med `for`løkken er at den nåværende indeksen til løkken lett kan nås.

### `For... of` løkker – Over arrays

Hvis du bare ønsker å gå gjennom en matrise og ikke trenger å få tilgang til gjeldende indeks, `[for... of](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of)`kan det være et fint alternativ, siden du kan få direkte tilgang til elementet uten indeks, noe som gjør syntaksen enklere.

Eksempel med en rekke gjøremål:

```

const items = ['buy milk', 'learn javascript', 'exercise'];for (const item of items) {
    console.log(`Current item: ${item}`)
}
```

### `for... in` løkker – Over objekter

Som nevnt tidligere, kan objekter løkkes gjennom. For å gjøre det, bruk `[for... in](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...in)`erklæringen, som er veldig lik `for... of`setningen i syntaks, men med noen mindre forskjeller.

For å få tilgang til verdien til hver nøkkel, skriv først objektnavnet og bruk `key`variabelen i hakeparenteser. Se eksempelet nedenfor.

```jsx
const person = {
    name: 'Dewald',
    surname: 'Els',
    email: 'dewald.els@noroff.no',
    phone: '123 456 789'
}

for (const key in person) {
    console.log(`Current key: ${key}`)
    console.log(`Current key's value: ${person[key]}`)
}
```

### `do… while`

Vi har allerede sett på `while`løkken. Den `[do... while](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/do...while)`er ganske lik `while`løkken. Hovedforskjellen er at koden i `do`kodeblokken vil bli utført minst én gang hvis betingelsen i `while`setningen er falsk.

```jsx
const items = ['buy milk', 'learn javascript', 'exercise'];

let count = items.length - 1;

do {
    console.log(`Current item ${items[count]}`)
    count--
} while (count >= 0)
```

### Oppsummering av løkketypene

`**while**`

- while : Pretest loop
- looping fortsetter så lenge tilstanden er sann
- vil *ikke* starte sløyfen hvis betingelsen *ikke* er oppfylt i utgangspunktet.

`**for**`

- for ( init; condition; increment;)
- `for` løkke er den mest grunnleggende løkken
- Den har en initialisering (oppsett av en teller).
- En betingelse for å sjekke om sløyfen er ferdig
- En inkrementor som "teller" antall løkker

`for... of`

- Kjør til alle elementene i en matrise har blitt iterert over

`for... in`?

- Kjør til nøklene til et objekt har blitt iterert over

`do... while`: Løkke etter test

- Bruk til en betingelse er oppfylt
- Vil starte kjøringen av løkken selv om betingelsen ikke er oppfylt i utgangspunktet

### Prøv det selv

- Naviger til [https://codepen.io/sean-noroff/pen/GRNbeGo](https://codepen.io/sean-noroff/pen/GRNbeGo)
- Loop gjennom arrayet som følger med og doble hvert andre element
- Vis den modifiserte arrayen i konsollvinduet

## Funksjoner av høyere orden

### Funksjoner som aksepterer funksjoner som parametere

I denne delen skal vi se på array-metoder som er funksjoner som aksepterer funksjoner som parametere. Vi har allerede sett `forEach`array-metoden, men det er mange flere, og vi vil dekke de mest vanlige. (For den fullstendige listen over matrisemetoder, naviger til **[MDN-dokumentene](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array) )**

- `.forEach`
- `.map`
- `.filter`
- `.find`
- `.reduce`

Callbacks og Promises (asynkrone)

### `forEach`

Et alternativ til `for`løkkevariantene vi så på tidligere. Det er en array-metode som gjør det samme og er veldig ofte brukt. Metoden `forEach`mottar en funksjon som en parameter, men returnerer ingen verdi. Så det er en betydelig forskjell mellom for eksempel `.forEach`, `.map`og`.filter`.

```jsx
const arrayOfWords = ['JavaScript', 'is', 'fun'];

arrayOfWords.forEach(function(word) {
    console.log(word);
})

// Output: JavaScript is fun
```

### Map

[.map](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map)**[-](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map)**metoden brukes når du vil utføre en operasjon med hvert element i en array.

Med en array bestående av tall kan du for eksempel multiplisere hvert element med seg selv for å finne kvadrattallet dens.

Fordi `.map`metoden oppretter en modifisert kopi av den originale matrisen, trenger den en retursetning i funksjonen der den returnerer verdien som har blitt endret. Denne verdien blir lagret i den nye variabelen.

```jsx
const numbers = [ 3, 4, 9, 12, 31, 4 ];

const numbersSquared = numbers.map(function(number) {
    return number * number
})

console.log(numbersSquared);

//Output: Array(6) [ 9, 16, 81, 144, 961, 16 ]
```

Som vist av utdata, er lengden på listen den samme, så ingen elementer har blitt fjernet eller lagt til. Vi har ganske enkelt tatt hvert element, utført en operasjon og lagret *hvert* nummer i en ny liste.

La oss se på et annet eksempel:

```jsx
// .map() function
const arrayOfWords = ['JavaScript', 'is', 'fun'];
// .map() always returns an array
const firstLetterOfWords = arrayOfWords.map(function(word) {
    return word.substring(0, 1);
})

console.log(firstLetterOfWords);
// ['J', 'i', 'f']
```

Vi bruker en *funksjonsoperasjon* per element. Å bruke en `for... of`erklæring som gjør nøyaktig det samme vil se slik ut:

```jsx
const firstLetterOfWords = [];

for (const word of arrayOfWords) {
    firstLetterOfWords.push(getFirstLetter(word))
}
function getFirstLetter(word) {
    return word.substring(0, 1);
}
console.log(firstLetterOfWords);
```

Vi har også en funksjon som vi bruker for å få den første bokstaven i hvert ord, hvor resultatet skyves til en tom array. Du kan se at dette er mye kode og å bruke `.map`metoden er mer elegant.

Map-funksjonen kan kobles sammen med andre funksjoner av høyere orden eller en hvilken som helst array-funksjon.

### Chain Map + join

Her ser vi bruken av `[.join](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/join)`metoden for å sammenføye alle elementene til ett element, så ['J', 'i', 'f'] blir 'Jif'.

```jsx
// .map() chaining
const arrayOfWords = ['JavaScript', 'is', 'fun'];
// Chain .join() to .map() function
const joinFirstLetters = arrayOfWords.map(function(word) {
    return word.substring(0, 1);
}).join('')

console.log(joinFirstLetters);
// [Jif]
```

### filter

`[filter](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter)`-metoden brukes til kun å returnere verdier som samsvarer med en bestemt betingelse. På denne måten filtrerer vi ut de ønskede resultatene og lagrer dem i en ny array.

I dette eksemplet returnerer vi bare verdier som er av talldatatypen:

```jsx
const mixedValues = [1, 'two', false, { number: 5 }, 12, []];

const onlyNum = mixedValues.filter(function(item) {
    if(typeof(item) === "number") {
        return item;
    }
})

//Output: [ 1, 12]
```

Vi vil lære mer om **[arrow-funksjoner](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions)** i neste leksjon, men bare for å illustrere hvor ryddig koden ser ut ved å bruke arrow-funksjoner med array-metoder, her er et eksempel som gjør akkurat det samme som det ovenfor:

```jsx
const onlyNum = mixedValues.filter(item => typeof(item) === "number")
```

Og et eksempel på kjeding av flere array-metoder og funksjoner:

```jsx
const onlyNumAsString = mixedValues.filter(item => typeof(item) === "number").map(item => item * item).join('').toString()
console.log(onlyNumAsString)
// Output: 1144
```

### Redusere

Metoden `[.reduce()](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce)`kan brukes til å kombinere hvert element til ett element. For eksempel hvis du trenger å legge sammen alle tallene i en array, `.reduce()`vil gjøre det for deg.

```jsx
const salesNumbers = [10, 15, 20, 50, 10];

const salesTotal = salesNumbers.reduce(function(previous, current) {
    return previous + current //Becomes previous
}, 0)

console.log(salesTotal);
// Output: 105
```

### Prøv det selv

1. Naviger til [https://codepen.io/sean-noroff/pen/GRNbeGo](https://codepen.io/sean-noroff/pen/GRNbeGo)
2. Bruk funksjoner av høyere orden, doble hvert av tallene i arrayet og vis summen av de doblete tallene i konsollvinduet.

## Fremtidige verdier

### Utvikling av JavaScript fremtidige verdier

Håndtering av asynkrone operasjoner

**MISSING IMAGE:** Evolusjon av fremtidige verdier

### Callback

> Planlegg funksjoner som skal kjøres etter at en annen funksjon returnerte et resultat
> 

### Aktivere asynkron kode

Vi kan sende en funksjon som en parameter, og når "resultatet" er klart, kjøres callback-funksjonen. Vi må fortsatt vente, men vi kan gjøre ting i mellomtiden, og det er her kraften til asynkron JavaScript kommer inn.

Vi kan også vente på flere ting asynkront.

### Grunnleggende syntaks for callback

```jsx
function sum(a, b, completed) {
    const result = a + b;
    //invoke the callback function
    completed(result);
}

//provide function as 3rd argument
sum(1, 4, function(result) {
    console.log('The result is: ', result)
    //Output: 5
})
```

Callback med asynkront eksempel

```jsx
//Create a function - with a callback function as argument
function getPosts(onCompleted) {
    const http = new XMLHttpRequest();
    http.addEventListener('readystatechange', function() {
        if(http.readyState === 4 && http.status === 200) {
            const posts = JSON.parse(http.responseText)

            // Execute callback function
            onCompleted(posts)
        }
    })
    http.open('GET', 'https://jsonplaceholder.typicode.com/posts');
    http.send();
}

//Invoke getPOsts - Pass function as argument
getPosts(function(posts) {
    console.log('Completed', posts);
    //Display posts
})
```

### Hva er problemet med callback?

Når asynkrone funksjoner starter avhengig av hverandre, fungerer JavaScripts callback-system godt for ett eller to tilfeller. Å legge til flere funksjoner skaper det som blir kalt "Callback Hell". Noe som gjør JavaScript-kode utrolig vanskelig å lese og vedlikeholde. Se nedenfor:

### Callback hell

```jsx
doSomething(function() {
    doSomethingElse(function() {
        doAnotherThing(function() {
            doOneMoreThing(function() {
                okImDone();
            });
        });
    });
});
```

### Promises (løfter)

Et løfte lar oss be om informasjon og kun håndtere resultatet når informasjonen er tilgjengelig. Et perfekt scenario er å be om informasjon fra en **[Rest API](https://developer.mozilla.org/en-US/docs/Glossary/REST)**.

Å få en liste over gjøremål kan ta x antall sekunder, og ved å bruke et løfte kan du gjengi gjøremålene når du har mottatt informasjonen.

```jsx
new Promise(function(resolve, reject) {
    try {
        const result = someProcess();
        //successfully complete
        resolve(result);
    }
    catch(error) {
        // something went wrong
        reject(error);
    }
})
```

```jsx
function getPosts() {
    return new Promise(function(resolve, reject) {
        const http = new XMLHttpRequest();
    })
    // Resolve
    http.addEventListener('readystatechange', function() {
        if (http.readyState === 4 && http.status === 200) {
            // Execute callback function
            resolve(JSON.parse(http.responseText))
        }
    })

    //Reject
    http.addEventListener('error', function(error) {
        reject(error)
    })

    // Using a promise with the XMLHttpRequest
    getPosts()
        .then(function (posts) {
            console.log('Completed:', posts)
        })
        .catch(function (error) {
            console.log('Error occurred: ', error)
        })
}
```

```jsx
fetch('https://jsonplaceholder.typicode.com/posts')
    .then(function (response) {
        return response.json() // Return value for next .then()
    })
    .then(function (posts) { 
        // Posts is the returned value from previous .then() method
        console.log('Completed: ', posts)
    })
    .catch(function (error) {
        console.error('Something went wrong', error)
    })
```

### Race-betingelser

Sender flere løfter

- Spiller det noen rolle hvilken som fullfører først?
- Er de avhengige av hverandre?

`Promise.all()` - Vent til alle løser seg

```jsx
Promise.all([
    fetch('https://jsonplaceholder.typicode.com/posts'),
    fetch('https://jsonplaceholder.typicode.com/users')
])
.then(function (completedPromises) {
    // Receives array of completed promises
    completedPromises[0] //POSTS
    completedPromises[0] //USERS
})
.catch(function(error) {
    console.error('Something went wrong', error)
})
```

### Prøv det selv

1. Naviger til [https://codepen.io/sean-noroff/pen/ExZYggr](https://codepen.io/sean-noroff/pen/ExZYggr)
2. Rediger koden slik at verdien av oppløsningen til løftet legges til DOM i et `<p>`element

## Async/Await

> Forenkle skriving av løfter
> 

### Hvorfor Async/Await?

Løfter er et flott alternativ til å bruke `XMLHttpRequest`API og gir kode som er lettere å lese og forstå. Men å lenke for mange løfter kan føre til forvirrende kode. Async/Await løser dette problemet ved å legge til "syntaktisk sukker" som gjør koden mye lettere å følge.

- De reddet menneskeheten fra [callback hell](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/01_Fundamental_JavaScript_Coding_Constructs.html#callback-hell).
- De er ganske greie, men kan vi gjøre det bedre?

```jsx
async function getPostsAndUsers() {
    try {
        // Use await to "Wait" for te request to finish
        const posts = await fetch('https://jsonplaceholder.typicode.com/posts');
        const users = await fetch('https://jsonplaceholder.typicode.com/users');
    }
    catch(error) {
        console.error('Something went wrong', error);
    }
}

//Invoke async function
getPostsAndUSers()
```

### Viktige regler

- Async/Await kan KUN brukes med løfter, ingenting annet.
- Enhver funksjon kan defineres med async foran.
- Bare asynkrone funksjoner kan ha setninger med *await*-nøkkelord.
- Alle asynkrone funksjoner returnerer et ugyldig løfte hvis de kalles uten *await*.
- Alle funksjoner som returnerer løfter kan løses ved å bruke *await*.
- Async/Await er bare løfter med et annet navn.
- Bruk alltid **[try... catch](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/try...catch)!**

### Prøv det selv - Fetch

1. Naviger til [https://codepen.io/sean-noroff/pen/jOyNMwZ](https://codepen.io/sean-noroff/pen/jOyNMwZ)
2. Endre `load()`-funksjonen for å returnere to poster fra API-en i stedet for en enkelt post (bruk [https://jsonplaceholder.typicode.com/posts/2](https://jsonplaceholder.typicode.com/posts/2) )
3. Bruk to await calls i load()-funksjonen

## DOM-manipulasjon

> Hente og angi verdier fra HTML
> 

### Dokumentobjektet

Hvis du prøver å lage et HTML-dokument og laste det inn i nettleseren, kan du se din markup i form av et **[DOM-tre](https://developer.mozilla.org/en-US/docs/Web/API/Document_object_model/Using_the_W3C_DOM_Level_1_Core)** . [Dokumentobjektet](https://developer.mozilla.org/en-US/docs/Web/API/Document) opprettes, som JavaScript har tilgang til og fungerer som overordnet for alle de andre HTML-elementene på siden. **[](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://developer.mozilla.org/en-US/docs/Web/API/Document)**JavaScript har tilgang til dokumentobjektet og gjennom det kan vi bruke JavaScript til å lese, legge til, manipulere eller fjerne HTML-elementer.

![DOM. Kilde: Wikipedia](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/js-document-object.png)

DOM. Kilde: Wikipedia

La oss prøve å få tilgang til et element vi har laget.

HTML:

```jsx
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Testing out the DOM</title>
</head>
<body>
    <main>
        <button id="btn-add-todo" class="primary-btn">Add todo</button>
    </main>
    <script src="index.js"></script>
</body>
</html>
```

For å gjøre dette, må vi først få tilgang til `document`og på en eller annen måte søke gjennom den for å finne ønsket element med matchende id eller klasse.

Det er flere `document`metoder du kan bruke for å få det elementet du ønsker fra `document`, to populære er `[.getElementById()](https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementById)`og `[.querySelector()](https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelector)`.

```jsx
// Select an element referencing its ID.
const elAddTodoButton = document.getElementById('btn-add-todo');
// Alternative. 
const elAddTodoButton = document.querySelector('#btn-add-todo');
```

Legg merke til at `.querySelector()`bruker `#` [id-velgeren](https://developer.mozilla.org/en-US/docs/Web/CSS/ID_selectors) som ble diskutert i CSS. Siden `querySelector`bruker CSS- og id-velgere for å finne og returnere HTML-elementer, kan du bruke denne metoden med en hvilken som helst gyldig CSS-velger eller id-attributt.

La oss prøve å få det samme `button`elementet ved å bruke klassevelgeren `.`.

```jsx
const elAddTodoButton = document.querySelector('.primary-btn');
```

<aside>
ℹ️ `document.querySelector()` metoden r fleksibel og kan brukes med CSS-klassenavn og `id`-attributter.

</aside>

Noen ganger kan det være en bedre idé å bruke `.getElementById()`siden det ikke krever at du legger til en CSS-velger, noe som gjør den mindre utsatt for feil ved å glemme å legge til velgeren i spørringen.

Nå kan vi prøve å få `button`elementet og logge det til konsollen. Når du logger et HTML-element, finner man interessant informasjon om det. Hvis vi klikker på det loggede `button`elementet, kan du se at en lang liste utvides og avslører mye informasjon. Siden HTML-elementet er et dokumentobjekt, har det mange arvede egenskaper, som vist i denne listen.

```jsx
const elAddTodoButton = document.getElementById('btn-add-todo');
console.log(elAddTodoButton)
// Output: <button id="btn-add-todo" class="primary-btn">
```

![Utvidet HTML-element i konsoll](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/js-node.JPG)

Utvidet HTML-element i konsoll

Legg merke til at verdien `[innerText](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/innerText)`samsvarer med det vi la til `button`elementet i markeringen. Ved å få tilgang til `innerText`, kan vi faktisk endre teksten. La oss prøve dette.

```jsx
elAddTodoButton.innerText = "Look, we've changed the text!";
```

![Endret `innerText` for knappen](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/js-btn-innertext.JPG)

Endret `innerText` for knappen

Alle egenskapene til et HTML-element kan endres, slik vi ville endret verdiene til objektegenskaper.

### Legge til `EventListener`

Når vi klikker på denne knappen, vil vi at noe skal skje. Det er her [Event-lyttere](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener) kommer inn. Event-lytteren "lytter" etter en spesifikk handling eller **[hendelse](https://developer.mozilla.org/en-US/docs/Web/Events)**, og når den hendelsen utløses, blir en funksjon påkalt.

`addEventListener()`tar to parametere: **[hendelsestypen](https://developer.mozilla.org/en-US/docs/Web/Events)** den skal lytte etter og en funksjon som skal utføres hvis hendelsen utløses.

Syntaksen til `addEventListener()`:

```jsx
variableName.addEventListener('click', function(){
    // Event Handler
});
```

La oss logge en melding når knappen klikkes.

```jsx
// Store a reference to the button
const elAddTodoButton = document.getElementById('btn-add-todo');
//Attach an event listener to the button
elAddTodoButton.addEventListener('click', function() {
    console.log('Button was clicked');
})
```

### Les verdi fra HTML-inndata

```jsx
// Store reference to <input id="add-todo" />
const elAddTodoInput = document.getElementById('add-todo');
//Attach event listener to button
elAddTodoButton.addEventListener('click', function() {
    const inputValue = elAddTodoInput.value;
    console.log(inputValue) //Output: Text user typed in <input /> 
});
```

### Lag et nytt element

Vi nevnte at du kan legge til elementer i et eksisterende HTML-dokument som ikke allerede var til stede i den initielle markeringen med JavaScript. For å gjøre dette, kan vi bruke `createElement()`dokumentmetoden og deretter bruke `.append()`metoden til å legge til dette opprettede elementet til det ønskede container-elementet.

La oss si at vi vil legge til et `p`element til `main`elementet. Kun et avsnitt med tekst.

```jsx
const main = document.querySelector('main');

//create the element
const paragraph = document.createElement('p');
//adding a class name
paragraph.classList.add("primary-text");
//adding id attribute and setting the value to 
paragraph.setAttribute("id", "text-home");
//setting the innerText 
paragraph.innerText = "This is the text that has been set using the innerText property";
//adding the new paragraph to the main element
main.append(paragraph);
```

Vi får tilgang til `[classList](https://developer.mozilla.org/en-US/docs/Web/API/Element/classList)`egenskapen til den opprettede `paragraph`variabelen og bruker `.add()`metoden for å legge til en klasse til denne egenskapen. Du kan legge til flere klassenavn til et element, derav egenskapsnavnet `classList`. Deretter bruker vi `setAttribute`metoden, som tar to parametere: attributttype og verdi, begge som en strengdatatype. Her ønsker vi å sette `id` for avsnittet til "text-home". Deretter får vi tilgang til `innerText`egenskapen igjen og setter verdien til den teksten vi vil vise.

Her er et mer komplekst eksempel hvor vi har en array med gjøremål som vi ønsker å vise i nettleseren som HTML.

```jsx
const elTodoList = document.getElementByID('todo-list');
const todos = ['buy milk', 'learn javascript', 'sleep a bit'];

function renderTodos(todos = [], parentElement) {
    // Reset the content of the container
    parentElement.innerHTML = "";

    for (const todo of todos) {
        const elTodo = document.createElement('li');
        elTodo.innerText = todo;
        // Add new todo to the parent container
        parentElement.appendChild(elTodo)
    }
}

renderTodos(todos, elTodoList);
```

### Dokumentobjektmodellen (DOM)

Noen få merknader om dokumentobjektmodellen:

- DOM-gjennomgang er dyrt!
- ALDRI bruk getElement-, querySelector- eller noen annen velger inne i en loop
- JavaScript krysser DOM fra starten i alle underelementer til det samsvarer med velgeren
- Lagre referanser til elementer i variabler
- **Husk**: Event-lyttere lager sin egen utførelseskontekst
    - Derfor  refererer `.this` til DOM-elementet som utløste den hendelsen

### Prøv det selv

1. Naviger til [https://codepen.io/sean-noroff/pen/gOLNyGG](https://codepen.io/sean-noroff/pen/gOLNyGG)
2. Legg til JavaScript-kode som vil legge til et nytt `<li>`element til `<ul>`elementet hver gang du klikker på knappen
3. Innholdet i det nye `<li>`elementet vil bli hentet fra `<input>`elementet

## Nettleser-APIer

### Hva er nettleser-APIer?

Nettleser-API-er refererer til funksjoner som ikke er direkte en del av JavaScript-motoren. Disse funksjonene blir eksponert av nettleseren. Siden JavaScript kjører isolert fra datamaskinens maskinvare, kan nettleseren eksponere funksjoner som Geolocation og Storage for JavaScript, noe som gir den begrenset tilgang til visse funksjoner.

Noen av de vanligste APIene som gjøres tilgjengelige av nettleseren (Chrome, Firefox, etc.):

- document – refererer direkte til HTML-en som gjengis av nettleseren
- navigator - Gir tilgang til posisjonsinformasjon om enheten
- fetch – Tillater asynkrone HTTP-forespørsler
- storage (lokal, session, indeks)* - Lagring og lesing av data i nettleseren

Funksjonene ovenfor er ikke tilgjengelige i Node. Node er en JavaScript runtime som kjører *utenfor* nettleseren.

### Hent eksterne data med fetch(): Promise

API-en `fetch`brukes til å utføre HTTP-forespørsler over et nettverk. fetch API-et returnerer et løfte. Når bare en URL er gitt til fetch API-et, utfører den en `GET`forespørsel.

<aside>
ℹ️ HTTP-metoder brukes til å fortelle mottakerserveren hva hensikten med forespørselen er.

Mer informasjon om dette finner du her: [MDN HTTP Methods](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods)

</aside>

### fetch() syntaks

```jsx
// Retrieve all the Posts from the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/posts')
// Parse the response receive to a JSON object
.then(function(response) {
    return response.json()
})
// The posts are available here
.then(function(posts) {
    console.log('Posts are ready to use: ', posts)
})
// An error occurred during the HTTP request
.catch(function(error) {
    console.error('Something went wrong: ', error)
})
```

### fetch() i en funksjon

```jsx
function getAllPosts() {
    // Retrieve all the Posts from the JSON Placeholder API.
    return fetch('https://jsonplaceholder.typicode.com/posts')
    // Parse the response received toa JSON object
    .then(function(response) {
        return response.json()
    })
}

// You can still use.then() as a Promise is returned
getAllPosts()
// The posts are available here
.then(function(posts) {
    console.log('Posts are ready to use: ', posts)
})
```

### fetch() i en asynkron funksjon

```jsx
async function getPostsAsync() {
    try {
        const response = await fetch('https://jsonplaceholder.typicode.com/posts');
        const posts = await response.json()
        console.log('Posts are ready: ', posts)
    }
    catch(error) {
        console.error('Something went wrong', error)
    }
    finally {
        // Always runs
    }
}
```

<aside>
⚠️ **NB**: Bruk ALLTID en try/catch-blokk med async/wait

</aside>

### Prøv det selv

1. Naviger til [https://codepen.io/sean-noroff/pen/poNXXpW](https://codepen.io/sean-noroff/pen/poNXXpW)
2. Skriv den manglende funksjonen `minusFromThirty`, som tar inn et ubestemt antall argumenter og reduserer verdiene deres fra 30

## Konklusjon

I denne leksjonen lærte du hvordan du skriver mer kompleks logikk inn i JavaScript-koden din. Du skal nå kunne å:

- Bruke funksjoner for å holde koden din modulær og gjenbrukbar
- Bruke lukkinger for å bevare det ytre scopet inne i et indre scope
- Bruke `bind()` for å spesifisere konteksten som en funksjon skal kjøre i
- Bruke *collections* til å administrere flere verdier i minnet
- Bruke betingede setninger for å velge hvilken kode som skal kjøres
- Bruke looping-konstruksjoner for å gjenta kode, for eksempel utføre operasjoner mens du itererer gjennom data i en samling
- Bruke funksjoner av høyere orden for å ta en funksjon som et argument og returnere en funksjon som svar
- Bruke *callbacks*, *promises* og *async*/*await* for å håndtere asynkrone og utsatte operasjoner
- Bruke nettleser-API-er for å inkludere kompleks funksjonalitet i koden din uten å måtte skrive den fra bunnen av

## Se fremover

I neste leksjon lærer du hvordan du bruker følgende JS-funksjoner for å skrive elegant og konsis kode:

- Emulere objektorientering i JavaScript gjennom funksjonsprototyper, objektarv og klasser
- Utnytt kraften til ES6-abstraksjoner som:
    - Pilfunksjoner
    - Malliteraler
    - Destrukturering
    - Objektliteraler
    - Spredningsoperatører
    - Moduler

---

Copyright 2022, Noroff Accelerate AS