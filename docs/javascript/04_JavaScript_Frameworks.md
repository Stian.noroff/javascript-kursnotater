# Oversikt over rammeverk

# JavaScript: Oversikt over rammeverk

## Enkeltsideapplikasjoner

Et forsøk på å få fart på nettet og skape en illusjon av en «App Like»-brukeropplevelse.

### Tradisjonelt nettsted med flere sider

![https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/js-frameworks-01-multipage-website.png](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/js-frameworks-01-multipage-website.png)

Generelt sett består et nettsted med flere sider av mange HTML-dokumenter. Hvert dokument representerer en enkelt side på nettstedet. Når en nettleser skal vise den siden, sendes en HTTP-forespørsel til serveren. Når forespørselen er mottatt av serveren, vil den svare med det forespurte HTML-dokumentet.

Nettleseren vil deretter analysere HTML-en og lese linje for linje mens dokumentet parses. Når nettleseren møter en lenke til en ekstern fil, JavaScript, CSS eller til og med bilder, må en ny forespørsel sendes for den filen. Denne prosessen fortsetter til hele HTML-dokumentet har blitt parset av nettleseren.

Når en ny side blir forespurt av nettleseren, starter denne prosessen på nytt. Et nytt HTML-dokument blir forespurt, og det parses også linje for linje, og laster igjen ned koblede filer .

Dette forårsaker en kort "flash" eller blank lasteskjerm mens HTML-dokumentet lastes ned. På en rask server er det ganske øyeblikkelig, men "flashen" er uunngåelig med den tradisjonelle flersidesarkitekturen.

### Enkeltsideapplikasjon (SPA)

![https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/js-frameworks-02-spa-website.png](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/js-frameworks-02-spa-website.png)

Single Page Application, eller SPA, ble opprettet for å gi brukeren en mer applignende opplevelse.

### App like

Begrepet "App Like" refererer til atferden der visninger lastes inn umiddelbart når du navigerer. Denne applignende-opplevelsen skapes ved å stole på JavaScript for å gjengi HTML-en til dokumentet.

Navnet "Single Page Application" stammer fra applikasjonens arkitektur. Et enkelt `index.html`dokument lastes ned av nettleseren, sammen med applikasjonens kjerne-JavaScript. Alle visningsendringer eller navigering håndteres av det nedlastede JavaScriptet. I stedet for å sende en ny HTTP-forespørsel til serveren, oppdaterer JavaScript lokalt DOM med det nye innholdet. Dette gir applikasjonen en mye mer "App Like"-følelse.

Dette påvirker imidlertid den første belastningen av nettsiden. Alt JavaScript som trengs for å gjengi visningen, lastes ned ved første innlasting av siden. Mange tilnærminger er laget for å løse dette problemet. Mest bemerkelsesverdig er ideen om "Lazy Loading" av JavaScript-koden.

<aside>
ℹ️ **Lazy Loading**

Lazy Loading-tilnærmingen laster bare ned nødvendig JavaScript for å gjengi den første visningen av siden. Deretter, når en ny del av applikasjonen er nødvendig, lastes JavaScript på det tidspunktet. Dette reduserer den intitielle innlastingstiden for siden betraktelig, samtidig som "App Like"-følelsen til nettapplikasjonen din opprettholdes.

</aside>

## Komponenter

Alle de moderne rammeverkene er i skrivende stund er avhengige av konseptet med komponenter.

En komponent representerer en del av brukergrensesnittet. Du kan tenke på en komponent som en del av et større puslespill, eller kanskje en gjenbrukbar Lego-kloss. Komponenter gir ofte en måte å "konfigurere" eller sende informasjon for å endre hvordan den oppfører seg eller hva den viser.

Komponenter isolerer både funksjonalitet og deler av brukergrensesnittet. Den kan være så stor som en layout eller så liten som en knapp.

![https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/frameworks-components.png](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/frameworks-components.png)

For å ta et mer praktisk eksempel kan du bruke skjermbildet av en Tweet fra [@javascript på Twitter](https://twitter.com/JavaScript/status/1520072430307487747?cxt=HHwWhsC9lcv0sJgqAAAA) .

![Header (1), Body (2), Metadata (3) og Actions (4)](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/js-frameworks-tweet.png)

Header (1), Body (2), Metadata (3) og Actions (4)

I eksemplet ovenfor kan hver av de uthevede seksjonene betraktes som en komponent. Det kan til og med deles inn i flere komponenter. Kanskje hver handling nederst kan separeres som en egen komponent.

### Strukturell representasjon

Vi kan lage en strukturell representasjon av komponentene:

### Tweet-komponent

```
Tweet Component
  1. TweetHeader
  2. TweetBody
  3. TweetMeta
  4. TweetActions
    4.1 TweetCommentAction
    4.2 TweetRetweetAction
    4.3 TweetLikeAction
    4.4 TweetShareAction
```

### [Rekvisitter](https://noroff--accelerate-gitlab-io.translate.goog/javascript/course-notes/javascript/04_JavaScript_Frameworks.html?_x_tr_sl=en&_x_tr_tl=no&_x_tr_hl=no&_x_tr_pto=wapp#props)

Hver av disse komponentene ville svare på informasjon som gis for å vise en unik *header*, *body* og *metadata* for en enkelt tweet. Disse opplysningene som gis blir ofte referert til rekvisitter.

> Siden vi ikke har tilgang til Twitter-kildekoden, er dette spekulasjoner og kun brukt som et eksempel.
> 

## Komponentforhold

### super-sub (parent-child)

Hvis vi går tilbake til [Tweet-komponenten](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/04_JavaScript_Frameworks.html#tweet-component), kan vi fastslå at rotkomponenten, Tweeten, er superklasse. Alle andre komponenter nestet med Tweet-komponenten vil bli ansett som subklasse-komponenter.

Men hvis vi ser på TweetActions-komponenten, kan den betraktes som en superklasse, og alle andre komponenter som er nestet i den, vil bli betraktet som subklasser.

Derfor kan forholdet mellom super- og subklasser endre seg avhengig av ditt nåværende perspektiv. Dette er kun en måte å kommunisere til andre hvor komponentene er i forhold til hverandre.

### Søsken

Søsken definerer forholdet mellom komponenter som er nestet på samme nivå. I [Tweet-komponenteksemplet](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/04_JavaScript_Frameworks.html#tweet-component) er TweetHeader, -Body, -Meta og TweetActions alle søsken til hverandre. De finnes alle på samme nivå i Tweet-komponenten. På samme måte som disse regnes som søsken, er Action-komponentene som er nestet i TweetActions-komponenten søsken.

## Komponentdeler

Komponenter består vanligvis av 3 deler. Det betyr ikke at det må være en fil opprettet for hver del av komponenten, men en del av koden vil representere en spesifikk del av hver komponent.

![https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/js-frameworks-components-parts.png](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/js-frameworks-components-parts.png)

## Mal

Malen til en komponent representerer visningen eller brukergrensesnittet til komponenten. Dette er innholdet som brukeren vil se og samhandle direkte med.

Malen er vanligvis skrevet i HTML. Ofte har malen til en komponent et malsystem som lar utviklere iterere over data, betinget vise eller skjule elementer betinget, og svare på interaksjoner fra brukeren. Hvert rammeverk har sin egen tolkning av dette.

Ett unntak er React, som bruker JSX til å generere HTML. JSX er en utvidelse av JavaScript, og selv om den ligner veldig på HTML, lar den utvikleren koble JavaScript-logikk tett til visningen av komponenten.

I de fleste tilfeller krever ikke rammeverk manuelt valg av DOM-elementer.

### Styling

Som emnet antyder, representerer stylingen CSS-en til en komponent. Hovedforskjellen mellom standardoppførselen til styling i komponenter er om komponentstilene er scoped eller ikke.

Når CSS er scoped til en komponent, vil ikke stilene "blø" over i andre komponenter. Dette betyr at du kan ha den samme CSS-velgeren i to komponenter med unik styling. På den annen side, når CSS ikke er begrenset til en komponent, betyr det at CSS skrevet i en komponent også vil påvirke elementer i en annen komponent. Avhengig av oppsettet ditt, kan begge tilnærmingene være nyttige.

### Skript

Skriptdelen av en komponent er der logikken for komponenten vil bli skrevet. Hver komponent kan inneholde et skript som kan manipulere data eller håndtere brukerinteraksjoner.

Noen komponenter krever kanskje ikke noen logikk, og skriptdelen vil derfor være valgfri. Dette blir ofte referert til som en "dum komponent". Det vil si en komponent som ikke utfører noen logikk og bare viser en del av brukergrensesnittet basert på gitte data.

Skriptet kan også inneholde lokal tilstand som kan oppdateres og endres gjennom hele levetiden til den komponenten. Mer detaljer om dette vil bli dekket i hvert rammeverk.

### Komponenttre

Et komponenttre er en representasjon av de tilpassede komponentene som finnes i applikasjonen din. Å lage et komponenttre for applikasjon er avgjørende for planleggingsstadiet for SPA-er. Det gir deg en umiddelbar oversikt over hvilke komponenter som er relatert og hvordan tilstanden vil deles mellom komponentene.

<aside>
ℹ️ Du bør ikke legge til HTML-elementer i et komponenttre, bare de egendefinerte komponentene som representerer applikasjonen din.

</aside>

![https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/js-frameworks-component-tree.png](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/js-frameworks-component-tree.png)

### Virtuelt DOM

Enkelt sagt er VirtualDOM en teknikk som reduserer antallet DOM-oppdateringer. Den samler inn endringene basert på komponentlogikken din og bruker en enkelt endring på DOM i stedet for å oppdatere flere deler.

VirtualDOM er et JavaScript-objekt som representerer den virkelige DOM. Endringer i data som påvirker DOM blir brukt på VirtualDOM først. Når alle endringene er tatt i bruk, forener VirtualDOM deretter endringene til den virkelige DOM. Dette er selvfølgelig en veldig forenklet forklaring og en enorm optimalisering skjer i bakgrunnen.

Den ene fordelen med å bruke VirtualDOM er at DOM-manipulasjon er forenklet og optimaliseringene er allerede gjort for utvikleren.

<aside>
ℹ️ Mer informasjon om [VirtualDOM](https://reactjs.org/docs/faq-internals.html).

Selv om lenken refererer til React spesifikt, forblir konseptet det samme gjennom ulike rammeverk.

</aside>

## Administrasjon av state (tilstand)

State i en applikasjon er ikke mer enn gjeldende data som er lagret på klientsiden. I JavaScript-rammeverk kan komponenter ha lokal eller global state.

### Lokal state

Lokal state i komponenter refererer til data som finnes i komponenten der den er deklarert. Eventuelle endringer i denne tilstanden vil føre til at komponenten og dens underordnede gjengivelser blir gjengitt. Dette vil sikre at komponenten alltid viser det nyeste innholdet.

### Global state

Global state i SPA-er anses generelt for å være state som er lagret på en sentral lokasjon.  Det tillater komponent å fritt få tilgang til state ved hjelp av et *state management*-bibliotek. Siden hvert rammeverk har en rekke av disse bibliotekene, vil vi ikke gå inn i detalj på dette punktet av kurset.

### Foranderlig vs uforanderlig (Mutable vs Immutable)

Det er to vanlige tilnærminger til state administrasjon i komponenter, nemlig Mutable vs Immutable.

> Enkelt sagt er mutering av en verdi prosessen med å endre verdien til en variabel i kode.
> 

```jsx
// Define a variable
let a = "ay";
// Mutate the value of a
a = "nay";
```

Ovenfor kan du se et eksempel på mutering av en verdi.

### Foranderlig state

Foranderlig state lar utvikleren mutere verdiene direkte ved å bruke tilordningsoperatøren.

### Uforanderlig state

Uforanderlig state forhindrer utvikleren i å direkte mutere tilstanden, men kaller heller en funksjon for å oppdatere state. Internt vil rammeverket endre verdien og varsle alle komponenter som er avhengige av den tilstanden for å laste inn på nytt.

## Ruting

Siden enkeltsideapplikasjoner(SPA) bruker JavaScript for å gjengi HTML-en til nettapplikasjonen din, må rutingsoppførselen til nettleseren endres. Når nettleseren navigerer til en ny side, prøver den å hente det oppdaterte innholdet fra serveren. Vi har fått vite at SPA-er ikke kommer med denne tilleggsforespørselen. Siden JavaScript-koden er ansvarlig for å administrere oppdateringen av innholdet.

Alle de moderne rammeverkene kommer med sin egen løsning for ruting. Dette gir brukeren en illusjon av at sidene blir navigert på tradisjonell måte, med den ekstra fordelen av "App Like" -følelsen som følger med SPA-er.

![https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/js-frameworks-routes.png](https://noroff-accelerate.gitlab.io/javascript/course-notes/javascript/img/js-frameworks-routes.png)

På dette tidspunktet er alt du bør forstå at en komponent kan kobles til en bane i nettleseren din. Rutingpakken for rammeverk vil avskjære nettleserrutingen og forhindre standardoppførselen.

---

Copyright 2022, Noroff Accelerate AS