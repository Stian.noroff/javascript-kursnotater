# Figma

## Komme i gang med Figma

I denne leksjonen skal vi se på Figma, som er et gratis verktøy som brukes til å lage *wireframes* og *prototyper*. Et populært alternativ til Figma er Adobe XD, som ikke vil bli dekket i denne leksjonen.

## Wireframing

### Hva er en wireframe?

Når du fortsetter reisen til å bli en front-end-utvikler, vil du høre ordet wireframing ganske ofte. En wireframe er det første skrittet en designer tar for å sette en idé på papiret. Det er fornuftig at wireframes også kalles blueprints, og akkurat som en blueprint, gir en wireframe designeren veiledning.

Ikke bare er wireframes der for å få strukturen og layouten til en side i orden, men det er en utmerket måte å organisere ideer og teste nye. Det lar også designere se hvordan brukere kan samhandle med et grensesnitt på forhånd.

![*Figur 1: Et eksempel på en håndtegnet trådramme (Kilde: careerfoundry.com)*](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/Introductiontowireframes-1.jpg)

*Figur 1: Et eksempel på en håndtegnet trådramme (Kilde: careerfoundry.com)*

### Formålet med en Wireframe

Wireframes er statiske representasjoner av designet, og skal i hovedsak svare på følgende spørsmål:

- Hvordan vil strukturen og layouten på siden se ut?
- Hvilken informasjon vil vises på siden?
- Hvordan vil grensesnittet fungere?

Enkelt sagt er wireframing-prosessen viktig siden den er grunnlaget for et design og hjelper til med å kartlegge informasjonsarkitekturen, brukerflyten og den generelle strukturen til en side.

![*Figur 2: En grunnleggende wireframe laget ved å bruke Balsamiq (Kilde: balsamiq.com)*](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/Introductiontowireframes-2.jpg)

*Figur 2: En grunnleggende wireframe laget ved å bruke Balsamiq (Kilde: balsamiq.com)*

### Wireframes og designprosessen

Wireframes lages tidlig i designprosessen før designere begynner å tenke på et grensesnitts visuelle elementer. Dette gjør at designere kan gjøre betydelige endringer tidlig i prosessen og ikke senere når designet er nær ved å bli ferdigstilt.

![*Figur 3: Wireframes og designprosessen*](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/Introductiontowireframes-3.jpg)

*Figur 3: Wireframes og designprosessen*

### Hvordan ser en wireframe ut?

Wireframes bør være enkle og rett til poenget, siden det vesentlige formålet ikke er å kommunisere nettstedets visuelle egenskaper, men i stedet strukturen og utformingen av grensesnittet. Wireframes er en enkel representasjon av et design, og består sådan normalt av svarte, hvite og grå grunnleggende layoutdesign. For eksempel kan en blokk med et kryss vise bildeplassering, og grå linjer kan representere skrevet tekst.

![*Figur 4: Wireframe-elementer*](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/Introductiontowireframes-4.jpg)

*Figur 4: Wireframe-elementer*

### Viktige termer

- Informasjonsarkitektur
    - Hvordan nettstedets innhold er organisert og strukturert
    - Ideelt sett bør et nettsted være strukturert slik at brukeren raskt kan finne de spesifikke ressursene de trenger
- Call to Action (CTA).
    - En melding som instruerer brukeren om å utføre en spesifisert handling, vanligvis en kommando eller handlingsfrase, for eksempel "Abonner nå" eller "Kjøp nå"

### Hvorfor wireframe?

### 1) Klarhet

Wireframes gir åpenhet til grensesnittdesign. Det lar designere tenke klart om nettstedets interaksjoner og layoutbehov og hjelpe dem med å komme opp med løsninger på problemer.

### 2) Hold kundens oppmerksomhet

Wireframes er et utmerket verktøy for å få klienten til å fokusere på sidens struktur, layout og funksjonalitet. På grunn av sin enkelhet vil ikke klienten bli plaget av andre elementer som farger og bilder. Disse elementene er forbeholdt senere i designprosessen.

### 3) Kommunikasjon til teamet

En godt designet wireframe kan kommunisere dine kreative ideer til teamet. Det fungerer også som et nyttig idédugnadsverktøy mellom designere og utviklere, og andre interessenter.

### 4) Tilbakemelding

Wireframes gir mulighet for tilbakemelding fra klienten og teammedlemmer. Det sikrer også at hovedbudskapet til et nettsted blir effektivt levert.

### 4) Respons

En god wireframe kan gi utviklere en klar ide om hvilke elementer som skal kodes, hvordan den generelle layouten må justeres fra store skjermer til mindre skjermer, hvilket innhold som er viktig, hvordan utviklere skal implementere det overordnede hierarkiet og om navigering på mindre skjermer er tilstrekkelig .

### Typer wireframes

Det finnes tre forskjellige typer wireframes:

- Low-fidelity wireframes (klikk for å utvide teksten)
    
    Low-fidelity wireframes kan sees på som startblokkene i designprosessen. Designeren tar en idé og skisserer den for å se om den gir mening og fungerer for kunden. Low-fidelity wireframes trenger ikke å være perfekte; de trenger bare å kommunisere konseptet.
    
    En annen ting som gjør low fidelity wireframes så nyttig, er at det skaper samtaler rundt det overordnede oppsettet, navigeringen av grensesnittet og skisserer brukerens flyt.
    
    ![Figur 5: Et eksempel på low-fidelity wireframes (Kilde: [uxdesign.cc](http://uxdesign.cc/))](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/Introductiontowireframes-5.png)
    
    Figur 5: Et eksempel på low-fidelity wireframes (Kilde: [uxdesign.cc](http://uxdesign.cc/))
    
- Mid-fidelity wireframes (klikk for å utvide tekst)
    
    Mid-fidelity wireframes er litt mer detaljerte enn low-fidelity wireframes, men fortsatt rimelig enkle. Mid-fidelity wireframes skal vise en nøyaktig, detaljert representasjon av den generelle utformingen av nettstedet eller appen.
    
    Designere kan legge til tekstvekter for å fremheve overskrifter og avsnittstekst og bokser som representerer bilder. Det hjelper også med å visualisere hierarkiet til elementene i designet. Mid-fidelity wireframes lages digitalt ved hjelp av verktøy som Balsamiq og Adobe XD.
    
    ![Figur 6: En mid-fidelity wireframe av Emov-appen (Kilde: [medium.com](http://medium.com/))](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/Introductiontowireframes-6.png)
    
    Figur 6: En mid-fidelity wireframe av Emov-appen (Kilde: [medium.com](http://medium.com/))
    
- High-fidelity wireframes (klikk for å utvide teksten)
    
    High-fidelity wireframes skal kommunisere grensesnittets utseende og følelse. Disse wireframene brukes først mye senere i designprosessen når hver sides struktur og layout er bestemt. På dette stadiet er brukerflyten og informasjonsarkitekturen til grensesnittet allerede bestemt.
    
    Her er plassholdertekst som Lorem Ipsum erstattet med relevant skriftlig innhold, og i stedet for en boks som indikerer et bilde, brukes ekte bilder. Andre visuelle elementer som farge, typografi og selskapets merkevareelementer er også lagt til.
    
    ![Figur 7: Et eksempel på low-fidelity, medium-fidelity og high-fidelity wireframes](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/Introductiontowireframes-7.png)
    
    Figur 7: Et eksempel på low-fidelity, medium-fidelity og high-fidelity wireframes
    

> Vi vil fokusere på low-fidelity wireframes gjennom denne leksjonen.
> 

## Prototyper

### Viktigheten av prototyping

Du lurer kanskje på hvorfor prototyping er så viktig og hvorfor du skal bry deg med å lage en wireframe og deretter en fullt fungerende prototype senere. Disse prosessene spiller alle en viktig rolle for å få ting riktig fra starten og gi brukeren en god brukeropplevelse.

Som vi har lært, er brukertesting avgjørende i designprosessen. Du vil teste wireframe og deretter, senere, gjøre mer grundige tester på prototypen din hvor du vil invitere brukere til å bruke grensesnittet ditt.

Med wireframe vil du teste strukturen og layouten og finne ut om disse elementene gir mening for en bruker. Med prototypingen vil du teste funksjonaliteten til nettstedet eller appen din. Ikke bare vil prototypen vise funksjonalitet, men den skal i hovedsak også vise de visuelle egenskapene til nettstedet.

Følgende spørsmål er eksempler på ting du bør stille deg selv når du bruker en prototype for å teste funksjonalitet:

- Vil brukere vært positiv til ditt valg av farger?
- Var skriftene som ble brukt lett lesbare, og har du et godt visuelt hierarki på siden for å trekke oppmerksomhet til viktig informasjon?
- Hvorfor gikk brukeren glipp av den primære handlingsfremmende oppfordringen (call-to-action)? Kanskje den var for liten, og du burde gjøre den litt større.
- Hvordan reagerte brukere på spesifikke elementer, for eksempel hovednavigasjonen?
- Ble de frustrerte på vei mot sluttmålet?
- Hvordan vil nettstedet ditt se ut og fungere på forskjellige skjermstørrelser?

![*Figur 8: Hi-fi-prototyper for alle skjermstørrelser*](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/Creatingprototypes-1.jpg)

*Figur 8: Hi-fi-prototyper for alle skjermstørrelser*

### Bygge en prototype

Bilde som vi har konsultert kunden, wireframe og papirprototype laget, og vi gikk gjennom brukerundersøkelser for å se hvilke hull vi må fylle med hensyn til produktet vårt og brukeren. Deretter ville vi fokusere på å bygge prototypen vår og deretter teste den før vi går videre til å kode en fullt funksjonell nettside eller app.

### Prototypingsverktøy

Det er mange verktøy designere kan bruke for prototyping, og til syvende og sist er det opp til deg å finne en som dekker dine behov. Vi vil fokusere på Figma i denne leksjonen.

## Figma

> Et verktøy for å lage prototyper
> 

### Hva er Figma

Figma er et veldig populært verktøy som brukes til å produsere prototyper av høy kvalitet med minimal innsats. Det er både nett- og skrivebordsversjoner tilgjengelig, og Figma er fullspekket med nyttige funksjoner for å strømlinjeforme design på høyt nivå til nettstedet ditt.

![*Figur 9: Designeksempel laget med Figma*](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/figma-design-example.JPG)

*Figur 9: Designeksempel laget med Figma*

### Komme i gang

For å lage en ny designfil, naviger først til dashbordet til Figma. Klikk på New Design File-ikonet og et nytt prosjekt åpnes.

![*Figur 10: Ny designfil*](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/figma-1.jpg)

*Figur 10: Ny designfil*

### [Regionverktøy](https://noroff--accelerate-gitlab-io.translate.goog/javascript/course-notes/figma-bootstrap/figma.html?_x_tr_sl=en&_x_tr_tl=no&_x_tr_hl=no&_x_tr_pto=wapp#region-tools)

Regionverktøyene definerer en region (skjermstørrelse) for wireframe. Du har mange alternativer å velge mellom avhengig av hvilken skjermstørrelse du designer for. Når du gjør ansvarlig design, må du ofte endre designet for mindre skjermstørrelser, så du bør i det minste inkludere en desktop- og mobilversjon av designet ditt.

Figur 11 illustrerer hvordan du legger til en ny ramme til prosjektet ditt. Du kan legge til så mange rammer du trenger, noe som vil være nødvendig når du lager en prototype av et nettsted med flere sider.

![*Figur 11: Regionverktøy - legge til en ramme*](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/figma-gif-1.gif)

*Figur 11: Regionverktøy - legge til en ramme*

### Formverktøy

Formverktøy kan brukes til å tegne forskjellig grafikk på rammen. For å legge til former i rammen, klikker du bare på formverktøyene og velger en form fra rullegardinlisten. Du kan omforme og endre størrelsen på disse grunnleggende formene ved å enten dra dem med markøren eller endre høyden og bredden i designvinduet (se figur 12).

Ulike egenskaper kan endres i designvinduet:

- Størrelse
- Rotasjon
- Transparens
- Skygger

![*Figur 12: Designvindu*](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/figma-2.JPG)

*Figur 12: Designvindu*

Hver form har sitt eget lag i Lagdelen. Plasseringen i lagseksjonen spiller en viktig rolle, for hvis figurene overlapper, bestemmer plasseringen i lagseksjonen hvilken av figurene som skal stables oppå den andre i rammen.

![*Figur 13: Formverktøy/Shape Tools*](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/figma-gif-2.gif)

*Figur 13: Formverktøy/Shape Tools*

### Tekst

Tekstelementer kan legges til som et ekstra lag i Figma-diagrammer. Designpanelet har unike egenskaper som kan angis for tekstelementer som skrifttype, størrelse og bokstavavstand. I figur 14 kan du se viktigheten av plasseringen av lag i stabelen. Hvis du plasserer laget du bruker som navigasjonsbakgrunn **over** lenketeksten, vil lenken være skjult **bak** bakgrunnen.

I figur 14 legger vi til tekst til navbardesignet vårt. I figur 15 grupperer vi tekstelementene (lagene) og bruker justeringsverktøyet i Design-vinduet for å justere og sentrere teksten.

![*Figur 14: Legge til tekst*](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/figma-adding-text.gif)

*Figur 14: Legge til tekst*

### Gruppering av lag

Lag kan grupperes sammen hvis de logisk danner en enkelt enhet. Gruppering av lag er viktig siden det gjør strukturen til lagene lettere å forstå, og det gjør det også mulig å flytte flere elementer som en enkelt enhet. La oss si at du vil flytte alle navigasjonskoblingene samtidig. Hvis de er gruppert, flytter du gruppen de tilhører som et enkelt element.

Slik grupperer du lag:

- Velg alle lagene du vil gruppere
- Høyreklikk -> Group selection

Det er god praksis å gi gruppene beskrivende navn slik at lagdelen er ryddig og lett å lese og forstå.

![*Figur 15: Gruppering av lag*](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/figma-grouping.gif)

*Figur 15: Gruppering av lag*

### Fiks posisjon når du scroller

Du har kanskje lagt merke til at enkelte sider har visse elementer fikset når du ruller. Typiske elementer du vil ha fikset på scroll er navigasjonslinjen og kanskje chat-ikonet. For å fikse elementer/lag når du ruller i presentasjonsmodus i Figma, merker du først laget du ønsker skal fikses og deretter går du til Design-vinduet og klikker "fiks posisjon ved rulling".

![*Figur 16: Fast posisjon ved rulling*](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/figma-fixed-position-1.gif)

*Figur 16: Fast posisjon ved rulling*

Hvis du nå går tilbake til presentasjonsmodus, vil du legge merke til at lagene du valgte er fikset når du ruller.

![*Figur 17: Fast posisjon ved rulling (resultat)*](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/figma-fixed-position-2.gif)

*Figur 17: Fast posisjon ved rulling (resultat)*

### Prototyping - lenke sammen sidene

Du har allerede lært at Figma er et prototypeverktøy og har kanskje lagt merke til knappen for å gå inn i prototypemodus (til høyre for designmodus). I prototypemodus kan du opprette "connections" mellom sider, så når du er i presentasjonsmodus kan du navigere mellom sidene dine ved å klikke på lenker eller knapper. På denne måten vil du og din klient få en følelse av brukerflyten og UX/UI-design for å se hva som fungerer og ikke. Når du og kunden er fornøyd med designet, kan du gå videre til produksjonsfasen av prosjektet.

I figur 16 kan du se hvor enkelt det er å skape forbindelser mellom sidene i Figma-prosjektet ditt.

![*Figur 18: Opprette tilkoblinger*](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/figma-creating-connections-1.gif)

*Figur 18: Opprette tilkoblinger*

I figur 17 kan du se resultatet i presentasjonsmodus.[Prøv det](https://noroff--accelerate-gitlab-io.translate.goog/javascript/course-notes/figma-bootstrap/figma.html?_x_tr_sl=en&_x_tr_tl=no&_x_tr_hl=no&_x_tr_pto=wapp#try-it-out)

![*Figur 19: Presentasjonsmodus*](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/figma-creating-connections-2.gif)

*Figur 19: Presentasjonsmodus*

## Prøv det selv

Lag en enkel wireframe i Figma basert på Portfolio-oppgaven.

## Konklusjon

Wireframing er nyttig for å definere den overordnede strukturen til nettsidene våre. Vi kan bruke verktøy som Figma til å lage wireframes og prototyper av applikasjonene våre raskt og holde kunden vår oppdatert under designprosessen, slik at vi alle har samme idé om designet retning.

## Se fremover

Vi skal se på å bruke Bootstrap for å forenkle reaktiv design.

## Tilleggsressurser

### Referanseguider

[https://help.figma.com/hc/en-us](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://help.figma.com/hc/en-us)

### Videodemonstrasjon

[https://www.youtube.com/watch?v=6t_dYhXyYjI](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://www.youtube.com/watch?v%3D6t_dYhXyYjI)

### Artikler, eksempelkode og veiledninger

[https://www.figma.com/blog/how-to-wireframe/](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://www.figma.com/blog/how-to-wireframe/)

---

Copyright 2022, Noroff Accelerate AS