# Bootstrap

# Introduksjon til Bootstrap

## Hva er Bootstrap?

Bootstrap er et front-end utviklingsrammeverk som brukes for front-end webutvikling. Det er det mest populære HTML-, CSS- og JS-rammeverket for å utvikle responsive, mobile first-prosjekter på nettet.

## Hvorfor bruke Bootstrap?

1. Sparer tid og er enkel å bruke - Bootstrap har forhåndsdefinerte designmaler og klasser.
2. Kan tilpasses - velg funksjoner avhengig av hva som trengs.
3. *Great grid system* er et av de beste responsive mobile grid-systemene.
4. Konsistens – konsistens sikres uavhengig av hvem som jobber med prosjektet.
5. Respons – responsiv design er enkel, takket være det flytende rutenettoppsettet som dynamisk justerer seg til riktig skjermoppløsning.

*Kilde: [creative-tim.com](https://www.creative-tim.com/blog/web-design/use-not-use-bootstrap-framework/)*

## Bruke Bootstrap for prototyping

I forrige leksjon lærte vi hvordan vi bruker Figma for wireframing og prototyping. Hvis vi ønsker å lage en fungerende nettside i farten, vil Bootstrap absolutt hjelpe oss med å oppnå dette på grunn av forhåndsdefinerte designmaler og klasser. Hvis vi skal lage en responsiv navigasjonslinje som endres til en hamburgermey på mindre skjermstørrelser, kan vi kopiere/lime inn noe av den forhåndslagde HTML-en fra [getbootstrap.com](https://getbootstrap.com/docs/5.1/components/navbar/).

La oss lage en enkel navigasjonslinje. Gå først til [getbootstrap.com](https://getbootstrap.com/docs/5.1/components/navbar/) og naviger til *Components* og deretter *Navbar* . Det er flere eksempler å velge mellom, men la oss ta det første ved å klikke på *Kopier* - knappen.

![Figur 1: Kopier navbar HTML](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/bootstrap-navbar-html-1.JPG)

Figur 1: Kopier navbar HTML

Nå limer vi bare inn navbar-HTML i vår eksisterende HTML, og det er det, en fullt responsiv navbar rett ut av boksen.

![Figur 2: Lim inn navbar HTML i Codepen](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/bootstrap-add-navbar.gif)

Figur 2: Lim inn navbar HTML i Codepen

## Kickstart-prosjekter med forhåndsdefinerte temaer

Det er noen forhåndsdefinerte temaer som vi kan bruke, tilgjengelig på [https://themes.getbootstrap.com/](https://themes.getbootstrap.com/) og [https://startbootstrap.com/themes](https://startbootstrap.com/themes). Noen er gratis, og andre må betales for. Disse temaene følger bransjestandarder for design og er godt bygget, uten lisensieringsproblemer.

Temaene er enkle å bruke, og pene nettsider kan lages med liten innsats. Vi vil ikke dekke hvordan du bruker temaer i denne leksjonen, men for mer informasjon, sjekk ut [https://themes.getbootstrap.com/guide/](https://themes.getbootstrap.com/guide/).

## Installer Bootstrap med CDN

I denne leksjonen skal vi bruke Boostrap gjennom et CDN (Content Delivery Network) kalt BootstrapCDN. Dette er et offentlig innholdsleveringsnettverk som alle kan bruke. Den lar folk laste CSS, JavaScript og bilder fra serverne eksternt.

I "Getting started"-delen av [getbootstrap.com](https://getbootstrap.com/docs/5.1/getting-started/introduction/) finner du en hurtigveiledning om hvordan du kommer i gang med å bruke Bootstrap med CDN.

Kort sagt (hentet fra getbootstrap.com):

1. Kopier og lim inn stilarket `<link>` i `<head>` før alle andre stilark for å laste inn CSS.

```html
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
```

1. Plasser følgende `<script>`nær slutten av sidene, rett før den avsluttende `</body>`-taggen, for å aktivere den.

```html
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
```

Bootstrap-teamet har gjort denne malen tilgjengelig på nettsiden deres, og kan brukes for å komme i gang.

HTML startmal:

```html
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    <h1>Hello, world!</h1>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
```

## Lær hvordan du bruker Bootstrap-byggeklosser

Når du arbeider med Bootstrap, vil komponenter bli kopiert og limt inn fra [dokumentene](https://getbootstrap.com/docs/5.1/getting-started/introduction/) og modifisert for å passe til prosjektet. Dette betyr å endre den eksisterende koden ved å fjerne/legge til HTML-elementer og verktøyklasser, slik at designet ser nærmere ut til det designeren hadde i tankene. Å jobbe med Bootstrap på et grunnleggende nivå er på noen måter som å leke med LEGO. Den mer avanserte siden av Bootstrap innebærer å bruke SASS for å endre eksisterende verktøyklasser og overstyre deres forhåndsdefinerte CSS-egenskaper eller legge til tilpassede verktøyklasser. Dette er utenfor kursets omfang og vil ikke bli dekket.

## Bootstrap-kategorier

Dette er de fire Bootstrap-kategoriene som vi skal se nærmere på i denne leksjonen.

[Untitled](https://www.notion.so/3b3c579ee34c4a0ab76537f5d0a39da9)

### Layoutelementer

Først skal vi se på Container-elementet. Container-elementet er det mest grunnleggende av layoutelementene. Et wrapper-element er nødvendig for å få wrapper-elementets layoutelementer inne i beholderen til å fungere som de skal med tanke på respons. Vi vil se nærmere på elementene `row`og `col`senere i denne leksjonen.

### Container-bruddpunkter

Den tilgjengelige dokumentasjonen er solid når det gjelder containerbruddpunkter, så sjekk ut dokumentene for dette: [https://getbootstrap.com/docs/5.1/layout/containers/](https://getbootstrap.com/docs/5.1/layout/containers/)

Det som er viktig å merke seg er at tre forskjellige containere oppfører seg forskjellig for hvert responsivt bruddpunkt.

De forskjellige bruddpunktene i Bootstrap er også nødvendig å kjenne til. I CSS lærte vi å bruke `@media`til å definere bruddpunkter. I Bootstrap er bruddpunktene forhåndsdefinert, og i stedet for å bruke `@media`, kan vi legge til et infiks i klassenavnet.

Denne CSS-en ser kanskje kjent ut. container-elementet har full bredde til skjermstørrelsen når `576px`.

```css
@media (min-width: 576px) {
  .container {
    width: 100%;
  }
}
```

I Bootstrap vil det bli skrevet slik i stedet:

```html
<div class="container-sm">100% wide until small breakpoint</div>
```

Følgende tabell viser bruddpunktene som Bootstrap opererer med:

[Untitled](https://www.notion.so/166e879db37049fa95d1e9877f22baad)

### Legg til en container

Nå skal vi se på hvordan du legger til en container og den praktiske forskjellen mellom `.container`og `.container-fluid`, og vi skal også undersøke hvordan `.container-{screen size}`oppfører seg når skjermstørrelsen justeres.

Først legger vi denne HTML-en til Codepen. For å lage et container-element med hvit bakgrunn for body, legger vi til en mørk bakgrunn til containeren ved å bruke `bg-dark`verktøyklassen. Deretter legger vi til litt polstring rundt teksten ved hjelp av `p-3`verktøyklassen og endrer tekstfargen til hvit med `text-white`.

HTML:

```html
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"><title>Bootstrap 101!</title>
  </head>
  <body>
    <div class="container bg-dark p-3">
        <h1 class="text-white">Hello Bootstrap</h1>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>

```

Output i codepen:

![Figur 3: Responsiv container](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/bootstrap-responsive-container.gif)

Figur 3: Responsiv container

## Bootstrap-rutenettoppsett

Bootstrap har sitt eget grid system, som er ganske enkelt sammenlignet med CSS grids. Rutenettsystemet består av rader og kolonner. Som nevnt tidligere, bør rutenettet være inne i .container for å fungere korrekt.

### Grid Rows

Rutenettrader fungerer som foreldre for kolonnene, og vi kan ha flere rader per side.

HTML eksempel:

```html
<div class="container">
    <div class="row">
        <!-- Columns go here> -->
    </div>
</div>
```

### Grid Columns

 `.col`er et barn av `.row`og krever derfor `.row`som overordnet element. Innenfor hver rad må vi ha minimum 1 kolonne og maksimum 12 kolonner.

På bildet under fra w3 Schools kan vi i praksis se `.col-1`, `.col-4`, `.col-6`og `.col-12`.

![Figur 4: Column span ( *kilde: w3schools* )](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/bootstrap-col-span.JPG)

Figur 4: Column span ( *kilde: w3schools* )

Det som skjer her, er at vi øker spennvidden til en kolonne og slår den sammen til en større kolonne, omtrent som `colspan`HTML-attributtet vi brukte til å slå sammen kolonner i en HTML-tabell (se HTML-leksjon). For å gjøre dette kan vi skrive `.col-`og et tall fra 1 til 12, som representerer kolonnespennet. Hvis vi ikke angir noe tall, fylles kolonnen ut automatisk.

La oss nå se på noen praktiske eksempler ved bruk av rader og kolonner.

### Bootstrap Grid – 2 kolonner

HTML:

```html
<div class="container">
    <div class="row">
        <div class="col">Column 1 of 2</div>
        <div class="col">Column 2 of 2</div>
    </div>
    <div class="row">
        <div class="col">Column 1 of 2</div>
        <div class="col">Column 2 of 2</div>
    </div>
</div>

```

Output:

![Figur 5: 2 kolonner](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/bootstrap-grid-1.JPG)

Figur 5: 2 kolonner

### Bootstrap Grid – Variasjoner i kolonner

```html
<div class="container">
    <div class="row">
        <div class="col">Column 1 of 3</div>
        <div class="col-6">Column 2 of 3</div>
        <div class="col">Column 3 of 3</div>
    </div>
    <div class="row">
        <div class="col-8">Column 1 of 2</div>
        <div class="col-4">Column 2 of 2</div>
    </div>
</div>

```

![Figur 6: Variasjoner i kolonner](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/bootstrap-grid-2.JPG)

Figur 6: Variasjoner i kolonner

### Bootstrap Grid – Sikt mot enhetsstørrelser

Endring av kolonnestørrelse avhengig av bruddpunkter

```html
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-4">Column 1 of 3</div>
        <div class="col-sm-12 col-md-4">Column 2 of 3</div>
        <div class="col-sm-12 col-md-4">Column 3 of 3</div>
    </div>
</div>
```

Output:

![Figur 7: Responsivt rutenett](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/bootstrap-responsive-grid.gif)

Figur 7: Responsivt rutenett

### Prøv det selv

1. I CodePen, bruk Bootstrap grid for å lage en grunnleggende mal for en blogg
2. Rutenettet skal inneholde to kolonner. Den første kolonnen skal inneholde et bilde relatert til bloggen og den andre skal inneholde en kopi
3. Bruk en tilnærming av det gyldne snitt for bredden på kolonnene (TIPS: 4 og 8 er nærme nok).

## Bootstrap-komponenter

Vi tok en rask titt på Bootstrap-komponenter da vi fant en navbar i *[Bootstrap-dokumentene](https://getbootstrap.com/docs/5.1/getting-started/introduction/)*, kopierte HTML-malen og limte deretter inn denne HTML-malen i vår egen HTML. En komponent har en bestemt klasse. For eksempel brukes `.navbar`klassen til å definere en navbar-komponent, og når vi bruker denne klassen, vil Bootstrap legge til noe forhåndskodet CSS og JavaScript til HTML-elementet. Det samme gjelder alle de andre komponentene.

Det er 24 unike komponenter, inkludert varsler, knapper, skjemaer, lister og navigasjonslinjer. Alle komponentene er identifisert med et unikt klassenavn.

### Ofte brukte komponenter

Vi vil bruke noen komponenter ofte, og vi vil se noen eksempler på hvordan disse komponentene implementeres i prosjektet vårt.

I figuren under kan du se en enkel nettside som består av en hjemmeside og en kontaktside. Komponentene som brukes er **Karusellkomponenten, Kortkomponenten, Formkomponenten, Navbar-komponenten og Modal-komponenten.**

![Figur 8: Sideeksempel med Bootstrap-komponenter](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/bootstrap-components-showcase.gif)

Figur 8: Sideeksempel med Bootstrap-komponenter

### Skjemaer

Det er enkelt å lage skjemaer med Bootstrap Form-komponenter, og vi har mange gode maler på *[getbootstrap.com](https://getbootstrap.com/docs/5.1/forms/overview/)*.

Nedenfor kan vi se malen for et skjema som inneholder inndatafelt, en avkrysningsboks, velgboks og send-knapp (se innebygde kommentarer i markeringen).

HTML:

```html
<form class="row g-3"><!--Text input fields-->
  <div class="col-md-6">
    <label for="inputEmail4" class="form-label">Email</label>
    <input type="email" class="form-control" id="inputEmail4">
  </div>
  <div class="col-md-6">
    <label for="inputPassword4" class="form-label">Password</label>
    <input type="password" class="form-control" id="inputPassword4">
  </div>
  <div class="col-12">
    <label for="inputAddress" class="form-label">Address</label>
    <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
  </div>
  <div class="col-12">
    <label for="inputAddress2" class="form-label">Address 2</label>
    <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
  </div>
  <div class="col-md-6">
    <label for="inputCity" class="form-label">City</label>
    <input type="text" class="form-control" id="inputCity">
  </div><!--Select box-->
  <div class="col-md-4">
    <label for="inputState" class="form-label">State</label>
    <select id="inputState" class="form-select">
      <option selected>Choose...</option>
      <option>...</option>
    </select>
  </div>
  <div class="col-md-2">
    <label for="inputZip" class="form-label">Zip</label>
    <input type="text" class="form-control" id="inputZip">
  </div><!--Check box-->
  <div class="col-12">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" id="gridCheck">
      <label class="form-check-label" for="gridCheck">
        Check me out</label>
    </div>
  </div><!--Submit button-->
  <div class="col-12">
    <button type="submit" class="btn btn-primary">Sign in</button>
  </div>
</form>

```

Output:

![Figur 9: Kontaktskjema](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/bootstrap-contact-form.JPG)

Figur 9: Kontaktskjema

### Kort (Cards)

Mange nettsteder har kortelementer, og i Bootstrap kan kortelementer enkelt inkluderes i prosjektet ved å bruke Kort-komponenten.

HTML-malen som brukes i figuren nedenfor er en litt modifisert versjon av Grid-kortmalen som finnes på [getbootstrap.com/cards](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://getbootstrap.com/docs/5.1/components/card/) , men prinsippet er det samme: bruk `row`og `col`verktøy for å lage et rutenett med kort. Vi kan se at hver `col`har en kortkomponent som et underelement.

HTML:

```html
<div
    class="row row-cols-1 row-cols-md-2 row-cols-lg-3 row-cols-xl-4 g-4"
>
    <div class="col">
        <div class="card w-100">
            <img
            src="./images/image-1.jpg"
            style="height: 13rem; object-fit: cover"
            class="card-img-top"
            alt="..."
            />
            <div class="card-body">
            <h5 class="card-title">Card title</h5>
            <p class="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card w-100">
            <img
            src="./images/image-2.jpg"
            style="height: 13rem; object-fit: cover"
            class="card-img-top"
            alt="..."
            />
            <div class="card-body">
            <h5 class="card-title">Card title</h5>
            <p class="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card w-100" style="width: 18rem">
            <img
            src="./images/image-3.jpg"
            style="height: 13rem; object-fit: cover"
            class="card-img-top"
            alt="..."
            />
            <div class="card-body">
            <h5 class="card-title">Card title</h5>
            <p class="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card w-100" style="width: 18rem">
            <img
            src="./images/image-4.jpg"
            style="height: 13rem; object-fit: cover"
            class="card-img-top"
            alt="..."
            />
            <div class="card-body">
            <h5 class="card-title">Card title</h5>
            <p class="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
        </div>
    </div>
</div>
```

Output:

![Figur 10: Kortkomponenter](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/bootstrap-cards.JPG)

Figur 10: Kortkomponenter

> **MERK**: Bildet viser åtte kort, og markeringen inneholder fire kort. Dette er gjort for å gjøre eksempelmarkeringen litt kortere.
> 

### Navbar

Vi har nevnt Navbar-komponenten et par ganger allerede. Det er mange maler å velge mellom, og i dette eksemplet bruker vi en enklere navigasjonslinje med to lenker og en nedtrekksliste for mindre skjermer.

HTML:

```html
<nav class="navbar navbar-expand-lg navbar-light bg-light w-100">
    <div class="container-fluid">
        <a class="navbar-brand" href="index.html">Navbar</a>
        <button
            class="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
        >
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <!--Unordered list of links-->
            <ul class="navbar-nav">
                <!--link to home page-->
                <li class="nav-item">
                    <a
                    class="nav-link active"
                    aria-current="page"
                    href="index.html"
                    >Home</a
                    >
                </li>
                <!--link to contact page-->
                <li class="nav-item">
                    <a class="nav-link" href="contact.html">Contact</a>
                </li>
                <!--more links can be added, if needed-->
                <!--Sign in Modal component would go here-->
            </ul>
        </div>
    </div>
</nav>

```

I eksempelkoden har vi ikke inkludert Sign-in Modal-komponenten som kan sees i utdataene. Den innebygde kommentaren som viser hvor koden for Modal-komponenten er plassert kan sees.

Output:

![Figur 11: Enkel Navbar-komponent](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/bootstrap-simple-nav.JPG)

Figur 11: Enkel Navbar-komponent

### Modal

En Modal-komponent brukes til å lage en dialogboks som dukker opp når brukeren klikker på utløserknappen. Denne dialogboksen svever over alt annet innhold på siden. Det er ganske vanlig å bruke Modal-komponenter når du oppretter påloggingsvinduer, som vist i figuren nedenfor.

Malen som brukes i eksemplet nedenfor er "Static backdrop" fra *[getbootstrap.com](https://getbootstrap.com/docs/5.1/components/modal/)* , og inne i body har vi satt inn et enkelt skjemaelement, også hentet fra *[getbootstrap.com](https://getbootstrap.com/docs/5.1/forms/overview/)* . Dette er nok et eksempel på hvordan komponenter kan blandes og matches etter behov for å lage UI-komponentene som passer for våre behov.

Output:

![Figur 12: Modal komponent](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/bootstrap-modal.JPG)

Figur 12: Modal komponent

HTML:

```html
<!-- Button trigger modal -->
<button
    type="button"
    class="btn btn-primary ms-auto me-5"
    data-bs-toggle="modal"
    data-bs-target="#exampleModal"
>
    Sign in</button><!-- Modal -->
<div
    class="modal fade"
    id="exampleModal"
    tabindex="-1"
    aria-labelledby="exampleModalLabel"
    aria-hidden="true"
    >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Sign in</h5>
            <button
                type="button"
                class="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
            ></button>
            </div>
            <div class="modal-body"><!--Inside the modal body, we paste a simple form component--><form>
                    <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label"
                        >Email address</label
                    >
                    <input
                        type="email"
                        class="form-control"
                        id="exampleInputEmail1"
                        aria-describedby="emailHelp"
                    />
                    <div id="emailHelp" class="form-text">
                        We'll never share your email with anyone else.</div>
                    </div>
                    <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label"
                        >Password</label
                    >
                    <input
                        type="password"
                        class="form-control"
                        id="exampleInputPassword1"
                    />
                    </div>
                    <div class="mb-3 form-check">
                    <input
                        type="checkbox"
                        class="form-check-input"
                        id="exampleCheck1"
                    />
                    <label class="form-check-label" for="exampleCheck1"
                        >Keep me logged in</label
                    >
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button
                    type="button"
                    class="btn btn-secondary"
                    data-bs-dismiss="modal"
                >
                    Close</button>
                <button type="button" class="btn btn-primary">
                    Sign in</button>
            </div>
        </div>
    </div>
</div>
```

### Karusell

Karusellkomponenter er en utrolig funksjon i Bootstrap, siden det ofte er tidkrevende å lage en karusell. Nedenfor kan du se HTML-koden for karusellkomponenten som er brukt på eksempelnettsiden vår. Koden har innebygde kommentarer som beskriver de forskjellige elementene i denne karusellkomponenten.

```html
<!--Carousel container-->
<div
    id="carouselExampleIndicators"
    class="carousel slide"
    style="height: 500px"
    data-bs-ride="carousel"
>
<!--Carousel indicators-->
    <div class="carousel-indicators">
        <button
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide-to="0"
            class="active"
            aria-current="true"
            aria-label="Slide 1"
        ></button>
        <button
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide-to="1"
            aria-label="Slide 2"
        ></button>
        <button
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide-to="2"
            aria-label="Slide 3"
        ></button>
    </div><!--Container for the images-->
    <div class="carousel-inner h-100">
        <div class="carousel-item active w-100 h-100">
            <!--100% height and width for carousel-item -->
            <img
            src="./images/image-1.jpg"
            class="d-block w-100 h-100"
            style="object-fit: cover"
            alt="..."
            />
            <!--100% height and width for img element.
            Now "object-fit: cover" will automatically size the image to fit the container-->
        </div>
        <div class="carousel-item w-100 h-100">
            <img
            src="./images/image-2.jpg"
            class="d-block w-100 h-100"
            style="object-fit: cover"
            alt="..."
            />
        </div>
        <div class="carousel-item w-100 h-100">
            <img
            src="./images/image-3.jpg"
            class="d-block w-100 h-100"
            style="object-fit: cover"
            alt="..."
            />
        </div><!--Buttons for navigation-->
        <button
            class="carousel-control-prev"
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide="prev"
        >
            <span
            class="carousel-control-prev-icon"
            aria-hidden="true"
            ></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button
            class="carousel-control-next"
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide="next"
        >
            <span
            class="carousel-control-next-icon"
            aria-hidden="true"
            ></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
</div>
```

![Figur 13: Karusell](https://noroff-accelerate.gitlab.io/javascript/course-notes/figma-bootstrap/img/bootstrap-carousel.JPG)

Figur 13: Karusell

## Bootstrap-verktøyklasser (utility classes)

Verktøyklasser brukes til å legge til CSS-egenskaper til HTML-elementer. La oss si at vi vil legge til avrundede hjørner til et bilde, så kan vi bruke `rounded`verktøyklassen `<img src="#" class="rounded-3">`. I dette tilfellet `rounded`er det samme som CSS-egenskapen `border-radius`og `3`er dens verdi, i dette tilfellet `.3rem`.

Praktiske verktøyklasser er avstandsklassene. Avstandsklasser som `p-2`og `mt-4`kan virke komplekse eller vanskelige å forstå, men det er helt motsatt - det er et veldig enkelt system. I eksemplene `p-2`refererer `p`til `padding`CSS-egenskapen og `2`er verdien. Jo høyere tall, jo bredere blir polstringen. `mt-4`kan brytes ned til `margin-top`med verdien av `4`.

Det er mange verktøyklasser som finnes i [docs] ( [https://getbootstrap.com/docs/5.1/utilities/api/](https://getbootstrap.com/docs/5.1/utilities/api/) ).

### Mye mer å lære i Bootstrap

Bootstrap-komponenter er enkle å implementere og den beste måten å lære på er å skrive og prøve det ut. **Merk**: Før du hopper inn i rammeverk, sørg for å ha mestret det grunnleggende i det underliggende språket.

### Prøv det selv

1. Bygg en enkel HTML-side som viser minst 10 av Bootstrap sine komponenter
2. Av de 10 må følgende 5 komponenter inkluderes
    - Modal
    - Carousel
    - Forms
    - Kort
3. Benytt deg av container samt rader og kolonner
4. Demonstrasjonssiden må være responsiv

## For å konkludere

Viktigst å ta med:

CSS Frameworks (Bootstrap) kan strømlinjeforme utviklingen av mobilvennlige nettapplikasjoner.

## Se fremover

I neste modul vil vi øve på hvordan du legger til interaktivitet på nettet med JavaScript og hvordan du serverer statisk innhold med Node og Express.

De tre siste modulene etter "Dynamisk webutvikling med JavaScript" vil fokusere på å lage enkeltsideapplikasjoner med Vue, React og Angular.

---

Copyright 2022, Noroff Accelerate AS