# React Forms

In this lesson, you will learn how to capture data and handle form events in React applications.

* The first section of the lesson will focus on [Controlled Components](#controlled-components) which will illustrate how to use HTML inputs with local state in a React application.
* Section two, [Uncontrolled Components](#uncontrolled-components), will investigate the `useRef` hook.
* Lastly, the [React Hook Form](#react-hook-form) section will show best practices using the [React Hook Form Library](https://react-hook-form.com).

## Controlled Components

Controlled components is a term used to describe a form element that is bound to local state in a React component. Binding elements to local state in React gives us control over the entered value. This can easily be done with the `onChange` event handler that React provides on `input`, `textarea`, or `select` elements.

### Controlled Text Input

The example below illustrates a simple `input` element being bound to local state, `name`.

> ℹ️ Remember, whenever the input value of a controlled component changes, the entire React component will trigger a re-render. Since there is a state update in the `onChange` event, it will have to re-render the React component to show the last state change.

```jsx
import { useState } from "react";

function Register() {
    // State to bind to the input
    const [ name, setName ] = useState({ value: "" });

    // Handler to update local state
    const handleNameChange = (event) => {
        setName({ value: event.target.value });
    };

    // Handle the form submission.
    const handleRegisterSubmit = (event) => {
        event.preventDefault();
        alert("Welcome " + name.value);
    };

    return (
        <form onSubmit={ handleRegisterSubmit }>
            <fieldset>
                <input type="text" value={ name.value } onChange={ handleNameChange } />
                <button type="submit">Register</button>
            </fieldset>
        </form>
    );
}

export default Register;
```

### Controlled Select

Creating a controlled `select` element.

```jsx
function CoffeeSize() {
    const [ size, setSize ] = useState("");
    
    // Bind the element to the size state.
    const handleSizeChange = (event) => {
        setSize(event.target.value);
    } 
    
    return (
        <select name="size" onChange={ handleSizeChange } value={ size }>
            <option value="small">Small</option>
            <option value="medium">Medium</option>
            <option value="large">Large</option>
        </select>
    );
}
```

### Controlled Checkbox

Creating a controlled `checkbox` input element.

```jsx
function DecafCheckbox() {

    const [ decaf, setDecaf ] = useState(false);

    // Bind the checkbox to the decaf state
    const handleDecafChange = (event) => {
        setDecaf(event.target.value);
    }

    return (
        <label>
            <input type="checkbox" checked={ decaf } onChange={ handleDecafChange }>
        </label>
    );
}
```

### Controlled Radio Buttons

Create a controlled radio button.

```jsx
function MilkType() {
    const [ milkType, setMilkType ] = useState("");

    // Bind Radio Buttons to milkType state.
    const handleMilkTypeChange = (event) => {
        setMilkType(event.target.value);
    }

    return (
        <>
            <input type="radio" value="fullCream" name="milkType"
              onChange={ handleMilkTypeChange } 
              checked={ milkType === "fullCream" } /> Full Cream <br/>

            <input type="radio" value="fatFree" name="milkType"
              onChange={ handleMilkTypeChange } 
              checked={ milkType === "fatFree" } /> Fat Free <br/>
        </>
    );
}
```

### Share Event Handler with Multiple Controlled Components

Pattern to bind multiple inputs with the same event handler.

```jsx
function CustomerInformation() {
    const [ customer, setCustomer ] = useState({
        name: "",
        nickname: "",
    });

    // Event handler for all inputs
    const handleCustomerChange = (event) => {
        // Get the name and value of input that triggered the change.
        const { name, value } = event.target;
        
        // Update the state using the "name" as a key.
        setCustomer({
            ...customer,
            [name]: value
        });
    }

    // Each input should use the same event handler: handleCustomerChange
    return (
        <form>
            <input type="text" name="name" value={customer.name} onChange={ handleCustomerChange } />
            <input type="text" name="nickname" value={customer.nickname} onChange={ handleCustomerChange } />
        </form>
    );
}
```

### Locked Inputs

Inputs in a React component may be locked. This prevents users from editing the value of the input and is sometimes called a "Read Only" input.

The only change required to an input is to **remove the `onChange` event** and **explicitly set a value.**

```jsx
function LockedInputs() {
    const username = "luke-skywalker";
    // No onChange event and value is set.
    return (
        <input type="text" value="Locked Input here!" />
        <input type="text" value={ username } />
    );
}
```

## Uncontrolled Components

At times, there might be a need to avoid re-rendering your component when input values change. To achieve this we can rely on Uncontrolled Components in React.

Uncontrolled components is an HTML input element that has no state bound to it.

> ℹ️ Uncontrolled components do **not** cause any re-renders to occur in the React component when the input value changes.

Since there is no state bound to the `input` element, the only way to retrieve a value from the input is by using React's `createRef` utility or `useRef` hook.

### What is the difference between `createRef`and `useRef`?

First and foremost, `useRef` is a React Hook, therefore it can only be used in function components. On the other hand, `createRef` be used in both class and function components.

Some other notable differences are:

* `useRef` will carry the reference between re-renders
* `createRef` will create a new reference on every re-render

### Uncontrolled Text Input

Create Uncontrolled Reference to a text input

```jsx
import { useRef } from "react"; // 1. Import from react

function UncontrolledInput() {
    
    // 2. Create a reference to the input using the useRef hook.
    const inputRef = useRef();

    const onSubmit = (event) => {
        event.preventDefault();
        // 4. Read the entered value of the input.
        const enteredValue = inputRef.current.value;
    };

    // 3. Use the ref prop on the input element.
    return (
        <form onSubmit={ onSubmit }>

            <input ref={ inputRef } />
        
            ...
        </form>
    );
}
```

### Uncontrolled Checkbox

Create an Uncontrolled Checkbox input. It is important to note that a checkbox does not use the `.value` property but rather the `.checked` property.

```jsx
import { useRef } from "react"; // 1. Import from react

function UncontrolledCheckbox() {
    
    // 2. Create a reference to the input using the useRef hook.
    const checkboxRef = useRef();

    const onSubmit = (event) => {
        event.preventDefault();
        // 4. Read the entered value of the input.
        const isChecked = checkboxRef.current.checked;
    };

    // 3. Use the ref prop on the input element.
    return (
        <form onSubmit={ onSubmit }>

            <input type="checkbox" ref={ checkboxRef } />
        
            ...
        </form>
    );
}
```

### What about Radio Buttons?

Using a `ref` with radio buttons is not practical since you would have to make a reference for each radio button. If the same ref is attached to multiple radio buttons, it would always be the value of the last input to which it was assigned.

### Default Values

Uncontrolled inputs use the defaultValue to set an initial value for the input element. Using the default value with a `ref` leaves the input open for editing but does not make it controlled.
Keep in mind that changing the `defaultValue` will not cause any updates to the value of the DOM. It is not a state value and, therefore, will not trigger any component re-renders.

When working with `input`, `select`, and `textarea` elements, the `defaultValue` property must be used. However, when working with the `checkbox` and `radio` elements, the `defaultChecked` property must be used.

### Uncontrolled Text Input with a default value

For Uncontrolled components with a default value, the `ref` should be attached along with the `defaultValue` property.

```jsx
// 1. Some constant value that won't change.
const DEFAULT_CUSTOMER = "anonymous";

function function CustomerName() {
    // 2. Create a reference for the input
    const inputRef = useRef();

    // 3.Assign the default customer name to the defaultValue property
    return (
        <form onSubmit={ onSubmit }>

            <input type="text" ref={ inputRef } defaultValue={ DEFAULT_CUSTOMER }>

            ...
        </form>
    );
}
```

## React Hook Form

This section will cover an introduction to the React Hook Form library and how it can be used to validate inputs in a React Component.

### Why use React Hook Form?

The first and most obvious reason is simply that form validation is complex.

Since React is a UI library, it has no opinions on form validation therefore React provides no "built-in" solution for form validation.

The React Hook Form library has 0 dependencies. This means that the library does not depend on anything other than the code that the team behind React Hook Form writes.
Internally, React Hook Form uses input [ref's](#uncontrolled-components), therefore, it does not require additional local state to be created. As we learned in the [Uncontrolled components section](#uncontrolled-components), the benefit is that there are no re-renders when input value changes.

Lastly, React Hook Form provides excellent documentation, making it easy to get started and dive deep into the library.

### Installing React Hook Form

Since React Hook Form is a third-party library, it must be installed into your project as a dependency.

Run the following command at the root of your project directory. In other words, where you find the `package.json` file.

```bash
npm install react-hook-form
```

This will add the library as a dependency to your package.json automatically.

### The `useForm` hook

As the name suggests, the React Hook Form library relies on React hooks.

The `useForm` hook provides everything needed to set up a form for validation with the library. It should be imported from `"react-hook-form"`.

### useForm - register

The `register` function is used to register a new input to the form.

### useForm - handleSubmit

The `handleSubmit` function will be provided to the form's onSubmit event. It will automatically prevent the default behavior of the form. Therefore, the form will not send an HTTP request when it is submitted.

### useForm - formState

The `formState` object provides the current errors of the form.

### useForm - watch

Since React Hook Form does not use [controlled components](#controlled-components), you can subscribe to the input changes using the `watch` function.

Below you can find a simple example of a form that is set up with React Hook Form.

```jsx
import { useForm } from "react-hook-form";

/**
 * 1. register: Add inputs
 * 2. handleSubmit: Bind to form onSubmit
 * 3. formState: Provide errors for current inputs 
 */

function BasicForm() {
    const { register, handleSubmit,  formState: { errors } } = useForm();

    // data - Inputs in form with values
    const onSubmit = (data) => {
        // data: { username: "", password: "" }
    };

    // register two inputs: username and password
    return (
        <form onSubmit={ handleSubmit(onSubmit) }>

            <input {...register("username")} type="text" />
            <input {...register("password")} type="password" />

            <button type="submit">Save</button>
        </form>
    );
}

export default BasicForm;
```

### React Hook Form Validation Rules

The `register` function provides an additional argument where you may specify the validation rules for the current input.

Some of the available validation rules are:

* `required`: true | false - The input value may not be omitted
* `min`: number -  If it is a number, the minimum value for that input
* `max`: number -  If it is a number, the maximum value for that input
* `minLength`: number - The minimum character count of the input value
* `maxLength`: number - The maximum character count of the input value
* `pattern`: regex - The input value should match the regex pattern
* `validate`: callback function - A custom function for complex validations

> 📚 You can read more about the validation rules in detail on the [React Hook Form website](https://react-hook-form.com/api/useform/register).

Below you can find a simple example for applying validations to your inputs.

```jsx
import { useForm } from "react-hook-form";

/**
 * 1. register: Add inputs
 * 2. handleSubmit: Bind to form onSubmit
 * 3. formState: Provide errors for current inputs 
 */

function BasicForm() {
    const { register, handleSubmit,  formState: { errors } } = useForm();

    // data - Inputs in form with values
    const onSubmit = (data) => { ... };

    // username is required and must be at least 3 characters
    return (
        <form onSubmit={ handleSubmit(onSubmit) }>

            <input {...register("username", {
                required: true,
                minLength: 3
            })} type="text" />

            { errors.username && <p>Username is required</p> }

            ...
        </form>
    );
}

export default BasicForm;
```

## Key takeaways

* Controlled components refer to HTML form controls that are linked to local state
* Controlled components that have a value set and no onChange event will be locked and can not be edited
* Uncontrolled components use the useRef function
* Uncontrolled components do not re-render the component when the value changes
* React Hook Form provides powerful form validation and has 0 third-party dependencies

## Additional resources

* [Get Started | React Hook Form](https://react-hook-form.com/get-started) - Simple React forms validation (react-hook-form.com)
* [Code Examples](https://react-hook-form.com/get-started) for React Hook Form

---

Copyright 2022, Noroff Accelerate AS