# React-komponenter og Router

# React-komponenter og Router

React-komponenter er byggesteinene i enhver React-app, og de tilrettelegger også for separasjon av brukergrensesnittet (UI) i uavhengige og gjenbrukbare deler. Dette lar utviklere tenke på hver komponent isolert. De forskjellige typene React-komponenter er allerede diskutert i forrige leksjon, og i denne leksjonen skal vi se nærmere på komponentens anatomi, nemlig: komponentstruktur, navngiving og implementering. Dessuten vil denne leksjonen undersøke tilstand og komponentlivssyklus i React and React Router.

## Komponentanatomi

Denne delen vil gi en oversikt over React-komponentens anatomi, inkludert navnekonvensjoner, hvordan komponenter er organisert, opprette og vise komponenter, lage komponenter av høyere orden og håndtere brukerinteraksjon med Events.

For navnekonvensjoner i React-komponenter, bør filtypene for JavaScript være `.js` eller `.jsx` filtypen og for TypeScript, `.tsx` filtypen, f.eks `ComponentName.js`. Komponenter og filnavn bør også være PascalCasing. Selv om hvert selskap har sine egne retningslinjer og standarder, er den generelle regelen å bruke logiske og beskrivende navn. Til slutt bør "navneområde"-komponenter brukes med mappenavnere, f.eks. Todo:

```
Todo/
- TodoList.js
- TodoListItem.js
```

Hvordan komponenter er organisert i React spiller ingen rolle. React-dokumentasjonen bemerker tydelig at hvordan filer organiseres i mapper er opp til utviklerne. Imidlertid er det felles tilnærminger som ofte brukes til å organisere komponenter. Disse inkluderer gruppering etter funksjoner eller ruter:

```
common/
   Avatar.js
   Avatar.css
   APIUtils.js
   APIUtils.test.js
feed/
   index.js
   Feed.js
   Feed.css
   FeedStory.js
   FeedStory.test.js
   FeedAPI.js
profile/
   index.js
   Profile.js
   ProfileHeader.js
   ProfileHeader.css
   ProfileAPI.js
```

Og gruppering etter type eller "rolle":

```
api/
   APIUtils.js
   APIUtils.test.js
   ProfileAPI.js
   UserAPI.js
components/
   Avatar.js
   Avatar.css
   Feed.js
   Feed.css
   FeedStory.js
   FeedStory.test.js
   Profile.js
   ProfileHeader.js
   ProfileHeader.css
```

La oss nå vurdere hvordan komponenter kan lages. Som allerede nevnt, kan React-komponenter enten være funksjonelle komponenter eller klassekomponenter. For å lage en funksjonell komponent må det være et funksjonsuttrykk eller en erklæring, en gyldig JSX må returneres, og funksjonen må eksporteres. Eksempler på hvordan du lager funksjonelle komponenter er som følger:

```jsx
// Create a function component
const TodoList = () => {
   // Return valid JSX
   return (
      <ul>
          <li>Item 1</li>
          <li>Item 2</li>
          <li>Item 3</li>
      </ul>
   )
}
// Export component
export default TodoList
```

```jsx
// Create a function component
function TodoList {
   // Return valid JSX
   return (
      <ul>
          <li>Item 1</li>
          <li>Item 2</li>
          <li>Item 3</li>
      </ul>
   )
}
// Export component
export default TodoList
```

For å lage en klassekomponent kreves det derimot at komponenten må være en JavaScript-klasse, må importere React, må utvide React.Component, og må ha en gjengivelsesmetode som returnerer gyldig JSX. Et eksempel på å lage en klassekomponent er som følger:

```jsx
import React from 'react'
// Create a class component
class TodoList extends React.Component {
   render () {
      // Return valid JSX
      return (
         <ul>
             <li>Item 1</li>
             <li>Item 2</li>
             <li>Item 3</li>
         </ul>
      )
   }
}
// Export component
export default TodoList
```

Å vise komponenter i React følger samme syntaks som brukes for funksjonelle komponenter og klassekomponenter. Det innebærer også å importere komponent etter navn og legge til JSX ved hjelp av PascalCasing. Komponenter kan være selvlukkende XML-taggerog kan gjengis flere ganger. Et eksempel på visning av komponenter og gjenbruk av komponenter er gitt henholdsvis som følger:

```jsx
// Import Component
import TodoList from "./TodoList"

function App() {
   // Render Component
   return (
      <div className="App">
        <TodoList />
      </div>
   )
}
export default App
```

```jsx
// TodoListItem.jsx
function TodoListItem() {
   return <li>Item</li>
}
export default TodoListItem
// TodoList.jsx
import TodoListItem from './TodoListItem'

function TodoList() {
   return (
      <ul>
          <TodoListItem />
          <TodoListItem />
          <TodoListItem />
      </ul>
   )
}
export default TodoList
```

Dessuten er det komponenter av høyere orden i React som gir avanserte teknikker for gjenbruk av React-komponenter. Disse komponentene forenkler delt funksjonalitet eller styling mellom flere komponenter. De kan også pakke andre komponenter med en komponent av høyere orden under gjengivelse eller under eksport og er basert på JavaScript Higher Order Functions og Closure-mønstre. Et eksempel på komponenter av høyere orden er vist i følgende kodebiter:

```jsx
// Add props to argument list
const Card = (props) => {
   // Render any children of the component
   return (
      <section className="Card">
          { props.children }
      </section>
   )
}
export default Card
```

```jsx
// Import Component
import Card from "./Card"
import TodoList from "./TodoList"

function App() {
   return (
      <div className="App">
         <Card>
           { /* TodoList is a child Card */}
           <TodoList>
         </Card>
      </div>
   )
}
export default App
```

Komponenter av høyere orden viser også et autentiseringsmønster ved at man kan pakke komponent med andre komponenter og deretter dele logikk med komponenter som pakkes. Denne funksjonen er bra for autentisering som vist i følgende kodebiter:

```jsx
// Closure with Component to render and props
const witAuth = Component => props => {
   // Pseudo auth function
   const isAuthenticated = hasValidSession()

   if (isAuthenticated) {
      return <Component {...props} />
   } else { // Redirect non authenticated users
      return <Redirect to="/login">
   }
}
export default withAuth
```

```jsx
import withAuth from './hoc/withAuth'
// Regular Component
const Profile = () => {
   return (
      <main>
          <h1>Profile Page</h1>
          <p>My awesome profile!</p>
      </main>
   )
}
// Wrap HOC around Profile
export default withAuth(Profile)
```

Til slutt gir React-komponenter mekanismer som kan brukes til å svare på brukerinteraksjoner. Disse hendelsene kan håndtere eller svare på brukerinteraksjoner i en applikasjon. Hendelsene kan også eksistere i både tilstandsfulle (stateful) og tilstandsløse (stateless) komponenter. De vanligste hendelsene er `onClick()` og `onChange()` arrangementer. Mens `onClick()`hendelser reagerer på klikkhendelser på ethvert element, `onChange()` brukes hendelser til inndata eller utvalgte kontroller, er gode for validering av data og kan brukes til søkefelt for å filtrere resultater. Eksempler på klasse- og funksjonskomponenthendelser `onClick()` er beskrevet i følgende kodebiter:

```jsx
import React from 'react'

class TodoItem extends React.Component {
   // Event handler method
   handleTodoItemClicked() {
      // Do something here!
   }

   render() {
      // Attach the onClick event
      return (
         <li onClick={ () => this.handleTodoItemClicked() }>
             { this.props.title }
         </li>
      )
   }
}
export default TodoItem
```

```jsx
import React from 'react'
// Create a class component
class TodoList extends React.Component {

   constructor() {
      super()
      // Bind method to class
      this.goToDetails = this.goToDetails.bind(this)
   }

   goToDetails(event) {
      console.log(this); // Logs Class Instance
   }

   render() {
      return (
         <ul>
             <li onClick={ this.goToDetails }>Todo Item 1</li>
             <li onClick={ this.goToDetails }>Todo Item 2</li>
         </ul>
      )
   }
}
// Export component
export default TodoList
```

```jsx
function TodoListItem(props) {
   // Function to handle click event
   const handleTodoItemClicked = () => {
      // Do something with clicked item
   }
   // Bind onClick to item's click event
   return (
      <li onClick={ handleTodoItemClicked }>
          { props.title}
      </li>
   )
}
export default TodoList
```

For `onChange()`hendelsene brukes den til å håndtere når en bruker skriver inn en tekstboks. Tekstboksen er bundet til onChange for å håndtere tekstendringer. Dessverre er det ingen toveis binding i React. Følgende to kodebiter er eksempler på `onChange()`funksjon og klassekomponent:

```jsx
function Login() {
   // State
   const [ username, setUsername ]= setState('')

   // Event to update username
   const handleOnChange = (event) => {
      setUsername(event.target.value)
   }
   // Bind change event to input
   return (
      <input type="text" onChange={ handleOnChange } />
   )
}
```

```jsx
class Login extends React.Component {

   state = {
      username: ''
   }

   // Event to update username
   handleOnChange(event) {
      this.setState({
         username: event.target.value
      })
   }
   render() {
      // Bind change event to input
      return (
         <input type="text"
             onChange={ (event) => this.handleOnChange(event) } />
      )
   }
}
```

## Tilstand og komponentlivssyklus

Fra og med React 16.8 ble en ny funksjon kalt Hook lagt til for å hjelpe til med å administrere komponenttilstand og koble til komponentlivssykluser. Hooks er kun tilgjengelige i funksjonskomponenter, og de muliggjør bruk av tilstand og andre React-funksjoner uten å skrive en klasse. De gir en direkte API til de eksisterende React-konseptene: props, tilstand, kontekst, refs, og livssyklus, og de tilbyr en ny kraftig måte å kombinere dem på. Hooks må også importere `useState`, `useEffect`, eller `useReducer`fra React og bør alltid kalles på toppnivået. Dvs. aldri bruk Hooks i kontrollstrukturer som if, while, for, osv.

`useState(defaultValue)`er en Hook som forenkler tillegg av React tilstand til funksjonskomponenter. Den oppretter lokal tilstand for en komponent med en setter og mottar et enkelt argument, som er standardverdien for tilstanden. Returverdien til `useState`er en array - Indeks 0 er tilstandsverdien og Indeks 1 er setteren. I tillegg kan array-destrukturering brukes til å trekke ut verdi og setter. Et eksempel på `useState`for å opprette en lokal tilstand er vist i følgende kodebiter:

```jsx
import { useState } from 'react'

function TodoList() {
   // Create Local state with setState hook
   const [todos, setTodos ] = useState([])

   return (
      ...
   )
}
export default TodoList
```

Det er svært viktig å huske å ikke oppdatere tilstand direkte ved å bruke tilordningsoperatør, dvs. `=`. Tilstand kan oppdateres enten ved å bruke en setter eller kan erklæres som et objekt, som deretter tillater oppdatering av objektet i tilstand. Oppdatering av tilstand ved bruk av setter og oppdatering av et objekt i tilstand er beskrevet i henholdsvis følgende kodebiter:

```jsx
// Replacing state
const newTodos = ['Work', 'Sleep']
setTodos(newTodos)

// Add a todo
setTodos([...todos, 'Study'])

// Removing a todo
const newTodos = [...todos] // Copy
newTodos.splice(indexToRemove, 1) // Remove todo
setTodos(newTodos) // Update state
```

```jsx
import { useState } from 'react'

function LoginForm() {
   const [ credentials, setCredentials ] = useState({
      username: '',
      password: ''
   })

   // credential is now { username: '', password: '' }

   return (
      ...
   )

}
export default LoginForm
```

```jsx
// Update username
setCredentials({
   ...credentials,
   username: 'luke-skywalker'
})
// Update password
setCredentials({
   ...credentials,
   password: 'sup3rs3cr3t'
})
```

`useEffect(callback, [dependencies])` er en hook som lar en utføre sideeffekter i funksjonskomponenter. Den simulerer livssyklus-hooks i funksjonelle komponenter og har to argumenter: callback-funksjon og en array av "overvåkere" eller avhengigheter. Det er to vanlige typer sideeffekter i React-komponenter: de som ikke krever opprydding og de som gjør det. Returfunksjonen som brukes for opprydding ligner på `componentWillUnmount` livssyklusen. `useEffect` er et flott sted å utføre forberedelser, lese fra lagring og sende HTTP-forespørsler. Følgende er eksempler på `useEffect`, og første eksempel starter med en kodebit som ikke har noen avhengigheter:

```jsx
import { useState, useEffect } from 'react'

function TodoList() {
   // Destructure value and dispatcher
   const [todo, setTodos] = useState([])
   const [error, setError] = useState('');
   // Must be Top Level - i.e. Not inside if, for etc.
   useEffect(() => {
      fetchTodos().then(todos => { setTodos(todos) })
      .catch(error => { setError(error.message) })
   }, []) // 0 Dependencies, will only run 1ce.
   ...
}
```

`useEffect`med async/await:

```jsx
useEffect(() => { // Using Async/Await in useEffect

   const getAsync = async () => { // Async function
       try {
          const response = await fetch('https://todos.com/api')
          const todos = await response.json()
          setTodos(todos)
       } catch (error) {
          setError(error.message)
       }
   }

   getAsync() // Execute async function
}, []) // 0 Dependencies - Will run once.
```

`useEffect`med avhengigheter:

```jsx
function App() {

   const [ title, setTitle ] = useState()

   // Runs every time the title changes due to the dependency
   // i.e. whenever the setTitle dispatch is invoked
   useEffect(() => {
      // Cause a side-effect when the title state changes.
      document.title = title
   }, [ title])

   return (
      ...
   )
}
```

`useEffect`med livssykluser:

```jsx
...
useEffect(() => {
   // componentDidMount
   window.addEventListener('mousemove', (event) => {
      // Moving the mouse...
   })

   return () => { // componentWillUnmount
      window.removeEventListener('mousemove', () => {})
   }

}, []) // componentDidUpdate
...
```

En annen viktig funksjon ved Hooks er at de ikke er tilgjengelige i klassekomponenter. Livssykluser for klassekomponenter kan imidlertid bruke livssyklusmetoder som er gitt i stedet, vist i følgende kodebit:

```jsx
class TodoList extends React.Component {

   componentDidMount() {
      window.addEventListener('mousemove', () => {})
   }

   componentDidUpdate() {
      // Component state changed.
   }

   componentWillUnmount() {
      window.removeEventListener("mousemove", () => {});
   }
}
```

`useReducer`er en Hook som kan sammenlignes med `useState`, som er nyttig når tilstanden er et objekt med flere egenskaper. Den trekker ut tilstandsoppdateringslogikk for å skille funksjon og bruker handlingstyper og payload. Det er også mulig å bruke *switch case* syntaks for å bestemme hvilken del av tilstanden som skal oppdateres. Følgende kodebiter beskriver `useReducer` reduksjonsfunksjon og komponent:

```jsx
function todoReducer(state, action) {
   switch (action.type) { // Check action type

      case 'setTodos': // return new version of state
          return {
             ...state,
             fetching: false,
             todos: action.payload
          }

      default: // Return default unchanged
          return state
   }
}
```

```jsx
const initialState = {
   fetching: false
   todos: [],
   error: ''
}
function TodoList() {
   const [state, dispatch] = useReducer(todoReducer, initialState)

   useEffect(() => {
      fetchTodos()
      .then(todos => dispatch({ type: 'setTodos', payload: todos }))
      .catch(error => dispatch({ type: 'setError', payload: error.message}))
   }, [])
   ...
}
```

Props og Events er også grunnleggende konsepter i tilstand- og komponentlivssyklus. Props brukes til å sende informasjon fra Parent til Child-komponenter. Syntaksen til props kan sammenlignes med vanlige HTML-attributter. De kan være statiske verdier (strenger, tall) eller kan være dynamiske verdier fra tilstand, men krever krøllparenteser. Følgende kodebiter er eksempler på props skrevet i både funksjonelle komponenter og klassekomponenter:

```jsx
import TodoListItem from './TodoListItem'
const TodoList = () => {
   const [todos, setTodos] = useState([
      'Learn React',
      'Build a Todo app'
   ])

   /* Pass the first todo item as a prop to the TodoListItem component */
   return (
      <ul>
          <TodoListItem todo={ todo[0] } />
      </ul>
   )
}
```

```jsx
const TodoListItem = (props) => { // Receive props as arg
   // Props is a JavaScript Object -
   // { todo: 'Learn React', children: <Any child components> }
   return (
      <li>{ props.todo }</li> 
   )
}
```

```jsx
class TodoListItem extends React.Component {
   // ...
   render() {
      // Props automatically added to "this"
      // True for all class components
      return (
         <li>{ this.props.todo }</li>
      )
   }
}
```

Events er derimot props som er funksjoner. De brukes til å sende ut informasjon fra Child til Parent-komponenter. Events kan også sende data til Parent-komponenter og derfor kjøres fra Child-komponenten og håndteres i Parent-komponenten. Eksempler på events vises i følgende kodebiter:

```jsx
const TodoListItem = (props) => { // Receive props as arg
    /* Props is a JavaScript Object - 
    props: { todo: 'Learn React', todoClicked: function}

    Bind the props.todoClicked to the onClick event, provide
    the current todo as the argument
    */
    
    return (
       <li onClick={ () => props.todoClicked(props.todo) }>
            { props.todo }
      </li>
    )
}
```

```jsx
const TodoList = () => {
   const [todos, setTodos] = useState([
      'Learn React',
      'Build a Todo app'
   ])

   // Event handler to bind to a prop
   const handleTodoClicked = todo => {
      console.log(todo); // outputs: Learn React
   }

   /* Pass handleTodoClicked as a prop named todoClicked */
   return (
      <ul>
          <TodoListItem todo={ todo[0] } todoClicked={ handleTodoClicked } />
      </ul>
   )

}
```

Et annet viktig konsept knyttet til tilstand og komponentlivssyklus er Context API. Context API er en måte å unngå prop-drilling på, da det muliggjør deling av verdier som ofte ikke endres mellom komponenter. Context API er ikke en erstatning for Redux, og det er ikke et bibliotek for tilstandshåndtering. Eksempler på å lage Context API, legge til tilstand til konteksttilbyderen, legge til kontekst i appen, få tilgang til konteksttilstand, forenkle tilgang for brukerkontekst og mønster for flere kontekster vises henholdsvis i følgende kodebiter:

```jsx
import { createContext, useState } from "React"

// Create a new Context object
export const UserContext = createContext(null)

//File: src/contexts/UserContext.jsx
```

```jsx
import { createContext, useState } from "react";

// Create a new Context object
export const UserContext = createContext(null)

// User Context Provider
const UserProvider = ({ children }) => {
   return (
      <UserContext.Provider>
          { children }
      </UserContext.Provider>
   )
}
export default UserProvider
//File: src/contexts/UserContext.jsx
```

```jsx
...
// User Context Provider
const UserProvider = ({ children }) => {
   // Create state for Context Provider
   const [user, userUser] = useState(null)
   // Attach state to Provider's value
   return (
      <UserContext.Provider value={[ user, setUser ]}>
         { children }
      </UserContext.Provider>
   )
}
export default UserProvider
//File: src/contexts/UserContext.jsx
```

```jsx
import App from './App'
import UserProvider from './context/UserContext'

ReactDOM.render(
   <React.StrictMode>
      <UserProvider>
          <App/>
      </UserProvider>
   </React.StrictMode>,
   document.getElementById('root')
)
//File: src/main.js
```

```jsx
import { useContext } from "react"
import { UserContext } from "../context/UserContext"

const Profile = () => {
   // Access the Context from UserContext.jsx
   // with useContext hook
   const [ user, setUser ] = useContext(UserContext)

   return (
      <main>
          <h1>{ user.username }</h1>
      </main>
   )
}
export default Profile
//File: src/components/Profile.jsx
```

```jsx
import { createContext, useContext, useState } from "react";

// Create a new Context object
export const UserContext = createContent(null)

// Custom Hook to use UserContext
export const useUserContext = () => {
   return useContext(UserContext)
}

...
```

```jsx
import { useUserContext } from "../context/UserContext"

const Profile = () => {
   // Access the Context from UserContext.jsx
   // with custom hook
   const [ user, setUser ] = useUserContext()
   
   ...
```

```jsx
import GuitarProvider from "./GuitarContext"
import UserProvider from "./UserContext"

const AppContext = ({ children }) => {
   // Return nested Providers
   return (
      <UserProvider>
          <GuitarProvider>
              { children }
          </GuitarProvider>
      </UserProvider>
   )
}

export default AppContext
//File: src/contexts/AppContext.jsx
```

```jsx
import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import AppContext from './context/AppContext'

ReactDOM.render(
   <React.StrictMode>
       <AppContext>
          <App/>
       </AppContext>
   </React.StrictMode>
   document.getElementById('root')

)
//File: src/main.js
```

Det er også noen ulemper med å bruke Context API. Det  inkluderer at Context API ikke har feilsøking eller dedikerte utviklingsverktøy. Det er ingen måter å spore tilstandsendringer på underveis. Det er også utfordrende å isolere logikk til "middleware", men komponenter fungerer fortsatt som oppsett og HTTP-forespørsler; og Context API er ikke designet for å håndtere stor/kompleks tilstand, og det er for dette formål bedre å bruke Redux, Redux Toolkit, etc.

## React Router

React Router er et fullt utstyrt klient- og routing-bibliotek på serversiden for React. Den er komponentbasert og er en samling av navigasjonskomponenter. React Router har to hoveddeler: React Router for web (`react-router-dom`) og React Router for native (`react-router-native`). Ved bruk av React for web, `react-router-dom`kan benyttes. Pakken `react-router`er ikke nødvendig for web.

For å installere React Router, bør følgende kjøres i prosjektets rotkatalog:

```
PS C:\Users\Owner\> npm install react-router-dom
```

React Router inneholder tre typer primærkomponenter: Router-komponenter, Route Matching komponenter, og navigasjonskomponenter. Router-komponenter bør være kjernen i enhver React Router-applikasjon. Web `react-router-dom` gir `BrowserRouter` og `HashRouter`, og begge lager et spesialisert historieobjekt. `BrowserRouter` brukes når en server svarer på forespørsler, mens `HashRouter` brukes for en statisk filserver, og den legger også til en # til URL-en. Når det gjelder Router Matching-komponenter, gjøres Routing-matching ved å sammenligne `<Route>` bane-egenskapen med gjeldende steds banenavn. Når banen samsvarer med banenavnet, vil den gjengi den andre komponenten, ellers vil den gjengi null. En `<Route>` uten bane vil alltid matche. `<Routes>` brukes til å gruppere Routes sammen, og den vil iterere gjennom hele Child`<Route>`komponentene og rendre den første `<Route>` som samsvarer med banen. For navigasjonskomponenter, genererer `<Link>` et anker (`<a>`) med href fylt ut. `<NavLink>`er en spesiell type lenke som automatisk vil legge til den "aktive" klassen til gjeldende aktive oute sin relaterte ankertag. Følgende kodebiter viser hvordan du konfigurerer og navigerer routes ved hjelp av React Router:

Importere pakker:

```
import {
   BrowserRouter,
   Routes,
   Route,
   NavLink
} from "react-router-dom"
```

Definere Routes - `App.js`:

```jsx
function App() {
   return (
      <BrowserRouter>
         <div className="App">
           <Routes>
              <Route path="/login" element={<Login />} />
              <Route path="todos" element={<TodoList />} />
           </Routes>
         </div>
      </BrowserRoute>
   );
}
export default App
```

Definere navigasjonslenker:

```jsx
function App() {
   return (
      <BrowserRouter>
        <div className="App">
          <nav>
             <li><NavLink to="/todos">Todos</NavLink></li>
             <li><NavLink to="/profile">Profile</NavLink></li>
          </nav>
          <Routes>
            ...
          </Routes>
       </div>
      </BrowserRouter>
   )
}
export default App
```

Definere route-parametere:

```jsx
function App() {
   return (
      <BrowserRoute>
        <div className="App">
          <nav>
            <NavLink to="/todos">Todos</NavLink>
          </nav>
          <Routes>

            ...
            <Route path="/todos/:todoId" element={<TodoDetail />} />
          </Routes>
        </div>
      </BrowserRoute>
   );
}
export default App;
```

Lese route-parameter - `TodoDetail.jsx`:

```jsx
import { useParams } from 'react-router-dom'

const TodoDetail = () => {

   // Extract the todoId from the route parameter
   const { todoId } = useParams()

   return (
      <main>
          ...
      </main>
   )
}
export default TodoDetail
```

Det er viktig å merke seg følgende Route-attributter: exact, som tvinger banen til å samsvare nøyaktig. path, som er banen til nettlesere. Component, som er komponenten som skal gjengis når banen samsvarer. Dessuten brukes "*" til å håndtere baner som ikke samsvarer med noen av de forhåndsdefinerte ruter, og den er vanligvis plassert helt på slutten av alle ruter:

```
<Route path="*" component={ NotFound } />
```

Og til slutt, et eksempel på Naviger Routes - `TodoList.jsx`er som følger:

```jsx
import { navigate } from 'react-router-dom'

function TodoList() {
   const onTodoItemClick = (id) => {
      navigate(`/todos/${id}`)
   }

   return (
      <ul>
         <li> onClick={() => onTodoItemClick(1)}>Item 1</li>
         ...
      </ul>
   )
}
```

## Konklusjon

For å oppsummere er følgende nøkkelpunkter fra denne leksjonen. Først må React-komponentnavn og filer bruke PascalCase. For det andre er det ingen foreskrevet måte å organisere komponenter på, men det anbefales alltid at en logisk struktur brukes. For det tredje kan gjenbrukbare React-komponenter opprettes ved hjelp av funksjons- eller klassesyntaks, og Higher Order-komponenter kan skrives for å dele logikk mellom urelaterte komponenter. For det fjerde, `onClick` og `onChange` er de vanligste måtene å håndtere brukerinteraksjon på. For det femte er Hooks bare tilgjengelige i funksjonskomponenter og kan brukes til å håndtere lokale tilstander og forårsake sideeffekter. Og for det sjette, React bruker Props. og Events for tilstands- og brukerinteraksjon mellom Parent- og Child-komponenter.

Andre viktige punkter inkluderer: Context API er ikke et statlig bibliotek for tilstandshåndtering; Context API er nyttig for å dele globale verdier som ikke endres ofte; en kontekst kan opprettes med `createContext`-funksjonen; en kontekst har en leverandør som kan "gi" verdier til resten av applikasjonen; Leverandører må pakkes inn på høyeste nivå der konteksttilstand er nødvendig, og flere kontekster kan brukes i en app.

Til slutt, er React Router en tredjepartspakke som må installeres. React Router er komponentbasert og har tre hovedkategorier: Routers, Router matchere og navigasjonskomponenter. Komponenter kan enkelt kobles til en bane ved hjelp av Route Component, og React Router gir enkel navigering mellom baner med navigasjonsfunksjonen eller navigasjonskomponenter (Nav og NavLink).

---

Copyright 2022, Noroff Accelerate AS