# Introduksjon til React

Denne leksjonen dekker det grunnleggende om React-biblioteket.

## Hva er React?

React er et open-source JavaScript-bibliotek opprettet hos Facebook for å utvikle interaktive nettbrukergrensesnitt (UI). Det er for tiden det mest populære front-end-rammeverket, og er egnet for utvikling av små til store applikasjoner.

<aside>
ℹ️ Merk deg at React er uten føringer angående prosjektstrukturen din. Derfor er det utviklerens ansvar å velge en passende arkitektur for prosjektet.

</aside>

React-biblioteket vedlikeholdes av Facebook og et fellesskap av utviklere og selskaper. Du kan også finne et stort sett med ferdige komponenter for å lette utvikling av nettapplikasjoner. React har også mange biblioteker som støtter avanserte konsepter som tilstandshåndtering, ruting, tilgjengelighet, og ytelsesoptimalisering.

### Bakgrunn

Den første versjonen av React er 0.3.0 og den ble utgitt 29. mai 2013. Den siste stabile versjonen av React er 18.0 og den ble utgitt 29. mars 2022.

Ofte vil en større versjon introdusere større endringer, mens en mindre versjon introduserer nye funksjoner uten å bryte den eksisterende funksjonaliteten. Feilrettinger utgis ved behov, og React følger prinsippet for semantisk versjonering.

### Fordeler med React

Det er flere fordeler med å bruke React, og de inkluderer:

1. React er lett å lære.
2. Verktøysettet for å lage React-apper er modent og sørger for testing rett ut av boksen.
3. React er lett å ta i bruk i både eldre og moderne applikasjoner.
4. Et stort antall ferdige komponenter og biblioteker finnes tilgjengelig.
5. React har et stort og aktivt fellesskap.

React gir flere unike fakta som gjør det attraktivt for både nybegynnere og seniorutviklere.

For det første er React uten føringer ved at den gir full kontroll over hvordan komponenter lages og hvordan prosjekter er strukturert.

For det andre er React *komponentbasert* (alt kan være en komponent). Komponenter i React er funksjoner eller klasser som representerer en del av brukergrensesnittet.

For det tredje bruker React JSX (en utvidelse av JavaScript som har en syntaktisk likhet med HTML) for å lage HTML. Dette betyr at JavaScript-kode og brukergrensesnittet er tett koblet sammen. Dette gjør komponenter mer vedlikeholdbare.

Til slutt bruker React et deklarativt paradigme som tillater resonnement om applikasjonsutvikling på en effektiv og fleksibel måte.

## Vi introduserer JSX

JSX er en XML-lignende syntaksutvidelse til JavaScript som brukes til å visualisere Document Object Model-trær (DOM). Målet med JSX er å produsere React-"elementer". Gjengivelseslogikk er derfor nært knyttet til brukergrensesnittet. Dette forenkler hvordan hendelser håndteres, hvordan tilstanden endres over tid, og hvordan dataene presenteres for brukerne.

For å oppnå dette, skiller React bekymringer med løst koblede enheter kalt "Komponenter". Hver komponent inneholder både markup og logikk. Følgende er et eksempel på JSX i en React-komponent kalt `App`.

<aside>
ℹ️ React-komponenter er generelt JavaScript-moduler. De krever en eksporterklæring slik at komponenten kan brukes i andre komponenter.

</aside>

```jsx
import React from `react`;
import logo from `./logo.svg`;
import `./App.css`;

// Function that creates the component
function App() {
    
    // Valid JSX being returned in Function components.
    return {
     <div className="App">
       <header className="App-header">
         <img src={logo} className="App-logo" alt="logo" />
         <p> Edit <code>src/App.js</code> and save to reload.</p>
         <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
           Learn React
         </a>
      </header> 
     </div> 
    };
}

// All components should be modules and should be exported
export default App;
```

I likhet med maler bruker React også enkle krøllparenteser `{variable}`og støtter grunnleggende JavaScript-uttrykk som if, loops og basic operators `(+, -, *, /)`.

## Komponenttyper og roller

React tilbyr to vanlige komponenttyper som kan brukes i applikasjonsutvikling: funksjonelle komponenter og klassekomponenter.

**Funksjonskomponenter** refererer til komponenter som er funksjoner. Selv om funksjonelle komponenter ikke har direkte tilgang til Lifecycle Hooks, krever de fortsatt bruk av Hooks (mer detaljer om Hooks vil bli gitt senere). Generelt er funksjonelle komponenter den foretrukne måten å lage komponenter på.

På den annen side er **klassekomponenter** komponenter som er klasser. De er litt mer komplekse enn de funksjonelle komponentene. Funksjonelle komponenter er ikke klar over andre komponenter i et program, mens klassekomponenter kan fungere med andre komponenter i et program. Klassekomponenter har også direkte tilgang til Lifecycle Hooks, og de kan opprettes ved hjelp av JavaScript ES6-klasser. Implementeringen av henholdsvis en funksjonell komponent og en klassekomponent er som følger:

```jsx
import React from `react`;
import logo from `./logo.svg`;
import `./App.css`;

function App() {
    return (
     <div className="App">
       <header className="App-header">
         <img src={logo} className="App-logo" alt="l
         ogo" />
         <p> Edit <code>src/App.js</code> and save to reload.</p>
         <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
           Learn React
         </a>
      </header> 
     </div> 
    );
}

export default App;
```

```jsx

import React from `react`;
import logo from `./logo.svg`;
import `./App.css`;// Class Components MUST extend React.Component
class App extends React.Component {// Class Components MUST have a render function that returns JSX
  render() {return (
       <div className="App">
          <header className="App-header">
             <img src={logo} className="App-logo"
                  alt="logo" />
             <p> Edit <code>src/App.js</code> and save to reload.</p>
             <a className="App-link" href="http://reactjs.org" target
                 Learn React
                 </a>
          </header>
       </div>
    );
  }
}export default App;
```

I React har komponenter relasjoner med hverandre. En komponent som inneholder andre komponenter blir referert til som *Parent*, mens de individuelle komponentene i overordnet er referert til som *Child*. Komponentene som er nestet sammen, refereres til som *Siblings*. For å utføre en oppgave, må disse komponentene kommunisere med hverandre, og sådan må det være dataflyt mellom dem.

React-komponenter er satt sammen i et hierarki som imiterer DOM-trehierarkiet. Det er de komponentene som er høyere (Parents) og de komponentene som er lavere (Child) i hierarkiet. Retningskommunikasjonen og dataflyten som React muliggjør mellom komponenter er som følger.

1. Fra Parent til Child med Props - dette er den enkleste dataflytretningen, og React bruker en mekanisme som kalles Props for å oppnå det. Imidlertid har denne metoden en ulempe ved at props er uforanderlige, og data som sendes via props bør aldri endres.
2. Fra Child til Parent med Callbacks - denne typen dataflyt bruker en spesiell prop av type-funksjon som overføres til et Child av Parent. Under en relevant hendelse som brukerinteraksjon, kan Child kalle denne funksjonen en callback.
3. Fra Parent til Child med kontekst – denne typen dataflyt lar en temakontekst bli definert, og oppgitt øverst i treet for komponenthierarkiet, og deretter konsumeres den av ethvert Child som trenger det.

React har også en type komponent referert til som en presentasjonskomponent som aksepterer props fra en container-komponent. Hovedoppgaven til presentasjonskomponenter er å gjengi en visning i henhold til stilen og dataene som er sendt dem. De inneholder ingen forretningslogikk og kalles noen ganger dumme komponenter. Container-komponentene spesifiserer på den annen side hvilke datapresentasjonskomponenter som skal gjengi oppførselen deres. Et eksempel på en presentasjonskomponent er gitt i følgende kodebit.

```jsx
const AppHeader = (props) => (
  <header>
     <img src={ props.image } alt={ props.title } />
     <h1>{ props.title }</h1>
     <h4>{ props.subTitle }</h4>
  </header>
)
export default AppHeader
```

## Forberedelser for React-utvikling

React-utvikling bruker CLI-verktøy som er avhengig av node.js og npm; og som et resultat trenger ikke React en lokalt installert CLI. Som alle andre moderne utviklingsplattformer, krever React at Git samarbeider om å koordinere arbeidet mellom teammedlemmer. React tilbyr også flere verktøykjeder som hjelper til med å lage, bygge, kjøre og distribuere applikasjoner. Disse verktøykjedene gir en startprosjektmal med den nødvendige koden for å starte opp applikasjonen. De anbefalte verktøykjedene for React-utvikling er som følger.

1. Create React App – opprett en ny enkeltside-app
2. Next.js - bygge et servergjengitt nettsted med Node.js
3. Gatsby - bygge et statisk innholdsorientert nettsted

## Instansiering av et React-prosjekt

For å instansiere et React-prosjekt, installer npx - en utvidelse av npm. Npx vil tillate programmer å kjøre uten å installere noen globale pakker. Et React-prosjekt kan deretter opprettes med create-react-app som følger.

```
> npx create-react-app my-react-project
```

Det er en kjent bug med npx for Windows-brukere som prøver å lage et React-prosjekt. For å løse dette må brukere med et mellomrom i mappenavnet Users/Brukere opprette en egen npm-cache-mappe, dvs. npm config set cache "C:\Users\YOUR_FIRST_NAME~1\AppData\Roaming\npm-cache" --global. Ytterligere informasjon om denne feilen finner du her: [https://github.com/zkat/npx/issues/146](https://github.com/zkat/npx/issues/146)

For å kjøre prosjektet, bør kommandoen "npm start" kjøres i roten av prosjektet (Bruker som standard localhost:3000). Et nytt React-prosjekt vil ha følgende struktur:

```
./hello-react
├── README.md
├── package-lock.json
├── package.json
├── public
│   ├── favicon.ico
│   ├── index.html
│   ├── logo192.png
│   ├── logo512.png
│   ├── manifest.json
│   └── robots.txt
└── src
    ├── App.css
    ├── App.js
    ├── App.test.js
    ├── index.css
    ├── index.js
    ├── logo.svg
    ├── serviceWorker.js
    └── setupTests.js

2 directories, 17 files
```

Det er lett å legge merke til at React-prosjektet deler den samme konvensjonen med at innholdet i den offentlige mappen kopieres ordrett inn i byggeprosjektet, og innholdet i src tolkes og transpileres til deres respektive byggeartefakter. Mens src er der all kildekoden er skrevet, inneholder public `index.html`, favicon og bilder. Indekssiden - `public/index.html`kan ha en lignende form som det følgende:

```jsx
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset=utf-8" />
    <link rel="icon" href="%PUBLIC_URL%/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1 />
    <meta name="theme-color" content="000000" />
    <meta name="description" content="Web site created using create-react-app" />
    <link rel="apple-touch-icon" href="%PUBLIC_URL%/logo192.png" />
    <link rel="manifest" href="%PUBLIC_URL%/manifest.json" />
    <title>React App</title>
  </head>
  <body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root"></div>
  </body>
</html>
```

Og avhengighetene - `package.json`vil få følgende struktur.

```jsx
"dependencies": {
  "@testing-library/jest-dom": "^5.15.0",
  "@testing-library/react: ^11.2.7",
  "@testing-library/user-event": "12.8.3",
  "react": "^17.0.2",
  "react-dom": "^17.0.2",
  "react-scripts": "4.0.3",
  "web-vitals": "^1.1.2"
}
```

Hovedinngangspunktet for React-applikasjonen er `index.js`. Den gjengir app-komponenten som standard og kan også brukes til å konfigurere app-rutingen. Et eksempel på `index.js`er gitt som følger.

```jsx
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
```

Når det gjelder den første gjengitte komponenten i React-applikasjonen, er den plassert i `App.js`. Denne filen gjengir Child-komponenter og brukes ofte til å administrere ruting. For eldre prosjekter er det behov for å importere React, men det er ikke nødvendig for nye prosjekter. `App.js` tar vanligvis følgende form.

```jsx
import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p> Edit <cod>src/App.js</code> and save to reload.</p>
        <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
```

For å vise data er det flere metoder som kan brukes. Disse metodene inkluderer bruk av objektegenskaper, arrays som bruker `.map()`, betinget rendering og bruk av ternær operator for betinget rendering. De beskrives henholdsvis ved hjelp av følgende kodeutdrag:

Vis data - Objektegenskaper:

```jsx
import './App.css';

const guitar = {
  model: 'Telecaster Thinline 69 Re-issue',
  manufacturer: 'Fender',
  released: '2006',
  construction: 'Maple Neck and Fretboard, Mahogany body'
}

function App() {
  return (
    <div className="App">
      <h1>{ guitar.manufacturer } - { guitar.model }</h1>
      <h2>Released: { guitar.released }</h2>
      <section>
        <h4>Materials</h4>
        <p>Neck: { guitar.construction }</p>
      </section>
    </div>
  );
}

export default App;
```

Vis data - Array som bruker `.map()`:

```jsx
function App() {
  // Map array to JSX elements
  const guitarList = guitars.map((guitar) => {
    return (
      <li key={ guitar.id }>
        <h1>{ guitar.manufacturer } - { guitar.model }</h1>
        <h2>Released: { guitar.released }</h2>
        <section>
        <h4>Materials</h4>
        <p>Neck: { guitar.construction }</p>
        </section>
      </li>
    )
  })

  return (
    <div className="App">
     <ul>
      { guitarList }
     </ul>
    </div>
  );
}

export default App;
```

Vis data – betinget rendering:

```jsx
import './App.css'

function App() {
  // Returns a boolean (true/false)
  const hasGuitars = guitars.length > 0

  // Conditional rendering in React
  return (
    <div className="App">
      { hasGuitars && <p>I have guitars</p> }
    </div>
  )
}

export default App
```

Vis data – Ternær operator for betinget rendering:

```jsx
function App() {
  // Returns a boolean (true/false)
  const hasGuitars = guitars.length > 0

  // Conditional rendering in React
  return (
    <div className="App">
      { hasGuitars ?
        <p>I have guitars! </p> :
        <p>I do not have guitars. </p> }
    </div>
  )
}

export default App
```

## Konklusjon

React gir en forenklet måte å lage enkeltsideapplikasjoner på. Å velge de riktige React-verktøyene vil øke produktiviteten og effektiviteten for applikasjonsutvikling. Det er også viktig å huske at React sin prosjektstruktur er uten føringer, og bruker JSX for å lage dynamiske DOM-komponenter.

---

Copyright 2022, Noroff Accelerate AS