# React tilstandshåndtering med Redux

Redux er et bibliotek som er bransjestandard, og har arkitektur med føringer for å dele data i komponenttreet for applikasjonen din.

## Hvorfor Redux?

### Dele tilstand

Redux prøver i hovedsak å adressere begrensningene til lokal tilstand. Når en applikasjon vokser, blir deling av tilstand mellom urelaterte komponenter utfordrende.

En tilnærming til å dele tilstand mellom ikke-relaterte komponenter er ved å "løfte tilstanden". Dette gjøres ved å flytte enhver tilstand som skal deles til en felles Parent-komponent.

![Figur 1. React komponenttre med delt tilstand](https://noroff-accelerate.gitlab.io/javascript/course-notes/react/img/React_Redux-Component-Tree.png)

Figur 1. React komponenttre med delt tilstand

### Prop-drilling

Ofte må tilstanden flytte opp flere komponenter. Tilstanden må deretter sendes nedover komponenttreet ved hjelp av props. Dette kan gjøre tilstanden vanskelig å spore og administrere over tid.

<aside>
ℹ️ Prop-drilling (også kalt "threading") refererer til prosessen du må gjennom for å få data til deler av React komponenttreet - Kilde: [https://kentcdodds.com/blog/prop-drilling](https://kentcdodds.com/blog/prop-drilling)

</aside>

### Uønsket re-rendering

En annen konsekvens av Prop Drilling er at komponenter som ikke direkte bruker tilstanden blir tvunget til å re-rendre. I figur 1 måtte tilstanden passere gjennom "Home"-komponenten. Hvis Profile eller FavouriteProducts tilstandene endrer seg, har komponentene App, Home, ProfileOverview og FavouriteProducts dermed ikke noe annet valg enn å re-rendre.

Sammenlign det med figur 2 nedenfor. Tilstanden eksisterer i en Redux Store. Siden vi vil isolere hver del av tilstanden i sin egen seksjon, vil bare ProfileOverview-komponenten trenge å re-rendre hvis tilstanden til Profile endres. La komponentene i App, Home og FavouriteProducts være urørt.

![Untitled](React%20tilstandsha%CC%8Andtering%20med%20Redux%2077c89791fca944bbb06757a22c52d91f/Untitled.png)

### Debugging

Selv om React Context API ser ut til å løse noen av de samme utfordringene som Redux, er den begrenset på flere måter.

Den viktigste fordelen for en utvikler som bruker Redux i forhold til Context API er de [kraftige debuggingsverktøyene](https://noroff-accelerate.gitlab.io/javascript/course-notes/react/03_State_Management.html#redux-dev-tools) som Redux tilbyr. Utviklerverktøyene vil bli dekket i mer detalj senere.

## Viktige begreper

For å forstå Redux fullt ut, må noen begreper avklares.

- Store
    - Et sentralt sted hvor dataene til applikasjonen vil bli lagret
- Action
    - Et JavaScript-objekt som beskriver en "kommando" eller "instruksjon" som skal utføres på butikken
- Reducer
    - En JavaScript-funksjon som returnerer en ny "versjon" av en del av tilstanden
- Dispatch
    - En funksjon som sender en handling til Redux store

## Informasjonsflyt i Redux

Flyten til en Redux er alltid i denne rekkefølgen:

1. Send en handling
2. (Valgfritt) Lag synkron eller asynkron sideeffekt med middleware
3. Reducer bestemmer ny tilstand
4. Bruk reducer-endringer på Store
5. Varsle Component om oppdateringer, som skaper en re-rendring

![https://noroff-accelerate.gitlab.io/javascript/course-notes/react/img/React-Redux-Flow-Of-Info.png](https://noroff-accelerate.gitlab.io/javascript/course-notes/react/img/React-Redux-Flow-Of-Info.png)

## Installer og konfigurer Redux

Installasjonen av Redux i en eksisterende React-applikasjon er en relativt smertefri prosess. To hovedpakker må installeres:

### Redux

Redux-pakken er en plattform-agnostisk løsning for global tilstandshåndtering. Dette er ikke unikt for React, men kan til og med brukes i et standard JavaScript-prosjekt.

### React Redux

På den annen side er React Redux et sett med funksjoner og komponenter laget spesielt for React Library. Den gir også hooks for enkelt å få tilgang til din Redux store sitt innhold.

## Installer Redux

### Installer i en eksisterende React-applikasjon

For å installere Redux i en eksisterende applikasjon, må du kjøre kommandoen nedenfor fra rotmappen (hvor `package.json`er plassert).

```
npm install redux react-redux
# Install React Redux into an existing React application
```

### Installer med en ny React-applikasjon

Når du oppretter en ny React-applikasjon, kan du bruke Redux-malen. Det er viktig å merke seg at dette vil bruke [Redux Toolkit](https://noroff-accelerate.gitlab.io/javascript/course-notes/react/03_State_Management.html#redux-toolkit-the-future-of-redux).

```
npx create-react-app my-app --template redux
# Creates a new React app with Redux Toolkit setup
```

## Opprette Redux store

Redux store er der applikasjonsdataene lagres. Det gir en sentral plassering hvor dataene våre kan eksistere, og fjerner ansvaret fra React-komponenter. Ved å bruke funksjonene som tilbys av React Redux, kan store nås fra hvilken som helst React-komponent eller tilpasset hook. Dette gjør det veldig enkelt å få tilgang til tilstanden for applikasjonen din.

### Opprett en ny Redux store

Opprett en redux store ved å bruke `createStore`funksjonen gitt av Redux.

```jsx
// File: src/store.js
import { createStore } from "redux";

const store = createStore();

export default store;
```

### Registrer Redux store

Registrer store i applikasjonen ved å bruke `Provider`komponenten av høyere orden gitt av React Redux.

```jsx
// File: index.js

import { createRoot } from 'react-dom/client';
import App from "./App";
import store from "./store";

const container = document.getElementById("root");
const root = createRoot(container);
root.render(
    <React.StrictMode>
        <Provider store={ store }>
            <App />
        </Provider>
    </React.StrictMode>
);

// Note: Using React 18.x
```

## Opprett handlinger

En handling er et vanlig JavaScript-objekt som beskriver hva som skjedde. Det kan sees på som en instruksjon til Redux og kan bære en payload. Payloaden er tilleggsdataene som kreves for å fullføre handlingsinstruksjonen. Siden det er et JavaScript-objekt, er egenskapsnavnene opp til utvikleren. En generelt akseptert navnekonvensjon er imidlertid å bruke `type`for å beskrive eller merke handlingen og `payload` for å legge ved tilleggsdata til handlingen.

### Handlingsskapere (Action Creators)

Handlinger opprettes ved hjelp av Action Creators. Dette er et navn gitt til en JavaScript-funksjon som ganske enkelt returnerer et JavaScript-objekt med de anbefalte egenskapene, `type` og `payload`.

```jsx
// File: ./components/Counter/counterState.js

// Action Creators
export const increment = () => ({
    type: "[counter] INCREMENT", // Describe the instruction.
});

export const decrement = () => ({
    type: "[counter] DECREMENT",
});

export const boost = (offset = 1) => ({
    type: "[counter] BOOST",
    payload: offset, // Additional data to complete instruction.
})
```

### Definere en handlings-payload

Legg merke til den tredje handlingsskaperen, `boost`. Den godtar et argument, `offset`, som er tilleggsinformasjon nødvendig for å fullføre instruksjonen. Handlingen vil til slutt bli videresendt til Reducer, der `payload`egenskapen er tilgjengelig.

En handlings-payload kan være en hvilken som helst datatype, men det anbefales å holde den så enkel som logisk mulig. For eksempel, hvis du sletter en "Todo" fra et array, bør ikke hele Todo-objektet være nødvendig. Du kan sende kun ID-en til instansen av Todo som må slettes.

## Lag Reducers

### Reducer-funksjoner

Hensikten med en Reducer-funksjon er ganske enkelt å forberede en ny versjon av tilstanden som skal videresendes til butikken. Butikken vil deretter sammenligne den eksisterende tilstanden og ta i bruk de nødvendige endringene.

Alle reducer-funksjoner har to argumenter.

- Argument 1: `state` - Den nåværende versjonen av tilstanden
- Argument 2: `action` **- Handlingen** som utløste reducer-funksjonen

Til slutt må reducer-funksjonen returnere en ny versjon av tilstanden. Eller tilstanden slik den ble mottatt hvis ingenting endret seg.

```jsx
// File: ./components/Counter/counterState.js
export function counterReducer(state, action) {
    // Check which action triggered the Reducer
    switch (action.type) {
        case "[counter] INCREMENT":
            return state + 1;
        case "[counter] DECREMENT":
            return state - 1;
        case "[counter] BOOST":
            return state + action.payload;
        default: // Always return current state if no action matches
            return state;
    }
}
```

Det er vanlig praksis å bruke en switch-erklæring i en reducer-funksjon

### Kombinere Reducere

Siden en React-applikasjon stort sett vil ha flere Reducer-funksjoner, bør den kombineres til en "App Reducer" eller "Root Reducer". Disse navnene brukes om hverandre.

Viktigere, det er også her du "navngir" tilstanden din for store. I eksemplet nedenfor vil `counterReducer` bli tildelt navnet `count` i Redux Store.

```jsx
// File: src/store.js
import { createStore } from "redux";

// Import the Reducers
import { counterReducer } from "./src/components/Counter/counterState.js";

// Combine all Reducer functions
const appReducers = combineReducers({
    count: counterReducer,
    // Additional Reducers added here. 
});

// Add the Combined Reducers to the Store. 
const store = createStore(appReducers);

export default store;
```

Reducers for applikasjonen er nå registrert i store.

## Bruke Redux i komponenter

React Redux tilbyr egendefinerte hooks for å lese og manipulere tilstand i en Redux store. De to vanligste hooks er `useSelector`å lese tilstand og `useDispatch`å sende handlinger.

## Velg tilstand med useSelector

`useSelector`er en hook levert av React Redux og brukes spesielt til å lese tilstand fra en Redux store. Hooken godtar ett argument, en callback som gir tilstanden.

```jsx
import { useSelector } from "react-redux";

function Counter() {
    
    // Select the count from the Redux store. 
    const count = useSelector(state => state.count);

    return (
        <div>
            <h1>Count: { count }</h1>
        </div>
    );
}

export default Counter;
```

### Egendefinerte velgere (Custom Selectors)

Når du skriver om den samme Selector-funksjonen flere ganger, blir det ofte aktuelt å skrive en egendefinert selector.

```jsx
import { useSelector } from "react-redux";

function Counter() {
    
    // Select the count from the Redux store. 
    const count = useSelector(state => state.count);

    return (
        <div>
            <h1>Count: { count }</h1>
        </div>
    );
}

export default Counter;
```

Koden ovenfor vil forenkle valg av tilstand fra forrige kodeeksempel til følgende:

```jsx
import { useSelector } from "react-redux";
import { selectCount } from "./counterState";

function Counter() {
    
    // Select the count from the Redux store with custom selectors. 
    const count = useSelector(selectCount);
    const lessThanFive = useSelector(isLessThanFive);

    return (
        <div>
            <h1>Count: { count }</h1>
            { lessThanFive && <p>Count is less than 5</p> }
        </div>
    );
}

export default Counter;
```

## Utløs handlinger med useDispatch

Dispatch-funksjonen brukes til å "utløse" handlinger til Redux store. Hooken `useDispatch`gir enkel tilgang til disptach-funksjonen i en funksjonskomponent i React. Dispatch-funksjonen godtar en handlingsskaper som et argument. Denne handlingen videresendes deretter til reducer-elementene i applikasjonen din. Alt dette gjøres automatisk av Redux og krever ingen intervensjon fra utvikleren.

Syntaksen for å sende en handling er ganske rett frem. Ta en titt på utdraget nedenfor.

```jsx
// Get the dispatch function
const dispatch = useDispatch();

// Trigger the action using the Action Creator - increment. 
dispatch( increment() );
```

Kodeutdraget nedenfor samler all grunnleggende informasjon som vi har dekket frem til nå.

1. Velg tilstand fra butikken ved hjelp av `useSelector` hooken
2. Få sendingsfunksjonen for å sende handling ved hjelp av `useDispatch` hooken
3. Send handlinger ved hjelp av handlingsskapere

```jsx
const Counter = () => {
    // 1. Get a portion of the state
    const count = useSelector(selectCount);
    
    // 2. Get dispatch function
    const dispatch = useDispatch();

    // 3. Event Handlers that dispatch actions
    const handleIncrement = () => dispatch( increment() );
    const handleDecrement = () => dispatch( decrement() );
    const handleBoost = () => dispatch( boost(5) );

    return (
        <section>
            <h1>Count: {count}</h1>

            {/* Buttons to dispatch actions */}
            <button onClick={ handleIncrement }>Increment</button>
            <button onClick={ handleDecrement }>Decrement</button>
            <button onClick={ handleBoost }>Boost</button>

        </section>
    );
};

export default Counter;
```

## Lag middleware for asynkron kode

Middlware i Redux kan brukes til å forårsake sideeffekter på grunn av tilstandsendringer. Hvis vi tar Counter-appen vår som et eksempel, hver gang "Boost"-handlingen utløses, vil vi gjerne logge hvor mye den ble boostet.

```jsx
// Counter.jsx
import { startBoost } from "./counterState"; // startBoost Action Creator

function Counter() {
    const dispatch = useDispatch();

    return (
        <section>
            <button onClick={ dispatch( startBoost(5) ) }>BOOST</button>
        </section>
    )
}

// counterState.js

export const startBoost = (offset = 1) => ({ // Action Creator with payload
    type: "[counter] START_BOOST",
    payload: offset,
})

// Counter Middleware that logs Boost amount and dispatches Boost Action 
export const counterMiddleware = ({ dispatch }) => (next) => (action) => {
    
    // Send the action tot he Reducer
    next(action);

    // Check which action triggered the middleware
    if (action.type === "[counter] START_BOOST") {
        
        setTimeout(() => { // Simulated Async Code with Timeout
            console.log("[counter] BOOST --- Someone is boosting number with " + action.payload +  "! 🚀 ");
            // Dispatch the actual Boost action after 1 second.
            dispatch( boost(action.payload) );
        }, 1000);
    }
};
```

Som du kan se fra kodebiten ovenfor, er Redux Middleware laget med 3 funksjoner.

- Den første funksjonen gir Redux Store. Store-objektet gir `dispatch`funksjonen og en
    
    `getState` funksjon.,
    
    - `dispatch`: Brukes til å sende handlinger
    - `getState`: Bruk for å få tilgang til tilstanden for Redux store
- Den andre funksjonen gir en next-funksjon. Denne er nødvendig for å videresende handlingen til Reducer. Uten dette vil ikke handlingen nå reducer, og ingen endring ville blitt påført
- Den tredje og siste funksjonen gir den gjeldende handlingen som utløste middlewaren. Denne funksjonen kan også merkes som `async`.

Andre vanlige brukstilfeller for middleware er å ta ansvar for HTTP-forespørsler. Siden hverken handlinger eller reducers skal gjøre asynkrone forespørsler, gir middleware den perfekte muligheten. Det har blitt opprettet mange ekstra biblioteker, spesielt [Redux Thunk](https://github.com/reduxjs/redux-thunk), for å tillate asynkrone handlinger. Imidlertid gir middleware et kraftig alternativ og er en løsning som ikke krever ytterligere avhengigheter.

```jsx
// Assume fetchTodos returns a Promise { todos: [], error: "" }
import { fetchTodos } from "./todoApi";

export const todoMiddleware = ({ dispatch }) => (next) => async (action) => {
    // Send the action tot he Reducer
    next(action);

    // Check which action triggered the middleware
    if (action.type === "[todo] FETCH") {
        
        // Since the last function is async, we can use await
        const { todos, error } = await fetchTodos();

        // Check if request was successful and dispatch next actions. 
        if (!error) { 
            dispatch( setTodos(todos) );
        }
        else {
            dispatch( setTodoError(error) );
        }
    }
};
```

## Redux Dev Tools

Et av de kraftigste verktøyene som hjelper en utvikler som bruker React med Redux, er Redux Dev Tools. Å gi en utvikler muligheten til å se tilstandsendringer i sanntid, spole tilbake og spille av hver hendelse som trigget tilstanden til å endre seg, gjør debugging av en Redux-applikasjon til en virkelig ålreit opplevelse.

Å sette opp Dev Tools for Redux er også enormt enkelt. Siden Dev Tools leveres som en [npm-pakke](https://github.com/reduxjs/redux-devtools) , vil en enkel `npm install`installere avhengighetene som trengs for å bruke Dev Tools.

Følgende kommando bør kjøres fra roten til prosjektet ditt. (Der `package.json`ligger.)

```
npm install --save @redux-devtools/extension
# Installs the Redux Dev Tools
```

Når Dev Tools er installert, er det siste trinnet å integrere Dev Tools i prosjektet ditt. Utvidelsen Dev Tools legges til som middleware i prosjektet ditt. Derfor wrapper den ApplicationMiddleware-funksjonen som er omtalt i Middleware-seksjonen av denne modulen.

```jsx
import { createStore, applyMiddleware } from "redux";
// Import the Dev Tools Middleware function
import { composeWithDevTools } from '@redux-devtools/extension';
// Import the Reducers
import { counterReducer } from "./src/components/Counter/counterState";

// Combine all Reducer functions
const appReducers = combineReducers({
    count: counterReducer,
});

// Add the Combined Reducers to the Store. 
const store = createStore(appReducers,
    // Add Dev Tools as Part of Middleware
    composeWithDevTools(applyMiddleware(counterMiddleware))
);

export default store;
```

Når du har fullført oppsettet, kan du se Redux-handlingene dine og hvordan det påvirker tilstanden direkte i nettleseren.

![https://noroff-accelerate.gitlab.io/javascript/course-notes/react/img/Redux-DevTools-Extension.png](https://noroff-accelerate.gitlab.io/javascript/course-notes/react/img/Redux-DevTools-Extension.png)

## Redux Toolkit - Fremtiden for Redux

Modulen diskuterer React Redux og gir kodeeksempler ved bruk av Actions, Reducers og Middleware. Teamet bak Redux har presset på adopsjon av Redux Toolkit. Den tar sikte på å løse noen av hovedproblemene med React Redux, hovedsakelig mengden boilerplate-kode som kreves for å lage selv en enkel tilstandsløsning.

Hvis du ønsker å lære mer om Redux Toolkit, anbefales det å følge den [offisielle dokumentasjonen](https://redux-toolkit.js.org/).

---

Copyright 2022, Noroff Accelerate AS