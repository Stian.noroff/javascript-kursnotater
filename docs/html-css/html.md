# HTML

# HTML

## Innholdsfortegnelse

- [Komme i gang](about:blank#komme-i-gang-med-html)
- [HTML-attributter](about:blank#html-attributter)
- [Layoutelementer](about:blank#layoutelementer)
- [Innholdselementer](about:blank#innholdselementer)
- [Blokker og innebygde elementer](about:blank#blokker-og-innebygde-elementer)
- [Medieelementer](about:blank#medieelementer)
- [Formelementer](about:blank#formelementer)
- [Tabellelementer](about:blank#bordelementer)
- [HTML-navigasjonssider](about:blank#navigere-i-html-sider)

## Komme i gang med HTML

HTML er den eneste måten å vise innhold på en nettside. Det er derfor viktig å forstå hvordan man skriver dokumenter ved hjelp av HTML.

### Historien om HTML

<aside>
ℹ️ Hva står HTML for?

**H**yper **T**ext **M**arkup **L**anguage

</aside>

I 1980 skrev fysikeren Tim Berners-Lee ENQUIRE, forgjengeren til World Wide Web. Den ble hovedsakelig brukt av forskere ved CERN til å lese og dele dokumenter seg imellom.

I 1990 spesifiserte Berners-Lee imidlertid HTML og skrev en nettleser og serverprogramvare for å gjengi og servere HTML. Første offentlige omtale av HTML var i 1991 og ble referert til som “HTML-tagger”.

### Hva er HTML?

HTML er standard markup-språk for dokumenter som vises i en nettleser.

HTML-markeringen gir ulike byggeklosser for ulike typer elementer, inkludert tekst, bilder, lyd og layouter.

HTML er ment å være et semantisk markup-språk for å beskrive et dokument. Dokumenter skrevet i HTML bruker filtypene `.html`eller `.htm`.

### Standarddokument

Standarddokumentet til et nettsted kalles vanligvis `index.html`. Serveren ser automatisk etter denne filen i roten til nettsidemappen din. Du kan ha html-filer med andre navn, men nettstedet ditt må ha én `index.html`side.

Andre varianter av index.html inkluderer:

- index.html
- index.htm
- default.html
- index.php*
- index.phtml*

<aside>
⚠️ **Merk om språk på serversiden** 
Dokumenter med . php eller .phtml-utvidelser krever en server som Apache eller Nginx for å kjøre serversidekode. Vanlige HTML-dokumenter krever ikke dette og kan kjøres fra en lagringsleverandør som Amazons S3 Buckets.

</aside>

### Andre filer

Hovedfilen i prosjektet skal være `index.html`. Andre sider kan ha et hvilket som helst annet navn du velger for å beskrive formålet.

Eksempelsidenavn kan inkludere: `about.html`, `contact.html`, etc.

### Navnekonvensjoner?

Generelt, på et statisk HTML-nettsted, bør du holde deg til små bokstaver uten mellomrom, men snarere `-`bindestreker for å skille ord.

<aside>
⚠️ i.e. `contact-detail.html` og IKKE `Contact Detail.html`

</aside>

Alle filer med filtypen .html må ha gyldig HTML. dvs. minimum nødvendige HTML-tagger.

### Hva er en Tag?

- HTML er skrevet med HTML “Tags”
- HTML-tagger må pakkes inn i vinkelparenteser `<>`
- Etiketter identifiseres med ett enkelt ord
- Majority Tags trenger en åpnings- og en lukkebrikke
- Noen tagger kan også være selvlukkende

### HTML-tagkomposisjon

![Untitled](HTML%204d38b6781e9040eb8250703de90119a2/Untitled.png)

### Eksempel på HTML-tag

Nedenfor er en avsnittskode med tekst som innhold. Åpnings- og avslutningstaggen må være nøyaktig den samme , i tilfelle en `p`-tagg, tegnet `p`. Den avsluttende taggen må alltid innledes med en skråstrek " `/`".

```html
<p>Hello World!</p>
```

### Selvlukkende tagger

Noen få HTML-tagger kan også være selvlukkende. De trenger ikke en ekstra kode på slutten. Disse taggene kan heller ikke ha noe innhold.

![Komposisjon av HTML-tags (selvlukkende)](HTML%204d38b6781e9040eb8250703de90119a2/Untitled%201.png)

Komposisjon av HTML-tags (selvlukkende)

### Eksempel på selvlukkende tag

I eksemplet nedenfor kan du se et HTML-inndataelement. Inndata er en selvlukkende tag og krever ikke duplisering av input-tag. En selvlukkende tagg må avsluttes med skråstrek og lukkevinkelbrakett `/>`.

```html
<input />
```

### Standard HTML-dokument

HTML-dokument har noen påkrevde tagger. Uten disse kodene er HTML ikke gyldig og kan føre til uventet gjengivelse av nettleseren.

### DOKTYPE

Et HTML-dokument må spesifisere en DOCTYPE, for HTML5 er det ganske enkelt `html`. Dette vil fortelle nettleseren at dokumentet må tolkes som HTML.

```html
<!DOCTYPE html> ...
```

### Root html-tag

Et HTML-dokument krever et par `html`-tags som vil inneholde dokument-markup og metadata.

```html
<!DOCTYPE html>
<html></html>
```

### Head-tag

Innenfor `html-`taggene kan `head`-taggen eller dokumentmetadata-headeren legges til. Metadataene, som dokumenttypen og tegnsettet, legges til i `head-`taggene til dokumentet.

```html
<!DOCTYPE html><html><head></head></html>
```

### Meta-tag

Enkelt sagt brukes `metakoder`for å legge til ekstra informasjon om siden.

La oss ta en titt på tre `metakoder`som du bør gjøre deg kjent med: `charset`, `description`og `viewport`.

**Meta-charset-tag**

Metakoden for tegnsett definerer dokumentets tegnkoding. Den eneste gyldige verdien i HTML5-dokumentet er “UTF-8” eller “utf-8”.

```html
<!DOCTYPE html>
<html>
  <head></head>
</html>
```

<aside>
⚠️ **Plassering av metakoden for `charset`**
Metakoden som definerer tegnsettet må defineres innenfor de første 1024 bytene av dokumentet. Derfor anses det som god praksis å plassere den først inne i head-taggen.
Kilde: [MDN Docs på Charset](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta#attr-charset)

</aside>

**Metabeskrivelse**

Metabeskrivelsen av siden er viktig for søkemotorer siden denne teksten vises på en resultatside under sidens tittel. Det skal være fristende og unikt for hver side på nettstedet.

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8"/>
  </head>
</html>
```

**Meta viewport**

Det synlige området på en side (kjent som viewport) varierer mye på tvers av ulike enheter, og derfor gir meta viewport-taggen `<meta name="viewport" content="width=device-width, initial-scale=1">`en måte å fortelle nettleseren hvordan den skal kontrollere sidens dimensjoner. Å legge til meta viewport-taggen i HTML-dokumentet ditt er veldig viktig for å lage nettsteder som ser bra ut på tvers av enheter.

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8"/>
    <meta name="description" content="The home page to my first ever website."/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
  </head>
</html>
```

### Titteltag

Tittelkoden angir tittelen på dokumentet. Du kan se tittelen vist på nettleserfanen. Det er også tittelen som vises på søkemotorer, og derfor er det viktig å velge klare, unike titler for hver side. På de fleste nettsteder vil tittelen inneholde nettstedsnavnet samt sidenavnet. For eksempel “Bedriftsnavn | Kontakt oss”

Tittelen er ikke nødvendig for at HTML-en skal være gyldig, men det anses som dårlig praksis å utelate tittelen på en side.

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8"/>
    <title>My Website</title>
  </head>
</html>
```

### Body-tag

Alt som er synlig på siden skal gå inn i body-taggen. Hodet er for all ekstra informasjon og styling av siden. HTML-innhold som skal vises på siden må være inne i brødteksten. Det er viktig å legge merke til hvordan elementer er nestet inne i andre tagger i HTML. Dette er et grunnleggende konsept for HTML. For eksempel er en `div`-tag nestet inne i body-taggen som er nestet i en HTML-tag.

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>My Website</title>
  </head>
  <body>
    <!--the webpage content goes here -->
  </body>
</html>
```

## HTML-attributter

### Hva er et HTML-attributt?

HTML-attributter kan gi tilleggsinformasjon om et element. Alle HTML-elementer har attributter.

Attributter er spesifisert i starttaggen, før lukkevinkelparentesen.

Bruker vanligvis nøkkel/verdi-par: f.eks `name="verdi"`. Noen HTML-attributter kan skrives som bare nøkkelen. f.eks `deaktivert`. Verdien for dette attributtet vil som standard være `true`.

```html
<!-- HTML Attribute id with the value of title -->
<h1 id="title">My Great Website</h1>

<!-- HTML Attribute defaults to true -->
<button disabled>Disabled button</button>
```

Noen vanlige HTML-attributter:

[Untitled](https://www.notion.so/7387e215908a44c7a504425f12e2c644)

For en fullstendig liste over html-attributter og for hvilke html-element(er) de gjelder, sjekk ut [MDN-dokumenter](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes)

## Layoutelementer

Her er en liste over de vanligste html-taggene som omhandler utformingen av nettsiden.

- `main`
- `section`
- `aside`
- `header`
- `footer`
- `nav`

![HTML Layout-eksempel](HTML%204d38b6781e9040eb8250703de90119a2/Untitled%202.png)

HTML Layout-eksempel

### Detaljert om Layout-elementer

### Main

```html
<main></main>
```

Main-tagen brukes til å definere hovedinnholdet i en del av `<body>` i et dokument. Dette kan inneholde innhold som avsnitt, bilder og mer. Generelt anbefales det å bare ha én hovedtag per nettside. Hovedtaggen skal ikke inneholde gjentatte elementer på en nettside som sidefelt, logoer, topptekster, bunntekster eller navigasjonsområder.

### Section

```html
<section></section>
```

Section-tagen kan brukes til å gruppere relatert innhold. 

Eksempel: En side kan ha en seksjon med kontaktinformasjon og en seksjon som forteller noe om en bedrift.

```html
<h1>My Company</h1>
<section>
    <h2>What we do</h2>
    <p>My Company is all about sustainability all the whilst making tons of profit</p>
</section>
<section>
    <h2>How to contact us</h2>
    <p>You can contact us on Twitter, Facebook and on live chat</p>
</section>
```

### Aside

```html
<aside></aside>
```

Aside-tagen brukes til å definere `innhold`som er i en sidefelt, noe som ikke distraherer oppmerksomheten fra hovedinnholdet på siden. Et eksempel på dette kan være på en blogg der du har et arkiv med blogginnlegg for de siste 3 månedene, eller det kan også være en sidefeltannonse.

### Header

```html
<header></header>
```

Toppteksten på en side inneholder en introduksjon til siden. Den kan vanligvis inneholde logoen, en overskrift og/eller introduksjon til siden, hovednavigasjonen for nettstedet, samt et overskriftsbilde. Generelt kan `<header>`være en gjentatt seksjon som gjenbrukes på flere sider.

Det er viktig å påpeke forskjellen mellom `<head>`og `<header>`.

- `<head>`brukes for ekstra informasjon om dokumentet .
- `<header>`er introduksjonen til siden.

### Footer

```html
<footer></footer>
```

Bunntekstelementet brukes til å definere innhold som ligger nederst på en nettside.

`footer`-taggen brukes ofte for å legge til følgende:

- Kontaktinformasjon,
- Lenker til sosiale medier,
- Copyright-informasjon,
- Og mer.

### Nav

```html
<nav></nav>
```

Nav-elementet brukes til å definere navigasjonselementene på en side. På de fleste nettsteder vil du sannsynligvis ha ett nav-element, mens lenker i bunnteksten ikke trenger å være i et nav-element. Nav - `taggen`brukes ofte sammen med et uordnet listeelement, som du ser i eksemplet nedenfor.

HTML:

```html
<nav>
  <ul>
    <li>Home</li>
    <li>Store</li>
    <li>About</li>
    <li>Contact</li>
  </ul>
<nav>
```

Et typisk HTML-oppsett av en nettside med alle elementene vi nevnte ovenfor ser slik ut:

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>My Website</title>
  </head>
  <body>
    <header></header>
    <nav></nav>
    <aside></aside>
    <main></main>
    <footer></footer>
  </body>
</html>
```

## Innholdselementer

Her er noen vanlige innholdselementer sammen med en kort beskrivelse av hver:

[Untitled](https://www.notion.so/d67711f334054941b36886a01fc8c656)

Nå skal vi se på hver tag i mer detalj.

### Heading-tags

Heading-tags brukes til å spesifisere titler og undertekster for seksjoner. Det er 6 varianter av overskriftskoden, fra `h1`til `h6`. Standard [skriftvekt](https://developer.mozilla.org/en-US/docs/Web/CSS/font-weight) for overskriftskoden er **fet skrift** . Nummereringen av overskriftstaggene ( `h1`til `h6`) er for å illustrere *betydningen* av følgende innhold og er en del av skriving av [semantisk](https://developer.mozilla.org/en-US/docs/Glossary/Semantics) html.

`h1`-taggen er den viktigste overskriftstaggen og fungerer som den visuelle tittelen for siden, derfor bør hver side ha **en** `h1`tag. Hovedforskjellen mellom `title`tag og `h1`tag er at `h1`er synlig i nettleservinduet og `title`tag ikke - dette på grunn av at `title`tag er plassert innenfor `head`tag i html dokumentet.

HTML:

```html
<h1>A title</h1>
<h2>A sub title</h2>
<h3>Less significant</h3>
<h4>Even less significant</h4>
<h5>Even less significant</h5>
<h6>Least significant heading</h6>
```

Output:

# [A title](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/html.html#a-title)

## [A sub title](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/html.html#a-sub-title)

### [Less significant](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/html.html#less-significant)

### [Even less significant](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/html.html#even-less-significant)

### [Even less significant](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/html.html#even-less-significant-1)

### [Least significant heading](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/html.html#least-significant-heading)

Hvis du vil endre størrelsen på overskriftskodene, bruk CSS for å gjøre det.

<aside>
⚠️ IKKE bruk overskriftskoder for å gjøre tekst større eller mindre. Dette vil ha en negativ innvirkning for brukere av skjermlesere og vil gjøre html-koden din semantisk dårlig.

</aside>

### Fet/strong-tag

`<b>-`eller `<strong>`-tagger legger vekt på tekst.

```html
<b>I am bold!</b> <strong>I am also bold</strong>
```

### Paragraf-tag

Paragraf-tagen brukes til å vise generell tekst. Avsnittstagger vil automatisk bryte til neste linje på grunn av at de er et *blokkelement* (du vil lære hva dette betyr litt lenger ned).

```html
<p>This is a paragraph tag!</p>
<p>This is another paragraph tag!</p>
```

Du kan legge til flere linjeskift i et avsnittselement ved å bruke `< br >`-taggen. Hvis du vil vise et dikt eller sangtekst, er det naturlig å bruke linjeskift.

HTML:

```html
<p>I love HTML, it's so cool.<br> I love HTML, no time for school.<br> That's why I do Accelerate. <br> Give me that job, no time to wait.</p>
```

Output:
I love HTML, it's so cool.
I love HTML, no time for school.
That's why I do Accelerate.
Give me that job, no time to wait.

### Lister

Noen ganger er det nødvendig å organisere tekstinnhold i en slags liste. Det kan være at du må bryte ned et større stykke tekst i nøkkelpunkter, eller en liste over ingredienser til en rett, osv. I html lager du en liste ved å bruke enten `ol`, `ul`og `dl`-taggen. Vi vil se på hver tag i detalj.

Hver av disse listene vil ha en viss standardformatering, for eksempel bruk av prikker, tall eller innrykk for underordnede elementer. Du kan bruke CSS for å fjerne standardformatering om nødvendig.

### Ordnet liste

Den ordnede listen vises med en nummerering som starter på 1.

- `ol`– ordnet liste

HTML:

```html
<ol>
  <li>Item 1</li>
  <li>Item 2</li>
  <li>Item 3</li>
</ol>
```

Output:

1. Item 1
2. Item 2
3. Item 3

### Uordnet liste

Den uordnede listen vil som standard bruke prikker for hvert listeelement.

- `ul`– Uordnet liste

HTML:

```html
<ul>
  <li>Item 1</li>
  <li>Item 2</li>
  <li>Item 3</li>
</ul>
```

Output:

- Item 1
- Item 2
- Item 3

<aside>
ℹ️ Direct Children til listeelementene må være et `li`-element.

</aside>

### Beskrivenede lister

### dl – Beskrivende liste

`dl`-tagen eren forkortelse for beskrivelsesliste og brukes til å lage en liste der underordnede elementer er rykket inn i stedet for å bruke tall eller punkt.

En viktig forskjell du kanskje har lagt merke til er at `dl`-taggen har to forskjellige underordnede tagger, i motsetning til `ul`og `ol`som bare har en, `li`-taggen. Underordnede elementer skal være en `dt-`eller `dd`-tag.

```html
<dl>
  <dt>Lord of The Rings</dt>
  <dd>Adventure/Fantasy</dd>
  <dt>The Avengers</dt>
  <dd>Action/Superheroes</dd>
</dl>
```

Output:

Lord of The Rings

Adventure/Fantasy

The Avengers

Action/Superheroes

### dt – Beskrivelse Listetittel

Som du ser i html-utdataene, er ikke tekstinnholdet i `<dt>`-elementet rykket inn og er ment å brukes for navnet/tittelen/termen som du deretter vil beskrive videre ved å bruke `dd`-taggen.

### dd – Beskrivelse Liste Beskrivelse

`dd`-taggen vil som standard bli rykket inn, som er en viktig forskjell mellom beskrivelseslister og uordnede/ordnede lister når det gjelder standardformatering .

## Blokker og innebygde elementer

[Untitled](https://www.notion.so/e8b4ec87666641489f5ed04ce08c387d)

Hvert HTML-element er som standard enten et [blokk-](https://developer.mozilla.org/en-US/docs/Web/HTML/Block-level_elements) eller [inline-](https://developer.mozilla.org/en-US/docs/Web/HTML/Inline_elements) element. Hvis du ser på bildet nedenfor , vil du se at blokkelementene strekker seg hele veien til slutten av den overordnede beholderen, og tvinger det neste elementet til en ny linje. Inline-elementet tar like mye plass som innholdet inne i det. Hvis du angir bakgrunnsfargen til et HTML-element, kan du enkelt se om det er et inline-element eller et blokkelement.

HTML:

```html
<div class=container>
  <div class="block-elements">
    <h2>Block Elements:</h2>
    <h1>heading element</h1>
    <p>paragraph element</p>
    <ul>
      <li>list element</li>
      <li>list element</li>
      <li>list element</li>
    </ul>
  </div>
   <div class="inline-elements">
    <h2>Inline Elements:</h2>
    <a href="https://developer.mozilla.org/en-US/docs/Web/HTML/Inline_elements">Link to read more about inline elements</a>
    <button>This button is another <i>inline</i> element</button>
    <h3>h2 element changed from being a <i>block</i> element to <i>inline</i> with CSS</h3>
  </div>
</div>
```

Output inkludert CSS-styling:

![Block- og inline-elementer](HTML%204d38b6781e9040eb8250703de90119a2/Untitled%203.png)

Block- og inline-elementer

> **NB**: Du kan endre standardstilen til et blokkelement slik at det vises som et innebygd element.
> 

### Eksempler på blokkelementer

- `<div>`
- `<p>`
- `<pre>`
- `<hr>`
- `<blockquote>`
- `<adresse>`
- `<h1>`til `<h6>`
- List tagger

## Inline-elementer

- `<button>`
- `<a>`
- `<span>`
- `<i>`
- `<b>`, `<strong>`
- `<small>`
- og mer…

## Medieelementer

Medieelementer lar deg bygge inn innhold som bilder, videoer og lyd på en nettside.

For å legge inn et bilde til en HTML-side bruker du `< img >`-taggen. I eksemplet nedenfor har vi brukt tre html - [attributter som](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes) er viktige for `< img >`-elementet: `< src >`, `<alt>`og `<width>`(valgfritt).

[Untitled](https://www.notion.so/d35a2eba5add458ba73dec2560738b30)

HTML:

```html
<img src="./image-directory/cat-image.jpg" alt="fat cat with ps4 controller" width="960" height="1280">
```

> **NB** : Bruk CSS for å endre størrelse på bilder, ikke bruk html-attributter for dette.
> 

Output:

![*Merk: denne bildestørrelsen er redusert ved å bruke width-attributten på grunn av leksjonen er skrevet i Markdown, men dette er ikke noe du generelt bør gjøre.*](HTML%204d38b6781e9040eb8250703de90119a2/Untitled%204.png)

*Merk: denne bildestørrelsen er redusert ved å bruke width-attributten på grunn av leksjonen er skrevet i Markdown, men dette er ikke noe du generelt bør gjøre.*

Hvis du roter til url-en for `src`-attributtet, vil ikke bildet lastes, men brukeren vil fortsatt se `alt`-teksten, noe som er nyttig. En annen fordel med å inkludere `alt`-taggen er at den muliggjør bruk av tekst-til-tale-verktøy for å beskrive bildene for synshemmede brukere. Dette er viktig i [tilgjengelig webdesign](https://developer.mozilla.org/en-US/docs/Learn/Accessibility/What_is_accessibility) .

```html
<img src="./image-directory/cat-image" alt="fat cat with ps4 controller" width="960" height="1280">
```

Output:

fat cat with ps4 controller

Andre populære medie-tags:

- `<object>`
- `<video>`
- `<source>`
- `<audio>`
- `<picture>`
- `<track>`

## Form-elementer

### Vanlige bruksområder for skjemaer

- Ta opp informasjon
- Send informasjon til serveren din
- Bruk for pålogging
- Kontaktforespørsler

### Vanlige formelementer

De vanlige elementene som brukes til å lage skjemaer i HTML-dokumenter er oppført nedenfor:

[Untitled](https://www.notion.so/f32db427baf645029d7dfd8b2e5668d1)

### `form`

Skjemaelementet `er`et beholderelement for et sett med inndata, etiketter og en send-knapp. Det kan være mer enn ett skjemaelement per side. Et skjemaelement krever også en `handling`og `metode`som definerer **hvor** ( `handling`) skal sende inndataene og **hvordan** ( `metode`) skal sendes.

### `input`

Inndataelementet har flere [inputtyper](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input) [og](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input) brukes til å fange opp informasjon fra brukeren.

### `label`

En etikett er knyttet til et input-element ved å bruke `for -`attributtet, og det brukes ofte som et beskrivende tekstelement for et `input -`element.

HTML eksempel:

```html
<label for="first-name">Inntastingstekst</label><input type="text" id="first-name" name="first-name" placeholder="...min. 8 tegn">
```

### `textarea`

Et tekstområdeelement brukes til å fange opp flerlinjers tekst fra brukeren. Et eksempel kan være en meldingsdel på en nettsides kontaktskjema.

### `select`

Select-elementet brukes til å gi forhåndsdefinerte alternativer til brukeren å velge mellom. option-taggen brukes til å lage en liste over alternativer å velge mellom og har `select -`elementet som sitt overordnede element.

```html
<select name="options" id="options">
  <option value="option-1">Option 1</option>
  <option value="option-2">Option 2</option>
  <option value="option-3">Option 3</option>
  <option value="option-4">Option 4</option>
</select>
```

Output:

![Untitled](HTML%204d38b6781e9040eb8250703de90119a2/Untitled%205.png)

### Form og Input attributter

Her er noen vanlige html-attributter som brukes i skjemaelementer.

[Untitled](https://www.notion.so/53b3c3dce7a64b13a5a3201d9a839036)

### Inndatatyper

Inndataelementer krever at typeattributtet spesifiseres. De vanligste typene finner du nedenfor:

[Untitled](https://www.notion.so/c8d64db353174f31988ddbe075e6b546)

På bildet nedenfor kan du se hvordan noen få forskjellige inndatatyper vises i nettleseren. Merk: Noe styling er brukt på skjemaet.

![Untitled](HTML%204d38b6781e9040eb8250703de90119a2/Untitled%206.png)

Nedenfor har du html for bildet over. Noe å merke seg med alternativknapper er at for å bare velge ett alternativ om gangen, må alle de relaterte knappene ha samme verdi for `name`-attributtet. En annen ting er at hvis du vil ha en alternativknapp eller en avmerkingsboks som standard, legger du bare til det `checked`-attributtet.

HTML:

```html
<form action="" method="">
  <label for="first-name">Your name:</label><input type="text" id="first-name" name="first-name" placeholder="...min. 8 characters">
  <label for="password">Password:</label><input type="password" id="password" name="password" placeholder="...">
  <label for="message">Your message:</label>
  <textarea id="message" name="message" rows="4" cols="50" placeholder="write here...">
  </textarea>
  <div>
    <p>Radio Buttons</p>
    <label for="button-1">button 1</label><input type=radio name="radio-buttons" id=button-1>
    <label for="button-2">button 2</label><input type=radio name="radio-buttons" id=button-2 checked>
    <label for="button-3">button 3</label><input type=radio name="radio-buttons" id=button-3>
  </div>
  <div>
    <p>Select box</p>
    <select name="options" id="options">
      <option value="option-1">Option 1</option>
      <option value="option-2">Option 2</option>
      <option value="option-3">Option 3</option>
      <option value="option-4">Option 4</option>
    </select>
  </div>## ms' input values.
```

### Form metoder

Et skjemaelement har to skjema “metoder” som endrer hvordan det sender data til en server.

### `GET-`metoden

Å sette skjemametoden til “GET” vil føre til at verdiene til innganger med et navneattributt sendes til serveren som spørringsparametere. Dette betyr at verdiene og navnene til alle underordnede input-elementer vil bli eksponert i URL -en til HTTP-forespørselen.

<aside>
⚠️ **Bruk av GET for passord**
Det er viktig å huske at du ALDRI skal bruke GETnår du arbeider med passordfelt i et skjema. Passordet vil bli eksponert i URL-en og angripere kan enkelt se råteksten til en forespørsels URL.

</aside>

- Dette vil bli dekket mer detaljert senere
- Skjemaer sendes med POST- eller GET-metoder
- ALDRI bruk GET for pålogging eller annen informasjon med sensitive data
- Bruk POST for påloggingsskjemaer

### `POST`metode

`POST`-metoden brukes til å sende inn skjemadata som hovedinnholdet i en HTTP-forespørsel .

## Tabellementer

![Untitled](HTML%204d38b6781e9040eb8250703de90119a2/Untitled%207.png)

*HTML-tabell gjengitt på skjermen*

Tabellelementet brukes til å vise data i et tabellformat med rader og kolonner. `Table`-elementet er ikke ment for sidelayoutformål, men for å vise data på en ryddig måte. Dataformatet vil se ut som i et Excel-dokument.

> NB: IKKE bruk til layout da det kan bli rotete hvis tabellen vokser
> 

### Tabell-elementer

[Untitled](https://www.notion.so/c251db83ddb34719ad2ded82baac110d)

Vi skal se på en enkel tabell. Klasseattributtet `brukes`her da det er mye lettere å målrette de spesifikke elementene med CSS. HTML-markeringen består av en `tabellbeholder`med en `annonse`som inneholder overskriften til tabellen. Hvert `tr -`element har tre `td`underordnede elementer som inneholder data for “fornavn”, “etternavn” og “e-post”. Du kan enkelt se fra utdataene at vi har å gjøre med tre kolonner, så for å få `annonsen`til samme lengde som antall kolonner setter vi verdien til `colspan -`attributtet til “3”.

HTML:

```html
<table>
  <thead>
    <tr>
      <th colspan="3">The table header</th>
    </tr>
  </thead>
  <tr class="data-heading">
    <th>First name</th>
    <th>Last name</th>
    <th>Email</th>
  </tr>
  <tr class="data-row">
    <td>John</td>
    <td>Doe</td>
    <td>john.doe@example.com</td>
  </tr>
  <tr class="data-row">
    <td>Jane</td>
    <td>Doe</td>
    <td>jane.doe@example.com</td>
  </tr>
</table>
```

Output:

![Untitled](HTML%204d38b6781e9040eb8250703de90119a2/Untitled%208.png)

## Navigere i HTML-sider

### Koble til sider med ankertaggen

For å endre gjeldende HTML-side som vises kan du bruke `a`-taggen. Når du navigerer mellom lokale html-sider på din egen nettside, vil du bruke `a`-elementet til dette.

Noen av brukssakene for `a`-elementet, fra MDN-dokumentene:

> `<a>` HTML -elementet (eller ankerelementet), med href -attributtet, oppretter en hyperkobling til nettsider, filer, e-postadresser, steder på samme side eller noe annet en URL kan adressere. MDN-dokumenter
> 

Det viktigste attributtet knyttet til `a`-elementet er `href-`attributtet, som tar en URL som verdi.

Dette er den grunnleggende strukturen til `a`-tag :

```html
<a href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/a" target="_blank">Link to MDN docs</a>
```

Output:

Link til MDN-dokumenter

Målattributtet er valgfritt, men hvis du vil at siden skal åpne i en ny fane, kan du sette `målet til "_blank"`. Som standard vil plasseringen endres innenfor samme fane hvis du utelater å bruke `målattributtet`. Du kan eksplisitt angi `target -`attributtet til `"_self"`, som vil gjøre det samme.

ganske enkelt å koble til dine egne html-filer .

```html
<nav>
  <ul>
    <li><a href="index.html">Home</a></li>
    <li><a href="store.html">Store</a></li>
    <li><a href="about.html">About Us</a></li>
    <li><a href="contact.html">Contact Us</a></li>
  </ul>
<nav>
```

Output:

• [Home](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/index.html)
• [Store](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/store.html)
• [About Us](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/about.html)
• [Contact Us](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/contact.html)

> NB: Du vil bruke CSS for å fjerne punktpunkter og endre `li`-elementene fra å være et `block`-element til `inline`, for å få dem på samme linje i stedet for stablet.
> 

Hvis du klikker på lenkene, vil du se at den omdirigerer deg til en side som ikke eksisterer, og grunnen er nettopp det: den eksisterer ikke siden vi ikke har laget den ennå.

### “Link” innhold ved hjelp av en ID

Når du har å gjøre med nettsider som inneholder for eksempel produkter eller innlegg, er det praktisk å kunne gjenbruke den samme html-filen for hvert produkt, i stedet for å ha hundrevis av individuelle html-filer for hvert produkt. Vi oppnår dette ved å bruke ID-en inne i url -en , som vist nedenfor. Vi bruker JavaScript for å hente data fra en server og finne produktet med ID 1, og produktdataene til dette produktet vil vises. Hvis vi endrer id-en i lenken til “2”, vil et annet produkt dukke opp, men fortsatt bruke samme html-side.

```html
<a href="products.html?id=1">Shampoo</a>
```

```html
<a href="products.html?id=2">Handsoap</a>
```

Senere i dette kurset vil vi ta en detaljert titt på hvordan du kobler innhold ved hjelp av en ID.

![Untitled](HTML%204d38b6781e9040eb8250703de90119a2/Untitled%209.png)

Lenking av sider med anchor-taggen

![Untitled](HTML%204d38b6781e9040eb8250703de90119a2/Untitled%2010.png)

<aside>
ℹ️ **Mer informasjon om HTML elementer.** 
På grunn av det store antallet HTML-elementer, kan det ikke diskuteres i detalj her. Bruk Mozilla Developer Network som en kilde for mer informasjon om HTML-elementer, deres tiltenkte bruk og standard stil.
[Mozilla Developer Network: Elements](https://developer.mozilla.org/en-US/docs/Web/HTML/Element)

</aside>

Copyright 2022, Noroff Accelerate AS