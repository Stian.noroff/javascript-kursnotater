# CSS

# CSS

## Komme i gang med CSS

Etter å ha lært om HTML kan du gå videre til å lære om CSS. HTML i seg selv er ikke det mest tiltalende grensesnittet å se på. Nettlesere legger til noen "basisstiler" kalt User Agent Styles. "User agent" refererer til nettleseren som brukes. Noen av brukeragentstilene kan variere noe fra nettleser til nettleser.

> ℹ️ CSS står for Cascading Style Sheets
> 

### Hva kan du gjøre med CSS?

- Endre farger på tekst, bakgrunner, kanter og alle andre HTML-elementer.
- Formater tekst for å ha forskjellige størrelser, tykkelser, skriftfamilier og mye mer.
- Endre størrelsen på elementer for å ha spesifikke eller "responsive" størrelser.
- Plasser og omorganiser elementene som skal plasseres på et bestemt sted.
- Animasjoner kan legges til nesten alle HTML-elementer ved hjelp av CSS.
- Og noen skikkelig sprø ting: [https://github.com/cyanharlow/purecss-francine](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://github.com/cyanharlow/purecss-francine)

### Se en nettside med og uten CSS

![Populær søkemotor - DuckDuckGo med CSS](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/duckduckgo-with-css.png)

Populær søkemotor - DuckDuckGo med CSS

![Populær søkemotor - DuckDuckGo *uten* CSS](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/duckduckgo-without-css.png)

Populær søkemotor - DuckDuckGo *uten* CSS

### Skrive CSS - Inline

CSS kan skrives flere steder i HTML-en din. Det første alternativet du vil se på er å skrive CSS direkte på HTML-tagger som et attributt. Dette kalles `inline styles`.

```css
<p style="background-color: red"></p>
```

Ovenfor kan du se et eksempel på hvordan du gjør bakgrunnen til et avsnitt rød. Du kan også legge til flere stilegenskaper.

```css
<body>
    <h1 style="font-family: sans-serif;">Welcome to CSS</h1>
    <p style="background-color:red; color: white;">Hello CSS</p>
</body>
```

Over kan du se avsnittets bakgrunnsfarge er satt til rød og tekstfargen er satt til hvit. Legg merke til tillegget av semikolon `;`for å skille egenskapene og verdiene deres.

### Unngå inline stiler

Å bruke inline stiler frarådes sterkt siden det begrenser gjenbrukbarheten til CSS. Egenskapene angitt på h1- og p-taggen ovenfor kan ikke gjenbrukes.

```css
<body>
    <h1 style="font-family: sans-serif;">Welcome to CSS</h1>
    <p style="background-color:red; color: white;">Hello CSS</p>

    <h1 style="font-family: sans-serif;">Goodbye to CSS</h1>
    <p style="background-color:red; color: white;">Goodbye CSS</p>
</body>
```

Som du kan se fra eksemplet ovenfor, må stilene skrives om for hver HTML-tag. Dette bryter en av kodings-"reglene" DRY

<aside>
ℹ️ DRY = Don’t repeat yourself

</aside>

Å vedlikeholde et prosjekt med inline stil krever flere endringer i mange dokumenter hver gang en endring må gjøres. Gjentakelse av samme kode anses å være dårlig praksis.

### Skrive CSS i dokumentets `[head](https://noroff--accelerate-gitlab-io.translate.goog/javascript/course-notes/html-css/css.html?_x_tr_sl=en&_x_tr_tl=no&_x_tr_hl=no&_x_tr_pto=wapp#writing-css-in-the-document-head)`

Et alternativ til å skrive innebygde stiler er å lage en ny `style`kode i taggen til dokumentet.

Dette vil tillate deg å definere gjenbrukbare klasser, målrette spesifikke elementer eller id-er og målrette HTML-attributter basert på verdiene deres.

```html

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Hello CSS</title>
        <!-- Creating styles in the head -->
        <style>
            h1 {
                background: blue;
                color: white;
            }
            p {
                background: green;
                color: white;
            }
        </style>
    </head>
    <body>
        <h1>Hello CSS</h1>
        <p>Welcome to styling</p>
    </body>
</html>

```

Eksempelet ovenfor vil målrette mot *alle* `h1` og `p`tagger i hele dokumentet. Derfor trenger du ikke gjenta stylingen flere ganger.

CSS skrevet i `style`taggen er betydelig mer vedlikeholdbar enn inline-styling.

Anta imidlertid at nettstedet vårt har en `index.html`, `contact.html`og `about.html`-sider. Hvis vi trenger å bruke samme stil på hver av disse sidene, må vi duplisere stilene i hvert dokument.

<aside>
ℹ️ Det er ingen måte for html-dokumenter å dele CSS skrevet med tags i `head`.

</aside>

### Skrive CSS i eksterne `.css`filer

Til slutt, den anbefalte måten å skrive CSS for nettsteder med flere sider er å bruke eksterne filer som vi kobler til HTML-dokumentet vårt.

Dette lar oss fjerne styling fra HTML-dokumentet og gjenbruke stilene på tvers av alle koblede html-filer. CSS er enklere å vedlikeholde og endring av stiler vil bare skje på ett sentralt sted.

I tillegg til å øke vedlikeholdsvennligheten til nettstedet ditt, åpner det også døren for forprosessorer (mer om dette senere.)

Koblede CSS-filer må inkluderes i taggen til HTML-dokumentet. Kobling av et CSS-dokument i HTML er veldig enkelt. Bruk `<link href="" rel="stylesheet" />`syntaks.

<aside>
ℹ️ Legg merke til `rel="stylesheet"` attributtet. Dette vil be nettleseren å tolke dette koblede dokumentet som en CSS-fil og ikke HTML, JavaScript eller et bilde.

</aside>

Du kan legge til flere CSS-filer ved å lage flere lenkekoder i hodet ditt. Det er viktig å merke seg at CSS-filer lastes synkront. Derfor vil for mange filer blokkere gjengivelsen av HTML på grunn av hvordan nettleseren analyserer et HTML-dokument. (Fra topp til bunn).

<aside>
⚠️ Å koble for mange CSS-filer kan blokkere gjengivelsen av HTML. Dette vil la brukerne vente på at nettsiden din skal lastes og føre til et fall i brukeroppbevaring.

</aside>

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Hello CSS</title>
        <!-- Include the styles.css file from the css folder. -->
        <link rel="stylesheet" href="css/styles.css">
        
        <!-- Include the other-styles.css file from the css folder. -->
        <link rel="stylesheet" href="css/other-styles.css">
    </head>
    <body>
        <h1>Hello CSS</h1>
        <p>Welcome to styling</p>
    </body>
</html>
```

Ovenfor kan du se HTML-dokumentet inneholder to CSS-filer. Hver fil krever at en egen `link`kode spesifiseres.

```css

/* CSS written in the styles.css file */
h1 {
    background: blue;
    color: white;
}p {
    background: green;
    color: white;
}
```

Ovenfor kan du se innholdet i styles.css-filen. Det er ingen tillegg av stiltaggene.

### Kommentere i CSS

Å skrive kommentarer i CSS starter med `/*`og slutter med `*/`tegnet. Dette kan gjøres i `<head>`taggen og i `.css`filen.

<aside>
ℹ️ Stiler skrevet i et `.css` dokument krever ikke at en `style` tag legges til.

</aside>

### Skrive- og CSS-velgere

Før vi fortsetter, la oss ta en rask titt på strukturen til en CSS-setning.

![Strukturen til en CSS-setning.](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/css-selector-structure.png)

Strukturen til en CSS-setning.

Det er mange måter å målrette HTML-tagger på med CSS. Vi bruker Selectors for å spesifisere hvilke tagger som skal ha hvilke stiler som skal brukes. Du finner tre hovedtyper velgere i CSS.

### Velgere (selector)

CSS-velgere målretter mot spesifikke HTML-elementer for å bruke stilene til den velgeren. En velger inneholder egenskaper hver med sine egne verdier.

### Velgeregenskaper

Egenskapen retter seg mot en egenskap ved elementet som skal styles. Noen av de vanligste egenskapene som er målrettet inkluderer skriftstiler, farger og rammer. Det er mange flere.

### Velger egenskapssverdier

Egenskapsverdier er veldig spesifikke avhengig av egenskapen som endres. Verdiene kan være forhåndsdefinerte strengverdier, tall eller enheter som piksler, em, rem eller en prosentandel.

```css

p {
    color: white;
    border-width: 2px;
    border-style: solid;
    border-color: yellow;
    background-color: black;
}
```

I eksemplet ovenfor kan du se at fem forskjellige egenskaper blir justert for `p`taggen. Du kan legge til så mange egenskaper du trenger for å oppnå ønsket resultat.

### Kobling av CSS-verdier

Noen eiendomsverdier kan lenkes for å spare plass og i noen tilfeller gjøre CSS mer lesbar. Ta eksemplet ovenfor. Det kan forenkles til følgende:

```css

p {
    color: white;
    border: 2px solid yellow;
    background-color: black;
}
```

Kjedingen av grenseeiendommene har gitt en betydelig besparelse. Når du arbeider med store dokumenter, kan de 2 eller 3 linjene som fjernes ved kjeding ofte utgjøre en massiv reduksjon i kode.

<aside>
🗒️ Hvor mange CSS-egenskaper er det?

I følge W3C er det **546** unike CSS-egenskaper.

Kilde: [https://www.w3.org/Style/CSS/all-properties.en.html](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://www.w3.org/Style/CSS/all-properties.en.html)

</aside>

### `<tag>`Velger

Den første velgeren vi kan snakke om er tagvelgeren. Det er ganske enkelt å bruke. Bare skriv tagnavnet du planlegger å målrette mot. Stilene spesifisert i velgerens kodeblokk vil bruke den stilen på **alle** taggene i dokumentet som samsvarer med den velgeren. Med andre ord vil målretting av en `div`tag påvirke alle `div`taggene i det gjeldende dokumentet.

Syntaksen bør være kjent siden eksemplene ovenfor alle brukte tagvelgeren.

```css

div {
    background-color: #efefef;
    font-weight: bold;
    margin-bottom: 1.5rem;
    color: #333;
    border: 1px solid #333;
}
```

<aside>
⚠️ Husk at bruk av tag-velgeren vil målrette mot alle taggene som samsvarer med den velgeren.

</aside>

### `[#id`Velger](https://noroff--accelerate-gitlab-io.translate.goog/javascript/course-notes/html-css/css.html?_x_tr_sl=en&_x_tr_tl=no&_x_tr_hl=no&_x_tr_pto=wapp#id-selector)

Den neste velgeren, `id`velgeren vil målrette mot et element basert på dets `id`attributt. Velgeren `id`identifiseres ved hjelp av et hash- `#`symbol ( ). Det eneste forbeholdet med å bruke `id`velgeren er at `id`den må være unik i et HTML-dokument. Derfor kan en `id`velger kun målrette mot ett spesifikt element eller gruppe av elementer.

Det frarådes å bruke `id`velgeren til å bruke stiler fordi det skaper en veldig tett koblet stil som aldri kan gjenbrukes.

```html

<!-- A snippet from an HTML form -->
<input id="firstName" placeholder="Enter your first name" />
<input id="lastName" placeholder="Enter your last name" />

```

I kodebiten ovenfor har vi en inngang med `id`av`firstName`

```css

#firstName {
    font-size: 1rem;
    padding: 0.5rem;
    border: 1px solid #ccc;
    border-radius: .25rem;
}
```

Kodebiten ovenfor vil bruke stilene på fornavn-inngangen og bare fornavn-inngangen. Selv om `firstName`og `lastName`begge er HTML-innganger, deler de ikke det samme `id`, derfor kan `firstName`ikke CSS-en brukt på gjenbrukes.

<aside>
⚠️ `id`-Velgeren vil i de fleste tilfeller overskrive stilene til en tag og klassevelger.

</aside>

### `[.class](https://noroff--accelerate-gitlab-io.translate.goog/javascript/course-notes/html-css/css.html?_x_tr_sl=en&_x_tr_tl=no&_x_tr_hl=no&_x_tr_pto=wapp#class-selector)`velger

Den siste velgeren vil målrette mot et HTML-element basert på elementets klasseattributtverdi. Den viktigste fordelen over tag- og id-velgeren ligger i gjenbrukbarheten til klassevelgeren. Klasseverdien kan gjentas i et HTML-dokument. Tagger kan derfor dele nøyaktig samme klasseverdi. I din CSS er klassevelgeren identifisert med dot ( `.`).

```html

<!-- The "box" class is repeated -->
<ul id="boxes" class="boxes">
    <li class="box">
        ...</li>
    <li class="box">
        ...</li>
    <li class="box">
        ...</li>
    <li class="not-a-box"></li>
</ul>

```

I eksemplet ovenfor kan du se at "boks"-klassen gjentas flere ganger. CSS kan målrette mot denne klassen for å bruke stil på alle elementer med "boks"-klassen.

```css

.box {
    box-shadow: 0 0 5px rgba(0,0,0,0.25);
    padding: 1rem;
    border-radius: 0.5rem;
}
```

Det er veldig viktig å merke seg at alle `li`elementene som har `box`klassen " " brukt vil dele samme stil. Men klassen med `li`" `not-a-box`" blir fullstendig ignorert.

<aside>
ℹ️ Merk at HTML-elementer kan dele samme id og klasseverdi.

</aside>

### Kjedevelgere

Kjede CSS med komma Målrett mot mer enn ett element Nyttig for delte stiler Eksempel Alle h*-tagger må ha farge satt til lilla og ha en kantlinje

## "Box-modellen"

### Anatomi av boksen

### Den innrammede malerianalogien

For å forstå Box-modellen kan vi bruke en analogi av et innrammet maleri. Vårt innrammede maleri består av tre deler: selve maleriet, passepartout-ramme (papprammen mellom fotografiet og ytterrammen) og en treramme. Box-modellen følger samme prinsipp: du har Content-boksen (maling), Padding-boksen (passepartout-ramme) og Border-boksen (treramme). I tillegg til disse tre boksene har du også Margin-boksen. Hvis vi bygger på vår eksisterende analogi, vil Margin-boksen fungere som rommet rundt det innrammede bildet. Deretter vil vi se på hver boks mer detaljert.

### Hvor innhold vises

### Innholdsboks

I innholdsboksen er det der innholdet vises, som navnet på boksen antyder. Innholdet kan være et hvilket som helst HTML-element, for eksempel tekst, bilder, figurer osv. Du kan ikke spesifisere fargen på alle bokstypene til Box-modellen, men du kan angi verdien `background-color`på innholdsboksen.

```html
<h1>Text content</h1>
```

Ta denne h1-taggen. Vi kan bruke CSS til å angi bakgrunnsfargen til innholdsboksen ved å bruke egenskapen bakgrunnsfarge. Vi har også satt `width`til 100 %, noe som betyr at størrelsen på innholdsboksen samsvarer med bredden på det overordnede elementet.

```css

h1 {
  background-color: lightblue;
  width: 100%;
}
```

### Padding-boks

Padding-boksen bor rett utenfor innholdsboksen og utgjør rommet mellom innholdsboksen og kantboksen. Du kan spesifisere denne plassen ved å bruke `padding`egenskapen, som vist i koden nedenfor.

```css

h1 {
  padding-top: 1rem;
  padding-right: 1.5rem;
  padding-bottom: 1rem;
  padding-left: 1.5rem;
}
```

### Border-boksen

Border-boksen sitter utenfor innholds- og polstringsboksene. La oss gå tilbake til "innrammet maleri-analogi" - Border-boksen er representert av trerammen. Med en treramme kan du velge farge, tykkelse og radius på hjørnene. Du kan gjøre det samme med Border-boksen.

Det er fire hovedegenskaper som gjelder kantboksen: `border-style`, `border-color`, `border-width`og `border-radius`.

1. `border-style`
    - stilen til Border-boksen
    - kan være ensfarget, stiplede linjer, doble linjer og mange flere.
    - er en obligatorisk egenskap. Hvis du ikke angir denne egenskapen, kan du ikke bruke `border-color` til å angi fargen.
2. `border-color`
    - angi fargen på kanten.
3. `border-width`
    - angi bredden på kanten.
4. `border radius`
    - still inn radiusen til kantene på Border-boksen, dvs. "rundheten" på kantene.

```css

h1 {
  border-style: solid;
  border-color: blue;
  border-width: 1rem;
  border-radius: 1em;
}
```

I stedet for å bruke en egen egenskap for stil, farge og bredde, kan du bruke det som kalles en kortform-egenskap. Hensikten med en kortform-egenskap er å forkorte mengden kode. I koden nedenfor er det et eksempel på bruken av kortformgenskapen `border`.

```css

h1 {
  border: 1em solid blue;
  border-radius: 1em;
}
```

### Marginboks

Margin Box er den ytterste boksen i Box Model. Ved å angi størrelsen på Margin-boksen, skaper du usynlig plass rundt Border-boksen. Går tilbake til "innrammet maleri-analogi"; la oss si at du har fire innrammede malerier. To malerier er plassert øverst og to nederst, og danner en firkant. Vanligvis vil man gjerne ha litt mellomrom mellom maleriene, for å unngå at presentasjonen virker rotete; når det gjelder Box-modellen, vil denne avstanden oppnås ved å spesifisere størrelsen på Margin-boksen.

```css
h1 {
  margin-top: 2em;
  margin-right: 2em;
  margin-bottom: 2em;
  margin-left: 2em;
}
```

Bruke kortform `margin`egenskapen:

```css

h1 {
  margin: 2em;
}
```

### Prøv det selv

1. Lag en nettside i CodePen som har et enkelt div-element med noe tekstinnhold
2. Lag en elementvelger for body-elementet og sett bakgrunnsfargen til rosa.
3. Lag en elementvelger for div-elementer, sett utfyllingen til 2em, margin til 1em, sett en solid svart kant på 1 piksel i tykkelse og gi den en hvit bakgrunnsfarge.
4. Legg merke til plasseringen av innholdet og plasseringen av elementet
5. Juster størrelsen på polstringen, margen og kantlinjen og legg merke til effekten på gjengivelsen av elementet.

## Fargestyling

### Fargekoder

Når du vil angi fargen på et element, kan du sette verdien til css-egenskapen til *navnet* på fargen ( *[lenke](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://www.w3.org/wiki/CSS/Properties/color/keywords)* til liste over gyldige fargenavn), men du kan også lage egendefinerte farger ved hjelp av **fargekoder** .

Vi skal se på to typer fargemodeller som oftest brukes i webdesign; **RGB** - koder og **heksadesimale** (HEX) koder.

### RGB-koder

**RGB** er en forkortelse av **Rø**d, **G**rønn og **B**lå. Disse tre fargene er kjent som additive primærfarger; additiv, noe som betyr at ved å kombinere disse fargene vil det resultere i nye, forskjellige farger. Vi kan også justere lysstyrken til hver av primærfargene, og ved å gjøre det får du enda flere fargekombinasjoner. Faktisk har du over seksten millioner fargekombinasjoner som bruker RGB-fargekodene. *[Denne videoen](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://www.youtube.com/watch?v%3DCrBFNLvoL6A)* illustrerer prinsippene for RGB på omtrent tretti sekunder.

La oss ta en dypere titt på faktisk RGB-kode; hvordan den er formatert og hvorfor den er formatert på en slik måte.

I kodeblokken nedenfor spesifiserer vi fargen på et `<button>`element. Bakgrunnsfargen er hvit, skriftfargen er svart og kantfargen er grå. Hvis vi ser nærmere på RGB-koden ser vi at den består av `rgb`etterfulgt av tre verdier omsluttet av parenteser. Disse verdiene kan settes fra 0 til 255, og dette er lysstyrken til den primære fargen.

For å få en ren rød farge maksimerer du verdien for rød og setter verdien for blå og grønn til null: `rgb(255, 0, 0)`. Samme prinsipp gjelder for ren grønn `rgb(0, 255, 0)`og ren blå `rgb(0, 0, 255)`. Hvis du maksimerer verdiene for hver farge får du hvit, som verdien vi spesifiserte for `background-color`i css-koden nedenfor. Hva om vi setter alle verdiene til 0? Som du kanskje har gjettet, blir du svart. Hvis alle verdiene for rød, grønn og blå er de samme får du en gråskalafarge, som verdien vi angir for `border`egenskap: `rgb(128, 128, 128)`.

```css

button {
  background-color: rgb(255, 255, 255);
  color: rgb(0, 0, 0);
  border: 0.2rem solid rgb(128, 128, 128);
  padding: 1rem 2rem;
  margin: 2rem;
}
```

*[Denne RGB-velgeren](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://www.rapidtables.com/web/color/RGB_Color.html)* er et pent verktøy du kan leke med. Prøv å flytte sirkelen i firkanten rundt og se hvordan RGB-verdiene til høyre endres.

RGB-koder tar også en fjerde verdi, som kalles alfaverdien. Alfaverdien varierer fra 0,0 til 1,0, og kontrollerer opasiteten til fargen, dvs. dens gjennomsiktighet.

### Hex-kode

Den andre fargemodellen som brukes i webdesign bruker heksadesimal kode, eller kort sagt HEX. HEX-koder består av et #-symbol etterfulgt av seks alfanumeriske verdier. HEX-fargekoder er basert på det heksadesimale systemet og å konvertere RGB-kode til HEX-kode er ikke noe du skal gjøre; det er mange konverteringsverktøy der ute som vil gjøre den jobben for deg. For å bli kjent med formatet til HEX-kode, her er noen:

- Hvit:`#FFFFFF`
- Svart:`#000000`
- Grå:`#808080`
- Rød:`#FF0000`
- Blå:`#0000FF`
- Grønn:`#00FF00`

Fordelen med å bruke HEX over RGB er at den er kortere. Du kan se HEX som en kortform-versjon av RGB. Du vil stort sett bruke et verktøy som "RGB-velgeren" for å finne en farge og deretter kopiere HEX-koden som verktøyet sender ut.

## Prøv det selv

Kopier koden nedenfor til [codepent.io](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://codepen.io/pen/) . Prøv å endre RGB-verdier for disse egenskapene til `h1`elementet: `color`, `background-color`og `border`. Prøv å bytte ut RGB-verdiene for tilsvarende HEX-verdi. Når du bruker RGB-kode, prøv å justere alfaverdiene for å kontrollere fargeopasiteten.

HTML:

```html
<div class="container">
  <h1>What does the cat say?</h1>
</div>
```

CSS:

```css
.container {
  width: 100%;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  background-image: url("https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/cute-cat-photos-1593441022.jpg");
  background-size: cover;
  background-position: center;
  background-repeat: norepeat;
}h1 {
  font-family: Arial, Helvetica, sans-serif;
  font-weight: 100;
  font-size: 30px;
  color: rgb(255, 255, 255);
  background-color: rgb(125, 125, 125, 0.5);
  padding: 1rem;
  border: 0.2rem solid rgb(0, 0, 0);
  border-radius: 1rem;
}
```

## Font-styling

### Grunnleggende typografiske designelementer

### CSS `font-family`egenskapen

I følge *[MDN-dokumentasjon](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://developer.mozilla.org/en-US/docs/Web/CSS/font-family)* :

> Font-family CSS-egenskapen spesifiserer en prioritert liste over ett eller flere fontfamilienavn og/eller generiske familienavn for det valgte elementet.
> 

I koden nedenfor ser du en stabel med fonter med prioritet fra venstre til høyre. Så hvis nettleseren har Segoe UI installert, vil denne skriftstilingen bli brukt på tekstinnholdet. Det er imidlertid ikke alle nettlesere som har alle fontene installert, dette er en grunn til at du har en eller flere "reserve"-fonter. Så hvis Segoe UI ikke kan brukes, vil nettleseren velge neste font på listen, som er Tahoma.

CSS:

```css
p {
  font-family: "Segoe UI", Tahoma, Geneva, Verdana, sans-serif;
}
```

### Skriftstørrelse

Når du angir størrelsen på fonten/tekstinnholdet, kan du bruke `font-size`CSS-egenskapen. De vanligste [størrelsesenhetene som](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Values_and_units) brukes til å angi verdien til `font-size`eiendommen er disse tre: `px`, `em`og `rem`. Disse enhetene er forskjellige i måten de fungerer på, og det er viktig å forstå hvordan de skal brukes.

Enheten `px`er en enhet med fast størrelse, og hvis du bruker denne størrelsesenheten til å angi størrelsen på et html-element, vil størrelsen alltid forbli den samme. `px`er en forkortelse for piksler, og når du angir størrelsen på et element med denne enheten, spesifiserer du bokstavelig talt hvor mange piksler høyt eller bredt et element eller skrift skal være.

Du vil bli introdusert for *responsiv design* i dette kurset, som er en viktig del av moderne nettløsninger. Kort sagt, en responsiv nettside vil justere oppsettet i henhold til størrelsen på [visningsporten](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://developer.mozilla.org/en-US/docs/Glossary/Viewport) . Ta en mobiltelefon med en viewport-bredde på rundt `360px`og en skrivebordsskjerm med en viewport-bredde på `1980 px`. Hvis du setter `font-size`verdien med `px`, vil teksten vises mye større på mobile enheter. La oss si at du setter `font-size`verdien til `30px`; på en skrivebordsskjerm med en viewport-bredde på `1980px`, `30 px`utgjør omtrent 1,5 prosent av den totale viewport-bredden. På den annen side, på en mobil enhet `30px`utgjør omtrent 8 prosent av den totale visningsportbredden, og vil som sådan virke mye større.

Enheten `em`justerer størrelsen på html-elementet i henhold til skriftstørrelsen til det **overordnede elementet** . `1 em`lik 100 % av `font-size`verdien satt for det overordnede elementet. Hvis skriftstørrelsen til det overordnede elementet er `30px`, er `1em`det underordnede elementet lik `30px`.

I eksemplet nedenfor har vi satt `font-size`verdien `.parent`til `10px`og `font-size`verdien av `.child`er satt til `3em`. Verdien `font-size`av `.child`er tre ganger størrelsen på dens overordnede, så 10 x 3 = `30px`.

HTML:

```html
<html>
    <body>
        <main class="parent">
            <p class="child">Check this out</p>
        </main>
    </body>
</html>

```

CSS:

```css

.parent {
  font-size: 10px;
}.child {
  font-size: 3em;
}
```

Enheten `rem`fungerer på samme måte som `em`enheten, men justerer størrelsen på html-elementet i henhold til skriftstørrelsen til **rotelementet** . Rotelementet på en HTML-webside er `<html>`taggen som omslutter alle de andre elementene. Standard skriftstørrelse for rotelementet er `16px`, så som standard `1rem`er lik `16px`. Du har kanskje lurt på hvorfor skriftstørrelsen til en `<p>`tag alltid er `16px`før du bruker styling med CSS. Som standardstørrelsen på en `<p>`tag er `1rem`, og som vi nå vet, `1rem`tilsvarer `16px`.

CSS:

```css
.child {
  font-size: 3rem;
}
```

I dette eksemplet `3rem`er lik standard skriftstørrelse for rotelementet ganger tre, så `3rem`lik `48px`. La oss prøve å endre standard skriftstørrelse for rotelementet og se hvordan det påvirker skriftstørrelsen til `.child`.

CSS:

```css
html {
  font-size: 30px;
}.child {
  font-size: 3rem;
}
```

Som du ser ovenfor, har vi endret skriftstørrelsen til rotelementet til `30px`, og er nå `3rem`lik `90px`.

### Importere fonter

Hvis du ikke vil bruke noen av de *[nettsikre](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://www.hostinger.com/tutorials/best-html-web-fonts)* skriftene, kan du importere fonter fra den store samlingen som er tilgjengelig på Internett. En god kilde til gratis-å-bruke skrifter finner du på [https://fonts.google.com/](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://fonts.google.com/) . Gå nå over til Google-fonter og søk etter ["Poppins"](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://fonts.google.com/specimen/Poppins%23standard-styles) .

![Google fonts-siden](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/google_fonts_how_to_full.JPG)

Google fonts-siden

Når du er på "velg stiler"-siden, rull ned for å finne fonten som sier "400 vanlig" og klikk på "+velg denne stilen". En sidestang glir inn fra høyre, som du ser på figuren over. Kopier `<link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">`og lim inn denne kodeblokken i `head`html-en din, som vist nedenfor. Når dette er gjort kan du få tilgang til "Poppins"-fonten gjennom CSS-stilarket ditt, som du ville gjort med alle andre fonter.

HTML:

```html
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <title>CSS fonts 1</title>
</head>

```

CSS:

```css
h1 {
  font-family: 'Poppins';
}
```

En annen måte å importere fonter fra nettet på gjøres direkte gjennom CSS ved å bruke `@import`nøkkelordet, som du ser i koden nedenfor. Etter å ha gjort det, er skriften tilgjengelig ved å bruke `font-family`egenskapen og skriftnavnet, som du ser nedenfor.

CSS:

```css
@import url('https://fonts.googleapis.com/css2?family=Poppins&display=swap');h1 {
  font-family: 'Poppins';
}
```

### Laste inn lokale fonter

Det er mulig å laste ned fonter og lokalt laste dem inn i CSS-filen din ved å bruke `@font-face`nøkkelordet. Når du bruker denne metoden, må du opprette et tilpasset navn for `font-family`, og dette navnet vil bli brukt for å få tilgang til skriften.

CSS:

```css
@font-face {
  font-family: main-font; /*Setting font name*/
  src:  url('../fonts/Poppins.tff') format('tff'),
        url('../fonts/Poppins.woff') format('woff');
}

div {
  font-family: 'main-font'; /*Accessing font by our custom name*/
}
```

### Prøv det selv

1. I CodePen, lag en nettside som bruker Akaya Telivigala-fonten for alle p-elementer, bruk serif som reservefont.
2. Skriftstørrelsen skal være 2em.
3. Prøv å justere skriftstørrelsen til forskjellige enheter, legg merke til forskjellene.

## Layout styling

### Posisjonering

Å bruke posisjonsegenskapen er en annen nyttig teknikk for sidelayout. Det er viktig å forstå posisjonering på riktig måte før du dykker inn - mange juniorutviklere blir snublet ved å bruke feil posisjon. Det er fem typer posisjonering: statisk, relativ, absolutt, fast og klebrig.

### posisjon: statisk

Alle elementer i normal dokumentflyt har statisk posisjonering som betyr at elementene ikke justerer posisjoneringen til egenskapene topp, høyre, bunn eller venstre. Den holder seg bare i normal flyt. Fordi statisk posisjonering er standard, er det lite sannsynlig at du trenger å angi elementer som posisjon: statisk med mindre CSS-en din blir overskrevet utenfor din kontroll.

### stilling: relativ

I relativ plassering holder elementet seg innenfor normal flyt av dokumentet, men justeres i henhold til topp-, høyre-, bunn- eller venstresett på elementet. Den nye posisjonen settes 'relativt' til sin opprinnelige posisjon.

### posisjon: absolutt

Absolutt posisjonering flytter elementet ut av den normale flyten av dokumentet. Den setter sin posisjonering basert på det neste overordnede elementet til å ha relativ eller absolutt posisjonering. Hvis det ikke er noe element med posisjonering, vil det som standard brukes i visningsporten. Vi bruker ofte relativ posisjonering på et overordnet element for å begrense et absolutt posisjonert element.

### posisjon: fast

Fast posisjonering flytter også elementet ut av normal flyt (ligner på absolutt posisjonering), men med fast posisjonering blir elementet upåvirket av rulling. Et fast element er plassert basert på nettleserens visningsport.

Praktisk eksempel: `position: absolute`og `position: fixed`

![https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/gif-position-absolute-fixed.gif](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/gif-position-absolute-fixed.gif)

### z-indeks

Med posisjonering er det enkelt å få elementer oppå hverandre. For å bestemme rekkefølgen elementer stables i, kan vi bruke z-indeks. Z-indeksen er ganske enkel å bruke; jo høyere z-indeksnummer, jo høyere i rekkefølgen går det.

Praktisk eksempel: `z-index`

![https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/gif-position-z-index.gif](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/gif-position-z-index.gif)

### Prøv det selv

1. Naviger til [https://codepen.io/sean-noroff/pen/JjbxdPx](https://codepen.io/sean-noroff/pen/JjbxdPx)
2. Fordel eksemplet og legg til et element med en klebrig posisjon

### Flytende

Hvis du vil plassere et bilde ved siden av tekst og la teksten pakkes rundt bildet, er float det ideelle verktøyet. Vi pleide å bruke float for å bygge oppsett, men burde ikke lenger fordi vi nå har Flexbox og CSS Grids for å hjelpe til med å lage solide, responsive nettsteder med flere kolonner.

Flytende fjerner et element fra den normale flyten av HTML-elementer og plasserer det på venstre eller høyre side av beholderen eller nærmeste element.

Egenskapen `float`har tre verdier: `left`, `right`og `none`.

`float: left`

![https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/float-2.JPG](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/float-2.JPG)

`float: right`

![https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/float-1.JPG](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/float-1.JPG)

### Prøv det selv

1. I CodePen oppretter du en nettside som gjenskaper layouten i de to foregående lysbildene
2. Bruk både float:venstre og float:høyre

### Boksstørrelse

Går tilbake til temaet Box-modellen; `box-sizing`CSS-egenskapen påvirker størrelsen på innholdsboksen når den er `width`satt til 100 % av det overordnede elementet. Når du legger til bredde til egenskapen `padding`og `border`, legger den til den totale størrelsen på innholdsboksen, og som et resultat vil elementet falle utenfor grensene til det overordnede elementet.

For å fikse dette problemet kan vi sette `box-sizing`egenskapen til `border-box`. Som standard `box-sizing`er satt til `content-box`.

![Praktisk eksempel: `box-sizing: border-box`](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/box-sizing.gif)

Praktisk eksempel: `box-sizing: border-box`

### Prøv det selv

1. Bygg på fra forrige boksmodelleksempel, lag en kopi av `div` innholdet "Dette er innholdet".
2. Opprett en ny klassevelger, la den bruke `box-sizing: border-box`
3. Legg merke til forskjellen mellom hvordan de to `div`elementene gjengis.

### Visningstyper (Flexbox & Grid)

### Flexbox

Flexbox brukes til å lage endimensjonale oppsett som lar oss plassere elementer i rader eller kolonner. Den kalles Flexbox fordi gjenstander justeres for enten å fylle tomrom eller krympe for å passe inn i mindre rom. Dette gjør Flexbox ideell for å bygge responsive nettsider.

Det er verdt å merke seg at Flexbox tilbyr mye funksjonalitet til utviklere for å lage komplekse oppsett, men den er også flott for enkle oppsett også.

Oppsett`display: flex`

For å begynne å bruke Flexbox må du sette container til `display: flex`. Dette muliggjør en fleksibel kontekst for alle sine direkte barn. De underordnede elementene vil som standard vises i en rad, og vil dele den tilgjengelige plassen i beholderen mellom underelementene. Hvis underordnede gjenstander er for små til å ta opp hele beholderen, vil de stables opp fra venstre marg.

HTML

```html
<section class="container">
  <div class="item">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec neque pharetra, 
      porta ipsum sed, porta turpis. Proin tristique quam nunc, sed rutrum nulla varius 
      vitae. Nulla non leo sapien. Pellentesque viverra felis eu nunc ullamcorper 
      imperdiet. Nulla a mattis elit. Pellentesque vitae tortor nulla.</p>
  </div>
  <div class="item">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec neque 
      pharetra, porta ipsum sed, porta turpis. Proin tristique quam nunc, sed rutrum 
      nulla varius vitae. Nulla non leo sapien. Pellentesque viverra felis eu nunc ullamcorper 
      imperdiet. Nulla a mattis elit. Pellentesque vitae tortor nulla.</p>
  </div>
  <div class="item">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec neque pharetra, 
      porta ipsum sed, porta turpis. Proin tristique quam nunc, sed rutrum nulla varius vitae. 
      Nulla non leo sapien. Pellentesque viverra felis eu nunc ullamcorper imperdiet. 
      Nulla a mattis elit. Pellentesque vitae tortor nulla.</p>
  </div>
</section>
```

CSS

```css
.container {
  display: flex;
  flex-direction: column;
}.item{
  margin: 5px;
}
```

Egenskapen`flex-direction`

Som standard er `flex-direction`egenskapen satt til rad, så de fleksible elementene vil vises på rad. For mobile enheter er det nyttig at vi kan bytte denne retningen til kolonne ( `flex-direction: column`) slik at elementene nå vises i en vertikal kolonne.

Du kan også sette retningen til rad-revers eller kolonne-revers, og elementene vil vises i motsatt rekkefølge til hvordan de er skrevet.

Egenskapen`flex-wrap`

Som standard vil alle elementene passe på én linje for å ta opp den tilgjengelige plassen i beholderen. Men noen ganger kan gjenstandene bli ganske klemt. I dette tilfellet, sett flex-wrap: elementer vil brytes på en annen linje hvis det ikke er ledig plass.

Egenskapen`justify-content`

Egenskapen justify-content angir justeringen og avstanden til de bøyde elementene langs hovedaksen.

Her er andre mulige verdier for `justify-content`eiendommen.

- `center`plasserer elementer i midten av forelderen.
- `start` og `end` plasserer elementer enten ved starten av overordnet klasse eller på slutten.
- `space-between`plasserer det første elementet i starten og det siste elementet på slutten. Elementer i mellom er deretter fordelt jevnt.
- `space-evenly`plasserer alle gjenstander jevnt med jevn plass mellom dem.

### Prøv det selv

1. Ved å bruke CodePen, bruk Flexbox til å lage en grunnleggende layout med 3 div-elementer med lik flex, justert i midten av beholderen `div`.
2. Eksperimenter med å endre egenskapene til flex-elementene basert på forrige seksjon, legg merke til effektene på oppsettet.

### Gridbox

Mens Flexbox fungerer endimensjonalt, enten vertikalt eller horisontalt, kan CSS Grids fungere todimensjonalt samtidig. Dette lar deg lage mer avanserte oppsett ved hjelp av enkeltsystemet.

CSS Grids heter det fordi det er basert på ideen om et rutenett som løper horisontalt og vertikalt over og nedover skjermen. Vi kan bruke CSS Grids til å bestemme hvor mange horisontale og vertikale rom i rutenettet hvert element skal ta opp.

### Viktige konsepter

1. Grid (rutenett) container
    - For å komme i gang med å bruke CSS Grids, må du lage det som kalles Grid Container.
    - For å gjøre dette, sett `display: grid`på et overordnet element.
    - Dette lar deg bruke CSS Grids for å lage rader og kolonner inne i det elementet.
2. Rutenettelement
    - Et rutenettelement er et av de direkte underordnede elementene til rutenettbeholderen.
    - Dette er elementene som vil ta opp plass i rutenettene våre.
3. Rutenettgap
    - Rutenettgapet refererer til mellomrommet mellom kolonner og rader i rutenettet.
4. Rutenettmal-kolonner og rader
    - For å kontrollere størrelsen på rader eller kolonner bruker du `grid-template-columns` og `grid-template-rows`.
5. Rutenettmalområder
    - En av de fineste aspektene ved CSS Grids er muligheten til å lage rutenettområder. Det som er spesielt nyttig er at syntaksen (hvordan CSS er skrevet) er hvordan den vil vises på siden.
    - Hvis du hadde en topptekst, nav, hoved, bunntekst i HTML-koden din, kunne du angi disse områdene som et rutenettområde.
    - Merk at eiendommens grid-område brukes til å opprette området, og verdien myheader etc. bestemmes av utbygger.
    - Med grid-områdene definert, kan vi bruke grid-mal-områder til å ordne innholdet vårt i rader og kolonner.

### [Oppsett for grunnleggende rutenett](https://noroff--accelerate-gitlab-io.translate.goog/javascript/course-notes/html-css/css.html?_x_tr_sl=en&_x_tr_tl=no&_x_tr_hl=no&_x_tr_pto=wapp#setup-for-basic-grid)

```html
<body>
  <main>
    <div class="item item-1">Item 1</div>
    <div class="item item-2">Item 2</div>
    <div class="item item-3">Item 3</div>
    <div class="item item-4">Item 4</div>
    <div class="item item-5">Item 5</div>
  </main>
</body>
```

CSS

```css
.item {
  border: 2px solid orange;
  background-color: rgb(255, 183, 50);
  padding: 15px;
}
```

![Oppsett grunnleggende grid - resultat](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/css-grids-1.JPG)

Oppsett grunnleggende grid - resultat

### Spesifiser kolonner

*3 kolonner med lik avstand*

CSS

```css
main {
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
}
.item {
  border: 2px solid orange;
  background-color: rgb(255, 183, 50);
  padding: 15px;
}
```

![3 kolonner med lik avstand](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/css-grids-2.JPG)

3 kolonner med lik avstand

*3 kolonner, 1 større*

CSS

```css

main {
  display: grid;
  grid-template-columns: 1fr 2fr 1fr;
}
.item {
  border: 2px solid orange;
  background-color: rgb(255, 183, 50);
  padding: 15px;
}
```

![3 kolonner, 1 større](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/css-grids-3.JPG)

3 kolonner, 1 større

### Legge til plass

### Like mellomrom mellom elementer

CSS

```css
main {
  display: grid;
  grid-template-columns: 1fr 2fr 1fr;
  column-gap: 5px;
  row-gap: 5px;
}
.item {
  border: 2px solid orange;
  background-color: rgb(255, 183, 50);
  padding: 15px;
}
```

![Like mellomrom mellom elementer](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/css-grids-4.JPG)

Like mellomrom mellom elementer

### Eksplisitte rader

*Angi eksplisitt malen for rader*

CSS

```css
main {
  display: grid;
  grid-template-columns: 1fr 2fr 1fr;
  column-gap: 5px;
  row-gap: 5px;
  grid-template-rows: 1fr 2fr;
}
.item {
  border: 2px solid orange;
  background-color: rgb(255, 183, 50);
  padding: 15px;
}
```

![Mal for rader angitt eksplisitt](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/css-grids-5.JPG)

Mal for rader angitt eksplisitt

### Implisitte rader: grid-auto-row

CSS

```css
main {
  display: grid;
  grid-template-columns: 1fr 2fr 1fr;
  column-gap: 5px;
  row-gap: 5px;
  grid-template-rows: 1fr 2fr;
}.item {
  border: 2px solid orange;
  background-color: rgb(255, 183, 50);
  padding: 15px;
}
```

![Grid Auto Row: Implisitt](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/css-grids-6.JPG)

Grid Auto Row: Implisitt

### Rutenettkolonne

CSS

```css
.item {
  border: 2px solid orange;
  background-color: rgb(255, 183, 50);
  padding: 15px;
}.item-1 {
  grid-column: 1 / 4;
}.item-2 {
  grid-column: 1 / 3;
}
```

![Grid Auto Row: Implisitt](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/css-grids-8.JPG)

Grid Auto Row: Implisitt

### Rutenettrad

CSS

```css
.item {
  border: 2px solid orange;
  background-color: rgb(255, 183, 50);
  padding: 15px;
}.item-1 {
  grid-column: 1 / 4;
}.item-2 {
  grid-column: 1 / 3;
  grid-row: 2 / 4;
}
```

![Grid row](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/css-grids-7.JPG)

Grid row

### Prøv det selv

1. I CodePen, reproduser oppsettet i skjermbildet nedenfor ved å bruke CSS-rutenett.
2. Endre rutenettet slik at element 8 til element 13 spenner over et enkelt element.

![Eksempel-layout](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/css-grids-7.JPG)

Eksempel-layout

## Responsivitet på nettet

### [Nettleser vs skjermoppløsning](https://noroff--accelerate-gitlab-io.translate.goog/javascript/course-notes/html-css/css.html?_x_tr_sl=en&_x_tr_tl=no&_x_tr_hl=no&_x_tr_pto=wapp#browser-vs-display-resolution)

En ting å huske på når du designer responsive nettsider er nettleser kontra skjermoppløsning. La oss si at skrivebordsskjermen har en maksimal oppløsning på 1080 x 1980 piksler og telefonen (i dette tilfellet iPhone 10) har en oppløsning på 1125 x 2436 piksler med omtrent 458 PPI pikseltetthet. Fra å se på tallene alene, vil man anta at iPhone har en større fysisk skjerm, selv om vi vet at det ikke er tilfelle. Fra et CSS-perspektiv har iphone 10 en skjermstørrelse eller *visningsportstørrelse* på 375 x 812 piksler med et *enhetspikselforhold* (DPR) på 3. Hvordan beregner CSS dette tallet? CSS får dette tallet ved ganske enkelt å dele enhetsoppløsningen (piksler) med DPR, i dette tilfellet 1125 / 3 og 2436 / 3. Resultatet er 375 x 812 piksler.

### Mediespørringer

Som en del av CSS lar mediespørringer oss tilpasse utseendet til HTML-innhold for flere enheter. Selv om «Mediespørringer» kan høres ut som et komplisert konsept, er ideen faktisk ganske enkel.

Syntaksen til en mediespørring:

```css
@media (min-width: 600px) and (max-width: 1200px) {
  /*code goes here*/
}
```

Du vil se at det er en rekke attributter vi kan inkludere i en medieforespørsel. Disse inkluderer minimum og maksimum bredde, enhetsorientering (for eksempel hvis målet er en håndholdt eller bærbar enhet) og mange andre egenskaper.

Vi kan referere til hver av disse mediespørringene som å definere et bruddpunkt - det vil si hvor CSS-utseendet vil bli endret basert på mediespørringsattributtene. Det er flere ting her som vi må være klar over:

- Først av alt vil du se at vi bruker et nytt tegn i ut CSS: alfategnet "@", brukt sammen med "media".
- For det andre kan mediespørringen legges til CSS. Akkurat som visse HTML-koder kan være nestet, kan CSS-velgere være nestet i mediespørringer. I hovedsak for hvert mediesøk vi legger til, kan vi lage et nytt sett med CSS-regler som gjelder for det.
- Til slutt vil en medieforespørsel bare brukes hvis alle dens **betingelser** er oppfylt. I henhold til de boolske "og"-argumentene i eksemplet ovenfor, vil CSS bare brukes hvis størrelsen på enhetsvinduet er minst 600 piksler bredt og er mindre enn eller lik 1200 piksler bredt. Teoretisk betyr dette at et hvilket som helst antall medieforespørsler kan opprettes, men generelt sett vil det ikke være nødvendig med mer enn to eller tre mediespørringer.

### Bruddpunkter

Som vi har sett med å jobbe med CSS og bilder, kan det hende vi legger ut bilder til CSS, i stedet for å bygge dem inn i HTML. Vi kan ta dette enda lenger ved å bruke mediespørringer til å definere forskjellige bilder for forskjellige bruddpunkter. Dette er utrolig nyttig for bilder. Som et enkelt eksempel kan mindre bilder som lastes ned raskere spesifiseres for mobile enheter, noe som vil gi innhold som er lettere og raskere å laste.

Generelt sett er det best å designe for de minste visningsportene (enheter i mobilstørrelse for øyeblikket, men kanskje klokker snart) først, og lage regler for gradvis større skjermstørrelser derfra. Argumentet for dette er todelt: For det første vil du fokusere på en visningsport med begrenset størrelse, som vil tvinge deg til å vurdere det viktigste. For det andre betyr det faktum at folk bruker så mye tid med sine mobile enheter at nettstedet ditt bør være optimalt på en mobilenhets visningsport. Jo mer tiltalende innholdet ditt er på en hvilken som helst enhet, jo større er sjansen for at folk vil bruke det på alle typer enheter.

### META-attributter for responsiv CSS

Vi har allerede sett på bruksområder for elementet. Elementet kan også inneholde informasjon om hvordan siden skal vises. Dette er avgjørende for optimal funksjonalitet til responsive nettsteder.

Mens medieforespørsler hjelper med å lage gode responsive CSS-sider, holder elementet den siste brikken i puslespillet. I tillegg til å ha egenskaper for sidebeskrivelsen og forfatteren, inkluderer den egenskaper for enhetens visningsport - det vil si størrelsen på enhetens nettleservindu. HTML5 har lagt til denne egenskapen for å optimalisere utseendet til HTML-sider stilt med responsiv CSS.

Følgende metatag-eksempel er hvordan den kan brukes til å optimalisere en responsiv side:

```html
<meta name="viewport" content="width=device-width, initial-scale=1">
```

Den opprinnelige skala-attributtet er også en viktig komponent for å optimalisere responsivt innhold. Dette vil sikre at elementene passer best til bredden på visningsporten.

### Prøv det selv

1. I CodePen oppretter du et oppsett som inneholder en div som representerer sidelinjens innhold på en nettside, og en `div`
    
    som representerer hovedinnholdet på nettsiden.
    
2. Ved å bruke mediespørringer, hvis skjermen har en bredde på mindre enn `600px`, bør sidefeltet og `div`-innholdselementene stables under hverandre.
3. Hvis skjermen har en bredde på `600px`eller mer, bør `div`-elementene plasseres ved siden av hverandre, med sidefeltet som opptar 25 % av sidebredden, og innholdet opptar 75 %.

![Eksempel-output](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/responsive-try-it-out.JPG)

Eksempel-output

## Egendefinerte egenskaper

På store nettsteder kan du oppleve at du begynner å gjenta vanlige verdier. Farger er et vanlig sted hvor du vil gjenta de samme fargekodene. CSS-variabler (også kjent som Custom Properties) lar deg lage variabler du kan bruke på nytt i hele stilarket. Det hjelper deg ikke bare å følge DRY (Don't Repeat Yourself)-prinsippet, men gjør også koden lettere å lese.

### Syntaks

Vi setter en variabels navn og verdi ved å bruke følgende syntaks: --bright-blue: #2b88ff.

Egenskapsnavnet settes ved hjelp av en dobbel bindestrek --. For å gjøre variabelen tilgjengelig globalt, kan vi sette den til :root pseudo-klassen, men det kan være tider du vil begrense variabelens omfang.

Når vi har satt variabelnavnet og tildelt den en verdi, kan vi bruke den til å style et element ved å bruke var()-funksjonen som i koden nedenfor.

CSS:

```css
:root {
  --bright-blue: #2b88ff;
  --white: #FFF;
  --horizontal-space: 0px 10px;
}
button {
  background-color: var(--bright-blue);
  color: var(--white);
  padding: var(--horizontal-space);
}
```

Du kan angi mange forskjellige egenskaper og vil sannsynligvis bruke det på ting som skriftfamilier, farger i skriftstørrelser, mellomrom og mer.

CSS-variabler er underlagt CSSs kaskaderegler, og kan derfor overstyres. Dette kan være spesielt nyttig hvis du endrer verdien til en variabel i mediespørringene dine.

### Kompatibilitet

Det er noen kompatibilitetsutfordringer. CSS-variabler støttes ikke på Internet Explorer (IE), men de fleste moderne nettlesere støtter det. Hvis nettstedet du utvikler har et betydelig antall brukere på IE, kan det hende du må legge til reserveverdier når du bruker variabelen din, selv om dette beseirer ganske mye av hensikten med å bruke CSS-variabler. Som med mange kompatibilitetsproblemer, spesielt med IE, er det et valg du må ta om hvor viktig IE er.

### Prøv det

1. I CodePen, lag en layout med ett enkelt `div`-element.
2. Bakgrunnsfargen til elementet skal være grønn, sett dette som en egendefinert egenskap.

## Pre-prosessorere

### Programmering i CSS

> En CSS-forprosessor er et program som lar deg generere CSS fra forprosessorens egen unike syntaks. [MDN-dokumenter](https://developer.mozilla.org/en-US/docs/Glossary/CSS_preprocessor)
> 

Ved å bruke preprosessorer kan du bruke programmeringsbaserte funksjoner som variabler, funksjoner (mixins), looper, beregninger og mye mer. Preprosessorer har sin egen unike syntaks som er ment å bidra til å gjøre bruken av CSS litt enklere. De mest populære CSS-forprosessorene er SASS og LESS.

### Hvor skriver vi SASS?

Du skriver SASS i en fil med en `.sass`eller `.scss`utvidelse. `.sass`filer bruker ikke krøllete klammeparenteser, men er avhengige av innrykk, lik det du ser i Python-syntaksen. `.scss`filer bruker krøllete klammeparenteser, som vanlig CSS.

I koden nedenfor erklærer vi SCSS-variabler ved å bruke $-symbolet, som ligner på hvordan du erklærer variabler i PHP.

```scss
$col-primary: #f74d4d;
$col-secondary: rgb(241, 241, 54);
$spacer: 25;

```

Når du vil bruke SCSS-variabelen, refererer du til variabelnavnet.

```scss
.btn-primary {
  background-color: $col-primary;
}
```

Et mål i programmering er å lage gjenbrukbar kode for å minimere mengden unødvendig repetisjon. **Å bruke mixins vil gjøre det lettere å lage SCSS** som følger DRY-prinsippet.

```scss
@mixin square-box($size, $radius) {
  width: $size;
  height: $size;
  radius: $radius;
}
```

Først må du skrive mixin, som ligner mye på en JavaScript-funksjon med funksjonsargumenter.

Når du ringer mixin bruker du `@include`nøkkelordet etterfulgt av mixin-navnet. Siden vår mixin `square-box`forventer to verdier som argumenter, sender vi inn disse når vi kaller mixin. I dette tilfellet passerer vi inn `100px`som setter verdien for `$size`og `5px`som setter verdien for `$radius`.

```css
.box{
  background-color: $col-primary;
  @include square-box(100px, 5px);
}
```

### Gjør om SCSS/SASS til CSS

Nettleseren leser ikke `.scss`filer rett ut av esken. Vi må *kompilere* vår SCSS til CSS, og det er noen få programmer der ute som kan hjelpe oss med dette.

For å nevne noen:

- Node-Sass
- Dart-Sass
- Compass
- Ghostlab

### Last ned installasjonsprogrammet for Dart-Sass

Klikk på **[denne lenken](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=%25e2%2580%258b%25e2%2580%258bhttps://github.com/sass/dart-sass/releases/tag/1.25.0)** for å laste ned Dart-Sass-installasjonsprogrammet.

### Legg Dart-Sass til filbanen din

- Windows-tast, og søk deretter etter "PATH"
- Velg "Rediger systemmiljøvariabler"
- Klikk på "Miljøvariabler".
- Under "Systemvariabler", finn PATH-variabelen, velg den og klikk "Rediger". Hvis det ikke er noen PATH-variabel, klikk "Ny".
- Legg til katalogen der du lagrer dart-sass-mappen
    - For eksempel C:\Users\Dewald Els\Apps\dart-sass
- Klikk "OK".
- Start terminalen på nytt.

*Kilde: https://katiek2.github.io/path-doc/*

Kjør følgende kommando fra en terminal eller Powershell:

![Konverter .scss til .css](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/scss_convert1.JPG)

Konverter .scss til .css

Å legge til `--watch`flagget kan automatisk kompilere .scss.

![Konverter .scss til .css](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/scss_convert1.JPG)

Konverter .scss til .css

Se etter endringer.

### Prøv det selv

1. I CodePen, lag en enkel layout med et enkelt div-element
2. Du bør bruke tre SCSS-variabler for å lagre fonten til div, skriftstørrelsen og bakgrunnsfargen
3. Fargen på `div`skal være rød og skriften skal være Georgia med størrelse på `16px`
4. Bruk SCSS-variablene på`div`

## Debugging og feilsøking

En av de viktigste ferdighetene en utvikler trenger er feilretting. Det er usannsynlig at koden din vil fungere slik du forventer at den skal hver gang, og å kunne diagnostisere og fikse den er en viktig del av utviklingsprosessen.

Det er ikke noe galt med feilsøking, og det er bare en naturlig del av utviklingen. Noen ganger kan vi føle oss litt overveldet over feilene vi får tilbake, og ofte er det lurt å ta et skritt tilbake en stund, og komme tilbake til koden med et par friske øyne.

En teknikk kalt "rubber duck debugging" er ideen om at du bør ha en gummiand med deg mens du utvikler deg i tilfelle du blir sittende fast. Når du blir sittende fast, snakk med gummianden og forklar koden din til denne anda. Grunnen til dette er at noen ganger kan vi bli så fanget i koden vår at det å ta et skritt tilbake og forklare det til en person (eller en and) kan gi oss litt perspektiv.

Et vanlig problem du vil bli bedre på etter hvert som du blir mer erfaren, er overkompliserende løsninger. Vi prøver å skrive CSS på en slik måte at vi ikke gjentar oss selv og ikke skriver for mye kode, og dermed ender vi opp med spaghettikode som ikke kan fortelle hva som påvirker hva.

### Utviklerverktøy

De fleste større nettlesere har innebygde utviklerverktøy for å hjelpe deg med å jobbe med prosjektene dine. Her kan du legge til og fjerne HTML-elementer og CSS-styling for å finne ut hvor problemene er, og hva som forårsaker dem.

I denne leksjonen skal vi se på Firefoxs utviklerverktøy, men Chrome har lignende verktøy.

For å åpne utviklerverktøyene dine, trykk: Ctrl + Shift + I på PC, og Cmd + Opt + I på macOS.

Du kan også åpne den ved å høyreklikke på et element og deretter klikke på "Inspiser element".

Klikk på fanene nedenfor for å finne ut mer om Firefoxs utviklerverktøy.

- Slett node
    
    Når du debuggeret nettsted, spesielt hvis det er et horisontalt scrollefelt og du ikke finner hva som forårsaker det horisontale rullefeltet, er det en god idé å finne ut hvilket element som forårsaker problemet.
    
    Fjern ett og ett element fra siden til du til slutt ser hvilket element som forårsaker problemet.
    
    For å fjerne elementer fra siden, høyreklikk og velg ‘slett node’
    
    ![https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/Bugfixing-1.png](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/Bugfixing-1.png)
    
    Du kan også opprette en ny node, eller duplisere en node.
    
- Rediger som HTML
    
    For å endre HTML-en som vises på siden ved å redigere direkte fra konsollen, velg "Rediger som HTML". Velg det overordnede elementet du vil redigere, og legg til/endre/fjern deretter HTML-koden i nettleservinduet.
    
- Stilregler
    
    Du kan se hvilke stiler som brukes på et element ved å velge det i Inspektøren og deretter se stilene som er brukt under "Regler".
    
    I dette eksemplet ser vi på h1.
    
    ![https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/Bugfixing-2.png](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/Bugfixing-2.png)
    
    Vi kan se følgende:
    
    - den har en skriftstørrelse på 3em, som brukes i en mediespørring på linje 18.
    - fargen er satt på linje 12.
    - størrelsen på 2em overstyres av mediesøket og er krysset ut.
    
    **WCAG Checker**
    
    Et annet nyttig verktøy er WCAG Checker som lar deg se om fargen på teksten din er WCAG-kompatibel. Bare klikk på fargesirkelen under "Regler", og det åpner et alternativ for deg å justere fargen. Nederst sammenligner den tekstfargen med bakgrunnsfargen og forteller deg om den er kompatibel eller ikke. I dette tilfellet er ikke fargene WCAG-kompatible.
    
    ![https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/Bugfixing-3.png](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/Bugfixing-3.png)
    
- Oppsett
    
    Et hendig verktøy når du arbeider med Flexbox og CSS Grids, samt for å oppdage eventuelle problemer med CSS Box Model, er "Layout"-fanen.
    
    I eksemplet nedenfor kan vi se at main er satt til flex, og vi kan se hvilke elementer som er flex-elementene. Vi kan også se at overskriften bruker CSS Grid.
    
    Under det kan vi se at det valgte elementet har en margin på 10px, en kantlinje på 0px og utfylling på 5px. Disse verdiene kan redigeres direkte i utviklerverktøyene.
    
    ![https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/Bugfixing-4.png](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/Bugfixing-4.png)
    
- Responsiv designmodus
    
    Den responsive designmodusen lar deg teste nettstedet ditt i forskjellige skjermstørrelser. For å slå den på, kan du klikke på ikonet som ser ut som en telefon og et nettbrett, eller trykke Ctrl + Shift + M på PC og eller Cmd + Opt + M på macOS.
    
    ![https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/Bugfixing-5.png](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/Bugfixing-5.png)
    
    Herfra kan du velge en forhåndsinnstilt skjerm å se på. Du kan oppdatere og velge hvilken enhetsskjermstørrelse du vil at den skal etterligne. Du kan også oppdatere retningen for å se hvordan en bruker kan se skjermen i liggende format på for eksempel et nettbrett.
    
    ![https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/Bugfixing-6.png](https://noroff-accelerate.gitlab.io/javascript/course-notes/html-css/img/Bugfixing-6.png)
    
    En fin funksjon på Firefox er at den lar deg lukke inspektøren og fortsatt ha den responsive designmodusen på.
    

### Testing og validering

En sentral del av utviklingen er å validere koden din for å sikre at det ikke er enkle feil. Hvis du har jobbet med den samme koden lenge, kan det hende du ikke oppdager en grunnleggende skrivefeil eller feil. Å bruke eksterne verktøy for å validere og teste nettstedet ditt er nøkkelen til å lage feilfri kode.

### Validator.w3.org

Et verktøy du kan bruke for å validere HTML-en din er [https://validator.w3.org/]. Bare lim inn nettadressen til nettsiden din i adressefeltet, så vil den påpeke grunnleggende feil eller feil. Den vil vanligvis også gi anbefalinger om hvordan du kan fikse hver feil.

### Google Lighthouse

Et annet populært verktøy for å revidere nettstedet ditt er Google Lighthouse, som er tilgjengelig gjennom Chromes utviklerverktøy. Google Lighthouse kjører en rekke sjekker på nettstedet ditt for å fortelle deg hvordan siden har scoret på aspekter som ytelse, tilgjengelighet, beste praksis og SEO. Den gir deg også forslag til forbedring av disse forskjellige aspektene.

### Testing på tvers av enheter

En veldig nyttig måte å teste nettstedet ditt på er å bruke det på en rekke enheter. Jo flere enheter, operativsystemer og nettlesere du kan bruke til å teste nettstedet ditt på, jo bedre. Det er lett å oppdage ting som fonter som ikke lastes inn riktig. Noen ganger vil en nettleser fikse en feil i koden for å presentere en side som ser riktig ut, men som ikke er det. En annen nettleser kan kanskje ikke fikse den samme feilen.

Det er verdt å merke seg at for studiene dine forventer vi ikke at du støtter Internet Explorer.

### Prøv det selv

- Naviger til [https://codepen.io/sean-noroff/pen/vYybOXN](https://codepen.io/sean-noroff/pen/vYybOXN)
- Eksperimenter med å bruke Chrome-utviklerverktøyene for å inspisere nettsiden

## For å konkludere

### Viktige punkter

- CSS skrives best i en egen fil
- Bruk "Box-Model" når du styler komponenter
- Det er flere måter å bruke fargestyling på for å forbedre utseendet og følelsen til nettsider (RGB, HEX, RGBA)
- Velg en skriftfamilie og bestem hvordan tekststørrelsen skal passe (px, em, rem)
- Velg mellom statisk, relativ, fast, absolutt og klebrig plassering av HTML-elementer
- Bruk flytende hvis du vil vikle tekstelementer rundt andre elementer
- Bruk kantlinjestørrelse når bredden og høyden må vurderes i kant- og polstringsberegninger
- Bruk Flexbox, CSS Grid og Media Queries for å forbedre responsen på nettet når du arbeider med forskjellige skjermstørrelser
- Du kan lagre gjentatte verdier ved å bruke egendefinerte egenskaper
- Bruk forprosessorer til å skrive mer veltalende CSS
- Verktøy for feilsøking og feilsøking er avgjørende for å løse problemer med styling på nettsider

### Vi ser fremover

Vi har nå fullført modulen "Statisk webutvikling med HTML og CSS". I den neste modulen vil du øve på hvordan du legger til interaktivitet på nettet med **JavaScript** og hvordan du serverer statisk innhold med **Node** og **Express** .

De tre siste modulene etter "Dynamisk webutvikling med JavaScript" vil fokusere på å lage enkeltsideapplikasjoner med **React** og **Angular**.

## Tilleggsressurser

### Referanseguider

- [Få visuell representasjon av CSS-stiler](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://cssreference.io/)
- [CSS-egenskaper](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://css-tricks.com/how-many-css-properties-are-there/)
- [CSS-nettmalområder](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://developer.mozilla.org/en-US/docs/Web/CSS/grid-template-areas/)
- [CSS-egenskaper](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://css-tricks.com/how-many-css-properties-are-there/)
- [Google fonter](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://fonts.google.com/)
- [Sass: CSS med superkrefter](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://sass-lang.com/)

### Videodemonstrasjoner

- [Grunnleggende om CSS](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://youtu.be/yfoY53QXEnI)
- [CSS-rutenett](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://youtu.be/jV8B24rSN5o)
- [Slutt å bruke @import med Sass](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://www.youtube.com/watch?v%3DCR-a8upNjJ0)

### Artikler, prøvekode og veiledninger

- [CSS øvelser](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://www.w3schools.com/css/exercise.asp)
- [Flexbox](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://internetingishard.com/html-and-css/flexbox/)
- [Forbehandlere](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://vanseodesign.com/css/css-preprocessors/)
- [Chrome-utviklerverktøy](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://developers.google.com/web/tools/chrome-devtools/javascript)

---

Copyright 2022, Noroff Accelerate AS