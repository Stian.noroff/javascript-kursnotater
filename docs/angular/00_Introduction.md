# Angular Introduction

## What is Angular

### The Beginning

The Angular framework is a free and open-source project that is maintained by Google and a community of developers. Angular 2 was as a replacement for AngularJS, also made by Google. AngularJS and Angular are incompatible and are only related by its name.

### TypeScript in Angular

TypeScript was introduced as the standard way of writing Angular code since the first release in 2016. Angular is quite opinionated regarding its file and folder structure. Naming files is also very strict.

### Angular Modules

The learning curve for Angular is slightly steeper than other current frameworks due to its custom Module system. Angular relies on Components and Angular Modules to build and organize your application.

![Angular Modules](./img/Angular_Modules.png)

Modules act as "containers" for groups of Components and other files. As with other frameworks, Components contain UI logic for your application.
The main difference with Angular is that it exclusively uses the TypeScript `class` syntax above function syntax for Components and other file types. However, you may create functions for utilities.


Angular ”natively” provides features for Routing, Authentication and shared state as part of the core framework.

## Preparing for development

The Angular framework depends on [NodeJS](https://nodejs.org/en/) and [Git](https://git-scm.com/). In addition, the Angular CLI will also be installed.

## Angular CLI

The Angular CLI will be installed globally on your device.

```
npm install -g @angular/cli
# Install the Angular CLI globally
```

After successful installation, the `ng` command will be exposed on your system. The `ng` command will be used to run any task related to the Angular application.

## Creating a new project

Once the dependencies have been installed, creating a new Angular project is quite simple. Run the following command from the terminal application.

```sh
ng new ng-products
# Create a new Angular application named "Ng Products"
```

The above command will create a folder called `ng-products`. This is where you can specify the name of your project. All the dependencies will automatically be installed.

### Enable Routing?

The first prompt the CLI will present is where Routing should be enabled out the box. Generally it is recommended to choose **`Yes`**.

The setup will pre-configure routing in your Angular application and create a `AppRoutingModule` which will be discussed in more detail in the [Routing section](./01_Components_and_Router.md) of this course module.

### Stylesheet

The second prompt is which stylesheet pre-processor should be used. It is recommended to use SCSS since standard CSS is fully supported.

![Angular CLI Stylesheet Selection](./img/Angular_CLI_Stylesheet.png)

## CLI Basic commands

> Note: Replace both the angle brackets `< >` and text in the below examples.
>
> e.g. `/<component-name>` could change to `/product-list`, without the angle brackets (`<>`).

Server an Angular application

```sh
ng serve -o
# Start Angular server and open in default browser
```

Generate a new Angular component

```sh
ng generate component components/<component-name>
# Generate a new component in the components folder
```

Generate a new Angular "Page" component

```sh
ng generate component pages/<page-name> --type=page
# Generate a new page component in the pages folder
```

More commands will be discussed as the course continues.

### CLI Options

The Angular CLI provides many useful commands to assist your app development. Use the [CLI Overview and Reference](https://angular.io/cli) to learn more about the available commands.

## TypeScript Introduction

TypeScript is a superset of JavaScript. It provides additional syntactical sugar, specifically types. There are other features provided like [interfaces](#interfaces-and-types) and special types like [tuple](#tuple-types) and [union](#union-types) types.

### Data types

Defining types in TypeScript is fairly straight forward. The general syntax of variables and functions remains unchanged from JavaScript with the addition of `: type`

The definition of variables is always as follows:

`[lexical declaration] [variable name]: [data type] = [value]`

### `string`

```typescript
const myString: string = "Hello World!";
```

### `number`

It should be noted that both `float` and `integer` are of type `number` in TypeScript.

```typescript
// Integer
const myNumber: number = 42;

// Float
const myFloat: number = 42.0;
```

### `boolean`

```typescript
const myBoolean: boolean = true;
```

### `Symbol`

The [Symbol type](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Symbol) is used to create a symbol primitive and is guaranteed to always be unique.

Symbols are often used to create unique Object keys that will not conflict with existing keys of an object.

```typescript
const mySymbol: Symbol = Symbol("woof");
```

### Array

```typescript
// String array
const myStrings: string[] = ["a", "b", "c", "d", "e"];

// Number array
const myNumbers: number[] = [1, 2, 3, 4, 5];

// Boolean array
const myBools: boolean[] = [true, false, false, true, true];
```

Using types with arrays prevents unmatched data types to be added. Therefore, an error would occur when trying to add a string to the `myBools` array.

Similarly, adding `"42"` of type script to the `myNumbers` array would cause an error. TypeScript will not attempt to coerce the string to type number when adding to the array.

### Function signatures

Functions are also similar to JavaScript's syntax.

```typescript
// No return - i.e. a void
function printHello(): void {
  console.log("Hello!");
}

// Return a string
function getGreeting(): string {
  return "Hello World!";
}

// Return a number
function theAnswer(): number {
  return 42;
}

// Return an array of strings
function getGreetings(): string[] {
  return ["Hello World", "Hei, World!", "Hallo!"];
}
```

Function expressions share a similar syntax, but the signature should be assigned to the value not the declaration.

```typescript
// Function expression
const makeMyString = (): string => {
  return "Wow! A string!";
};

// Function expression shorthand.
const makeMyNumber = (): number => 42;
```

### Function arguments

Function arguments can also be typed and is extremely useful when invoking functions.

```typescript
// Define the "name" argument as type string.
function buildGreeting(name: string): string {
  return "Hello there, " + name;
}

// Define the "names" argument as a string array.
function buildGreetings(names: string[]): string[] {
  return names.map((name) => "Hello " + name);
}
```

### Interfaces and Types

TypeScript allows creation of custom data types.

Using a TypeScript `type`

```typescript
// Define a custom data type Product with type
type Product = {
  id: string;
  name: string;
  price: number;
  isFeatured: boolean;
};

// Using the custom type
const guitar: Product = {
  id: "dasd-2dff-234o-23fjo",
  name: "Fender Stratocaster",
  price: 7500,
  isFeatured: true,
};
```

Using a TypeScript `interface`.

```typescript
// Custom data type Product with interface
interface Product {
  id: string;
  name: string;
  price: number;
  isFeatured: boolean;
}

// Using the custom interface
const guitar: Product = {
  id: "dasd-2dff-234o-23fjo",
  name: "Fender Stratocaster",
  price: 7500,
  isFeatured: true,
};
```

The key differences between type and interface

| Feature                             | type | interface |
| ----------------------------------- | ---- | --------- |
| Create custom data types            | ✔️   | ✔️        |
| Use for [tuple types](#tuple-types) | ✔️   | x         |
| Use for [union types](#union-types) | ✔️   | x         |
| Merge data types with same name     | x    | ✔️        |
| Classes can inherit                 | x    | ✔️        |

### Tuple Types

```typescript
const productPrice: [number, string] = [1000, "Fancy Keyboard"];
```

### Union Types

```typescript
// Union Type
type Category = "Computer" | "Music" | "Gaming";

// Create a data type with Category union type.
type Product = {
  id: number;
  category: Category;
};
```

When using the Union Type, the code editor will provide practical hints for code completion.

![Union Types Intellisense](./img/union-type-intellisense.png)

### TypeScript classes

TypeScript also provides additional features to the class declaration.

```typescript
// Define a TypeScript class
class Product {

  // Public property
  public name: string;

  // Private property
  private id: number;

  // Constructor for initialization logic.
  constructor(id: number, name: string) {
    this.id = id;
    this.name = name;
  }

  // Public method
  public getId(): number {
    return this.id;
  }
}

const computer = new Product(1, "HackerStation Z9000");

computer.id; // Not accessible

computer.name; // "HackerStation Z9000"

computer.getId(); // Returns 1

```

### Combining Interfaces and Classes in TypeScript

```typescript
interface IProduct {
  id: number;
  name: string;
  createPrintLabel: () => void
}

class Product implements IProduct {

}
```

Leaving the code as shown in the above snippet would cause an error in the Product class. It does not correctly implement the IProduct interface. The id, name properties and createPrintLabel method is missing. 

See the below screenshot for an example of the error.

![Class with Interface](./img/Angular_Class_with_Interface.png)

## Writing HTML in Angular

Angular Component provides two ways for writing HTML.

### Templates - External

Writing HTML in an external template file is the default way to write UI for Angular components. The file is linked to the component in the `@Component` decorator's `templateUrl` property. More on that in the section.

```typescript
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html", // Link to external file
})
...
```

### Templates - Inline

Alternatively, HTML can be written directly in the component using the `template` attribute. The HTML must be written in back ticks (`) to accommodate a multiline string. Angular does provide Intellisense for inline templates.

```typescript
// Inline Template
@Component({
  selector: "app-root",
  template: `
    <h1>The App!</h1>
    <p>The best app ever made.</p>
  `
})
...
```

| Inline                                                 | External                                                                      |
| ------------------------------------------------------ | ----------------------------------------------------------------------------- |
| Less context switching (Moving between multiple files) | Suited for larger templates                                                   |
| Great for smaller components                           | Useful when using many other Angular components (i.e. imported Components)    |
| -                                                      | Better option if Component contains large amounts of logic in TypeScript file |

## Templates

Angular uses the [Moustache Template Syntax](#moustache-template-syntax) to interpolate variables into the template.

### Moustache Template Syntax

The moustache template syntax relies on double curly braces on each side of the variable being interpolated

```html
<!-- Moustache Template Syntax -->
<p>{{ myName }}</p>
```

## Linking CSS

Similarly to Templates, Styles in Angular Components have two ways of being written.

### External Stylesheet

Linking an external stylesheet is the default behaviour in Angular. The `@Component` decorator provides a `styleUrls` property, which is an array of file urls. This may be `.css`, `.scss` or `.less` depending on your initial choice during the [projectcreation step](#creating-a-new-project).

```typescript
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"] // Link to external stylesheet
})
...
```

### Inline Styles

Inline styles are less common and provide the same syntax as mentioned in the [inline template property](#templates---inline).

The `@Component` decorator provides a `styles` property which is an array. The elements of this array is of type string written with back ticks (`) to enable multi-line strings for the CSS.

```typescript
 // Inline styles
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styles: [
    `h1 {
      color: red;
    }`
  ]
})
...
```

## Angular Rendering

### Shadow DOM

It is important to note that Angular does **not** use the VirtualDOM like React or Vue. Instead, it uses an customised version of the [ShadowDOM](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_shadow_DOM).
The [Shadow DOM](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_shadow_DOM) encapsulates CSS, HTML and Scripts to the component, ensuring that it won't interfere with other components that may share the same CSS class names or functions in the scripts.

### Web Components

Angular also emulates the idea of [web components](https://developer.mozilla.org/en-US/docs/Web/Web_Components). However, web components can be used in any project, irrespective of the framework. Angular components can **only** be used in Angular projects.

![Angular Web Component Emulation](./img/Angular_WebComponent_Emulation.png)

In the screenshot above, it can be noted that `<app-root></app-root>`, which is not a native HTML element, is rendered in the DOM. This appears to be a [Web Component](https://developer.mozilla.org/en-US/docs/Web/Web_Components), but is in fact an Angular Component. This component will **not** work outside of the Angular Framework. 

## Structural Directives

Angular uses structural directives to construct and manipulate HTML in templates. Structural directives reshapes the DOM by adding, removing and manipulating DOM elements.

Structural directives can be identified in Angular by the star symbol (*).

### Common Structural directives

This section will cover two of the most common structural directives. With the two most commonly used structural directives you can achieve most rendering needs in Angular. 

- `*ngIf` - Conditionally render HTML based on truthy/falsy values
- `*ngFor` - Loop over array values

In the example below, assume a property `lightsOn` of type `boolean` exists in the Component's TypeScript file. More will be discussed on this in the next section. 

### Conditional rendering with `*ngIf`

Conditional rendering using the `*ngIf` directive. 

```html
<p *ngIf="lightsOn === true">Lights are on</p>
<!-- Conditionally renders the p tag -->
```

The above example will be converted by Angular to the following code snippet:

```html
<ng-template [ngIf]="lightsOn">
  <p>Lights are on</p>
</ng-template>
```

> Note: Angular creates the `ng-template` element but is not rendered to the DOM. It is for internal purposes only to identify which elements are to be rendered. 

### Iteration with `*ngFor`

The `*ngFor` is also called `*ngForOf` since it uses the `of` keyword to iterate over array values. 

Assume an array named `products` of type `string[]` exists in the Component's typescript file. 

```html
<ul>
  <li *ngFor="let product of products">
    {{ product }}
  </li>
</ul>
<!-- Iterate over a string array named products. -->
```

During each iteration of the loop a `let product` is created. That variable is exposed to any element **inside** the loop. I can be displayed inside an `li` element as show above. 

> **NOTE:** Only a single structural directive may exist on an element. Therefor, you can not have a `*ngIf` and `*ngFor` on the same element. A wrapper element should be created for at least one of the structural directives. 

---

Copyright 2022, Noroff Accelerate AS