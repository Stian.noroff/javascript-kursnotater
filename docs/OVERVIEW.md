# JavaScript kursoversikt

# JavaScript kursoversikt

Ved slutten av dette kurset vil du ha blitt introdusert til kjernekomponentene i frontend programmering for webapplikasjoner som bruker JavaScript og vil vite hvordan du oppretter, forstår og vedlikeholder disse komponentene.

JavaScript-kurset er logisk gruppert i fem **moduler**

- HTML og CSS
- Responsivt webdesign
- JavaScript
- React JavaScript Framework
- Angular JavaScript-rammeverk

Hver **modul** består av **leksjoner** som inneholder det nødvendige materialet for å oppnå visse mål som representerer avgjørende praktiske ferdigheter for frontend webutviklere. Disse målene er beskrevet i begynnelsen av hver leksjon.

Modulene og de respektive leksjonene er vist nedenfor.

## HTML og CSS

Denne modulen introduserer deg til de grunnleggende HTML- og CSS-funksjonene som er avgjørende for å utvikle brukergrensesnitt på nettet.

Når du har fullført denne modulen, vil du ha en fungerende forståelse av de grunnleggende HTML-elementene og hvordan du kan style elementer ved hjelp av CSS.

1. HTML
2. Cascading Style Sheets (CSS)

## Responsivt webdesign

Denne modulen vil introdusere deg til de grunnleggende konseptene for responsiv webdesign. Du vil lære hvordan du bruker **Figma** til å lage Wireframes for webapplikasjonene dine. Deretter vil du bli vist hvordan du kan bruke **Bootstrap** til raskt å bygge prototyper ved å stole på de forhåndslagde klassene som Bootstrap tilbyr.

Følgende leksjoner dekkes i denne modulen.

1. Figma
2. Bootstrap

## JavaScript

Denne modulen gir de grunnleggende konseptene som trengs for å lage interaktive webapplikasjoner ved å bruke JavaScript-språket.

Når du har fullført denne modulen, vil du ha en fungerende forståelse av JavaScript-språket. Du vil vite hvordan du integrerer JavaScript i en HTML-side og skriver kode for å håndtere interaksjoner initiert av en bruker. I tillegg vil du også lære om forskjellige JavaScript-runtimes, få en forståelse av konseptuelle ideer som deles mellom JavaScript Frameworks. Til slutt vil du også forstå fordelene med TypeScript.

Følgende leksjoner dekkes i denne modulen.

1. Introduksjon til JavaScript
2. Grunnleggende JavaScript-kodingskonstruksjoner
3. OO, Designmønstre og ES6-abstraksjoner
4. Node
5. JavaScript rammeverk
6. TypeScript

## React - JavaScript rammeverk

Denne modulen introduserer deg til React Framework. Etter å ha fullført denne modulen vil du kunne opprette en ny React-applikasjon ved å bruke CreateReactApp-kommandolinjen. Du vil vite hvordan du lager komponenter som kan vise brukergrensesnittelementer, svare på brukerhendelser og hente data fra eksterne endepunkter.

Innen du har fullført denne modulen vil du kunne validere HTML-skjemaer ved å bruke React Form Hook og dele global tilstand ved å bruke ContextAPI.

Du vil bli introdusert til konseptet med global tilstandshåndering og hvordan du bruker React Redux for å bruke denne kraftige løsningen for tilstandshåndtering.

Følgende leksjoner dekkes i denne modulen.

1. Introduksjon til React
2. Komponenter og ruter
3. Skjemaer
4. Tilstandshåndtering

## Angular - JavaScript-rammeverk

Denne modulen introduserer deg til Angular Framework. Etter å ha fullført denne modulen vil du kunne bruke Angular CLI til å lage et nytt prosjekt, generere komponenter og bygge prosjektet ditt for utgivelse. Du vil vite hvordan du bruker Angular Components til å bygge brukergrensesnitt.

Ved slutten av modulen vil du kunne bruke Angular Services til å hente eksterne data ved å bruke Angular HTTPClient og dele tilstand mellom urelaterte komponenter. Du vil bli introdusert til Observables og det grunnleggende om RxJS.

Følgende leksjoner dekkes i denne modulen.

1. Introduksjon til Angular
2. Komponenter og ruter
3. Skjemaer
4. Tjenester

Leksjoner i dette kurset gir innholdet som kreves for å oppnå relevante læringsmål (detaljert i begynnelsen av hver leksjon). Det er imidlertid visse emner som kan berettige ytterligere undersøkelser eller selvstudier. Referanser til passende ressurser vil bli gitt for disse emnene.

Offisiell dokumentasjon vil foretrekkes ettersom utviklere bør være vant til å bruke offisiell dokumentasjon for å holde seg oppdatert.

<aside>
ℹ️ Blokker som ser slik ut vil inneholde noen ekstra tanker eller hint, samt selvstudieressurser og lenker til tilleggsinformasjon for videre lesing. Nedenfor detaljer om disse og andre typer blokker du vil møte og deres formål

</aside>

<aside>
📖 LES Indikerer en artikkel eller annen type litteratur som må leses.

</aside>

<aside>
📄 DOKUMENTASJON Indikerer en lenke til offisiell dokumentasjon for valgfri referanse.

</aside>

<aside>
💎 RESSURS Indikerer en kobling til en ressurs utviklet av Noroff Accelerate, vanligvis i form av et eksempelprosjekt i et eksternt Git-repo. Disse kan deretter lastes ned for å eksperimentere med og referere til.

</aside>

<aside>
📚 SELVSTUDIE Av og til vil det dukke opp relevante temaer som er verdt å vite, men utenfor rammen av dette kurset. Disse bør undersøkes på egen tid. En lenke for å komme i gang vil bli gitt

</aside>

<aside>
ℹ️ INFO Indikerer et viktig sidenotat eller hint.

</aside>

<aside>
⚠️ ADVARSEL Indikerer et viktig punkt som må forstås da det kan oppstå negative effekter om det ikke tas i betraktning.

</aside>

---

Copyright 2022, Noroff Accelerate AS